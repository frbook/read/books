---
name: {{VALUE:bookname}}
cover: {{VALUE:cover}}
tags:
- reading
- {{VALUE:tag}}
- project/index
douban_url: {{VALUE:douban_url}}
author: {{VALUE:author}}
isbn: {{VALUE:isbn}}
rating: {{VALUE:rating}}
banner:  {{VALUE:cover}}
banner_icon: 📚
publish: {{VALUE:publish}}
pagecount: {{VALUE:pagecount}}
date: {{DATE}}
star: {{VALUE:star}}
banner_x: 0.5
---
## 内容简介
《{{VALUE:bookname}}》
![bookcover|inlR|260]({{VALUE:cover}})
{{VALUE:intro}}

## 评论:

## 相关:

