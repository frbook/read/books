### 24.构建女性旅程

在女性旅程中，主角鼓起勇气面对死亡，脱胎换骨，像重生一般成为一个掌控自己人生的完整的人。

她从质疑权威开始，然后获得勇气为自己挺身而出，最后愿意独自面对自己象征意义上的死亡。这个包含九个阶段的过程通常体现为三幕，是经典的故事结构。

她的旅程的九个阶段如下：

第一幕：牵制

1.对完美世界的幻想

2.背叛或领悟

3.觉醒——为旅程做好准备

第二幕：改变

4.下降——穿过审判之门

5.风暴之眼

6.死亡——失去一切

第三幕：上升

7.支持

8.重生——真相的时刻

9.周而复始——回到完美世界

**第一幕**

**阶段1：对完美世界的幻想**

一个叫莎拉的女人坐在窗前，看着云朵在湛蓝的天空里飘浮，她告诉自己：“现在这样很好。”

莎拉这些年在自己周围建了一个玻璃罩，使自己远离那些伴随成长而来的疼痛和不确定。她就像睡美人一样，对周围的真实世界一无所知，也不知道自己拥有唤醒自我的力量。她相信只要自己一切照着规则来就会得到奖赏。随着时间流逝，玻璃墙逐渐向她逼近，越来越近，近到她一站起来头就会碰到上面的玻璃天花板。

主角没有采取措施对付头顶的天花板，相反，她决定不要站起来。她把她的创造力用在忍受她的处境上，不去承认这个事实，即她所处的世界限制了她的成长。

她对自己的未来有种虚假的安全感，以为只要她保持原状大家就都会喜欢她。这是个安全的世界，里面是她熟悉的一切；生活的重复给她带来安全的幻觉。

她天真地活在谎言里，希望其他人会照顾她，或是她做一个牺牲者，接受命运的安排。她没有动力为自己寻找另一种生活的可能。

她无法想象冒着失去一切的风险来逃离这个世界。她不知道外面有更好的世界。“每个人不都在受苦么？”她想着。她想要尽力让自己适应目前这个世界，这样她就可以避免改变和成长带来的痛苦。

那些她在乎的事物也使她不想离开——养家，等待晋升，挽救每一个人，除了她自己，或是别人的看法。有了这些借口，她就可以不采取行动，不用去看周围那些可能的机会。

为了使读者相信就算主角在故事的后面部分遇到困难的处境，也比留在原地强，目前这个环境必须构建成一个消极、负面的空间，主角在这里无法发挥自己的才能。一开始她可能会尽力对自己去解释发生在自己身上的种种坏事，但是她的借口和理由迟早会用尽。

必须把目前这个“完美世界”刻画成一个消极、负面的环境，这样主角才有足够的动力觉醒。在整个故事中，她必须一直寻求更好的事物，因为很明显，她无法在目前的环境下好好生活。

为了在这个“完美世界”熬下去，有五种策略是主角可用的。主角通过使用其中的一种来逃避现实。她学会适应环境，并利用她的原型所特有的一些特质来立足。

这五种策略分别是：

天真策略：她不自觉地抱有这样的幻想，认为“其他女人可能会受到伤害、攻击或是被忽略，但是我不会；坏事不会发生在我身上；生活是美好的，我会因为自己的努力而得到奖赏；我要做的就是耐心等待”。

在《绿野仙踪》中，多萝西生活在一个单纯的世界中，那里的一切都循序渐进，没有色彩，没有激动人心的事，只有大量的重复和安逸。她试图找些事来做。她过得很无聊，当她想要帮忙做家务的时候，大家都叫她走开。她在那里无所适从。她没有意识到自己需要成长，而她必须离开农场才能成长。

灰姑娘策略：这样的女人在生活中要依靠男人的保护和指引。“我的男人会一直在这里帮助我，当他不在身旁的时候，其他男人也会来帮我，所以我不用担心会有什么坏事发生。男人们都愿意照顾我、善待我。”她可能会觉得自己要打扮得美丽动人来抓住男人。

《乱世佳人》中，郝思嘉坐在长椅上，身边围着两个迷恋她的男人。她不断地把话题从战争转到自己的身上。她不愿面对她的塔拉庄园之外的现实世界。家庭的财富给了她舒适的生活和体面的社会地位。她拒绝去想，她被困在这样一个社会里——女人即使在完全不累的时候也必须要去午睡。她只关心如何吸引住她遇到的所有男人的注意力。

例外策略：这个女人生活在男人的世界里，或者她自己是这样认为的。她觉得自己跟男人一样强大，也不断从同龄人中得到这样的肯定。“其他女人做不到，但是我可以，因为我是个例外。男人们欣赏我，因为我能跟他们一样，能融入他们的团体。我从不哭泣或是抱怨。”她会忽视办公环境里看到的各种性别歧视，因为她不觉得自己是个“女人”。

在《上班女郎》中，凯瑟琳·帕克完全压制了身上的一切女性特质。她总是跟男人们在一起，捉弄着办公室里的女秘书们。她觉得自己是唯一一个能够做好这份工作的女人，当另一个女人——一个秘书——接手她的工作时，她感到极度震惊。泰丝·麦吉尔（梅拉尼·格里菲思饰）相信自己比其他秘书都要好；如果她没有这样的想法，就抓不住那些机会。她把自己从其他女人那里区分出来，以打入男人的世界。

取悦策略：这类女人活着就是为了取悦他人。只要其他人高兴，她就高兴。她需要别人的认可。她就像一个温顺纯真的小女孩，什么都照着书上说的，克制自己的女性直觉和力量。她的行为都在支持现状，她排斥自己真实的欲望，来让自己融入环境。这给她一种人生在握的幻觉；她不会被人吼叫批评，因为她循规蹈矩地做着每件事。大多数时候，她都战战兢兢，如履薄冰。

在《末路狂花》中，塞尔玛像人们期望的那样，做着各种家务，守着一个糟糕的婚姻，但是内心深处她很不快乐。即便她的房子漂亮、干净，属于中上层阶级，她的生活却是自我毁灭型的。我们看到她在她丈夫的言语暴力下小心翼翼地生存。她看上去对他说的话有些麻木了，这样的生活一定已经持续了相当长的时间。

失望类型：这类女人对自己的命运感到愤怒又压抑，但是不采取行动去改变——目前还没有。她可能说话尖刻、语含讽刺，通常是办公室里最直言不讳的一个，或是扮演殉道者的角色，为了他人牺牲自己的利益。内心深处，她梦想着自己真正的目标，而它看上去如此遥不可及。她是五种类型中最清醒的一个，渴望找到一个正面的好榜样。她知道自己的世界不够好，但是没有欲望或是动力去改变它。

在《泰坦尼克号》中，萝丝过着一种受保护和控制的生活。一开始，我们觉得她是一个富有的女人，有一个爱她的未婚夫，还有仆人服侍她。一切看起来如此完美，可是她的眼眸里却藏着一种绝望的神情。慢慢地，我们了解到她的未婚夫和她的母亲监视着她的一举一动。她生活里的每件事都被安排好了，小到她晚餐要吃什么。我们意识到她为了家人而牺牲了自己，注定要抑郁地度过一生。

阶段1的例子

《伊南娜的冥界之旅》

在女神伊南娜的神话中，有一天，伊南娜认为自己已经做好登上王位并继承随之而来的智慧的准备了。她种了一棵树，默默地等待了十年，等这棵树长大，树干裂开，她可以用它来做一个宝座，但是一条蛇（新生的象征）、一只安祖鸟（知识的象征）和莉莉丝[^1]（叛逆的女人）已经在树干里面筑巢，因此这棵树无法生长。（这三个象征物有点像《绿野仙踪》里多萝西寻找的内心、头脑和勇气。）

为了证明自己配得上王位，伊南娜必须亲手抓住这三个生物，但是她找来她的兄弟做这件事。他杀了这些生物，为她做了这个王座。他的保护使她没能开始独自的旅程，她继续留在了她安全的、受保护的世界里。

《绿野仙踪》

像之前提到的，多萝西生活在一个单纯的世界里。

《泰坦尼克号》

像之前提到的，萝丝过着一种受保护和控制的生活。

《觉醒》

埃德娜是一个对自己的命运感到压抑的牺牲者。故事的开篇，她和她的上流社会的朋友们一起去海边的别墅度假，虽然她的丈夫性情刻薄，她还是试图过得开心。她和罗伯特关系不错，这是一个妇女之友类型的人，跟她丈夫的性格完全相反。她既有钱又有闲暇，还有丈夫和孩子。像她这样的人应该快乐、满足、感恩，可是她却感到一丝绝望。

性别偏移：《美国丽人》

在影片的开始，我们看到了全家福，告诉我们这是一个幸福的家庭。我们看到一家人坐在一起分享美食，但是我们很快意识到事情远没有这么完美。就像莱斯特说的，“我希望我可以告诉女儿，她的愤怒、不安和困惑都会过去，但是我不想对她撒谎。”

莱斯特这辈子都在做他讨厌的工作。有一天他回顾人生，意识到自己的人生就是个空洞的谎言。事业上的成绩对他来说毫无意义，因为那只是别人眼中成功的标准。他只是为了工作而工作，在公司里爬着他不确定自己是否想爬的社会阶梯。

_女性旅程阶段1的创作技巧_

●至少想出五个不同的环境来展示“完美世界”。如果办公室是她的完美世界，那么想一想她上班的一天里去过的各种不同的地方，选出一个最特别、最可以发挥的。

她可能每天早上九点零三分会在办公室后面的小巷里抽一根烟，鼓起勇气去面对一个挑剔的老板。她可能是唯一一个每天下午跟门卫们一起去自助餐厅吃葡萄果冻的医生，而他们可能在后面的故事里成为帮助她的人。

●记住，你是在这个阶段向你的读者介绍你的主角，定下故事的主题。

●有时候可以想一下故事开场之前主角碰到了什么事情，来增加一点趣味，或是透露更多关于主角的信息。她常为了送孩子们上学而手忙脚乱么？她的车坏掉了么？她曾中过一次奖？她遇到过一个很棒的男人？

**阶段2：背叛或领悟**

当莎拉在她的玻璃罩里望着天空时，面前出现了一大团乌云，挡住了明亮的阳光。

由于害怕黑暗，她从窗边移开了。闪电离她越来越近。突然一声巨响，她的玻璃罩破碎了。玻璃碎片飞得到处都是。

玻璃罩破碎，生活的汁液溢满了整个地面。对主角重要的每样事物都被带走了，她被迫面对着生活的分叉路，必须做出选择，要么走出去主动面对自己的恐惧，要么留在原地成为一个消极的牺牲品。她遭遇了背叛——被生活、被她自己或是被一个坏人。这个阶段常被称为“突发事件”。

对于那些被情节和角色推动的故事，比如悬疑故事，要用好这些悬疑元素，来暗示主角内心的斗争。当她试图解开这个谜团，这个悬疑故事能够带她经历所有阶段。她的第二阶段很可能更像是一种醒悟，她意识到自己想从生活中得到更多，哪怕只是为了改变而做一点正确的事，比如破案。这个旅程会使她面对她的那些恶魔。

这个背叛如此彻底，以致主角无法忽视它。它直视着她，而她必须加以处理。她意识到人生跟她原先想象的不同，没有穿着闪亮盔甲的骑士来拯救她。

这个阶段定下赌注，给主角提供了改变生活的动机。她试图适应的这个系统并没有像她期待的那样，奖励她所付出的努力。她一直循规蹈矩地走每一步，可是还是失败了；她的世界坍塌了。

主角会问自己：“这样的生活有什么意义？我为什么在这里？这一切究竟是为了什么？”这个阶段她可能会精神崩溃，或是沉溺于药物，试图重组她的人生。这个阶段给主角提供了动机，使整个故事进行下去。所以必须是强大的动力。她无法抹去或是轻易忘记这里发生的一切。

在这个阶段，主角之前选择的应对策略都土崩瓦解。它不再起作用，她的整个人生、她过去相信的一切都改变了。她的某个社会角色结束了（比如身为人母），她也无法让时间倒退来阻止这一切。

天真策略：这个女人受到了伤害或是虐待。一个亲密的朋友或者照顾她的家人去世了。一个巨大的危机使她的世界天翻地覆。她失去了她的工作、房子或是钱。

在《绿野仙踪》中，多萝西感到叔叔背叛了自己，当可恶的邻居想伤害她的小狗托托时，他没有出来保护它。她觉得自己很孤独。事实上，她没有父母，他们很早就去世了，留下她孤单一人。

灰姑娘策略：这个女人失去了男人的保护或支持。那个男人可能已经去世。

在《乱世佳人》中，男人们离开了郝思嘉去参加战斗、照顾伤员。她也遭遇了她最爱的阿什利的背叛，他跟另一个女人结婚了。

例外策略：当有晋升机会时，这个女人被忽视了，或被她的男性同伴们背叛，证明她不如一个男人或这些男同事。她无法真正融入男人的世界。她失去了一个很大的合同。她失去了婚姻，因为她无法兼顾事业和婚姻。如果她在教会中任职，她会失去对神的信仰，因为她发现自己无法成为一个牧师，这使得她开始质疑她的整个信仰。

在《上班女郎》中，泰丝·麦吉尔遭遇了男友的背叛，当她回到家里时，发现他跟另一个女人躺在床上。她还被凯瑟琳·帕克背叛，凯瑟琳在泰丝的工作报告上写了自己的名字，把她的功劳据为己有。

取悦他人的类型：这类女人发现自己被践踏了；人们利用她。有些人甚至仅仅把她看作一个母亲、秘书、助手之类。她觉得自己完全被轻视了。她甚至还因为留出时间生小孩而被处罚。

在《末路狂花》中，塞尔玛先是遭遇了一个刻薄又可恶的丈夫，后来又遇到一个试图强暴她的男人。她一直善良、礼貌，从未看到自己被卷入的危险。

失望的类型：这类女人被逼迫得太狠，通常是被一个当权者所逼迫。她感觉自己已经退到背靠墙壁，没有办法向任何一个方向移动，来躲开即将来临的攻击或是羞辱。

在《泰坦尼克号》中，萝丝先是被父亲背叛，因为他挥霍完家里的财产，然后死了，留下她们母女一文不名。接着她的母亲又背叛了她，强迫她嫁给一个她不爱的、有暴力倾向的男人。她一直按照别人的要求活着。当她的未婚夫殴打她的时候，她想要自杀。全世界都背叛了她；女人的身份也背叛了她，因为身为女人，她不能骑马，不能像她向往的那样探索世界。

当你的角色内心发出“我人生的意义到底是什么”这样的疑问时，她将会愿意接受改变，直面那些说她没有能力完成这个旅途的反派。我们将在下个阶段讨论这个话题。

在这个阶段要设定反面角色。你要确保你的反派——那个背叛者——有充足的理由做出那些事。

想一下《沉默的羔羊》中的反派汉尼拔·莱克特是多么令人难忘。他没有被简单地刻画成一个精神失常的人，他在有些时刻头脑非常清楚，并且展示出高度的专业水平，这使他甚至有些讨人喜欢。

反派从不认为自己是坏人或是错误的。他们做每一件事都有具体的理由，并且真心相信自己做的是对的，错的是其他人。

《泰坦尼克号》中，萝丝的未婚夫觉得自己真心爱着萝丝，她能嫁给自己真是三生有幸。他甚至送给她世界上最昂贵的项链来证明自己的爱。他觉得自己已经为了取悦她而做过头了。他觉得自己为她付出了那么多，她真是忘恩负义。

阶段2的例子

《伊南娜的冥界之旅》

伊南娜遭遇的背叛之一来自智慧之神。有一天，伊南娜拜访了他，“她带着初生牛犊的鲁莽，跟他吹嘘说自己会祝福他。他称她为年轻的女子，并让仆人把她当作跟自己同样尊贵的人来恭敬对待”。

在酒足饭饱之后，他高兴地把自己所有的珍宝都送给了她。她接受了他的礼物，开心地带着礼物离开了。当他清醒过来，意识到自己的珍宝都不见了时，勃然大怒。他派自己的仆人去伊南娜那里索回礼物。伊南娜听到这个消息之后，非常难过。“她觉得他是一个暴君，一个说话不算数的人，一个可怕的骗子。”

在抑郁和心碎的时候，她觉得连地上所有的狗都有家园，而她，虽贵为女王，却没有可以称为家的地方。天空之神恩利尔（Enlil）使她成为一个流浪者。“他让我，天国的女王，心中满是惶惶不安……狗跪在门槛前，而我——我连门槛都没有。”

她的兄弟也背叛了她，他侮辱她，想要窃取她的权力。

她被乌鲁克的畜牧之神杜牧茨（Dumuzi）追求，不过她说了好几次，她并不爱他，她更愿意嫁给一个农夫。她的母亲和兄弟则劝她选择杜牧茨。

《绿野仙踪》

像之前提过的，多萝西感到被叔叔背叛了，因为他在可恶的邻居试图害死小狗托托时没有去救它，她也感到被父母背叛了，因为他们早已离世，抛下了她。

《泰坦尼克号》

像之前所说的，萝丝被父亲背叛，因为他挥霍掉了家里的财产，接着她又被母亲背叛，因为母亲为了金钱安排了她的婚姻。

《觉醒》

埃德娜的丈夫成天在外面打台球。他从来都不大在意她。他回到家就开始烦躁。他会无缘无故地突然指责她对孩子们没有尽到母亲的责任。她的丈夫想要控制她的每一步，而社会期待她满足于母亲的角色，周围都是其他贤妻良母型的女人，她看起来跟周围的一切格格不入。

性别偏移：《美国丽人》

在工作上，莱斯特是一个习惯取悦他人的人。他分明讨厌自己的工作，不过却始终保持微笑，做事随大溜，以保住这份工作。他被叫到新老板的办公室，被告知他必须写下他对公司的价值，以便他们来决定是继续留他还是解雇他。他已经在那里工作了14年，现在竟然受到这样的对待，这让他异常愤怒。他感到自己无人需要、无人欣赏、毫无用处。那天晚上他回到家里后，我们看到他在家里也是这种感觉。他过着消极被动的生活；甚至从不自己开车。

_女性旅程阶段2的创作技巧_

●在这些背叛发生之后，读者们应该会问：“她现在该怎么办呢？”

●为了构建这些背叛的悬念、可信度和戏剧性，问自己这些问题：

——你能否增加一个角色，比如一个家庭成员，来构建起悬念？

——你有没有把反面角色塑造得足够饱满，以使他的所作所为看起来可信？

——你能否通过改变故事的背景、时间，甚至主角的原型，使得整个故事更加戏剧化？

●确保反面角色有足够的理由做那些伤害主角的事。

●想一想你能否增加别的配角，来制造更多的冲突。

●艾伯特·布鲁克斯（Albert Brooks）导演的喜剧通常会在这个阶段发挥得淋漓尽致。《迷失的美国人》（Lost in America）中，主角失去了晋升的机会，《母亲》（Mother）中，主角失去了妻子，《缪斯女神》（The Muse）中，主角失去了事业和创造力。

**阶段3：觉醒——为旅程做好准备**

突然有人出现，提出帮助莎拉修复她的玻璃罩，但是要价很高。她在考虑时，看到一条弯曲的小路可以走出这堆碎片。她第一次呼吸到新鲜的空气，她意识到不值得用自己剩下的那点钱来修复玻璃罩。

她伸长脖子想要看到这条道路通往何处，但是看不到尽头。她决定冒险走走看。好几个人试图劝她放弃这个念头，跟她说沿路的玻璃碎片多么尖利。

看不出道路通往何处，莎拉就带上那些她觉得自己能用上的工具。当她跟周围的人道别的时候，她得到了一些支持。不管她有没有意识到，她还是有支持她的朋友的。

主角遭遇了背叛，或是终于对自己的生活有了一个残酷的认识。她现在该怎么做？绝望和无助使她变得抑郁、愤怒和痛苦。她可以选择消极的道路，然后：

●责备他人。

●责备自己。

●做一个牺牲品，自问“为什么是我？”

●投入工作，逃避现实。

●决定自杀，像《泰坦尼克号》中的萝丝一样。

或者，她可以选择积极的道路，把这些背叛视为：

●一个教训。

●一个通往自由和改变的邀请。

●一个追求她想要的事物的挑战。

一开始她可能用消极的方式对待背叛，或是为自己浪费的生命而愤怒，不过很快她就决定采取行动。

如果主角陷在消极的反应里，其他角色可以帮助她回到正轨，不过只有她自己决定行动，才能成为一个转折点，构建起主要目标，推动故事前进，永远地改变她的命运。她下决心正视自己想要的东西，更重要的是，她下决心拒绝那些她不想要的东西。

这个时候很多角色会突然冒出来告诉她，她不可能达成目标，她需要帮助，她疯了。或是她内心的一个声音在试图破坏她的行动。但是背叛使她遭受的痛苦会推动她战胜这些反对的声音。

如果她足够幸运，会有另一个角色帮助她前行，问她“你为什么要让他们这样对待你呢？”但是不要让别人帮她彻底扭转局面，因为那样她就不能自己开始旅程了。

她可能会：

●放弃女超人的神话，拒绝同时照顾好家里和工作单位里的每个人。

●对爱人说真话，不考虑后果。

●要求得到她一直想要的加薪，或是要求做一个特别的项目。

●设定新的界限。

●开始新的事业或是离开原有的工作。

●同意做证指控某个人。

●决定找出凶手。

●到处旅行来寻找自我。

●在别人指望她沉默的时候提出问题。

●决定为自己的信仰而斗争。

●坚信她看到了她所看到的东西，即便那是一个飞碟或是一个鬼魂。（她的女性直觉不肯再受到压制。）

不管她做出什么决定，这个决定都将改变她的人生，把她推向一个具体的目标。这个阶段是某种逆转。主角的整个人生方向都将被她这个阶段的决定永远改变。

_应对策略的失效_

在这个阶段，她之前使用的那些在“完美世界”生存的应对策略都已经失去作用了。她抛弃了这些东西来彻底地步入自己所属的原型及其相关的特质。那些应对策略正是阻碍她看清背叛的东西。她步入自己的原型，用它的优点来帮助自己。

想一下《末路狂花》中的塞尔玛。她觉醒了，决定跟路易丝一起前行。她不再在意她的丈夫会怎么说，不在意行动的后果是什么。她看到了自由的曙光。

_为旅程做好准备_

这个阶段的另一部分是为旅程做好准备。

看不到道路通向何处，主角带上了她觉得可能有用的那些工具。在她跟周围的人道别的时候，不管她自己有没有意识到，她已经找到了几个盟友。在她心里有一个名单，里面的人可能会在她困难的时候帮助她。

就像小红帽带着她的篮子一样，主角也带着那些她觉得会用得上的工具。问题是她依然在寻找身外的东西来帮助自己。

她的准备工作可能包括：

●跟人道别。

●向别人询问她该怎么做。

●带上武器——手枪、钱、乔装用品。

●记录下自己遭受的不公对待，这是所有遭受性骚扰或是被跟踪的人必须做的。

●带上她认为自己需要的一些衣服和物品，她可以用这些使自己看上去像《上班女郎》里面的女人们那么美丽或专业。

如果她是一个社会活动激进分子，她可能会收集工具，把自己绑在她想要拯救的一棵树上，或是做一些写着激进标语的牌子。如果她正在跟大公司做斗争，她可能会复印一些档案和数据。如果她是一个母亲，正打算从虐待她的丈夫身边逃走，她会在离开前带上她的孩子们、衣服和钱。

带着这些武器，主角更有安全感，但是小红帽的篮子是没法把她从大灰狼那里救出来的。真正能帮助她的是她自己的勇敢和聪明，只是她目前还不能真正相信自己的力量。

可能会有一个导师出现，但是他并没有主角所需的全部信息，因为在她的旅程里她需要更多地了解自己，找到自己的力量，而不是依赖他人。

所有的恶魔和暴君都会登上舞台：

●自我怀疑开始爬上她的心头，她想“可能他们是对的，我做不到”。

●有人告诉她，她不够聪明，准备不够充分，搜寻那些她用不到的信息只是在浪费时间。

●男主角会出现以拯救她。男人们通常被告知，帮助和拯救女人是他们旅程的一部分，但是如果他们这么做，女人就没有机会开始她们自己的旅程了。

●还有一类男人，作家艾德丽安·里奇[^1]称他们为“会理解的男人”。这类男人装作理解她所经历的事情，却不肯帮助她。在她觉醒之前，他看上去像是对她抱有同情，而一旦事情变得困难时，他便抛弃了她。

●规则发生了变化：

——突然间她的工作任务急剧增加，使她无法完成旅程;

——学校改变了政策，增加了获得学位的难度;

——卑鄙的骗子在她够不着的地方晃着金萝卜，对她说“继续呀，浪费你的时间去抓它”;

——汽车监理处决定把她的新地址透露给一个骚扰者。

●她害怕伤害他人，这种心情可能会占上风。

●她无法拒绝他人的请求，停下来帮助别人，而这会使她陷入麻烦。

●她卷入了其他人的纠纷里。

阶段3的例子

《伊南娜的冥界之旅》

伊南娜意识到自己不能再因为别人的所作所为而哭泣，她要把主动权握在自己手中。她要到冥界去认识自己。“为了这次旅程，伊南娜收集了七个自我（文明的属性，相当于七个脉轮[^3]；参见阶段4的七个问题）……她把它们变成美丽的女性饰物，比如王冠、珠宝和礼服，作为自己的装备。考虑到自己不一定能从冥界回来，伊南娜让她的朋友宁舒布尔来提醒她的前辈们，好让他们想起她。”

《绿野仙踪》

多萝西醒过来，打开房门，看到了奥兹国的明亮国土。眼前的一切都是前所未见的。她要在没有家人陪伴的情况下独自踏上旅程。他们一直以来都对她过于保护了。

《泰坦尼克号》

萝丝的觉醒时刻是在她被母亲希望她嫁的那个未婚夫殴打以后，她看到旁边的餐桌旁坐着一个穿着白色百褶裙的小女孩，就像看到镜中的自己一样。这个小女孩被要求坐得笔直，拿餐巾的姿势要得体。这个女孩被剥夺了童年，而萝丝被剥夺了她的成年。

她意识到自己永远无法成为梦想中的快乐女人。萝丝改变了心意，决定去见杰克——那个她被禁止去见的下层阶级的艺术家。当她张开双臂站在船头，他在身后抱着她时，她告诉杰克自己信任他。她还做了他的模特，让他为自己画像。她犹如蝴蝶，破茧而出。

《觉醒》

埃德娜跟罗伯特一起去海洋里游泳，虽然这不是很合适的事。海浪拥抱着她的身体，她开始意识到在这个世界上有自己的一席之地。她信赖拉蒂格诺拉太太，在准备改变的时候把她当作自己的朋友，不过结果并不尽如人意。

雷兹小姐出现在她的生活里，扮演了一个导师的角色，她带来警告，如果埃德娜想要挑战习俗，她就要像自己一样忍受被他人孤立。

性别偏移：《美国丽人》

莱斯特偷听到他的女儿跟她的朋友在讨论他的事。他冲到车库，找到哑铃，脱掉衣服。他看着自己的身体，开始锻炼。他走出了改变生活的第一步。

_女性旅程阶段3的创作技巧_

●创造几个不同类型的配角来打击主角的决定。被自己最亲近的人嘲笑往往是最伤人的。她尊敬的人是谁？

●记住你要具体展示出主角的觉醒，而不是简单地说她觉醒了。让她主动地展示她做出的决定。当她已经是副总裁的时候，她的老板是不是还总是叫她倒咖啡？也许她这次把咖啡倒在他身上，走出了办公室。想一下奥兹国跟堪萨斯州比起来，是多么生动和独特。

●在这个阶段主角抛开了她一贯的应对策略。

●所有主要的配角在这个阶段应该都已经出现在读者面前了，不管主角有没有遇到他们。

**第二幕**

**阶段4：下降——穿过审判之门**

莎拉站在玻璃罩外有点不自在，但是她注意到外面的空气要新鲜得多。她紧紧抓着自己的武器，站在地下世界的大门前。她正视了自己的恐惧，也拒绝了那些试图阻止她行动的骗子和暴君。

她迈入大门，走下楼梯，然后发现还有六扇门在等着她。现在想要回头已经来不及了。在接下来的每一扇门处，都有一个守门人会走出来拿走她的一样武器，直到她手无寸铁、孤零零地留在黑暗中。

现在主角已经做了一个改变人生的决定，她必须要面对随之而来的改变。在她前进的道路上，她还需要面对社会关于女人是软弱的、被动的、无力的等各种假定。

主角面临着某种让她恐惧的事物、某种实质性的威胁，不再是主角的自我怀疑那么简单。她也许想要原路返回。她试图用上她的武器——操纵、胁迫、她的性感、她迷茫的过去和伤痕——但是这一切都不管用。她一个一个穿过审判之门，每到一处，就要面对一种恐惧、失去一样武器。她原先以为可以拯救她的所有外在物件都被剥夺了。

记住，主角在这个阶段遭遇的一切只是她在第六阶段要面对的死亡的前兆。在这个阶段，《绿野仙踪》里的多萝西因为在家人需要自己的时候离开了家而内疚，但是在第六阶段，她要面对的内疚感更加强烈，因为她的离家几乎害死她的婶婶。在第六阶段，风险加大了。

在《伊南娜的冥界之旅》中，伊南娜穿过七扇门，在每一处都失去她的一件饰物或是女王身份的象征物。这七扇门就像是人体的七个脉轮，或是灵修道路上要驱除的七个魔鬼，或是彩虹的七种颜色。

这七个问题可以帮助你充实这个阶段主角落难的情节。所有的原型都可能在旅程中碰到这些问题。由你来决定她会碰到哪些问题，以及它们如何成为情节的一部分。

面对恐惧、生存和寻找安全与保障的问题：主角是否因为害怕被抛弃而抗拒亲密的关系？她是否害怕自己依赖他人？她是否过于苛求自己？她有能力养活自己么？她在逃避什么恐惧？

面对内疚、表达关于性的需求和其他情感、了解自己的欲望等问题：主角是否想要拒绝，却因为害怕别人的排斥而不敢说？她是否更喜欢离别人远一些？她是否因为不想让他人生气而在划清界限时左右为难？她是否放弃了一段恋情？她知道自己真正喜欢什么东西么？她对什么感到内疚呢？

面对羞愧、定义权力和意志、获得自己的身份等问题：主角是否对自己过分挑剔？她是一个完美主义者么？她是想要高于他人的权力还是跟别人共掌权力？她是否着意锻炼过自己的意志力？她是否想要控制一切？她对金钱的态度是什么？她有没有放纵自己？她对自我有认识么，还是正处于一种身份危机中？她为什么感到羞愧？其他人故意羞辱过她么？

面对悲伤、付出和接受爱、接纳自己等问题：主角是否觉得自己无法融入身边的圈子？她是否在社交场合感到坐立不安？她是否需要别人来告诉她什么是正确的做法？她在别人面前如何表现？她能否拥有一段长久的恋情而不中途放弃？她是否接受自己真实的样子？她因为什么而感到悲伤？

面对谎言、交流、表达自我等问题：她能说出自己的真实想法么？她是否害怕挺身而出？她是否行为叛逆、过于活跃？她是否相信别人对她做的一切负面的评价？她对自己说了怎样的谎言？别人对她撒了什么谎？她感到迷茫么？她是不是压制了自己的创造力？

面对幻想、看重直觉和想象力等问题：主角是否害怕看清自己和把各种人事看得太透彻？她是否拒绝接受事实？她缺乏想象力么？她无视自己的直觉么？她是否觉得自己值得拥有她的原型的正面品质？她相信什么样的幻想？

面对依恋、找寻自我意识等问题：主角思想开明么？她会盲目地照别人说的去做吗？她内心是否总有个消极的声音瓦解她获得成功和承担责任的能力？她是否任由生活教训她？她有没有从自己的过错中学到东西？她能否理解她身上发生的事情、看清她的行为带来的后果？她对自己的动机清楚吗？她是否过于依赖家庭或是工作，而无法认清自我？

主角最终必须放弃所有控制的想法，在下降的过程中交出自己和所有的武器。被剥夺了所有应对策略后，她必须正视她的恶魔。如果羞愧对她来说是一个问题，那么她就会感到羞愧，这样她才能正视这个问题并逐步解决它。如果生存是一个问题，那么她会失去所有来自他人的收入，这样她才能学会养活自己。

想一下《绿野仙踪》中的多萝西——家园和家人对她来说是最重要的，也是她生存的依靠。结果她发现自己孤身一人身处奥兹国，她的家园和家人都不见了。这个故事的整体情节是关于她如何找到回家的路的，而这个下降阶段则涉及她如何面对自己的恐惧，面对离开婶婶的内疚感，面对在森林里和奥兹国里遇到的种种幻象，以及她如何结交新朋友。

主角必须放弃反抗的道路，反抗是男性旅程的属性，他们抗拒事情的走向，相反，她需要接受，顺着事情发展的方向，泰然自若地接受发生的一切。头脑和勇气是她现在需要找到的——就像多萝西找到象征“头脑”的稻草人和象征“勇气”的狮子。等到后来上升的阶段，她还要学会拥有铁皮人代表的“心”，拥有同情心可以控制她的勇气和头脑。

她还必须学会信赖自己的直觉，直觉会告诉她不要相信某一个特定的人或是某个情形，不然她就会被拉上错误的道路，打开一些行不通的门。

她第一次面对面遇到恶人或是他的手下，侥幸逃生。她陷入困境，不相信自己还能多撑一刻。这不是她想要的结果。

她有点怀念过去，可能只是短暂的片刻，觉得过去的生活没有那么艰难。她甚至想要回到旧世界——那个安全的世界。所以旧世界必须要被刻画成一个糟糕的地方。当时的背叛（或者醒悟）一定要足够刻骨铭心，使她得以撑过这个阶段。她会渴望熟悉的环境里的舒适，靠她已有的东西凑合着过下去。她会想要停留下来，不再去找寻她真正想要的东西。

例子：

●她辞掉了工作，她可能找不到一个新工作，她的身边已经堆满了各种各样的账单。她被从家里赶了出来。（关于生存。）

●她的丈夫离开了她，她感到孤独和抑郁。当她新的感情进展不顺的时候，她丈夫可能想要重新回到她的身边。（关于排斥和孤独。）

●她可能被迫面对一个用言语或肢体攻击过她的人。（关于权力。）

●她可能为了救某个人或保护某样东西而忍受羞辱和伤害。（关于羞愧。）

●她可能需要放弃她拥有的一切、她知道的一切来逃走。她可能要躲进一个新的城镇或乡村来隐藏自己。（关于依恋。）

●她可能遇到一些独特的事情使她感到自己跟别人不同，也许是某种神秘或是超自然的经历。（关于幻觉和直觉。）

●她可能已经知道凶手藏在哪里，可是仍然晚到一步，看着又一个受害者死去。她失败了，但是依然在寻找线索。（关于内疚。）

她就像小红帽，拿着一个在对抗大灰狼时毫无用处的篮子。只有头脑和勇气能给她力量安全到达外婆家。

这个阶段常在恐怖片中的女主角身上看到。主角遇到凶手，凶手尾随她，于是她开始了地狱之旅。想一下《月光光心慌慌》中的劳丽·斯楚德（洁美·李·寇蒂斯饰）。她手无寸铁，却必须要在严酷的考验中存活下来。

阶段4的例子

《伊南娜的冥界之旅》

伊南娜在下降的过程中，在七扇门的每一处都受到审问，被剥去衣衫，受到羞辱。每一件从身上脱下的衣物或者饰品都是原先穿戴在她的某一个脉轮位置上的。（参见之前提过的七个问题。）

她的七个自我（文明世界的特质）被剥夺了，她赤裸地站在黑暗女神厄里斯奇格（Ereshkigal）的面前。她所有旧的幻想、虚假的身份和她的辩护在冥界都毫无用处。她说她想通过看别人的葬礼来获得超越死亡的知识和权力。“但是进入冥界只给伊南娜带来一种可能性，即目击她自己的葬礼。”

《绿野仙踪》

多萝西得到一双红宝石鞋子来帮助她完成她的旅程。她冒险进入森林，沿路遇到几个帮助她的人。

他们碰到邪恶的西方女巫，她想要烧掉稻草人。多萝西对她说：“我不想惹麻烦，我们已经走了好长的一段路。”女巫回答：“这点路你就觉得长了？你的旅程才刚开始呢。”接着他们来到一个黑暗的森林里，里面都是令人毛骨悚然的野生动物。

《泰坦尼克号》

萝丝和杰克在船舱里奔跑，她的未婚夫的手下在追逐他们。他们一起躲了起来，二人在情网中陷得更深。后来杰克被关了起来，萝丝拒绝登上救生艇，她冒着生命危险去救杰克。

《觉醒》

雷兹小姐弹奏钢琴引发了埃德娜的痛苦情绪。她又去大海里游泳，她游得太远，感到了恐慌。她告诉丈夫，但是他说自己一直看着她。

她的内心发生了改变，她决定考验一下他们的婚姻。晚上当丈夫命令她进屋去时，她不肯，他就一直在屋外和她僵持着，直到她先进屋去。她试图砸坏自己的婚戒，她拒绝陪他出差、拒绝见来访者、拒绝维持他们正常的社交生活，她还决定把所有时间花在画画上。于是她的丈夫去问医生为什么她突然开始讲平等、权利之类的东西。

性别偏移：《美国丽人》

莱斯特·伯恩汉姆告诉他的妻子：“这根本就不是一个婚姻，只要我什么都不说，你就高兴……现在我变了。”他开始慢跑、抽大麻，他的妻子试图阻止他。

莱斯特辞去了工作，并且要挟他的老板。他回到家里，在餐桌上大吼大叫，还把食物扔得到处都是。他买了一辆庞蒂克火鸟汽车，因为这是他一直想要的。他试图找寻自我。

_女性旅程阶段4的创作技巧_

●记得加大她内心冲突和外部冲突的筹码。她的原型会借助哪些优点？她会依赖哪些缺点？

●因为这个阶段是由内心冲突驱动的，找到五个方式来把角色的情感外在化。很多时候，当角色遭遇爱人的死亡时，她们会回到家里拼命擦洗橱柜，好像这样就可以清洗她们的人生和情感一样。所以思考一下把情感外在化的方式。

●记住在这个阶段她的恐惧会被用来对付她。

●想出几种需要她面对的恐惧，记住在第6阶段她需要面对她最大的恐惧。

●这个阶段会以一个小高潮结束。

**阶段5：风暴之眼**

莎拉倒在地下室的地板上。她倾听黑暗中有没有声音，不过周围一片寂静。她松了一口气，稍稍放松了自己的神经。她回想了片刻，意识到自己并没有踩到任何一块玻璃碎片。她安然无恙。

她看到远处有一丝亮光。她以为那里就是另一个世界，她的旅途已经结束了。突然传来一阵脚步声。

在正视她的恐惧并与一些反派遭遇之后，主角逐渐接受了发生的一切，而且觉得自己应对得很好。她有了一种虚假的安全感。她觉得这就是旅程的终点，心情开始放松。

但是，她没有那么容易就能脱身。她依然需要展开行动，主动去争取达成自己的目标；单单面对问题是不够的。在目前这个时候，她舔着自己的伤口，安慰自己，急切地想要回到家里，告诉每个人发生的一切。

她尝到了一点胜利的味道，尽管不真实，这味道在将来会继续鼓舞她去夺取胜利，因为她终于知道了胜利的感觉有多美好。在当下这个时刻，她感觉很安全。不过，读者会得到旅程并未结束的提示，尤其是这本书才看到一半！

例子：

●她被告知，只要不去做证，她就是安全的。

●她跟爱人在一起，感觉活了过来。

●她终于做成了一件一直在尝试的事情，她可能觉得心满意足，不去寻求真正的目标了。

●也许她的丈夫又回到家中，她感到有他在也不错，至少没那么孤单。

●虐待她的人被逮捕了。

●她被告知她终将得到晋升。

●人们告诉她不要再担心，因为一切都结束了。

●她被告知人们实际上是看重她的，她之前感受到的一切轻视只是她的幻想。

通常，这种虚假的安全感会以蒙太奇的手法来展现，幸福和充满希望的场景一幕幕浮现。

主角放松了片刻，可能还冒了个不该冒的险。反派在远处冷笑着看着这一切，盘算着，等待着。

其他配角想要带她回家。他们害怕她一旦打破陈规会影响到他们。如果她成功了，他们的世界也要跟着改变。“如果她真的离开这里，我们该怎么办？”他们思索着。“我们也就没有借口继续留在这个所谓的完美世界里了。”

她可能会遇到处境比她更糟的人。她会帮助这个人，并且以为自己待在这里是为了帮助其他人，她自己不需要去经受什么改变，但是她很快会明白唯一的出路是自己亲身经历痛苦。

阶段5的例子

《伊南娜的冥界之旅》

伊南娜遇到黑暗女神厄里斯奇格，目击了一场葬礼，并跟着黑暗女神一起哭泣。她觉得自己做得很好，因为厄里斯奇格欢迎她的到来，而且她是唯一一个亲眼见到厄里斯奇格还能活着的人。她感到安全。

《绿野仙踪》

多萝西走到了光明的境地，她穿过了有毒的罂粟花田，站在了奥兹国的大门前——她和她的伙伴们做到了！他们获准进入奥兹国，并且得到了皇家贵宾般的礼遇。

《泰坦尼克号》

萝丝上了一艘救生艇，以为杰克会坐上后面那艘。看起来好像他俩都能在这番严峻的考验中活下来，但是她有些疑虑。

《觉醒》

埃德娜的丈夫出差去了，把孩子们都送到了爷爷奶奶家。埃德娜现在一个人在家，安静而满足。她尝试结交新的朋友、做些新的事情。她在赌马中赢了钱，还遇到一个叫阿罗宾的人。

她决定自己搬到一个小房子里去住。她想要变得自给自足，不再从属于丈夫。

性别偏移：《美国丽人》

莱斯特人生里第一次感到愉快。他每天慢跑，状态良好。他有机会跟自己常幻想的那个年轻女孩偷欢，不过他克制了自己，拒绝了她。他好像变得振作了。

_女性旅程阶段5的创作技巧_

●想出各种让她觉得安全的方式，让她获得那种虚假的安全感。

●这个阶段最适合加入一些悬念。主角感到安全，可是读者不一定相信她是安全的。

●用一些预兆来暗示一切很快会土崩瓦解。

●反派在这个阶段看上去好像被打败了，但是给他留一条出路。

●她跟伊南娜一样，以为自己去那里只是为了帮助另一个人渡过难关，还没有意识到唯一的出路是亲身经历痛苦，而到了那个时候，没有人站在她的身边握着她的手支持她。

**阶段6：死亡——失去一切**

莎拉听到脚步声越来越近。她手无寸铁。她退到一个角落里，缩成一团，一个巨大的身影走近了，嘲弄着她。

空气里弥漫着腐烂的味道，四周肮脏泥泞。她为什么要来这里呢？一切看起来那么让人绝望。“我放弃。我应付不了。”她侧躺着，闭上眼睛。她已经没有力量继续斗争了。

突然间，反派杀回来，一切都来了个大转变。她本以为磨难已经结束，她可以焕然一新地回去继续人生，可是现在麻烦又重新冒了出来。这个阶段是个逆转，以失去一切的黑暗时刻告终。反派占据上风——人们觉得一个倒下的女人自己应该爬不起来，这样子其他人才有机会去英雄救美。没有人支持她自立的想法。

反派这次没有再自大地笑。他感觉到被她的成就所威胁，决心摧毁她。他看出她内心的风暴已经帮了他一个忙。他要做的就是再推她一把，打击她，放大她的弱点。他会带给她一个新的背叛，或是想办法让她像一个受到排挤的人那样，感到自己的愚蠢。

●她被抓住，受到羞辱;她被抛掷一旁，等死而已。一切都结束了。她的旅程失败了，她接受失败的事实。她茫然地徘徊着，对事态的变化深感困惑，想不通为什么一切都变糟了。她看不到在另一面等待她的礼物。

●如果她的丈夫离开了她，这个阶段描绘的就是她看到他跟另一个女人在一起，同时自己还失去了别的一些东西——工作、房子、金钱。

●如果她曾经遇袭，这个阶段描绘的就是她看到那个袭击者被释放。

●如果她在晋升方面被忽略，这个阶段就是晋升机会给了别人的时刻。他们甚至在她的档案里捏造一些事来抹黑她，这样他们就可以顺理成章地炒掉她了。

●她看着自己受到践踏和羞辱，她的所有资源都被切断了。

●她再次遭到背叛。

●她可能真的经历死亡。

第4阶段“下降”更多地展现了她内心的冲突和风暴。这个阶段则展现了外在的冲突、她跟反派的推动情节的斗争。小红帽之前在森林里遇到大灰狼，而现在她必须要面对的是，大灰狼吃掉了她的外婆，正在家里等着吃掉她。

这个阶段很适合再加进一个配角，让主角的处境变得更加艰难。

很多关于女主角的小说就停在了这个阶段，尤其是弗吉尼亚·伍尔芙和伊迪丝·华顿的小说。她们那个时代的主角试图挑战常规、反抗社会，但是完全得不到任何支持，以至于不可能成功地走出这个阶段。没有人帮助她或是支持她的想法。她要么选择死亡，要么被流放，要么回到原先的生活，总之都是殉道者，在杰奎琳·米恰德（Jacquelyn Mitchard）的小说《海洋深处》（The Deep End of the Ocean）中，女主角执迷于寻找失踪的儿子，进入了下降阶段。在死亡阶段，她在抑郁中昏昏睡去，放弃了生命中的一切。她迷失在“灵魂的暗夜”经历中。

阶段6的例子

《伊南娜的冥界之旅》

伊南娜站在黑暗女神厄里斯奇格面前。“冥界守门的全知的法官们觉察到伊南娜隐藏的心口不一的部分，并指控她。厄里斯奇格叫道：‘有罪！’伊南娜就被杀了。”她被吊在一个木桩上，等待腐烂。

《绿野仙踪》

巫师要求多萝西把女巫的扫帚带来给他，否则就不让她实现心愿。这对她来说是另一种背叛，也是故事的一大转折。在她寻找扫帚的路上，她被绑架了，并被邪恶的西方女巫判处死刑。

《泰坦尼克号》

萝丝跳出救生艇，朝杰克奔去。他们拥吻。他问她为什么要这样做，她回答：“你跳，我也跳，还记得么？”他是她唯一在乎的，她愿意不顾一切跟他在一起。有他在身边，她敢于面对死亡。电影接下来的部分都是关于他们困在垂死阶段，等着轮船沉没。

《觉醒》

埃德娜冲到朋友的家里帮她接生。她朋友经受的剧痛吓到了她。当她准备离开的时候，她的朋友告诉埃德娜自己已经知道了埃德娜跟阿罗宾的私情，并提醒她，她应该“想一想孩子们！”

埃德娜感到“她的孩子们会把她束缚在一个悲惨人生里”。当她回到自己的新住处时，她发现自己挚爱的罗伯特永远离开了她。因为一旦她为了他离开丈夫，他就要面对那些无可避免的嘲弄，而他无法承受这一切。

埃德娜赤裸地站在海滩上。世界上唯一支持她的是一个警告过她如果挑战常规就会被孤立的女子。埃德娜走进水里，游向了自己的死亡。

性别偏移：《美国丽人》

莱斯特坐着欣赏一张全家福，被另一个在寻找自我过程中受到惊吓的男人开枪打死。社会同样容不下挑战常规的男人。

_女性旅程阶段6的创作技巧_

发挥你的想象力，想一下你的角色在不同的情形里会有怎样的反应。用好角色原型。不要模式化。不是所有的女人在丈夫离开后都会陷入抑郁，有些人会出去寻找一夜情，有些会去买把枪，或是回学校读书。

看几集《宋飞正传》，看看里面的配角在故事开头的一些行为是怎样在故事的结尾破坏了主角的所有计划。这部电视剧在这方面的技巧真是出神入化！你能否想出一个配角，使这个阶段的形势更加严峻呢？

**第三幕**

**阶段7：支持**

莎拉躺在潮湿寒冷的地上，她闭上眼睛，对周围无知无觉。她找不到出去的路。没有一丝光亮，她分不清东南西北。

黑暗中有个声音在呼唤她，她抬起头。远处，一根火柴照亮了一个上升的楼梯。那个楼梯一直在那里，只是她没有发现。她努力爬起来，朝光亮处走去。她想着：看来这里只有我一个人。

女性旅程包括个体跟集体之间的关系。主角经历了她自身的觉醒，愿意接受他人伸出的援手。她不再害怕背叛，因为她已经有了自己的力量，有了足够的自我了解，这些是别人夺不走的。她就像一个囚徒，在牢狱中找到了心灵上的自由。别人对她有什么企图，或是想从她这里夺走什么，都不重要了。

她接受别人原本的样子，相信女人之间可以相互帮助。她开始看到人与人之间的同一性。

很多养过孩子的女人都曾为找不到人帮忙带孩子而烦恼。现代社会里每个家庭都是个孤立的单元，能够叫邻居帮忙看下孩子的时代已经一去不复返。人多是种力量，主角也意识到待在群体中的好处，哪怕只是跟一个懂她的人在一起。

有些时候，比如在悬疑片和恐怖故事里，主角发现只剩下自己一个人，因为其他人都死了或是消失了。而在这个故事里，另一个角色已经准备好了工具，或是提供了她找寻出路需要的信息。她得到了别人的帮助。

在其他故事里，她可能是独自一人，但她从自己的信念中汲取了力量，或是听从一个“声音”的引导，就像圣女贞德那样。

男主角可能需要完全依靠自己完成任务，来向团队证明自己；女主角需要的是向自己证明自己，并把这种经验跟团体分享。她接受自己是个女人的事实，并且觉得这是一件好事。过去她总是想做一个男人，活在男人的世界里。现在她开始定义自己的世界。

主角接受别人的帮助，让他们推自己一把，而那个帮助她的人也能获益，借此开始自己的内心旅程。她的旅程影响和引导着其他人，所以她并不是在接受施舍，她是在树立一个榜样。很多时候，配角也有自己的问题需要解决，主角接受他们的帮助，让他们得以对过去的行为做出补偿。

阶段7的例子

《伊南娜的冥界之旅》

三天之后，伊南娜的朋友宁舒布尔在她的父辈们面前为伊南娜求情。他们都不愿帮助她，除了伊南娜的外公恩基（Enki）。他是唯一赞同伊南娜尝试冥界之旅的人。他派了两个雌雄同体的人来到冥界，向厄里斯奇格说情。她让伊南娜复活了，但是“没有人可以毫发无损地离开冥界，如果伊南娜想要回到阳间，她必须用别人来替换她”。

《绿野仙踪》

多萝西被邪恶的西方女巫抓住，等待她的是死亡。她的朋友们——稻草人、铁皮人和胆小的狮子都来救她，而他们也因此找到了各自需要的头脑、心和勇气。她有支持她、关心她的朋友。他们会不顾自己的安危来帮助她。

《泰坦尼克号》

萝丝身边有杰克，他是她的翻版（象征角色），一个有着积极和雄心勃勃的欲望的她。他鼓励她、引导她，但也承认“唯一能够拯救萝丝的人是萝丝自己”。他并没有代替她去做这些事，他只是给她提供改变所需的空间、知识和榜样。他对她的帮助贯穿整部影片，陪伴着她经历下降阶段和面对死亡。他要她承诺，她会活下来，完成所有那些他们说好要经历的事。

《觉醒》

没有人帮助埃德娜完成她的旅程。社会太强大，难以对抗，其他角色都不愿意牺牲自己来帮助她。她孤立无援，只能选择死亡或是回到旧有的生活方式和旧有的自己。如果她挑战常规，过自己想要的生活，她就会被社会孤立，她也不敢想象她的孩子们会因为她的行为受到怎样的波及。

性别偏移：《美国丽人》

莱斯特想要改变生活，改变那种认为真正的男人就应该好好表现、养家糊口和保护他人的观念，但是没有人支持他的决定。他想要寻找自我，不想回到原先那个安全但不快乐的世界。

_女性旅程阶段7的创作技巧_

●你有没有曾经情绪低落，觉得自己是全世界唯一一个感到这种痛苦的人，后来才意识到成千上万的人跟你有着同样的遭遇？从自己的人生经历里汲取灵感。

●确保帮助主角的人们在故事的开头已经出现过，这样他们不会显得凭空冒出来。他们应该是我们在第一幕已经听说过，或在第二幕接触过的人，这样他们再次出现的时候，读者能想起他们是谁。

●主角依然要面对反派，不过她已经见识过各式各样的恶魔，已经变得更强，做好了充分的准备。

**阶段8：重生——真相的时刻**

莎拉重新经过这几扇门的时候，收回了她的所有工具。她走入光明中，身边的一切依旧，但在她看来已完全不同。

她积极主动地去追寻自己的目标。她不再是那个只会被动应对生活的充满恐惧的小女孩，而是一个能主动出击的强大女人。

主人公已经找到了她的力量和决心，并且充满热情地追寻自己的目标。现在没有什么能阻挡她了。暴君和食人魔在这个阶段再也吓不倒主人公了，它们只是被嘲笑的丑角。她看到了人生的全景，明白自己不可能再回到过去，也不想回去。

她掸去身上的灰尘，带着新的力量，无畏地直闯虎穴。她不再惧怕死亡，因为她意识到自己在原先的完美世界里虽生犹死。她在风暴之眼的阶段里尝到了胜利的滋味，感觉很棒。现在她要夺取更大的胜利。她无法想象自己在下降的阶段里居然曾经想过放弃。

主人公学会了划定界限、采取行动、倾听自己内心的声音。她找回了自我和武器，并且意识到恐惧来源于内心。她找到了勇气，学会了使用头脑，还赢得了自己的心。这三者的结合是达成目标不可或缺的条件。

拥有善心并不是说主人公不能杀戮，如果剧情需要，她也可以杀人。拥有善心意味着有觉悟，有相互关联的意识，对自己的行为负责任；行动时冷静得像个武士，而不是像个狂怒的野兽。拥有善心，她就拥有力量。她所做的一切不是出于恐惧，而是出于对力量和真相的认知。

她的目标触手可及。她必须迈出最后一步。“她会这么做么？”是读者思考的问题。有时候这个阶段和下降的阶段形成对照，那个时候她第一次直面反派。

她迈出了最后一步，证明自己已经脱胎换骨。她曾经软弱哭泣，现在却面带微笑；她曾经犹豫不决，现在却步履坚定；她曾经害羞胆怯，现在却大胆自信；她曾经麻木冷漠，现在却变得关心他人、思虑周全；她曾经是个脆弱的女人，现在却是一个坚定的斗士。现在的她跟过去完全相反。

阶段8的例子

《伊南娜的冥界之旅》

当伊南娜从冥界返回地面时，厄里斯奇格的两个魔鬼跟着她，要找一个替代品。伊南娜不肯让他们带走她的任何一个孩子。她看到自己的丈夫“坐在高高的王座上……他借着我的力量让自己更显赫……却不肯走下王座来帮助我”。她想让他也走一回自己的道路，于是把死亡之眼对准了他。魔鬼就把他带走了。

《绿野仙踪》

当邪恶的西方女巫试图杀死多萝西和她的朋友们时，多萝西端起一桶水泼在女巫身上，反过来杀死了她。她接着要求巫师满足她和朋友们的心愿，却发现巫师实际上只是一个矮小的老人，没有那么强大的力量。

她意识到自己一直拥有回家的力量。她需要的就是了解到这一点。当她回顾自己这一路走来的经历时，她才发现自己多么强大。

《泰坦尼克号》

杰克在萝丝下降的过程中一直支持着她，鼓励她踏出每一步。当形势变得艰难时他也没有离开她。他引导她经历这些严峻考验，让她屏住呼吸，穿上救生衣。他要她保证无论遇到什么情况，她都要生存下去，他甚至为她牺牲了自己的生命。他死后，她放开了他的手，游向了安全和自由。她有了振作起来的力量。

在救援船卡帕西亚上，她有最后一次机会回到原先的完美世界，现在杰克也不在了。不过她却藏起来，不让未婚夫看到自己，她换了身份，走向了一个新的世界。

《觉醒》

因为埃德娜在她的旅程中得不到任何帮助，她的旅程以悲剧告终。有人认为她选择自杀是因为那是她能获得自由的唯一方式，这样的结局是一个好结局。她坚持她的生命应由她自己来结束，她不相信社会摆在她面前的那些假象。

性别偏移：《美国丽人》

同样，莱斯特没能得到帮助，在上个阶段死去了。他的死亡向他展示了人生的价值，整个故事是由已故的莱斯特以倒叙的手法讲述的，他没有怨恨，相反，他找到了平静。

_女性旅程阶段8的创作技巧_

●想出五种不同的方式来展示她的重生。确保它们都是具有积极意义的。再一次审视她的恐惧。

●想一下有哪些东西可以象征出生和重生。你能想出一个物体来象征她的整个旅程么？把它加入背景设置中来渲染整个主题。

●这个阶段明确后，你可能需要回到第一幕，加入更多关于她的旅程主题的元素。

●你要在故事开端加入一些铺垫，使这个阶段的故事发展显得更为可信。

**阶段9：周而复始——回到完美世界**

莎拉回到完美世界，她已经是一个彻底觉醒的人。她能清楚地看到她的朋友们都活在玻璃罩里，她迫切地想要帮助她们。

一个朋友站起来，她的头撞到了玻璃天花板上。她抬起头，看来这是她第一次撞到头。莎拉带着微笑向她走去。

主角回到家里，看自己到底走了多远。她达成了自己的目标，但是她回到完美世界后，能否保证自己不再被拉回到原来的角色上？

这个阶段是故事的一个小高潮，主角回到完美世界，看清了它本来的样子。其他人会被她的经历触动，甚至会不得不正视她们自己的恐惧。她曾经跟她们一样，而现在却过得更好。这是不是意味着她们也有可能改变呢？

通常，她觉醒之前最亲近的那个人最可能被她的改变所影响。

她可能会选择某个人来踏上新旅程，继续这个周而复始的过程，把她的经历跟他人分享。她是下一批旅行者的支持者。

许多女作家觉得她们的故事很分散，像一个圆圈，而不像一条直线。有人觉得女性旅程的故事原型完全没有一个结局。

女性故事是有结局的，女主角们达成了自己的目标，她们在自己的人生和性格方面确确实实做出了具体的改变，不过有时候在旅程的这个阶段可能会看到对未来生活的暗示——某种继续发展、构成循环的趋势。

男主人公通常“抱得美人归”，或是在故事的结局里得到某种外在的奖品，女主人公得到的往往是内在的东西——一种使她继续前进的精神力量。她个人达成了目标、改变了人生，并不意味着社会已经跟她一起改变了。世界上依然存在暴君、食人魔、种族主义者、性别歧视者，她只是有了更多的力量来面对这些障碍。

因为故事本身有个结局，让她完成了自己的旅程或任务，这给读者留下了一个希望，即女性可以有所成就、迈向成功。如果你想写一个没有结局的故事，可以考虑将这个阶段发展成循环叙事，让主角在上一个阶段成功地解决问题，哪怕成功是指放弃自己的目标。这里可以提一些道德层面的问题，不是所有次要情节都需要环环相扣。

阶段9的例子

《伊南娜的冥界之旅》

杜牧茨的妹妹恳求伊南娜帮助杜牧茨。她要分担哥哥在冥界的部分时间，一年中六个月代替他留在冥界，伊南娜答应了她的请求。杜牧茨的妹妹隐喻了伊南娜富有同情心的一面，伊南娜将会再次下降，去帮助旅程中的其他人。

《绿野仙踪》

在书中，多萝西回到了完美世界。她向婶婶跑去。很明显已经过去了一段时间，每个人都知道她去了另一个地方，这使得她的旅程很可信。她的叔叔造了一幢新房子，这象征着她经历的所有改变。她迫不及待想要告诉每个人发生在她身上的故事，他们也迫不及待想要听听。

在电影中，多萝西把她的故事告诉了所有人，但是没有人相信她。她说自己不应该离开家，她应该只待在后院里寻找自我，不应该出去冒险。她一回到完美世界，又处在它的掌控下。“哪里都没有家好，我永远不会再离开了。”她说。没有人相信她的故事或是关心她的旅程，也就没有人从她的经历中有所收获。

《泰坦尼克号》

电影的开篇就是这个阶段，年老的萝丝把她的经历告诉那些搜寻钻石的船员们。每个人都被她的故事深深吸引，他们用科学数据看待历史的观念彻底改变了。

电影尾声，她站在船尾，就像开头时泰坦尼克号上的完美世界。她拿着那串钻石项链，让它坠入海洋深处，我们看到她在这些年里完成的事——飞行、骑马、前往圣塔莫尼卡的码头。她的孙女听着她的故事，眼中满是泪水，她被听到的故事改变了。

《觉醒》

回归跟死亡阶段同时发生。埃德娜回到了格兰德岛屿——故事开始的地方，来结束她的生命。她静静站了一会儿，看着一只鸟儿拍打翅膀，落入海洋，想起她在故事开篇时听到的一个传说。

性别偏移：《美国丽人》

莱斯特死后，通过用第一人称讲述自己故事的方式回到了完美世界。他用不同的眼光看待一切、接受一切。

_女性旅程阶段9的创作技巧_

●再一次回顾第一幕，并在这个阶段中设置一些呼应的元素来展示她的改变。她对完美世界的反应是否有所不同？

●你能在世界中做一些改变，来作为她的改变的隐喻么？

●她是作为一个全新的人留在这个旧世界里，还是会彻底离开这个旧世界？

●会有人愿意倾听她的故事么？她们会对她的改变感到愤怒么？

●她有回去的念头么？

注释：

[^1]: 莉莉丝（Lilith），最早出现在苏美尔神话中，亦同时记载于犹太教的拉比文学。她被指为《旧约》里亚当的第一个妻子，由上帝用泥土所造，因不满上帝而离开伊甸园。  

注释：

[^2]: 艾德丽安·里奇（Adrienne Rich，1929—2012），美国诗人、散文家和女权主义者。  

[^3]: 印度瑜伽中，脉轮是人体能量的中心，位于身体的中轴线上，主要影响的是人的心理状态。位于身体底部的脉轮主要主导本能部分，身体顶端附近的脉轮则影响我们的思想部分。七个脉轮分别为海底轮、本我轮、脐轮、心轮、喉轮、眉心轮和顶轮。