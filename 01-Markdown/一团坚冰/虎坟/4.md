## 4

大山半空着肚子，被关在一辆车后斗上的笼子里，在一个夜晚，被送达郊外的动物园。小城远在最北，秋天短暂得像从未来过，很快入了冬。陈寿在收拾行李了，房子已经退租，今天是他住的最后一天。老袁给他和同行人定了明早八点的车票，天一亮，就要走进另一段人生。陈寿不无平静，给空掉的兽笼挂上锁头时，耳边似乎仍有大山熟悉的呼吸声，团里各个房间，也仍有人在走动，响马的魂儿还是趴在窗台上抽烟。烟雾下，他的黑西服一耸一耸，肩膀不时移动，人一点儿不老实，张望着各处，问候每一个走过他面前的人。走啦？响马雄浑的音色在喊。陈寿走到窗前，看外边天色悄然变红，按说十一月不该下雪，但那是别的地方，他们这里，四月也下雪，冬季总是漫长得过不完。陈寿无所事事，自由成为壮大了的空虚，包裹住他每寸皮肤，使他对冷热寒凉，一概感受不能。陈寿想着，第二天起早，他要带上行李，揣好车票，去墓园念叨念叨。先看父亲，再看师傅，最后看响马。等告别死的，他再继续活。

姚梦露站在动物园门前，夜里十点了，他一直在等她。是姚梦露在电话里约定说，等关园，等入夜，他们再相见。她还穿一身黑。赫然入目的，是她肩披着响马的黑西服，在路灯下抽烟的场景。她问陈寿，该怎么进去？陈寿拿出工作证，给看门大爷看，说，我来喂老虎。大爷说，我们喂过了。陈寿说，我是马戏团来的，刚送来的虎，先前一直是我喂。明天我就走了，来看望看望。待一会儿，不会多留。大爷又问跟着的姚梦露是怎么回事儿。姚梦露轻蔑地不说话。陈寿替她答，她和老虎，感情更深。大爷把铁门拽开，说，出了事，你们领导来找我们领导。姚梦露跟在陈寿身后走，寒风呼啸的园子里，一盏灯不亮，寒风一吹，净是腥臊的味道。陈寿一手拿着向看门大爷借的手电，一手拎着十斤带骨肉，姚梦露踢踏着尖鞋跟，笃笃地随他开出道的光线走。

大山住的地方门已锁住，黑漆漆的虎山后，它和另外两只老虎一齐趴在里头。按开廊灯，陈寿直直站在笼外，看清每只老虎的脸。看望来者时，同样的怨恨瞬间满溢了它们的褐色瞳孔。他能一眼认出大山，三只老虎谁都没有动，陈寿内心狂叫着大山的名字，从大山恐怖的眼神里，透露出的却是陌生。姚梦露也变得恐怖，在响马死后，她第一回眼睁睁地又见到了虎，像个被点燃的炮仗，狂轰滥炸，炸出一叠声的脏话，同时狠拽陈寿的衣服，质问他，哪一只是大山？陈寿不说话。姚梦露用自己的高跟鞋，一下下踹着笼门。老虎们终于有了反应，它们被激怒了。有一只发出凶叫，嗖地一下扑了过来，脸孔变得扭曲。发狂的老虎头上白色鬃毛根根竖立，两眼盯着姚梦露的细腿，瞳仁占满眼眶，疯狂侵蚀，不剩一丝理智。它准备等她再抬脚时，就冲破笼子，将其咬碎。陈寿扭头看姚梦露，发现她竟然和老虎露出一模一样的表情。

许是骂累了，许是哭累了，姚梦露蹲在地上，渐没了声音。陈寿解开塑料袋上的扣，拿出肉块，用带来的小刀削好，一下下离远着，往里扔。起初老虎以为陈寿在拿东西砸它们，都躲远开。一只先近前，陈寿热泪盈眶，因知道那是大山。大山离得越近，越能闻见陈寿身上的气味儿，相信扔在地上的就是食物。可大山吃的一直都是鸡骨架，牛肉价贵，在团里时，没给过它这样的待遇。大山嗅着陌生的牛油味，试图张口，咬碎牛骨里的油脂。仅几秒钟，它就咽完快两斤的牛肉。另两只虎追着它嘴边的剩肉咬，怕大山被咬伤，陈寿紧着往里扔剩下的肉块儿，三虎分头而食。肉全吃空了，它们各自虎视眈眈，边舔舐面前地上的牛油，边打量对方是不是还有没吃完的肉，随时预备扑过去。大山舔完油，抬头望陈寿。不知道是不是因为饿了太久，突然吃到食物，它原本耷拉的肚子，开始一抽一抽，发出呼呼的声音，就像个成年男人，在境遇不堪时，头埋在手臂里哭泣传出的那种浑浊又绝望的哽咽。

陈寿突然张口，大山！大山还是发出呼呼的动静。姚梦露爬起来，扔进一只鞋，砸大山的头。大山没提防被砸中了身体，接着又是一只鞋。姚梦露往空掉的塑料袋里乱抓，里面装着刚才陈寿割肉用的刀。陈寿抱住她，抵死不放，姚梦露挥舞小刀，眼红睁着，对他喊，不是它就是你。一番搏斗后，陈寿将她制服，压在了身下。兽笼里吃饱了的老虎们则先后回到睡觉的地方，它们波澜不惊。在混杂了响马和姚梦露两者的味道中，陈寿告诉姚梦露这些话。现在夜深人静。杀了你，把你分了，和刚才那些肉块一个样。我丢进笼子，它们不用几分钟，就能给你吃干净。知道吗，你想象一下？姚梦露被按在水泥地上，她盯着他。我一点不害怕，她说，真的，陈寿。你这么一说，我心里挺舒服的。我希望无论什么地方，都能和他一起去。陈寿也盯着她，姚梦露美妙的眼睛里，血丝一览无余，连她上飞的眼角周围，几道因人生中几次撕心裂肺才积攒下的皱纹，也都十分清楚。陈寿说，可我害怕。姚梦露把头埋进陈寿怀里，问他怕什么。陈寿说，怕自己完全变成大山，对人命越来越不在乎，只把你们当成一堆肉。

走了好，走了对。他们坐起来，靠在走廊外斑驳的白墙上。陈寿重复着，走了好，走了对。姚梦露笑出声，明天一早，我要去广西。陈寿问她几点的车。姚梦露说八点。陈寿说，好，我们一起去车站。她说现在她就要回去了，累了，什么都想放下了。陈寿固执地抓着她手，他知道，今日一别，和死去一样，是再不能相见。往后，他的马戏，她的话剧，都将成为这座城市留给上一时代的古老记忆。人们只会记得，话剧团从不开门，马戏团虎咬死人。陪我去个地方，陈寿说。出租车上的姚梦露，抿着黑西服的襟，半开车窗，望向外头空荡荡的城市。纷纷雪片下起来了。车载着他俩，向一个反而热闹着的地方去。那里死生仍继续，唯独肉眼看不穿，那里，好多已被雪埋下了的爱恨，仍和春苗一样，蛰伏生长的迹象。

陈寿清楚，姚梦露和响马会有单独的话要说。他自己先去看了父亲，父亲的黑白小像，在暗红色的天空下映出，没一丝阴阳相隔的气氛。他记得父亲一直话少，现在父亲不说一个字，也没让他感受多失落。睡在父亲身畔不多远，就是师傅。师傅在像里，是笑着的，怪模怪样，连死也是个表演中的状态。坟前，陈寿将从路上超市买来的两瓶白酒，给俩老头各洒了一瓶。酒味蔓延开来，经大雪快速覆盖，融合为清新好闻的一缕香。陈寿在心里念叨，儿走了，也许还在马戏团，不练动物了。现在好些地方，都不让练动物了。往后，我练自己。爹，师傅，练自己我挺习惯的，小时候好些次，疼得骨头快碎了，你俩在我边上喝着酒，醉眼迷离，跟拿我当下酒菜似的，不时递两句骂。爹你说得是，大丈夫不能无本领在身。师傅你说的是，陈寿啊陈寿，人这辈子，总得死上一两回。

姚梦露蹲在坟茔里，半昏半暗中，雪花降落在她头顶上，似一片片纸钱，花白又缥缈。陈寿站得远些，手里攥着和酒一块买的给响马的一盒中华烟。他轻轻叫她，哎。姚梦露不动。他又喊她的名字，披黑西服的背影还是像没听见一样的。陈寿唤，露露。她肩膀一松，转回头，像四五晚没睡过觉了，头靠在响马的碑上，眼皮半闭。陈寿过去给响马的小像擦一回灰，方头宽脸的响马含笑看他俩，眉毛仍是粗黑，眼神也熠熠，比活人更像活人。一夜未眠，陈寿眼前有点花，响马小像上的脸，渐渐被大山因饥饿而疯狂的虎脸占据了，正发出无可奈何的、低沉的一声吼。他和姚梦露，双双跪在响马坟前，像一对孝子，哭得喘不上气。香烟散尽，纸灰随雪飘远，陈寿看着时间，两辆列车可能都已停在了站台。姚梦露一人起身，拽下肩上的黑西服，披在了陈寿身上，如一个轻飘的拥抱。她只穿袜子，双脚落在雪地里，悄没声地越走越远。

陈寿也走。没什么机会再回来，无论是给响马扫墓，还是给大山喂肉吃。但他总会梦见响马，每次响马都说，不用他惦记。久了，陈寿真不再惦记，连生养他多年的城市、四季飘过的沙、一冬漫长的严寒，也一并不落心上。常年穿响马西服的陈寿，变成了另一个响马。因和响马总在梦中相见，倒也不觉得能忘了。反而是老虎大山，听老袁说它病了，也有说死了的，更多说法是，动物园也不收它了，这让陈寿渐渐认定，大山是他记忆里一段虚幻象。对一只本该啸聚山林的动物来说，大山活过一世特殊的命运，即作为老虎而生，作为猪肉而死。有时陈寿在南方想起它，会想到应该给它立个坟。不用了，每次转刻又觉得，响马的坟就是大山的坟。坟里灵魂是响马的，游荡于四方，坟里骨肉是大山的，虎皮都已烂透。