   

## 36

今年四个人报副高，只有一个名额。按惯例学校要求院里排一个序报上去，为的是减轻学校评委们的压力。怎么排序由院教授委员会定。按说我应该相信这些教授，他们都是很好的人。可汪燕燕是童校长的弟子，如果童校长逐一给他们打电话呢？那他们就没有办法了。得罪我总比得罪童校长心里轻松点吧。小蒋又告诉我说，汪燕燕已经逐个上门拜访那些教授。这让我危机感陡然上升。上升之后又回过头想，她这样做表现了她内心的焦虑，那么童校长应该是没有下决心为她办成这件事。童校长虽然是个副校长，对别人的议论还是有顾忌的，到关键时刻才会出手。

这样想着我宽心了一点，像划着小船从急流险滩进入了平缓的大江。宽心之后又觉得这宽心没有充分理由，说真的我应该相信那些教授，他们都是我的老师，也都是很好的人。可是我也不能保证他们在双重压力之下，不会做出违心的选择啊。我觉得自己真的非常危险，很可能又吃个哑巴亏，到头来连个倾诉的地方都没有。你在院里倾诉，你等于打那些教授的脸。到家里倾诉呢？那简直是找骂。

我想起小蒋告诉我的那件事，就到院资料室去找汪燕燕的那本书，有一次我在书架上看到过。如果真是非正式出版的，逼急了我，我也可能把这事抖落了出来，我得有个准备。走进阅览室我心里很痛苦，都是几个读书人，怎么要这样兵戎相见？搞学术不应该搞到白刀子进红刀子出的地步。可是，真的没有别的选择，资源就那么多，少数牛人已经占了大头，剩下的大家都拼了命去争，你不争你就没有。

我找了很久没找到那本书，这让我更加恐慌。难道有人借走了？那不太可能，谁会借这样的书？应该是汪燕燕自己拿走了，她已经在防着这件事了。我问管理员李灿云大姐，是不是见过这样一本书？她马上说：“前两天汪老师借走了。”从汪燕燕的卡片袋里把卡片抽给我看，说：“咦，还是汪老师自己写的呢？是我们这个汪燕燕吗？”我说：“大概肯定是吧，也可能绝对不是。”

出了门我垂着头走在林荫道下，想着别人下了这么大的功夫，我真的有些绝望了。要是就我自己吧，我也就算了。可一想起平平和安安，我心里就绞得痛，我多么想让她们过上平平安安的日子啊！还有蒙天舒，他都报正教授了，这也让我心里绞得痛。我把嘴唇咬得快要滴血，这样来平衡心中的那个痛。我似乎感到湿乎乎的嘴唇有点咸味，就掏出手绢在嘴唇上按了一下，没有血，那湿湿的并不是血。我又用力咬着下唇，再按一下，还是没有血。我对自己说：“也好，不然又吃个哑巴亏。”我四下张望，看着周围没人，自己也很意外地，抬起头，把嘴歪着，“哈哈哈哈”地笑了。

小蒋跟我打电话，告诉我过两天院教授委员会就要讨论排序的问题了，问我采取了什么步骤没有？我说：“我又能采取什么步骤？刚才去资料室找那本书也没有找到。”他说：“这件事你就信我的吧，这么大的事我敢去诬陷一个人？我这有一本，我送给你吧！”我觉得跟他见面有点不好，好像搞地下活动似的，就说：“你什么时候放我信箱，我过去拿。”

过了半个小时他又打电话来说，书已经放信箱了，又说，“汪燕燕在外面怎么说你，你知道吗？”我说：“知道，她说我的学术不算学术。也许我的学术真不算学术，但是比她的学术还是要学术一点吧。在历史学院，长了一双眼睛的人都看得懂的。”他说：“哎哟！致远，有些文章那么烂，也发表在那么高档的刊物上，编辑看不懂吗？这是懂不懂的问题吗？还有，你知道她在外面怎么说你？她说你是小人呢。君子成人之美，不成人之恶，小人反之。她用孔子的话来说你，你没有成她的美，你就是那个反之。”我说：“前几天她还说我是君子呢，怎么突然又成了小人？说得好啊，给我勇气去回她的死信呗。如今小人都有勇气说别人是小人了。”

我到信箱拿到那本书，塞到衬衫里，溜到教研室，翻开来仔细研究。这真的不像一本正式出版的书，哪里不像，我也说不出来。我想着是不是要打个电话到北岳文艺出版社去，就说要买几十本做教材，请他们帮着印证一下。想着打了又怎么样？真是非正式出版的，我能去揭她？她不过也就是想省几万块钱罢了，也可怜呢。又想着打了总比不打好，到了关键时刻，自己还有一张牌可打。

正犹豫着，龚院长打电话来，要我去一趟。进了办公室龚院长说：“小聂，你对今年评职称有什么想法？”我说：“尽量争取评上吧。”他说：“谁都想尽量评上，名额只有一个。”他伸出右手食指晃了晃，“一个。”又晃了晃。我说：“那就看材料呗，都是专家，谁看不懂材料呢？只要瞟一眼心中就有数了。”他说：“材料是你的好一些，但是现在有一种说法，资历也要兼顾一下。”我说：“汪老师资历比我多一年，但是学校的文件说了兼顾资历的问题没有？没有。”他说：“其实我是支持你的，但是你也知道，历史学院的事情也不是我这个院长说了就算数的。”我说：“谁那么有能耐，他多搞个名额给她，我不说什么。”他说：“今年的名额已经公布了，我们学院伸手，每个学院都要伸手，校长就当不成了，人事处长也当不成了。”我说：“那就只能看材料。有些人的材料，拿是拿到桌面上来了，那也可能有水分在里面。”他笑了一下，“可能，很可能。但是讨论的时候谁会说呢？皇帝的新衣，有人说吗？”

看来龚院长也知道那本书有问题，但不好说。我说：“不要说有水分，就算没有水分，那也是我的材料好一点点吧，何况那一点点是一点点吗？”他也不说那一点点是多少，摇摇头说：“我这个院长跟别的院长不一样，特别难当。”他能跟我说得这么明显，也是向我交底了。我体会到了，这些年来，他也是在走钢丝，不容易。我说：“学院的事，院长该拍板就要拍板。”他笑了说：“有这么容易，历史学院早就跨越式发展了。从我心里来说，有些局面我也还想控制一下。”又说，“比如你的事。”我说：“那我还是希望龚院长能控制一下。”他说：“那也要我能控制得了啊！有些话我在院务会上都不好讲，我讲了，马上就有人汇报上去了。教授委员会开会也是一样的。我今天叫你来，就是希望你自己有个坚定的态度，让教授们都知道，如果把你牺牲了做人情，那是不行的。”我说：“我怎么让他们都知道呢？”他说：“别人是怎么去怎么的，那你也就怎么去怎么，不然还能怎么去怎么？”我说：“知道了。”又叹气说，“要我那么去怎么，好为难啊！”他说：“活着就是件为难的事。”又说，“就这两天了。票投完了，排序就定了，复议那是不可能的。”我说：“知道了。”就出来了。

龚院长暗示我去跟教授们沟通一下，这让我很为难。可是汪燕燕已经做了这个工作，我如果不做，他们的情感天平往那边倾斜一点，我就没希望了。吃了亏我如果不嚷嚷，事情就这么过去了。我嚷嚷呢，人家有个现成的理由在那里，资历。我如果真的被牺牲了，还真的不能嚷嚷，那不是让那些教授们丢脸吗？那下次就更成问题了。

可是我怎么去沟通呢？也学汪燕燕提点什么上门？或者送个购物卡什么的？这些事情，别人做了我没做，那大家的情感就站到别人那边去了。说真的我还是愿意相信那些教授，他们大多数都教过我的，都是很好的人，也有水平，材料的好坏看得懂。可如果万一呢，万一呢，万一呢？我被自己提出来的这个“万一”难住了。

正犹豫着，小蒋又来电话，问我拿到那本书没有？我说拿到了。他说：“那你赶快行动啊！”我说：“万一是正式出版的呢？”他说：“你怎么这么不相信人呢？没把握我会乱说？”我说：“你是怎么知道的呢？”他说：“院里知道的人好多，可还是要有个人把真相提到桌面上来啊！上不上桌面，那完全是不一样的，完全不一样。不上桌面人人知道也不是个事，一定要上了桌面才算个事。”我说：“我这就打个电话去出版社澄清一下。”他说：“最后一天了，哥！快下班了，哥！”我收了线，又马上把电话打到太原，问114要了北岳文艺出版社的电话，再打过去，没有人接，再打，还是没人接。

我几乎彻夜失眠了，趴在床上不动，听见赵平平确实睡着了，才敢轻轻翻个身。好几次我想把她叫醒商量一下对策，又觉得毫无意义，她会说什么，怎么说，我都知道。好不容易熬到天亮，我起来了，等着到上班时间去打那个电话。赵平平去了学校，我又打电话到出版社去，没人接，再打，还是没人接。一直到九点多，有人接了。我说要买那本书做教材，要几十本，请她查下还有货没有，要得急。那边说尽快去查，要我下午打电话去问结果。我想着下午教授委员会就开会了，说：“我十一点再打电话来问行吗？”她答应了。十一点我再打电话过去，又没人接。一直打到十二点，都没人接。

我在心里恨着自己，这个信息早就知道，为什么要拖到今天？我有点绝望，非常绝望，觉得自己又一次被牺牲已成定局，翻盘是不可能的。只怪自己太相信自己的材料了。材料是死的，投票的人是活的，你说自己的材料好又有什么用？就像论文是死的，编辑是活的，你说自己的论文好他就给你发了吗？又想到小蒋说得那么肯定，龚院长都暗示了，我就用赵平平的手机把情况发给那些教授们，又怎么样？想到这里我忽然觉得事情非常容易，我把内容写好，要致高去转发也行啊！拿起手机我又犹豫了，万一小蒋的信息不准确呢？那我不是诬陷？就算准确，对同事下这么重的杀手，我也非常痛苦。我想着今年实在不行就算了，就等明年，不就是晚一年吗？

中午一点多，小蒋打电话来，问我把汪燕燕的事揭出来没有？我说没有。他说：“怎么不揭出来，不揭你就危险了。”我说：“万一不是那么回事呢？就算是那么回事，那她受的打击也太大了。”他说：“那就算了。”就收了线，过一分钟又打过来说：“我跟你说过什么没有？我什么都没跟你说过，是不是？”我说：“是的，是的。”他说：“那我也什么都不知道。”我说：“是的，是的。”到了四点多钟，龚院长发信息来说，你排第一。我想，这怎么可能？天上就算有馅饼掉，也不会砸在我怀中啊！心里对那些教授充满了感激，觉得对世事不必那么悲观，对人性也不必那么悲观。

过了一两个星期，学校开评了。我听说汪燕燕又在校评委那里活动，心里又紧张起来。她的意志这么坚强，这么执着，这么不辞劳苦又这么拉得下面子，她不赢那难道还是我赢？学校的评委跟院里的还不一样，他们不是历史专业的，对材料不可能看得那么清楚，因此情绪的成分就更大些。汪燕燕把他们逐个都拜到了，我呢？谁都不认识，我不输那难道还是她输？不让老实人吃亏，那让谁吃亏呢？面对这样的局面我没有办法，要我也像汪燕燕那样去奔走，我实在是做不出来。我停在原地被动地等待命运的宣判，希望结果再一次证明对世事和人性都不必那么悲观。

对学校评委的评定过程我不了解，只知道投票就在明天了。晚饭后我在厨房洗碗，心里突然冒出“困兽犹斗”这个成语。自己怎么就这样等着，连一只困兽都不如呢？性格就是命运，也许我只配这样的命运吧？这时手机响了，我把手上的水甩了甩，在抹布上擦干，一看是汪燕燕打来的。她说：“聂致远，你这个人大家都知道你是个小人就算了，你怎么还这么卑鄙呢？”我一下火了，我忍让退，忍让退，一直在忍让退，她还说我是卑鄙小人。我说：“汪老师，你想想你自己都做过什么，我又做了什么，你还说我是小人？”我左手的食指在自己鼻子上点了一下，“我是小人？我卑鄙？”我问一声就点一下，“谁是小人谁自己清楚。”她说：“你有什么想法你放到阳光下面来说，你怎么在黑暗的角落使阴？你就那么害怕阳光吗？”

我怔了一下问：“谁使了什么阴呢？我是使阴的人吗？”在鼻尖上点一下，“我？”又连点几下，“我？我？我？”她说：“那难道往校长信箱泼我的污水那还是别人泼的？他是雷锋，担心你评不上？”我马上想起了那本书的事，又想起小蒋，说：“是你那本书的事吧？我没发什么给校长。”刚说出来我又后悔了，我想证明自己是君子，事情是知道的，检举信我不会写，可她怎么会相信？她说：“是吧，是吧，你还敢说你不知道？我知道你是知道的，有铁的证据！”她把铁证说出来，是我去资料室借过那本书。我说：“事情我知道，可我不知道是不是真的，我怎么会出去说？绝对不是我，绝对绝对！”

我感到自己的表达很无力，把“绝对”说一万遍都没有用，虽然这绝对真的是那么绝对。她说：“这是一个阴谋。你不知道阴谋论的原理吗？谁受益就可以倒推他是阴谋的主使。陈水扁挨一枪擦破点皮，就选上去了，那一枪能是别人的人安排的吗？谁会去做这个好人好事？你知道你不往上面捅，那可能吗？”我说：“别人可能不可能我不知道，我是这样做人的。”她说：“做人这两个字你就不要说了，这是该你说的吗？”我说：“汪燕燕，我聂致远说的都是事实，你不相信就算了。我以人格担保那封信不是我写的。”她说：“人格这两个字你也不要说了，这也不是你该说的。你是君子？朝三暮四，阳奉阴违，见风使舵，落井下石。小人啊，小人！历史学院谁不知道你是什么人？谁不知道你那点学术是什么学术？小人啊，小人！”我说：“那我们就不要说了。”就挂了机。

我把手机塞进裤口袋，接着洗碗，心里想着，别人怎么就那么有勇气？出书的错是她犯下的，怪错人的也是她，她倒还像个道德的审判者来审判我？像这样，做个好人还有意义吗？这样想着，我把手中的碗用力往下一墩，一声脆响，那只碗裂成了两半。