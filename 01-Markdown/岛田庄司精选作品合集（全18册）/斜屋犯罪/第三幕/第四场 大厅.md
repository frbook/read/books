## 第四场　大厅

翌日清晨，天气比昨天晴朗。不知从哪里传来了榔头敲硬物的声音，咣咣咣响彻整座流冰馆。三个警察又坐在沙发上讨论开了。

“敲敲敲！一大早吵什么啊！”

“哦，是那两个女人说太可怕了，要把通风口堵起来。所以日下和户饲就发挥他们的骑士精神。愿助一‘锤’之力。日下还说要顺便把自己房间的通风口也封上。”

“嗯，这样他们也能安心了。不过这‘咣咣咣’的声音实在很烦人啊！好像过年似的。”

“是啊，敲得我头都大了。”

就在这时，一个让刑警们头变得更大的男人慌慌张张地跑了进来。他嘴里喊着人名，又似乎在说着别的什么，总之一时听不清他到底在叫什么。

“南大门先生！南大门先生！”

没人反应，大厅里变得异常沉默。

御手洗一脸疑惑，歪着脑袋扫视着众人。阿南巡警大概是凭借第六感发觉了那个“南大门”是自己的名字，立刻站了起来。

“我是阿南……”

“失礼了！可以告诉我到稚内署怎么走吗？”

“啊，当然可以。”

御手洗这个人，对于别人的出生年月日，只要听过就能立刻记住。但是对于人的姓名，却往往“转脸不认人”，即便这样，他也不会好好地询问别人到底怎么称呼，而是想到什么就叫什么，并且只要搞错一次，无论别人纠正过多少次，他到死也会继续叫那个错误的名字。

御手洗兴冲冲地走出了大厅，幸三郎跟着出现了。

“啊，滨本先生。”

幸三郎叼着烟斗走了过来，坐在大熊的身边。牛越问他：

“那位名侦探准备到哪里去？”

“那人真是与众不同。”

“太不同了，简直就是个疯子。”

“他把格雷姆的头拆下来，送到鉴证课再检查一遍，还说问题就出在脑袋上。”

“真是的……”

“到时候他会不会把我们的脑袋也拆下来调查调查。”大熊说，“我看他这么仔细，去超市当保安肯定不错。”

“我可不想和那种蠢货同舟共济！”尾崎毫不犹豫地说。

“你说的那个‘占卜舞’说不定就要开始了，没准他一回来就准备开始跳了！”

“那我们是不是把柴火准备一下？”

“现在不是开玩笑的时候。他干吗要把人偶的脑袋取下来呢？”尾崎一本正经地问幸三郎。

“这个……”

“总有他的理由吧！”

“或许跳舞的时候，有个脑袋太麻烦？”

“反正他说要把脑袋拿走，我是不太情愿，虽然拆下来很方便……大概他要采指纹吧？”

“那位‘大师’有那么聪明吗？”大熊也忘了掂掂自己的分量。

“指纹什么的，早就找过了。”牛越说。

“那有发现吗？”幸三郎问。

“最近，特别是对这种智慧型犯罪，调查指纹基本没用。凶手也会从电视上了解到指纹的重要性。而且，如果凶手就是这个家里的某一个人的话，那谁都有接触门把手的可能。”

“是啊。”

等御手洗回到流冰馆的时候，已经是午后了。他好像有什么好消息似的，飘飘然地穿过大厅，走到了我的身旁。

“我是坐法医的车子回来的，他说刚好要到附近办事。”

“是吗？”我回答。

“是啊，我问他要不要来屋里喝点茶。”他好像已经把这里当作了自己家一样。

直到发现一个穿白袍的男人从门口走进来，御手洗才想起让人准备茶点，大声喊了起来。

“南大门先生！可以帮我叫下梶原先生吗？”

我没想到他居然记得梶原的名字，大概梶原是厨师吧。靠在厨房附近墙壁上的阿南没多抗议就消失在屋内，看来他决定改名了。

当我们惬意地啜着红茶时，大厅内的时钟响了三声。

我将这时在大厅里的人仔细地记录下来。除了我和御手洗，还有三位刑警，以及那位老实的巡警阿南君，以及滨本幸三郎、滨本嘉彦、金井夫妇、早川夫妇。虽然梶原不在大厅，但我看见他在厨房忙碌。也就是说，不知去向的人只有英子、久美、户饲以及日下四人。对了，那位姓长田的法医，此时也坐在我们身边。

突然，不知从哪里传来了男人的吼声。那声音不像是惨叫，而像是看到了什么令人惊奇的东西所发出的惊呼。

御手洗推开椅子站了起来，往十二号室的方向跑去。

我反射性地看了眼房间角落里的时钟，还不到三点五分，准确地说是三点四分三十秒。

刑警们搞不清声音是从哪里发出的，不知该往哪里跑。但跟着御手洗似乎太没面子，最后只有牛越和阿南两人跟了过来。

我觉得声音的主人应该是日下或者户饲，因为只有他们两个男人没有出现在大厅。但我很难判断发出吼声的是他们两人中的哪一个，御手洗却毫不犹豫地敲响了十三号室的门。

“日下君！日下君！”

御手洗拿出手帕包住门把手，开始咔嚓咔嚓地转动。

“门锁住了！滨本先生，你有备用钥匙吗？”

“康平，快叫英子把备用钥匙拿过来！”早川康平听到命令后就跑了出去。

“快闪开！”姗姗来迟的尾崎蛮横地说道。他拼命地敲门，但无论谁敲结果都一样。

“要破门吗！”

“不，还是等备用钥匙。”牛越说，英子跑了过来。

“慢着！是这把吗？快给我！”

尾崎把备用钥匙插入了锁孔，转动之后发出了“咔嗒”一声，表示门锁被打开了。尾崎使劲转动着门把手，但不知为何门没打开。

“看来里面还有道锁也被锁上了！”幸三郎说。

原来每个房间的房门上除了弹簧锁外，在门把手的下方还有一个椭圆形的旋钮，这是一个只要转动一圈，就会伸出一截铁棒的内锁，所以也只能在屋内操作。

“撞！”牛越做出了决定，尾崎和阿南全力以赴撞了几次，终于把门撞开了。

日下仰面躺倒在房间的中央，桌上放着一本读到一半的医学书，房间没有搏斗过的痕迹。

他穿着毛衣，心脏附近插着一把同样的登山刀，刀柄上也系着一根白线。但这次和前两次命案相比有个最大的不同点——日下的胸部还上下起伏着。

“他还活着！”御手洗说，日下的脸色苍白，眼睛微张着。

尾崎一走进房间就环视四周。这时，站在他身后的我发现了墙壁上的一样东西。是一张小纸条！用钉子固定在墙上的小纸条。这个发现太怪异了。

“你看见了什么！喂！你肯定看到了吧！快说话啊！”

尾崎叫着，他想握住日下的手，御手洗制止了他。

“南大门先生，外面的车子上有担架，麻烦你拿过来！”

“你说什么！凭什么要听你这种胡搅蛮缠的人指挥？疯子给我让开！别碍事，这里交给专家来处理。”

“本来就打算让专家来处理，我们这些疯子还是快退下吧。长田医生，麻烦您了。”

身穿白大褂的长田医生用手拨开众人，走进了房间。

“很危险，他现在还不能开口说话，请不要和他讲话。”专家说。

御手洗的判断很正确，担架一会儿就拿来了，接着他和长田两人小心翼翼地把日下抬上了担架。

日下几乎没流一滴血。当长田和阿南抬着担架往屋外走的时候，发生了难以置信的事。

“日下君！你不能死啊！”滨本英子哭着抱住了担架上的日下。她哭得如此伤悲，不知何时出现在她身边的户饲默默地注视着这一幕。

留在房间里的尾崎，慎重地取下了钉在墙上的小纸条，这无疑是凶手留下的信息。当然，他并没有马上就向我们公开纸条上的内容，事后我才看到，上面短短地写了如下几句话：

我要向滨本幸三郎复仇，你马上就会失去最宝贵的东西，那就是生命。

虽然刚才质问濒死者的行为有些失态，但现在尾崎已经恢复了他平日里作为刑警的冷静。放眼望去，十三号室房门紧锁，两扇窗户也是关着的，玻璃没有被卸过的痕迹。壁柜、衣橱、杂物柜以及床下、浴室都被很快地搜了一遍，没有人躲在里面，也没有任何异常的地方。

最值得一提的，就是在这起事件中，连那个二十厘米见方的通风口也被三合板死死地封上了，这是一个完全的密室。房门关紧时严丝合缝，没有一点空隙。

门被撞开时，第一个进入房间的是刑警，然后立刻有一大群人在旁围观，所以绝对不可能有人在破门进入房间的一瞬动手脚。也就是说，现在唯一能够依靠的，就是“日下到底看到了什么”这至关重要的一点。

大约一小时后，日下死亡的电报送到了众人聚集的大厅。凶手行凶的时间推断为下午三点过后，死因当然就是那把登山刀。

“户饲先生，你三点左右在哪里？”

牛越把户饲叫到了大厅的角落，用低沉的声音问他。

“我在外面散步，因为天气不错，我在思考一些问题。”

“有人能够证明吗？”

“这个……”

“是这样啊，那我就直说了，我们警方不能断定你没有杀害日下的动机。”

“这太过分了……我现在受到的打击可是比谁都要强烈啊。”

久美和英子都说自己待在房间里，这两人的供述十分简单。但接下来梶原春男的证词却让心灰意冷的刑警们重新萌生了希望。

“之前我一直认为那是不值一提的小事。不，和日下先生的死无关，是菊冈先生被杀的晚上，我靠在厨房门口的柱子上，那时候，我听见屋外暴风雪的声音里夹杂着‘咻咻’的声音，就好像是蛇爬过地面的响动。我能肯定。”

“蛇？”

刑警们吓得几乎要跳起来。

“那是几点左右？”

“嗯……我想大概是十一点吧！”

“正好是凶手杀人的时间。”

“还有别的人听到吗？”

“我问过康平先生他们，但他们说没有。我想大概是我幻听吧！这件事一直没说，十分抱歉！”

“关于那个声音，请再仔细描述一下。”

“嗯……除了‘咻咻’的声音，好像还有女人哭的声音……那声音很小，日下先生死的时候我没听到。”

“你说有女人哭的声音！”刑警们面面相觑，这根本就是怪谈！

“那上田一哉被杀的时候呢？”

“这没注意，对不起。”

![](/岛田庄司精选作品合集（全18册）/images/00039.jpeg)

“你的意思，只有菊冈先生被杀的时候你听到了？”

“是的。”

刑警们就这种怪声的问题逐一询问了其他的人。但除了梶原，他们都说从来没听到过这种声音。

“这到底是怎么回事？难道！真的是那个……”大熊对两名刑警说。

“我受不了了！我快要疯了，谁能告诉我到底是怎么回事！”

“我觉得我们已经尽力了。”

“这房子里有妖怪啊……或者这房子就是个吃人魂魄的妖孽？我真怀疑这房子是不是‘活的’，所有死者都在它的操控下被杀掉。尤其是日下，这绝对不是人类能够办到的，能够在那种环境下杀人的，只有这座房子本身。”

“要么，就是有一个我们绝对想不到的机关……比如利用机械移动房间，或者会弹出飞刀……或者房子会转……”尾崎说。

“真是这样的话，那凶手就不是受到招待的一方，而是招待客人的一方了……”

牛越自言自语道，大熊接着他的话说：

“但这不可能啊。就我看，这十一个人中最可疑的应该是相仓。为什么这么说呢？因为人偶会在窗户外面偷窥这种事实在是太荒唐了！绝对不可能！所以她就是在骗人。看她的样子也像是那种会说假话的女人。而且这三起谋杀案，她都没有不在场证明。”

“但是大熊先生，如果按照你的说法，就有一点令我不解了。那个叫久美的女人在二十九日之前应该从未看到过三号室内格雷姆的容貌，那为何她一开始供述的怪人相貌和那个人偶的长相一模一样？连细节都一致。”

尾崎说完后，大熊皱了皱眉头喃喃自语道：

“那么凶手一定不是我们每天都能碰到的人！那家伙一定躲在哪里，需要彻底地搜查，墙壁、天花板，全部掀开！尤其是十三、十四号室，除此之外，别无他法……你说是吧。牛越先生。”

“是啊，明天就是新年了，却毫无进展，凶手不会也放年假吧？我看也只能这样了。”

这时御手洗正好经过。

“哟，占卜大师，你不是说你来以后就不会再出现尸体吗？”大熊挖苦他道。

但御手洗没有理睬大熊的挑衅，他看起来也没什么精神。