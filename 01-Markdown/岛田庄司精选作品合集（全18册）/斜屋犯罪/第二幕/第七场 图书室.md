## 第七场　图书室

“这种混账案子我从来没遇到过！”大熊警部补愤愤地说道。

“到底搞什么鬼！死因是刀伤已经确认了吗？”

“是的，解剖的结果显示的确是刀伤引起的失血过多导致被害人的死亡。虽然毒理检验发现血液中有安眠药的成分，但那远远不到致死的剂量。”

“那会不会有什么机关？在这所房子里。”

“鉴证课的人已经大致搜过一遍十四号室了，和十号室一样，没发现什么暗门地道。”

“天花板呢？”

“天花板也一样，非常普通。如果把天花板和墙壁的隔板都撬开的话，说不定会有意外的发现。但现阶段还不需要这么彻底的搜查吧？我想能做的事还有很多。”

“你的意思就是没必要调查天花板吗！那你告诉我那根线是什么意思？刀子上系着的那根线！”大熊突然嚷道。

“这个家里除了金井夫妇，十一点前后这段时间里大家都有不在场证明，但金井夫妇又没有杀人动机。所以，一定要认定凶手是住在这里的人的话，这也太像小说的情节了。会不会是凶手玩了什么花样，十一点左右让刀子自动插进菊冈的后背。我是这样想的，你们怎么看？”

“嗯，只能说有这种可能性。但这样一来……”

“你也认同吧？既然这样，天花板就是最需要调查的地方了！因为有根线系在刀子上。用这根线吊着刀子，然后到了十一点左右，就自动落到了床上……”

“所以你认为天花板上肯定有玄机？但那真的是非常普通的天花板。我们拼命敲了半天也没发现可以打开的地方，更不要说什么自动机关之类的玩意儿了。”

“再说你这个假设也不能成立，有两个理由。第一是高度，那把刀插得非常深，只剩一截刀柄露在体外。如果只是从天花板掉落下来的话，不可能插得这么深。不，能不能刺伤他都不一定。从天花板掉下来，我看只能造成很小的伤口，和被蜜蜂蜇了一下差不多。而且刀子也会滚落到一边。”

“那么假设刀子从更高的地方掉下来又会如何？大熊君，别忘了睡在十四号室上面那个房间的人可是你啊！要让刀子插得那么深，起码需要两层楼的高度。这只是个假设，是否真的能够实现暂且不论。十四号室内的最高点在哪里？顶多是十四号室天花板的上方，以及楼上十二号室地板下方那个位置。就算刀子从那个地方掉下来，也无法造成如此之大的伤害。”

“嗯，你说的没错。”大熊说。

“第二个理由是毛毯。按照你的理论，刀子掉下来的时候，应该插在毛毯上，也就是胸口，而不是背后。”

“说不定他是趴着睡觉的。”

“是啊。”

“我知道我的想法站不住脚，但现在只能得出这样的结论。那就是，这个屋子里藏匿着一个我们从未见过的犯罪者。也只能这样想了！因为那十一个人都不是凶手。”

“但所有的房间我们都搜查过了，包括那些空房。总不会是那些客人把凶手藏起来了吧？”

“这个，也不是没有可能啊……”

“要不把他们召集起来，在全员在场的情况下再重新搜查一次？但是……”

“算了，我觉得这房子里或许还有一个可以藏人的暗室。以这个暗室为目标，再慎重搜查一次比较好。所谓机关，就应该是这样的东西。这本来就是座怪房子，有这样的房间也不奇怪啊。”

“我插一句……”尾崎说。

“如果按您的看法，这座房子的主人，滨本幸三郎和滨本英子就变成杀人帮凶了。从动机来看，他们父女两人应该是和日下、户饲一样，是一开始就被排除嫌疑的人。他们和上田一哉没有任何瓜葛，也没有杀害菊冈的动机。”

“根据我们调查上田时的资料显示，滨本幸三郎和菊冈荣吉并非旧友，也不是幼时玩伴。他们相识的时候，都有各自的事业了。而且他们是因为工作上的关系才开始支往，也就是菊冈轴承和滨氏柴油机两家企业的合作关系。”

“那也就是十四五年前的事。两人的关系也不是特别亲密，而他们的企业在业务上也没有产生特别危险的摩擦。幸三郎和菊冈见面的次数一双手都数得过来。菊冈受邀参加滨本的聚会是最近的事，也就是流冰馆建成之后开始的。所以，他们两人之间的关系不可能发展到萌生杀意的地步。”

“他们老家也不一样吧？”

“是的，滨本是东京人，菊冈则是关西人。他们身边的人也说，这两个人在事业合作前没什么往来。”

“英子就不用说了。”

“当然，英子和菊冈一共才见过两次面，就是今年夏天的一次和这次。”

“嗯。”

“两次聚会中都出现的，还有日下、户饲、滨本嘉彦以及梶原春男。他们也只和菊冈见过两次。从常理判断，应该也没有萌生杀意的时间。”

“嗯，从常识、动机来判断，刚才那几个人都可以排除了，你们看呢？”

“从动机判断的确如此。”

“我们办过的案子里，除了那些精神异常者，不存在没有动机的杀人行为。”

“是啊。”

“所谓动机，不就是怨恨、金钱、妒忌、仇杀、情死……唉，尽是些可怕的事。”

“刚才没有提到的人里面，除了秘书小姐和金井夫妇我能理解。为什么连早川夫妇也没有排除嫌疑呢？这里有什么内因吗？”

“有一个我们昨天还不了解的重要线索。根据今天才得到的调查报告显示，其实早川夫妇有个二十岁的女儿。这个女儿在去年夏天，因为来这里避暑和菊冈有过接触。”

“哦！”牛越和大熊似乎发现了新大陆。

“听说那姑娘长得雪白粉嫩，一张脸蛋儿也很漂亮，是讨男人喜欢的那种类型。我这里没照片，如果有需要的话可以找早川夫妇要。”

“嗯，然后？”

“听说那姑娘在东京台东区浅草桥附近一家叫‘卑弥呼’的小吃店里上班。今年八月的时候来这里避暑，然后就认识了菊冈。菊冈那种人，碰到漂亮姑娘怎么会没想法呢？凡是认识他的人都知道他这个‘爱好’。”

“说起来，菊冈是单身吗？”

“不是，他有老婆，大儿子都读高中了，还有个女儿在读中学。”

“哼，还挺美满。”

“菊冈这个人，表面上装得光明磊落，背地里阴险得很哪！如果公司里有人对他不义，他表面上笑笑不当回事，其实睚眦必报。就是这种口蜜腹剑的家伙。”

“上个班真是辛苦……”

“早川良江，也就是早川夫妇的女儿，也碰到了这种情况。因为顾及早川夫妇，所以在流冰馆的时候菊冈完全没有显露出来。但一回到东京，他就频频光顾‘卑弥呼’。”

“‘卑弥呼’的顾客大多是年轻人，是家中低档次的小店，服务生就老板娘和良江两个人。菊冈轴承的大老板每天都来光顾，自然一下就被收买了。”

“有钱有地位的色老头的危害仅次于恶德警察。”

“那家伙只要是为女人花钱，就绝不手软。”

“还真是一掷千金啊。”

“人家有钱嘛，有什么办法。”

“反正他是扔了不少钱，和良江的关系也维持了一段时间。但后来菊冈就突然人间蒸发了。”

“嗯？”

“据‘卑弥呼’的老板娘说，菊冈本来答应给良江买公寓和跑车，但却突然把她甩了，让良江很不甘心。”

“原来是这样啊……”

“老板娘也觉得良江成天想着要靠男人养很没用。甩了就被甩了，还老是打电话给菊冈。反正菊冈从来也不接，就算找到菊冈本人，他也否认自己说过要给她买房之类的话。”

“后来呢？”

“就自杀了。”

“啊！死了吗？”

“不，好像没死，吃了安眠药，不过马上被送去洗胃。可能只是要吓吓菊冈吧？据老板娘说，良江这么不甘心的理由，是因为在自己面前夸下海口，结果被甩了，觉得很没面子。”

“哼，我看他们是一路货色。那后来怎么样了？”

“她身体康复后，就一直在外面混，上个月初出车祸死了。”

“这次真死了吗？”

“是的，这起车祸和菊冈完全没有关系，只是单纯的交通事故。不过早川夫妇或许不这么看，他们认为是菊冈杀死良江的。”

“这么想也很正常……毕竟是独生女啊……那滨本幸三郎知道这件事吗？”

“大概不知道，可能只知道早川夫妇有个独生女因为交通事故死了吧？”

“原来如此！玩女人也应该有点分寸。那菊冈还有脸来见早川夫妇？”

“这可是滨氏柴油机的会长亲自邀请，怎么可以拒绝？”

“还真是可怜，我算是明白了，早川夫妇有杀害菊冈的动机，怪不得早川康平什么都不肯说。但是上田呢？”

“这点我也觉得很奇怪，早川夫妇绝对没有杀死上田的动机。他们和上田接触的机会只有在这个家里的两次而已。”

“嗯，有杀死菊冈的动机，却没有杀死上田的动机，还真是奇怪……再加上他们又有完美的不在场证明……”

“先不管他们了，接下来讨论金井夫妇有没有杀死菊冈的动机。”

“有的！说起来有点像八卦新闻……”

“哦？”

“金井道男在公司里的确属于菊冈派，这是毋庸置疑的事实。那家伙当了十几年的跟班，无论刮风下雨都在菊冈身边伺候着，总算熬到了今天的地位。这些都和他刚才那么激动时说的一样。不过关键点不在他身上，而是他的老婆初江。”

“他老婆……”尾崎想要吊吊众人的胃口，慢慢地点燃了一支香烟。

“初江和金井的媒人就是菊冈。那是二十多年前的事了，据说初江原本是菊冈的情人。”

“不会吧！”

“真是本性难移啊！”

“倒不如说是天性好色。”

“真是甘拜下风。那么，金井知道这件事吗？”

“怎么说呢，这就很微妙了，可能是表面上装作不知道，其实已经心知肚明吧！”

“嗯……不过就算金井真的察觉了，会为了这种事情杀人吗？”

“我觉得不会！因为失去了菊冈这座靠山，金井在公司内的职务就形同虚设。只有菊冈在，金井才能出头，我想他这点自觉还是有的。再说，即使发现了又怎么样？都已经是陈年往事了，按照金井的性格，他不会那么不顾一切地想要杀死老婆的旧情人解恨，倒是或许会用这件事来要挟菊冈才是上策，他不会做那种鸡飞蛋打的蠢事。”

“还有种可能，他的确非常想要杀死菊冈，出出这口已经在心里堆积了很久的怨气。但这样做之前他应该为自己找好后路。比如改投别派啊，或者和别的大人物搞好关系。但根据我们的调查，他丝毫没有这样做的迹象。”

“还真是忠心耿耿啊。”

“是啊。”

“原来如此。”

“从利害关系上来看，金井应该没有非要杀菊冈的动机。这是我的看法。”

“那他老婆呢？”

“他老婆啊。你认为她有那个能力吗？”

“金井和上田关系怎样？”

“根据以前的调查，他们也不是很熟。总之，金井完全没有杀死上田的动机。”

“唉，真是让人绝望的调查啊。”

“接下来是相仓久美。”

“她和菊冈的关系在公司里已经半公开化了吧？但久美也是靠着菊冈才能有现在的生活……杀了他，恐怕就失去了一棵摇钱树。即便真的有不得已的理由要杀他，我想绞杀比较现实一些，而且也应该是在菊冈准备和自己分手的时候动手。我看，现在他们两个简直是如胶似漆，菊冈对这个丫头很着迷呢！”

“菊冈和良江交往的时候，难道脚踏两条船？”

“正是如此。”

“佩服！佩服！”

“亏他有那个体力。”

“我们做个假设，久美会不会是为了杀死菊冈，才作为秘书接近他的。”

“我看没那个可能！久美是秋田县人，小时候和父母一直住在秋田，而且菊冈也从未去过秋田。”

“嗯，了解了。也就是说，只有早川夫妇有杀害菊冈的动机。但上田的案子，却谁都没有动机。而且这次的犯罪现场是个‘密室’——光听见这两个字我就头大。大熊先生，您对此有什么意见要发表？”

“这真是闻所未闻的混账事件！在一个从外部绝对打不开的密室里，一个色老头被人从背后用刀刺死。有动机的人一个也找不到，好不容易找到一个，那人居然在死亡时间和巡警一起待在大厅里！”

“我现在想做的事情只有一件！就是拿把锤子、斧子，还是别的什么把十四号室的墙壁天花板都给砸开！就是一个老鼠洞也别想逃过本大爷的法眼！”

“我看那个暖炉最可疑！那后面肯定有个密道！密道的尽头说不定有个能够藏人的房间！那里就藏着第十二个人。或许是个小矮人……不！我可没开玩笑，小矮人的话不就能躲藏在狭小的地方，并且爬过细长的密道吗？”

“那暖炉只不过是个装饰，并不能真的点火，里面放了一个用煤气的取暖器，所以也没有烟囱。我敲了半天，连接缝也仔细检查过了，没有发现什么暗门。”

“那么牛越先生，您的看法是？”

“嗯……尾崎君，你怎么想？”

“我觉得要从逻辑关系上去找原因。”

“我也有同感！”

“两起事件，两个密室，也就是说凶手在杀人之前就已经想好了两个密室的诡计。所以他在杀人时只要按照计划一步步实行就可以了。十号室中，被害者上田的手腕上不知为何缠着一条绳子，地上绑着铅球的线也被加长了。而这次的十四号室中，沙发和茶几翻倒在地，让我们认为是凶手和菊冈搏斗造成的，现场的确留下了有人入室的痕迹。所以这两间密室，都是在杀人之后，刻意布置出来的。我认为应该这样想才对！”

“嗯……或许的确如您所说的那样。”

“但这两个房间，尤其是十四号室，上下各有一个门闩，再加上一个弹簧锁，一共三道锁，都锁得好好的。如果门的四周有缝隙的话还好说，可是那十四号室简直就是铜墙铁壁，上下左右都找不出一条缝来。门框和门接合得也是浑然天成，根本找不到能够做手脚的地方。墙的高处有个二十厘米见方的通风口，如果要通过这里操作机关，只能使用丝线了。但我检查了门附近的地板，没有发现任何可疑的痕迹。没有掉落的图钉，或者新生的划痕、孔眼之类的东西。”

“嗯……”

“难道翻倒的沙发和茶几是密室诡计必需的道具？”

“鬼才知道！杀人现场为什么必须是密室？我看这也是个问题。尸体背后中刀是他杀所致，傻子都看得出来！布置成密室的目的难道是要我们将菊冈的死亡当作自杀？这未免也太小看警察了吧！”

“呵呵。我们暂且将沙发和茶几当作密室必需的道具，假设有一种方法，是利用这两件翻倒的家具，在上面布置用丝线控制的机关，可以隔着门上锁。那必定需要十分结实的线。而且这条线可以通过通风口回收。牛越先生，您刚才说昨晚敲过十四号室的门？”

“敲门的是滨本先生。”

“大概是几点？”

“十点半吧！”

“那时通风口附近有丝线之类的东西垂挂着吗？”

“没有。因为敲门没有回应，我下意识地看了一眼通风口，那里什么也没有。”

“嗯……那时菊冈应该还活着，只不过睡熟了。不过，大概三十分钟后，他人就魂归西天了——这种色鬼能成佛吗？应该下地狱才对——十一点三十分左右，三个用人经过门口，他们也没注意看通风口，不过从常识判断，那时机关线已经收走了。”

“那个通风口很高，即使踏在床头柜上也看不清里面的情形。如果凶手没有使用垫脚台的话，那根机关线肯定非常长，挂在通风口下很显眼。如果有人通过，即便不是靠得很近，也应该很容易发觉。凶手必定想到了这点。”

“也就是说，凶手必须在十一点十分之前就处理完。这样一来，只有十分钟时间。”

“是的，昨晚用人们十一点三十分才下楼只不过是偶然，这点凶手没有料到，因为平时他们回房休息的时间还要早一些。如果拖拖拉拉的，说不定会被他们撞个正着，那么这个计划就彻底失败了。换了我是凶手，或许会更早下手，毕竟拖得越晚，越有可能被下楼的用人们发现。”

“嗯，所以我们去敲门的时候，他早就处理完毕了，这也说得通。”

“是的。”

“但是，按照这个计划来看，从表面上就可以决定凶手的身份了。凶手计划决定在十一点行动，这是改变不了的，当时能够避开众人的视线，单独访问十四号室的人，只有九号室的客人！”

“嗯，话是没错……但十一点这个时间就让人难以理解了。而且这个计划本身在时间上要掐算得很准，风险太大。你们觉得呢？”

“换了我是不会这么干的，我一开始就不会想要杀人。”

“还有别的方法，也必须仔细考虑一下。”

“什么？”

“如果那个一到十一点就能把刀子自动插入菊冈后背的机关真的存在，并且运行良好的话。凶手完全不用顾及阿南在场，说不定还泰然自若地和他打打球喝喝酒呢。”

“这点我也想到了！”大熊嚷道。

“这比用细线制造出密室更难实现。首先，凶手必须潜入十四号室布置机关，但他根本进不了房间。”

“再加上十四号室的格局和普通房间没太大区别，周围也没有什么动力设备或者利于穿针引线的支架。角落里的书桌整理得很干净，上面放着墨水瓶、笔、镇纸，书架也很整齐。根据滨本的回忆，书架上的书都没被翻动过。暖炉右边的墙上有个固定的衣柜，里面一切正常，柜门也是关紧的。”

“如果要说有奇怪的地方，那就是这个房间里的椅子特别多。角落里的书桌和配套的椅子没被动过，规规矩矩地靠在一起，暖炉前的摇椅也在原来的位置上。再加上两张似乎是给客人坐的椅子和翻倒的沙发，一共是五张椅子。还有那张床，也能够当作椅子来用。难道凶手用这些椅子布置了一个迷魂阵？对了，那两张给客人坐的椅子也没动过。”

“对了，我还想起件更要命的事，这个房间除了菊冈以外，根本没人能够进入。因为钥匙只有一把，而且没有备用的。所以一旦从内部锁死了，外面的人想进来只有用斧头劈了……不知道是原本就只做了一把，还是后来搞丢了备用钥匙。大概滨本考虑到自己书房里都是贵重文件，所以只做了一把钥匙吧。这唯一的一把钥匙就在菊冈的身上，今天早上在他脱下的外套口袋里找到了。”

“如果不小心把钥匙忘在了房间里，又按下弹簧锁关上了门，不就麻烦了吗？”

“不，没关系，听说门开着的状态下，即使按下了弹簧锁再关门的话，门是不会锁上的，这时锁会自动解除。所以外部的人是无法在没有钥匙的情况下将门锁死的。”

“原来是这样啊，还真先进……”

“菊冈只要走出房间，就会用钥匙将门锁好。他好像把钱放在房间里。这事不仅当用人的早川夫妇知道，很多人都可以作证。”

“这样看来，的确没人能够进入他的房间了。”

“是的，其他的房间空的时候，两把钥匙都由早川夫妇管理。如果有客人入住，则把备用钥匙交给英子保管，就是这样一套管理系统。十四号室的情况特殊，所以才会让贵客入住吧！”

“真够麻烦的！”

“当着大厅里那些人的面我不好讲，就我本人而言，对于这个案子，我很想高举双手投降。大熊先生已经说过了，根本没有凶手，那十一个人都不是凶手！”

“唉……”

“想不到又发生了命案，上田的案子已经让我一筹莫展，一堆搞不清的问题摆在那里。首先是没有足迹的问题，密室方面，十号室的门锁没有十四号室的牢固，凶手或许有机可乘。但主要的问题还是那些雪，十号室门口、阶梯上甚至是主屋附近的雪都积得很厚。如果发现异常的日下和主客们都没有说谎的话，在昨天那些家伙乱踩之前，雪面应该是‘初雪’的状态，雪地上没有其他人的足迹，实在很难想象凶手是怎样到达十号室的。”

“还有日下在晚上看到的那两根木棒，以及那个被拆得七零八落的脏兮兮的人偶‘格雷姆’……对了，牛越先生，上田是二十五日深夜被杀的，你不是说要去确认一下那个人偶在二十五日的白天是不是也放在隔壁的三号室里吗？怎么样？”

“在的，二十五日的白天，滨本先生确定那个人偶就放在三号室里。”

“是吗，看来凶手是杀人之前才把它拿出来的……等等！慎重起见，我还是再去看看那个人偶。”

人偶已经被放回天狗的房间，尾崎走出图书室。

“所以说，我觉得凶手并不是从外部进入十号室的。也就是说，不是通过门进来的。那个房间的通风口是朝向主屋的内侧吧？说不定是在通风口上动了手脚。”大熊又提出了自己的看法。

“但那个通风口开在离地面很高的墙壁上……”

“难道他挖了个洞？不，或者是类似的机关？”

“牛越先生！”尾崎回来了。

“那个人偶的右手绑着线！”

“什么？”

“您快去看看吧！”

三人争先恐后地跑出图书室来到了天狗屋。格雷姆耷拉着两条腿坐在窗边，而它的右手腕上果真系着一根白线。

“这是谁的恶作剧？把我们警察当傻瓜耍吗！”牛越说。

“是凶手干的吧？”

“废话！鉴证课的人早就检查过人偶，怎么会没发现这么重要的证据，这是成心在耍我们！”三人怏怏不乐地回到了图书室。

“再回到刚才说的足迹问题。如果说消失的足迹是凶手使用了某种诡计造成的，我认为他完全没有必要那么做。因为这次的菊冈被杀事件，已经使我们警方可以确定凶手就藏匿在这座房子里了。如果凶手最初的目标就是菊冈，那他在杀害上田的时候就完全没必要隐藏足迹。”

“是吗……算了，你继续说。”

“所以没有足迹恰恰能说明凶手是从屋内进入十号室的！”

“刚才我就是这个意思！”大熊大叫道。

“那个人偶怎么解释？难道是它自己飞出去的？我不同意你的看法，即便我们事后判断凶手一定是屋内的人，也可以通过足迹发现很多意想不到的线索。首先，可以知道凶手穿的是男鞋还是女鞋，从步伐大小能够轻易判断出身高和性别。假如步伐像女人却是男鞋的足印，那能够拿到男鞋的女人最可疑。所以我认为，凶手消除足迹是怕暴露身份。”

正说到这里，外面突然有人敲门。

“请进！”

对案情毫无进展而感到头疼的刑警们一齐喊道。门被小心翼翼地打开了，早川康平弯着腰站在那里。

“各位，午餐已经准备好了……”

“啊！都给忘了，多谢！”

早川刚要关门离去，牛越开门见山地问道：

“早川先生，菊冈死了，你解恨吗？”

早川的脸色变得像死人一样难看，他睁大了双眼，一只手紧紧地握住门把。

“什么？你的话是什么意思，难道你认为是我干的？”

“早川，你可别小看了警察，你女儿良江的事我们都知道了。你去东京参加了她的葬礼吧？”

早川无力地垂下双肩。

“请坐下，我们有些事想问你。”

“不……我无话可说……”

“叫你坐下听到没有！”尾崎吼道。

早川慢吞吞地走到三人面前，拉开椅子。

“上次问你话的时候，你也是坐在这里，不过你没说实话，我们就不追究了。不过这次！如果你还打算隐瞒的话，别怪我没提醒过你！”

“警察先生，我不会再隐瞒了，其实上次我就打算说的，但那时死的不是菊冈，而是上田先生，我觉得说不说都无所谓。”

“那今天呢？菊冈不是已经死了吗？”

“警察先生，您还在怀疑我？我怎么去杀他啊？的确！良江死的时候我恨他恨得不得了，我老婆也是，毕竟我们就这么一个独生女。”

“我恨他这点我不否认，但也没想过要杀死他。就算我真的想，也不可能办到。我一直待在大厅里，而且根本进不了他的房间。”

牛越一直盯着早川的眼睛，仿佛要从瞳仁窥视他脑内真实的想法。早川说完后，众人沉默了片刻。

“早川，菊冈先生还在大厅的时候，你没有偷偷地溜进十四号室吧？”

“没有！小姐吩咐过，有客人留宿的时候，绝不能擅自进入客人的房间。再说我也没钥匙，根本进不去。”

“嗯，还有个问题，外面那间仓库，就是早上梶原去拿斧头和人字梯的小屋，有没有上锁？”

“是上锁的。”

“但今天早上我没看见他拿着钥匙。”

“那间仓库的锁是数字锁，只要输入正确的数字就可以打开。”

“是那种有数字转轮的挂锁吗？”

“是的。”

“那个密码，大家都知道吗？”

“只要是这个家里的人都知道，需要我告诉你吗？”

“不用了，需要的话会问你的。你刚才说‘这个家里的人’也就是除了客人之外，只有滨本先生、英子小姐、梶原先生还有你和你的妻子知道？”

“是的。”

“其他人不知道吗？”

“是的。”

“好的。可以了，你先走吧，通知大家我们三十分钟后下去。”

早川的表情像是松了一口气，立刻站了起来。

“那老头绝对有办法杀死上田一哉！”门一关上，尾崎就说。

“呵呵，不过没有动机却是致命弱点。”牛越笑着说道。

“其实他的条件很充分，比如夫妇合谋，那样更容易下手。而且作为管家，或许比主人更熟悉这房子的一草一木，也更容易利用各种工具或者地形。”

“也不能说完全没有动机吧！比如他一开始就打算杀菊冈，但上田这个保镖却很碍眼，于是在杀死菊冈之前要先把他干掉。”

“你这么说也太牵强了。照你的说法，杀死上田的晚上，也是杀死菊冈的良机。毕竟他只是一个司机，又不是保镖，而且住在只能从室外进入的房间里，根本无法进入主屋保护菊冈。这是杀死菊冈再好不过的机会了，他可以毫不犹豫地下手。再说上田又年轻，还加入过自卫队，而菊冈又老又肥，但块头很大，早川打不打得过他还不一定。你说早川有那个必要去特意杀死上田吗？”

“可能上田知道早川良江的事，如果不杀人灭口的话，恐怕对日后不利。”

“也不排除这个可能，但如果是为了这个原因，早川更应该担心的是金井和久美。菊冈未必会把良江的事情告诉上田。之前也调查过了，他们的关系很一般。”

“总之，如果真是早川夫妇做的，那为何要将十四号室布置成密室？先不管密室，在死亡推定时间内，他们二人的确是坐在大厅里，这点不需要再调查了。我看我们还是先把动机问题暂且搁在一边，从物理条件上去寻找具有嫌疑的凶手吧！”

“唉……只能如此了……”

“那样的话，金井夫妇，甚至久美和英子都值得怀疑。”

“英子？”

“说了先不管动机。”

“但他们是怎么杀死菊冈的呢？先不管犯人是谁，牛越先生你考虑过行凶的手段吗？”

“这个，我倒是想到了一个办法。”

“哦？洗耳恭听！”尾崎一脸严肃，大熊倒是半信半疑地盯着牛越。

“我认为门和锁都十分完整，没有被动过手脚，用线来操作上下的两道门闩，以及中间的弹簧锁，那根本是办不到的。”

“您的意思是，上锁的人只能是死者自己？”

“是的！那个房间位处地下，没有窗户，门又被锁着，唯一和外界相通的地方，只有那个通风口。”

“那个二十厘米见方的通风口？”

“还有别的通风口吗？就是那个！所以凶手也只能从那个地方下刀将被害人刺死！”

“怎么刺？”

“那个通风口开在床铺的正上方，所以只要把刀子绑到木棒上，做成类似长矛的工具，然后伸进室内，就这么刺下去！”牛越说着还比划了一下。

“哈哈哈！但那样的木棒至少要两米长，携带肯定不方便吧？会顶住走廊的。放在房间里也很惹眼，恐怕光带进这座房子就很麻烦了。”

“所以我在想，是不是一种可以伸缩或者折叠的木棒，比如‘钓竿’。”

“哈哈，原来如此。”

“如果是钓竿的话，即便伸进房间里也可以调整长度。”牛越很得意地说道。

“我有个疑问，刺中之后那把刀能够顺利地留在尸体上吗？刀子应该绑得很紧吧？”

“这点我也没想通，或许和刀柄上的那条线有关，但具体怎么实施我就不知道了。到时候直接问凶手吧！一定能捉住他！”

“那十号室也是用这个方法？”

“不，那我就不知道了。”

“但走廊上没有任何可以垫脚的东西，凶手是怎么操作那根钓竿的呢？早上我们搬来了床头柜，但还是不够高。普通的茶几就更低了，根本够不到那个通风口。对了，每个房间的床头柜都是同样规格的，高度也一样。”

“会不会是……把两个叠起来？”

“在这种倾斜的地板上？在普通的房子里倒可以考虑，再说床头柜每个房间只有一个，就算凶手搞到了能叠起来的东西，能在上面站稳都很困难。摇摇晃晃的，一个不小心非摔得骨折不可。”

“如果凶手是两个人，其中一个人就可以骑在另一个人的肩膀上。反正方法多得是，刚才我问早川那个仓库锁的问题，就是想到仓库里那张人字梯。”

“但这座房子向外开的出口只有三个，无论哪一个都与大厅相连。想要出入这所房子的话，必定会被大厅里的人看见。如果只是想出去，可以从一号房楼梯拐角处的窗户爬出去，但那样的话就进不来了。即便从那扇窗户爬进屋内，想要去十四号室，仍然会经过大厅。”

“你这样说，让我不禁怀疑大厅里的那些人是不是也和凶手串通好了……”

“别忘了阿南巡警也是其中之一啊。”

“唉，我看就算去问他们，他们大概也会这么回答——根本没看见一个像油漆工一样的人跨着人字梯经过大厅。”

这时，牛越的头顶仿佛亮起了一只灯泡，他想到了一件事。等等！还有一种方法不是吗？也就是住在一楼的人可以通过房间的窗户自由出入，那也就是指日下和户饲，但当菊冈遇害的时候，这两人正在大厅。英子和久美不在，这两人如果从东侧楼梯拐角处的窗户爬出室外的话……

“或许凶器是特制的枪，你们认为呢？”

牛越的思绪被大熊的发言打断了。

“用弹簧或者皮筋做动力，把刀子当作弩箭一样弹射出去的枪，那条线或许就是必要的装置……”

“但通风口高度的问题依然存在，再说十四号室内翻倒的沙发和茶几，显然是搏斗造成的，凶手也一定进入过十号室。我们不能忽视这点。”尾崎补充道。

牛越看看手表继续说：

“我看先别管这些了，还是叫人把他们的房间再搜一遍，我认为有这个必要。尤其是金井夫妇、英子、久美这三组要重点搜查。找找有没有伸缩钓竿、两米以上的棒子以及特制改造过的枪，还有能够折叠登高用的台子，就是这些东西。”

“我们还没有上头的许可，所以搜查要先经本人同意。我看那几个学生一定会让我们看的。有这几个人开了头，最后一定能全都搞定！警队还在待命吗？让他们和大厅的阿南分头行动，最好能同时进行。空房间也别放过，凶手可能把用过的道具从窗户扔了出去，所以房子周围的雪地，凡是抛物范围之内都要仔细搜查！还有暖炉！凶手有可能销毁证据，让他们仔细翻翻。”

“时间也不早了，我们还是下楼吧！用餐结束的时候我会告诉大家的，请他们配合。毕竟都是上流社会人士，不郑重点不行！”

吃完饭后，牛越和大熊都一声不吭地坐在图书室里。他们茫然地注视着西沉的斜阳，心中突然有种不好的预感。大概明天或者后天，自己仍然会坐在这里欣赏同样的景色。想到这里，两人都默不作声。

牛越发觉门被打开了，但他没有回头。在自己的名字被叫到之前，他不想回头，因为他对搜查结果的期望很大。

“结果呢？”他避开尾崎的脸问道。

“所有人，所有房间，都仔细检查过了。因为没有女警，搞不好会被那群女人起诉。”尾崎说法的方式有点啰嗦。

“啊……结果到底怎么样？”

“什么都没有发现，来客中没有人带鱼竿，房子里也没有。也没有很长的棒子，顶多只有台球杆，更不用说改造成枪之类的东西了，根本没有。”

“暖炉里除了木柴，没有烧过其他东西的痕迹。房子的周围我们也特别仔细地搜索过了，搜查的范围之大，恐怕连专业的标枪选手也扔不了那么远，但仍然是一无所获。”

“还有垫脚台也是，梶原的房间，早川的房间以及十四号室里都找过了……没有。前两个房间的装修没有十四号室那么豪华，但都有书桌。书桌是个庞然大物，没那么方便搬来搬去的，而且每个房间的书桌高度都差不多，顶多高二十厘米。”

“还有长棒，我们本来以为十号室里会有标枪，结果去看了没有发现，只找到滑雪板和滑雪杖。那个仓库里有锄头、铁锹、短柄铲、扫帚之类的东西，但如果想拿进主屋的话，和人字梯一样很容易被发觉，总是能找的地方都找过了。”

“唉，我早料到会这样。”牛越长长地叹了一口气。

“还有什么好可能性吗？”

“其实，后来我想了很多。”

“哦！有什么新的想法吗？”

“比如将绳子泡水结冰后，也可以当作长棒来使用。”

“了不起！然后呢？”

“但没有人带绳子，仓库里倒是有几条。”

“是吗？但我认为这是个关键。这个家里长的东西都是我们平常看到的，或许凶手就是利用了我们的盲点，制造了一种稍加改造就能变成长棒的工具。我想这座房子里一定有这样的东西……隔壁的房间也没有吗？”

“已经仔细搜过了，没有棒子……”

“一定藏在某个地方！不然这个室外作案的假设就说不通了……拆卸下来就可以变成长棒的东西……楼梯的扶手拆不下来……或许是用暖炉的木柴，捡几根用线绑起来，不就变长了吗？不……还是不可能，妈的！隔壁真的什么都没有吗？”

“真的没有！不如您亲自去看看。”

“好！”

“不过隔壁那个人偶，就是那个叫‘格雷姆’的家伙。它的手好像原本握着什么东西，我就把刀子放到它的手上试了一下，看看能不能握住。”

“哈……你的想象力到真丰富，好奇心也很强啊，结果如何？”

“正合适！就好像小孩含着奶嘴一样！”

“我看你满脑子都是稀奇古怪的念头，那肯定只是个巧合。”

“说得也是。”

“经过这次调查，我们之前的很多假设都被推翻了。目前能够确定的，只有九号室的金井夫妇没有不在场证明这一点。不过哪怕只有这一点，大家也不要太悲观。”

牛越的这番话听上去是在给众人鼓气，但更像是自我安慰。三个人又陷入了沉默。

“尾崎君，你好像有话要说？”

“实际上……牛越先生，有一件事我没告诉你。”尾崎扭扭捏捏地答道。

“什么？”

“这真有些难以开口，昨晚回房后，我一直心神不宁。仔细回想才发现，除了我和大熊先生，当时回房休息的只有菊冈和金井夫妇。我怀疑他们会不会走出房间去密谋什么，于是就在他们的房间弹簧锁的下方，房门和墙壁的中间，用发胶黏上了一根头发。如果他们开过门，头发就会掉下来。我们就可以判断他们回房后，是否又出来过。不过这样做有点偷偷摸摸的，不像警察所为，所以就一直没说……”

“怎么会呢！你小子真有鬼主意！那除了菊冈和金井的房间外，其他房间你也这样做了吗？”

“出门必须经过大厅的房间我就没弄。我只在那些别人看不到的房间门口黏上头发。西边的那些人，比如日下、户饲，还有用人们，我本想等他们回房后再去的。但他们弄得很晚，结果我先睡着了。”

“那你是在几点去黏头发的？”

“就在我和牛越先生说要回房间以后，大概是十点十五分或者十点二十分左右吧。”

“嗯，后来呢？”

“我半夜醒来过一次，就去这两个房间检查。”

“哦！结果呢？”

“菊冈房间的头发不见了，说明门开过。但金井夫妇的房间……”

“怎么样！”

“还在上面。”

“什么！”

“也就是说门没开过。”

牛越低着头，紧咬着下唇说道：“你这家伙！让人白高兴一场！这下子真的束手无策了！”