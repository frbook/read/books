---
title: 故事
author: 罗伯特∙麦基
publisher: 
status: false
tags:
  - book
cover: 故事/images/cover.jpeg

---
- [[故事/part0000|part0000]]
- [[故事/part0001|part0001]]
- [[故事/part0002|part0002]]
- [[故事/致中国读者|致中国读者]]
- [[故事/序言|序言]]
- [[故事/PART Ⅰ 作家和故事艺术/PART Ⅰ 作家和故事艺术|PART Ⅰ 作家和故事艺术]]
	- [[故事/PART Ⅰ 作家和故事艺术/CHAPTER 01 故事问题|CHAPTER 01 故事问题]]
- [[故事/PART Ⅱ 故事诸要素/PART Ⅱ 故事诸要素|PART Ⅱ 故事诸要素]]
	- [[故事/PART Ⅱ 故事诸要素/CHAPTER 02 结构图谱|CHAPTER 02 结构图谱]]
	- [[故事/PART Ⅱ 故事诸要素/CHAPTER 03 结构与背景|CHAPTER 03 结构与背景]]
	- [[故事/PART Ⅱ 故事诸要素/CHAPTER 04 结构与类型|CHAPTER 04 结构与类型]]
	- [[故事/PART Ⅱ 故事诸要素/CHAPTER 05 结构与人物|CHAPTER 05 结构与人物]]
	- [[故事/PART Ⅱ 故事诸要素/CHAPTER 06 结构与意义|CHAPTER 06 结构与意义]]
- [[故事/PART Ⅲ 故事设计原理/PART Ⅲ 故事设计原理|PART Ⅲ 故事设计原理]]
	- [[故事/PART Ⅲ 故事设计原理/CHAPTER 07 故事材质|CHAPTER 07 故事材质]]
	- [[故事/PART Ⅲ 故事设计原理/CHAPTER 08 激励事件|CHAPTER 08 激励事件]]
	- [[故事/PART Ⅲ 故事设计原理/CHAPTER 09 幕设计|CHAPTER 09 幕设计]]
	- [[故事/PART Ⅲ 故事设计原理/CHAPTER 10 场景设计|CHAPTER 10 场景设计]]
	- [[故事/PART Ⅲ 故事设计原理/CHAPTER 11 场景分析|CHAPTER 11 场景分析]]
	- [[故事/PART Ⅲ 故事设计原理/CHAPTER 12 布局谋篇|CHAPTER 12 布局谋篇]]
	- [[故事/PART Ⅲ 故事设计原理/CHAPTER 13 危机、高潮和结局|CHAPTER 13 危机、高潮和结局]]
- [[故事/PART Ⅵ 作家在工作/PART Ⅵ 作家在工作|PART Ⅵ 作家在工作]]
	- [[故事/PART Ⅵ 作家在工作/CHAPTER 14 反面人物塑造原理|CHAPTER 14 反面人物塑造原理]]
	- [[故事/PART Ⅵ 作家在工作/CHAPTER 15 解说|CHAPTER 15 解说]]
	- [[故事/PART Ⅵ 作家在工作/CHAPTER 16 问题和解决方法|CHAPTER 16 问题和解决方法]]
	- [[故事/PART Ⅵ 作家在工作/CHAPTER 17 人物|CHAPTER 17 人物]]
	- [[故事/PART Ⅵ 作家在工作/CHAPTER 18 文本|CHAPTER 18 文本]]
	- [[故事/PART Ⅵ 作家在工作/CHAPTER 19 作家的创造方法|CHAPTER 19 作家的创造方法]]
- [[故事/淡出|淡出]]
- [[故事/附录：文中涉及影片列表|附录：文中涉及影片列表]]
