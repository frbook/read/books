——## CHAPTER 01 / 故事问题

故事的衰竭 / 手艺的失传 / 故事要领 /

把好故事讲好 / 故事与生活 / 能力与天才 /

手艺能将天才推向极致 /

   

### ◎ 故事的衰竭

人类对故事的胃口是不可餍足的。设想在地球上的普通一日，有多少故事在以各种形式传送着：翻阅的散文书页、表演的戏剧、放映的电影、源源不断的电视喜剧和正剧、二十四小时的报刊和广播新闻、孩子们的睡前故事、酒吧吹牛、网上闲聊。故事不仅是人类最多产的艺术形式，而且和人类的一切活动——工作、玩乐、吃饭、锻炼—— 争夺着人们每一刻醒着的时间。我们讲述和倾听故事的时间可以和睡觉的时间相提并论—— 即使在睡着以后我们还会做梦。为什么？我们人生如此之多的时间为什么会在故事中度过？因为，正如评论家肯尼斯·伯克所言，故事是人生必需的设备。

日复一日，我们寻求亚里士多德在《伦理学》中提出的那一古老问题的答案：一个人应该如何度过他的一生？但，问题的答案总是在规避着我们。当我们力图使我们的手段合乎我们的梦想时，当我们力图将我们的思想融入我们的激情时，当我们力图让我们的欲望变成现实时，那一问题的答案始终躲藏在飞速流逝、难以捉摸的时间后面。我们犹如乘坐一艘飞船，险象环生地穿行在时间隧道之中。如果我们想让飞船减速，以便捕捉人生的模式和意义，人生就会像一个格式塔[^1]一样扑朔迷离：时而严肃，时而滑稽；时而静止，时而狂乱；时而意味深长，时而索然寡趣。重大的世界事务完全在我们的掌控之外，而个人事务又往往钳制着我们，尽管我们无不努力用双手牢牢掌握着自己的方向盘。

传统上，人类一直基于四大学问——哲学、科学、宗教、艺术——来寻求亚里士多德问题的答案，试图从每一门学问中得到启迪，从而编织出一种人生意义。但如今，如果不是为了应付考试，谁还会去读黑格尔或康德？科学曾经是最伟大的阐述者，如今却将人生解释得支离破碎、艰深复杂、令人困惑。谁还会不带讥诮地去倾听经济学家、社会学家和政治家的高谈阔论？宗教对许多人来说已经变成了一种掩饰虚伪的空洞仪式。随着我们对传统意识形态的信仰日益消减，人们转而寻求我们依然相信的源泉：故事的艺术。

世人对电影、小说、戏剧和电视的消费是如此的如饥似渴、不可餍足，故事艺术已经成为人类灵感的首要来源，因为它不断寻求整治人生混乱的方法，洞察人生的真谛。我们对故事的欲望反映了人类对捕捉生活模式的深层需求，这不仅仅是一种纯粹的知识实践，而是一种非常个人化和情感化的体验。用剧作家让·阿努伊的话说：“小说赋予人生以形式。”

有人认为这种对故事的渴求只不过是纯粹的娱乐——与其说是对人生的探索，不如说是对人生的逃避。但是，究竟什么是娱乐？娱乐即是沉浸于故事的仪式之中，达到一种知识上和情感上令人满足的目的。对电影观众来说，娱乐即是这样一种仪式：坐在黑暗的影院之中，将注意力集中在银幕之上，来体验故事的意义以及与那一感悟相伴而生的强烈的、有时甚至是痛苦的情感刺激，并随着意义的加深而被带入一种情感的极度满足之中。

无论是《捉鬼敢死队》中疯狂企业家们对赫梯魔鬼的胜利，还是《闪亮的风采》里对内心魔鬼的复杂解决办法，无论是《红色沙漠》中的特色浑一，还是《对话》里的个性分裂，凡是优秀的电影、小说和戏剧，都能通过其各不相同的喜剧和悲剧色彩来达到娱乐的目的，给观众一种具有感染力的新鲜的生活模式。如果一个艺术家认为观众只不过是想将他们的烦恼抛在门外，逃避现实，并把自己锁闭在这样的一种观念之中，那便是对艺术家责任的一种懦夫式的抛弃。故事并不是对现实的逃避，而是一种载体，承载着我们去追寻现实、尽最大的努力挖掘出混乱人生的真谛。

然而，尽管不断扩展的媒体现在已经能够使我们跨越国界和语言的壁垒，将故事传送到千家万户，故事讲述的总体质量却在每况愈下。我们偶尔能读到或看到优秀的作品，但大部分时候我们已经厌倦了在报刊广告、录像带出租店和电视指南上苦寻高质量的东西，厌倦了将只读了一半的小说慨然放下，厌倦了在中场休息时溜出剧院，厌倦了在走出电影院时这样慰藉自己的失望：“不过摄影还是挺漂亮的……”故事的艺术正在衰竭，即如亚里士多德在两千三百年前所指出的，如果连故事都讲不好了，其结果将是堕落与颓废。

漏洞百出和虚假的故事手法被迫用奇观来取代实质，用诡异来取代真实。脆弱的故事为了博取观众的欢心已经堕落为用亿万美元堆砌而成的炫目噱头。在好莱坞，影像已经变得越来越奢侈，在欧洲则是越来越浮华。演员的表演变得越来越做作，越来越淫猥，越来越暴力。音乐和音响效果变得越来越喧嚣。总体效果流于怪诞。文化的进化离不开诚实而强有力的故事。如果不断地耳濡目染于浮华、空洞和虚假的故事，社会必定会走向堕落。我们需要真诚的讽刺和悲剧、正剧、喜剧，用明丽素洁的光来照亮人性和社会的阴暗角落。不然的话，就会像叶芝所警告的一样：“……中心难再维系。”[^2]

好莱坞每年生产和发行影片四百到五百部，事实上是每天一部。其中有少数优秀影片，但大部分都属平庸或粗劣之作。市场上之所以充斥如此之多的陈词滥调，人们不禁要去怪罪那些批准投拍的巴比特[^3]式的人物。不过，我们可以回忆一下影片《幕后玩家》中的情节：蒂姆·罗宾斯笔下年轻的好莱坞经理人解释说，他有许多敌人，因为他的制片厂每年要收到两万多个电影剧本，但只能投拍十二部影片。这是符合事实的对白。大制片厂的故事部门要阅读成千上万个剧本、故事大纲、小说和戏剧，才能精选出一个上好的银幕故事。或者说得更准确些，挑选出一些半好的东西来开发成上好的剧本。

到二十世纪九十年代，好莱坞的剧本开发成本已经攀升至每年五亿多美元，其中有四分之三都付给了作家去选定[^4]或改写一些永远不可能投拍的影片。尽管耗资超过五亿，且剧本开发人员已经竭其所能，好莱坞还是找不到比他们实际投拍的影片更好的东西。一个令人难以相信的事实是，我们每年从银幕上所看到的便已是近年来最佳创作的合理写照。

然而，许多银幕剧作家却不能正视这一闹市区的事实，而始终生活在幻想中的远郊富人区，坚信好莱坞只是对他们的天才视而不见。除了个别罕见的特例之外，天才被埋没的情形只是一种神话。一流的剧本即使不被投拍，也至少会被选定。对于能够讲述优秀故事的作家而言，这是一个卖方市场——曾经是而且永远是。每年，好莱坞都稳守着数百部影片的国际业务，这些电影总是会被拍出。大多数会在影院发行，经过几周之后便偃旗息鼓，然后便谢天谢地地被人们遗忘。

然而，好莱坞不但生存了下来，而且还日益昌盛，因为它几乎没有竞争。过去的情形却并不总是如此。从新现实主义的崛起到新浪潮的汹涌，北美电影院曾一度辉映着杰出欧陆电影人的作品，严峻挑战着好莱坞的霸主地位。但随着这些大师的相继故去或先后退隐，过去二十五年来，优秀的欧洲影片则日见其少。

今天，欧洲电影人将其吸引观众的失败归结为发行商的阴谋。然而，其前辈雷诺阿、伯格曼、费里尼、布努埃尔、瓦依达、克鲁佐、安东尼奥尼、雷乃的影片却风靡全世界。制度并没有改变，非好莱坞影片的观众还是同样广大而忠实，发行商的动机也依旧和当年一样：钱。唯一变化的是，当今的“作者”[^5]已经不能以其先辈的实力来讲述故事。就像浮华的室内装修商一样，他们拍出的影片仅仅能够悦人眼目，除此之外别无他物。其结果是，欧洲的天才风暴荡涤之后，留下的只是一片枯涩的电影荒漠，形成了一个有待好莱坞来填补的真空。

然而眼下，亚洲影片却在北美和世界各地畅行不衰，打动和愉悦着千百万观众，成为国际影坛瞩目的焦点。其原因只有一个：亚洲电影人能够讲述精彩的故事。非好莱坞电影人不应将其失误推诿于发行商，而应开阔眼界，向东方学习，因为那儿的艺术家们既有讲述故事的激情，又有将其美妙讲出的手艺。

### ◎ 手艺的失传

故事艺术是世界上主导的文化力量，而电影艺术则是这一辉煌事业的主导媒体。世界观众都忠爱故事却只能渴慕。为什么？这并不是因为我们讲故事的人没有努力。美国作家协会剧本登记服务处每年记录在案的剧本多达三十五万多个，这还仅仅只是记录在案的剧本。在全美国，每年跃跃欲试的剧本数以百万计，而真正能称为上品者却寥寥无几。其原因固然是多方面的，但根本原因可以归结为一条：如今想要成为作家的人，根本没有学好本行的手艺便已蜂拥到打字机前。

如果你的梦想是当一名作曲家，那你是否会对自己说：“我已经听过许多许多交响曲……我也会弹钢琴……我想我这个周末便可以作出一首曲子。”不会。可是，现在许多的剧作家就是这样开始的：“我已经看过不少电影了，有好有坏……我的英语成绩都是A……度假的时刻到来了……”

如果你想作曲，你会去上音乐学院，学习理论和实践，专修交响曲这一样式。经过多年的勤学苦练，你会将自己的知识融入你的创造力，鼓起勇气大胆作曲。无数未露头角的作家从不怀疑，一个优秀剧本的创作，其难度无异于一部交响曲的诞生，而且在某种程度上而言还会更难。因为作曲家所摆弄的是具有数学般精密度的音符，而我们所染指的却是一种被称为人性的模糊的东西。

初生牛犊不怕虎，他会勇往直前，仅凭自己有限的经验，认为他所经历的人生以及他所看过的电影已经令他有所欲言并教会了他如何言之。然而，经验的作用却被过分高估。我们当然需要不去逃避生活的作家，需要能够深入生活并密切观察生活的作者。这是至关重要的，但远远不够。对于大多数作家来说，他们通过读书学习所获得的知识等于或超过了经验，尤其是当那种经验没有经过检验之时。自知是关键——生活加上我们对生活的深沉反思。

至于技巧，初学者误以为是手艺的东西，只不过是他对所遇到的每一部小说、电影或戏剧中故事要素的无意识吸收。当他写作时，会试图将自己的作品与他在阅读和观看过程中积累的一个模式用试错法[^6]进行匹配。非科班的作家把这称为“直觉”，然而这纯粹是一种习惯，而且具有严重的局限性。他不是模仿心目中的样板，就是把自己想象为先锋派的作家，对其进行反叛。然而，对那些无意识内化的重复模式所做的这种随意摸索或反叛绝不是什么技巧，于是便导致了那些充斥着陈词滥调的剧本的出笼，无论是商业片还是艺术片，都未能幸免于难。

情况并不总是这样，瞎猫碰到死耗子。在过去的几十年里，剧作家都是通过接受大学教育或在图书馆自学，通过在戏院工作或写作小说，通过在好莱坞制片厂体制中充当学徒，或通过以上多种手段的综合，来学习他们的手艺。

二十世纪初，一些大学开始相信，就像音乐家和画家一样，作家也需要接受类似于音乐学院或艺术学院的正规教育，来学习其手艺的原理。为了达到这个目的，威廉·阿彻、肯尼斯·罗[^7]和约翰·霍华德·劳森等学者都写出了有关剧作理论和散文艺术的杰出著作。他们的方法是内在的，从欲望的大肌肉运动、对抗力量、转折点、脊椎、进展、危机、高潮中汲取力量——从里面看到外面的故事。从业的作家，无论有无接受正规教育，都利用这些教诲来完善他们的艺术，把从喧嚣的二十年代一直到暴烈的六十年代的这半个世纪变成了银幕、小说和舞台上美国故事的黄金时代。

然而，在过去的二十五年，美国大学里创造性写作的教学方法已经从内在转向了外在。文艺理论的思潮将教授们的注意力从故事的深层源泉转向语言、符号和文本——从外面看到的故事。其结果是，除少数特例外，当今一代作家在故事的主要原理方面，修养严重欠缺。

美国以外的剧作家甚至更缺乏学习手艺的机会。欧洲学术界普遍否认写作是可以教授的，结果，创造性写作的课程始终未能进入欧陆大学的课表。然而，欧洲滋养着世界上许多最杰出的艺术和音乐学院。为什么他们可以认为，一种艺术是可教的，而另一种却不可教授呢？ 更有甚者，由于对银幕剧作的鄙薄，直到本书写作的九十年代末期，除莫斯科和华沙之外，欧洲所有的电影学院都不曾开设银幕剧作课程。

人们对好莱坞的旧制片厂体制也许会有诸多微辞，但制片厂内，由经验丰富的故事编辑大师们负责监督学徒的制度却是功不可没的。那样的日子已经一去不返了。一些制片厂也时常想重新恢复往昔的学徒制度，但在试图找回当年黄金岁月的热情之中，他们却忘记了，学徒是需要师父的。当今的制片厂经理人也许具有识别天才的能力，但他们却极少拥有能将一个天才训练成艺术家的技能或耐心。

故事衰竭的最终原因是深层的。价值观、人生的是非曲直，是艺术的灵魂。作家总要围绕着一种对人生根本价值的认识来构建自己的故事——什么东西值得人们去为它而生、为它而死？什么样的追求是愚蠢的？正义和真理的意义是什么？在过去的几十年间，作家和社会已经或多或少地就这些问题达成了共识，可是我们的时代却变成了一个在道德和伦理上越来越玩世不恭、相对主义和主观主义的时代—— 一个价值观混乱的时代。例如，随着家庭的解体和两性对抗的加剧，谁还会认为他能真正明白爱情的本质？即使你相信爱情，那么你又如何才能向一群越来越怀疑的观众去表达？

这种价值观的腐蚀便带来了与之相应的故事的腐蚀。和过去的作家不同的是，我们无从假定观众的期待。我们必须深入地挖掘生活，找出新的见解、新版本的价值和意义，然后创造出一个故事载体，向一个越来越不可知的世界表达我们的解读。这绝非易事。

### ◎ 故事要领

当我搬到洛杉矶后，就像许多人不得不靠写作糊口一样，我靠审读剧本为生。我为联美公司和全国广播公司工作，帮他们分析别人送上来的电影和电视剧本。在做完了前两百个分析之后，我觉得我可以事先写出一个通用的好莱坞故事分析报告，只需填上片名和作者姓名即可。我反复写出的报告大致如下：

> 描写精彩，对白可以演出。有一些轻松诙谐的场景，有一些感觉敏锐的场景。总而言之，这是一个文笔通顺、用词恰当的剧本。不过，故事却伤不起。前三十页一直拖着一个解释性大肚子吃力地爬行，余下的部分也一直未能站起来。主情节难以自圆其说，充斥着方便的巧合和脆弱的动机。没有明确的主人公。互不关联的紧张场面本可以编织成缜密的次情节，但作者却没有做到。人物塑造流于表面化，没有揭示出人物性格。对人物的内心世界及其所处社会环境毫无洞察力。是对一系列可以预见的、讲述手法低劣的、陈词滥调的片断所进行的毫无生命力的拼凑，最终沦为一团了无头绪的雾水。不予通过。

但我从没写过这样的报告：

> 故事精彩动人！从第一页开始便将我抓住，一直到最后都不忍释卷。第一幕便营造出一个突发的高潮，并由此辐射出一张由情节和次情节编织而成的缜密而优美的网。人物性格深邃，揭示力透纸背。对社会具有惊人的洞察力。亦庄亦谐，悲喜交织。故事进展到第二幕，高潮迭起，动人心魄，似近尾声。然而，从第二幕的灰烬中，作者却放飞出一只涅槃凤凰般的第三幕，是那样的俊美，那样的矫健，那样的壮观，让人肃然起敬，不禁匍匐仰视。然而，这部长达二百七十页的剧本却充斥着语法错误，每五个字里必有一个拼写错误。对白是那样的拗口，即使奥利维尔也无法口齿伶俐地演出。描写夹杂着镜头方位、潜文本解说以及哲理性的评说。就连打印的格式也不规范。显然不是一个专业的作家。不予通过。

如果我写了这样的报告，那我肯定已经失业了。

办公室门口的招牌并不是“对白部”或“描写部”，而是“故事部”。一个好故事使一部好影片成为可能，如果故事不能成立，那么影片必将成为灾难。审看剧本的人如果不能把握这一基本要领，理应被解雇。事实上，一个手法精巧而对白粗劣或描写枯燥的故事，是非常罕见的。更多的情形是，故事手法越是精巧，其形象则越生动，对白也越尖锐。故事进展过程的缺乏、动机的虚假、人物的累赘、潜文本的空洞、情节的漏洞以及其他类似的故事问题，才是文笔平淡乏味的根本原因。

仅有文学才华也是不够的。如果不能讲述故事，你经年累月精雕细琢出来的美妙形象和微妙对白也只是浪费纸张。我们为世界创造的，世界要求于我们的，是故事。现在如此，永远如此。无数作家沉溺于用精美的丝线来编织华而不实的对白和精雕细琢的描写，却始终不能明白他们的作品为何不能投拍；而其他文学才华平实但故事讲述能力超凡的作者，却能欣慰地看到他们的梦境在银幕的光影中再现。

在一部完成作品所体现的全部创作努力中，作家百分之七十五以上的劳动都用在了故事设计上。这些人物是谁？他们想要什么？为什么想要？他们将会采用怎样的方法去得到他们想要的东西？什么将阻止他们？其后果是什么？找到这些重大问题的答案并将其构建成故事，便是我们压倒一切的创作任务。

设计故事能够测试作家的成熟度和洞察力，测试他对社会、自然和人心的洞识。故事要求有生动的想象力和强有力的分析思维。自我表达绝不是问题的关键，因为，无论自觉还是不自觉，所有的故事，无论真诚还是虚假，明智还是愚蠢，都会忠实地映现出作者本人，暴露出其人性……或人性的缺乏。与这一恐怖的事实相比，写作对白便成了一种甜美的消遣。

所以，作家要把握故事的原理，把故事讲完……然后戛然而止。那么故事究竟是什么？故事的道理就像音乐的道理一样。我们终身听着各种不同的曲调，我们可以随之起舞、伴之吟唱。我们以为自己懂得音乐，直到我们试图自己去作一首曲子，结果从钢琴里蹦出来的东西却把小猫吓跑了。

如果《温柔的怜悯》和《夺宝奇兵》 都是讲得精彩神奇的银幕故事——而它们也的确如此——那么它们究竟有什么共同之处？如果《汉娜姐妹》 和《巨蟒与圣杯》都是讲得妙趣横生的喜剧故事——而且确实如此——那它们都妙在何处？试比较《哭泣游戏》和《温馨家族》、《终结者》和《命运的逆转》、《不可饶恕》和《饮食男女》， 或者《一条叫旺达的鱼》和《人咬狗》、《谁陷害了兔子罗杰》 和《落水狗》， 或者回溯到几十年以前，比较一下《迷魂记》和《八部半》、《假面》和《罗生门》、《卡萨布兰卡》 和《贪婪》、《摩登时代》和《战舰波将金号》——这一切都是精美绝伦的银幕故事，它们是那样的迥异其趣，却能产生同样的效果：观众离开影院时会异口同声地惊叹：“多好的故事！”

被淹没在类型和风格的海洋之中，作家们也许会认为，如果这些影片都是在讲故事，那么任何东西都能成为故事。不过，如果我们深入观察，如果我们剥开其外表，就会发现，在本质上，它们都是一样的。每部影片都以其独一无二的方式，在银幕上再现了完全相同的故事普遍形式。正是这一深层的形式，让观众情不自禁地发出了“多好的故事”的感叹。

每一门艺术都是由其根本形式决定的。无论是交响曲还是嘻哈说唱，其内在的音乐形式使其成为音乐，而不是噪音。无论是具象派还是抽象派，视觉艺术的基本原理使一幅油画成为一幅油画而不是涂鸦。同样的道理，无论是荷马还是英格玛·伯格曼，故事的普遍形式使其作品成为一个故事，而不是肖像画或艺术拼贴。无论经历多少文化的洗礼、朝代的更迭，这种内在的形式虽变幻无穷，但始终万变不离其宗。

然而，形式并不等于“公式”。世上绝无银幕剧作的食谱可以保证你的蛋糕一定松软可口。故事是那样的丰富多彩、纷繁复杂、神妙莫测、变幻万端，远非一个公式所能涵盖。只有傻瓜才会耽此臆想。不过，一个作家必须把握故事形式，这是谁也逃脱不了的。

### ◎ 把好故事讲好

“好故事”就是值得讲且世人也愿意听的东西。发现这些东西是你自己孤独的任务。这事儿得从天才开始。你必须拥有天赋的创造力，能以别人做梦都想象不到的方式把材料组织起来。然后，你必须将一种由对社会和人性的鲜活洞察所驱动的视觉印象注入你的作品之中，辅之以对自己作品人物和世界的深入了解。此外，正如哈莉·伯内特和惠特·伯内特在其精美的小册子[^8]中所揭示的那样，你还必须拥有很多的爱。

对故事的爱——相信你的视觉印象只能通过故事来表达，相信你的人物会比真人更“真实”，相信你虚构的世界要比具体的世界更深沉。对戏剧性的爱——痴迷于那种给生活带来排山倒海般变化的突然惊喜和揭露。对真理的爱——相信谎言会令艺术家裹足不前，相信人生的每一个真理都必须打上问号，即使是个人最隐秘的动机也不例外。对人性的爱——愿意移情于受苦的人们，愿意深入其内心，通过他们的眼睛来察看世界。对知觉的爱——不仅要沉迷于肉体的感官知觉，还要纵情于灵魂深处的内在体验。对梦想的爱——能够任凭想象驰骋，乐在其中。对幽默的爱——笑对磨难，以恢复生活的平衡。对语言的爱——对音韵节奏、语法句义探究不止，乐此不疲。对两重性的爱——对生活隐藏矛盾的敏锐触觉，对事物表面现象的健康怀疑。对完美的爱——具有一种字斟句酌、反复推敲的激情，追求完美的瞬间。对独一无二的爱——大胆求新，对冷嘲热讽处之泰然。对美的爱——对作品的优劣美丑具有一种先天的知觉，并懂得如何去粗取精。对自我的爱——无需时常提醒，从不怀疑自己的写作能力。你必须热爱写作，并且还能忍受寂寞。

不过，仅有对好故事的爱，对被你的激情、勇气和创造天才所驱策的精彩人物和世界的爱，还是不够。你的目标是要把一个好故事讲好。

即如一个作曲家必须精于音乐创作的根本原则，你也必须掌握故事构思的相应原理。这门手艺既不机械，也不花哨。它是一系列技巧的和谐统一，让我们创造出与观众之间的一种共谋利益。故事手艺作为所有方法的总和，吸引观众深深地投入你所创造的世界，流连忘返，并最终以一种感人至深、意味深长的体验来回报观众的炽热纯情。

一个作家如果没有掌握这门手艺，他最多只能做到抓住头脑中蹦出的第一个想法，然后不知所措地面对自己的作品发呆，无从回答这些恐怖的问题：这到底好不好？难道全是垃圾？如果真是垃圾，我该怎么办？人的意识一旦固着于这些可怕的问题，潜意识的流动就会被堵塞。但是当我们带着清醒的意识施展故事手艺、执行客观任务时，潜意识的暗流便会自然浮出水面。对手艺的精通可以释放潜意识，令其自由驰骋。

一个作家一天的工作节律是什么？首先，进入你想象中的世界。当你写作时，你的人物会自然地说话动作。下一步你该干什么？走出你的幻想，把自己所写的东西读一遍。那么，在读的过程中你应该做什么？分析。“这样好不好？观众会不会喜欢？为什么不喜欢？是否应该把它删掉？补充？重新整理？”你一边写，一边读；创作，批评；冲动，逻辑；右脑，左脑；重新想象，重新改写。你改写的质量，你臻于完美的可能性，取决于你对写作手艺的掌握，因为这种手艺可以指导你去改正不足。艺术家从不被一时冲动的奇思异想支配，而总是孜孜不倦地苦练手艺以达到直觉和思想的和谐。

### ◎ 故事与生活

审读多年，我注意到两种典型而层出不穷的失败剧本。

第一种是“个人故事”坏剧本：

> 在办公室的背景中，我们遇到了面临问题的主人公：她应该被提升，可是却被跳过。她很窝火，于是来到父母家，却发现父亲已经老年痴呆，母亲对此一筹莫展。回到自己的公寓，和邋遢而麻木不仁的室友又大吵了一架。然后出去和男友约会，没想到话不投机：她那感觉迟钝的情人把她带到了一家昂贵的法式餐厅，完全忘了她正在节食减肥。再回到办公室，惊喜地发现她被提升了……但是新的压力又起。回到父母家，好不容易把父亲的问题解决，母亲的精神又几近崩溃。回到自己家，发现室友偷走了她的电视机，房租未付便溜之大吉。她和情人分手，拿冰箱里的食品撒气，结果体重增加了五磅。但是她振作精神，把自己的升职看作一种胜利。在饭桌上与父母进行了一次温情脉脉的促膝长谈，治好了母亲的精神创伤。她的新房客不仅是一个提前几周就把房租付清的好人，还给她介绍了一位新朋友。现在我们已把剧本读到第九十五页。她坚持节食减肥，在最后的二十五页一直保持着良好的体形，因为这一段故事即是对她与新朋友之间爱情之花绽放之后，在雏菊丛中慢镜头追逐的文学描写。最后，她终于面临人生的重大抉择：屈从爱情还是抽身自拔？剧本在催人泪下的高潮中结束，因为她决定保留自己的生活空间。

第二种是“保证商业成功”坏剧本：

> 一名软件推销员从机场的一堆行李中得到了一种“能够毁灭我们当今所知人类文明的东西”。这个“能够毁灭我们当今所知人类文明的东西”非常小。事实上，它被隐藏在一支圆珠笔内，而这支圆珠笔不知不觉地进入了我们这位倒霉的主人公的口袋里。于是，他成为了影片中三十多位登场人物的追杀对象。这些人都具有双重或三重身份，为铁幕的两边工作，他们自“冷战”结束之后彼此认识，都有一个共同的目标，即杀死我们的主人公。剧本充斥着汽车追逐、枪战、令人毛骨悚然的逃跑，还有爆炸场面。在爆炸或杀人间隙，则是密集的对白场景，因为主人公要搞清这些双重身份的人物，找出到底能够相信谁。剧本在一片喧嚣嘈杂的暴力和亿万美元的特技场面中画下句点，其间我们的主人公设法销毁了那一“能够毁灭我们当今所知人类文明的东西”，从而拯救了全人类。

“个人故事”结构性欠缺，只是对生活片断的呆板刻画[^9]，错误地将表象逼真当成生活真实。作者相信，他对日常事实的观察越精细，对实际生活的描写就越精确，他所讲述的故事便也会越真实。但是，无论被观察得如何细致入微，这种“事实”也只能是小写的真实。大写的真实位于事件的表面现象之后、之外、之内、之下，或维系现实，或拆解现实，不可能被直接观察到。由于作者只看到了可见的事实，反而对生活的真实茫然无视。

另一方面，“保证商业成功”的剧本却是一种结构性过强、复杂化过度、人物设置过多的感官刺激，全然割断了与生活的任何联系。作者把动作误以为娱乐。他希望，撇开故事不谈，只要堆砌了足够的高速动作和令人目眩的视觉效果，观众便会兴奋不已。就目前如此之多的暑期档影片全靠电脑生成影像（CGI）驱动的现象而言，他似乎并没有全错。

这种类型的奇观场面用模拟现实取代了想象。它们只是把故事当作一个幌子，来展现迄今为止尚未见过的特技效果，将我们带入龙卷风、恐龙的血盆大口或未来世界的大浩劫。毫无疑问，这种炫目的奇观确实能引发围观马戏团般的兴奋。但是，就像游乐园的过山车一样，其愉悦只能是短暂的。电影创作的历史已经反复证明，一旦新鲜惊险的感官刺激达到风靡的顶点，很快便会面临一种明日黄花的冷遇。

每隔十年左右，技术创新便能孵化出一批故事手法低劣的影片，其唯一的目的只是为了开发奇观场景。电影发明本身作为对现实令人惊叹的模拟，给公众带来了极大的兴奋，随之而来的便是多年的乏味可陈。随着时间的推移，无声影片发展成为一种辉煌的艺术形式，结果却被有声影片—— 一种对现实更加真实的模拟——所毁。三十年代早期的电影，正是由于观众为聆听演员说话而对平淡无奇故事的甘愿忍受，而倒退了一大步。后来有声影片终于在力与美的层面上大有进阶，结果彩色片、立体片、宽银幕以及现在电脑生成影像的发明又将其大步击退。

电脑生成影像既不是瘟疫也不是万应灵药，它只不过是在故事的调色板上增添了几抹新鲜的色彩。多亏有电脑生成影像，我们所能想象的一切都能够达成，而且惟妙惟肖。当CGI为强有力的故事引发，如《阿甘正传》或《黑衣人》，其效果便会消失在故事后面，强化出它所要烘托的画面，而免去了哗众取宠之嫌。然而，“商业片”的作者往往被奇观的光焰迷住了双眼，看不到持久的娱乐只有在蕴藏于影像之下的人生真谛中才能找到。

无论是刻板故事还是奇观故事的作者，事实上所有的作家，都必须明白故事与生活的关系：故事是生活的比喻。

一个讲故事的人即是一个生活诗人，一个艺术家，将日常生活事件、内在生活和外在生活、梦想和现实转化为一首诗，一首以事件而不是以语言作为韵律的诗——一个长达两小时的比喻，告诉观众：生活就像是这样！因此，故事必须抽象于生活，提取其精华，但又不能成为生活的抽象化，以致失却实际生活的原味。故事必须像生活，但又不能一成不变地照搬生活，以致除了市井乡民都能一目了然的生活之外便别无深度和意味。

刻板故事的作者必须意识到，生活事实是中性的。试图把所有事件都包罗在故事内的最脆弱借口是：“可是这确实发生过”。任何事都会发生，任何可以想象的事都会发生。实际上，不可想象的事也会发生。但故事并不是实际中的生活。纯粹罗列生活中发生的事件绝不可能将我们导向生活的真谛。实际发生的事件只是事实，而不是真理。真理是我们对实际发生的事件进行思考后的想法。

我们可以来看一看关于众所周知的“圣女贞德的生活”的一系列事实。几百年来，知名的作家已经将这位不凡的女性送上了舞台，写入了书页，搬上了银幕，他们所刻画的每一个贞德都是独一无二的——阿努伊的精神的贞德、萧伯纳的机智的贞德、布莱希特的政治的贞德、德莱叶的受难的贞德、好莱坞的浪漫勇士，以及莎士比亚笔下疯狂的贞德——这是一种典型的英国看法。每一个贞德都领受神谕，招募兵马，大败英军，最后被处以火刑。贞德的生活事实永远是相同的，但每当其生活“真谛”的意义被作家发现时，整个样式便随之改变了。

同理，奇观故事的作者必须意识到，抽象的东西是中性的。我所谓的抽象，是指图像设计、视觉效果、色饱和度、音响配置、剪辑节奏等一系列技术性的策略。这些东西本身是没有意义的。用于六个不同场景的完全相同的剪辑模式可以得出六种完全不同的解释。电影美学是表达故事生动内容的手段，其本身绝不能成为目的。

### ◎ 能力与天才

尽管刻板故事和奇观故事的作者讲述故事的能力低下，但他们也可能天生具备两种根本能力之一。倾向于刻板描述的作家通常具有知觉能力，一种向读者传达肉体感官知觉的能力。他们的视觉和听觉是那样的敏锐和精微，读者一旦被其栩栩如生的精美形象触及，都会为之心动。另一方面，长于动作场面的作者通常具有一种超凡的想象力，可以将观众从现实空间提升到虚拟空间。他们能够将不可能的假设变为令人惊叹的现实，也能令观众怦然心动。无论是敏锐的知觉能力还是生动的想象能力，都是令人钦羡的天赋，不过，就像一段美满的婚姻一样，二者必须相辅相成。如果仅具其一，便只会在孤独中萎顿。

现实的一端是纯粹的事实，而另一端则是纯粹的想象。在这两极之间存在一个变幻无穷的“光谱”，所有风格各异的小说故事便游移于这光谱之间。强有力的故事讲述便是在这光谱之中找到了一个平衡点。如果你的作品飘向了其中一个极端，那你就必须学着将作品中人性的所有方面融为一个和谐的整体。你必须置身于这一创作的光谱之中：拥有敏锐的视觉、听觉和感觉，并与强大的想象力达成平衡。双管齐下，利用你的洞察和直觉来感动我们，表达你对人生事物的理解：人们如何以及为何做他们要做的事。

最后，除了知觉力和想象力是创作的先决条件外，还有两种超凡的基本天才是写作者必须具备的。然而，这两种天才却没有必然的联系。具备其一并不等于拥有其二。

第一是文学天才——创造性地将日常语言转化为一种更具表现力的更高形式，生动地描述世界并捕捉人性的声音。然而，文学天才是很普通的东西。在世界各地的每一个识字人群内，都有数以百计甚至数以千计的人能在某种程度上将属于其文化的普通语言转化为超凡脱俗的精品。从文学意义上而言，他们的写作堪称美妙，有些甚至壮美。

第二是故事天才——创造性地将生活本身转化为更有力度、更加明确、更富意味的体验。它搜寻出我们日常时光的内在特质，将其重新构建成一个比生活更加丰富的故事。纯粹的故事天才是罕见的。试问，有哪一位作家能够全凭本能和直觉年复一年地创作出优美的故事，而且从来不用思考如何才能写出这样好的故事、怎样才能写得更好？本能的天才也许偶尔能够炮制出一部优秀的作品，但完美和多产却不可能仅来自那无师自通的本能。

文学天才和故事天才不但各不相同，而且毫无关联，因为故事并不一定非要写下来才能讲给人们听。故事能够以人类交流的任何方式来表达。戏剧、散文、电影、歌剧、哑剧、诗歌、舞蹈都是故事仪式的辉煌形式，每一种都有其娱人之长。只不过在不同的历史时期，其中的某一样式会走到前台而已。十六世纪扮演了这一角色的是戏剧，十九世纪是小说，二十世纪则是电影——所有艺术形式的宏伟融合。银幕上最有震撼力和说服力的瞬间并不需要言语的描述来创造，并不需要人物的对白来演绎。它们是影像，纯粹而无声。如果说文学天才的材料是话语，那么故事天才的材料则是生活本身。

### ◎ 手艺能将天才推向极致

故事天才虽然罕见，我们却常常碰到与生俱来拥有它的人。比如，那些街头的说书艺人，对他们来说，讲故事就像微笑一样轻而易举。当同事们聚集在咖啡机旁，故事的讲述也会自然展开。它是人际交往的流通货币。在这一上午工间的休息仪式上，只要有五六个人聚在一起，至少会有一个具有这种天赋。

假设我们的故事天才今天上午向她的朋友们讲述“我是怎样把孩子们弄上校车的”。就像柯勒律治的《古舟子咏》一样，她抓住了所有人的注意力。听众就像中了她的魔法一般，全神贯注地手捧咖啡杯，半张着嘴。她时而明快、时而徐缓地编织着她的故事，令他们或笑或哭，将所有人的心高高地悬在半空，直到以一个炸药般的尾声戛然而止：“今天早上我就是这样才把那帮小捣蛋鬼弄上校车的。”她的同事们满足地伸了伸欠着的身子，喃喃道：“上帝，没错，海伦，我那帮孩子也是这样。”

再假设下面轮到她身边的一个男同事，接着讲他母亲周末去世的伤心故事……结果听得所有人烦得要命。他的故事全部停留在表面，从烦琐的细节到陈词滥调，唠唠叨叨，没完没了：“她躺在棺材里的样子还是很好看的。”当他讲到一半的时候，其他人都已经回到咖啡壶边去续咖啡，对他的伤心故事充耳不闻。

如果要在讲得精彩的琐碎素材和讲得拙劣的深奥素材之间进行选择的话，听众总是会选择讲得精彩的琐碎故事。故事大师懂得如何从最少的事件中挤出生命力，而蹩脚的讲故事的人却会使深奥沦为平庸。你也许具有佛一般的慧眼，但是如果你不会讲故事，你的思想将像白垩一样枯燥无味。

故事天才是首要的，文学天才是次要的，但也是必需的。这一原则对电影和电视来说是绝对的，对戏剧和小说来讲也比大多数戏剧家和小说家所愿意承认的要重要得多。故事天才尽管罕见，但你必须掌握一些，否则你便不会有写作的热切冲动。你的任务是从中拧出所有可能的创造力。只有充分利用你所掌握的一切故事技巧和手艺，你才能将你的天才锻造成故事。因为，只有天才而没有手艺，就像只有燃料而没有引擎一样。它能像野火一样暴烈燃烧，但结果却是徒劳无功。

一个讲得美妙的故事犹如一部交响乐，其结构、背景、人物、类型和思想融合为一个天衣无缝的统一体。要想找到它们的和谐，作家必须研究故事诸要素，把它们当成一个管弦乐队的各种乐器——先逐一精通，再整体合奏。