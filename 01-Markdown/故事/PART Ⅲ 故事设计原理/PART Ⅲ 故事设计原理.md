# PART Ⅲ 故事设计原理

The Principles of Story Design

  

如若强加一个严格的框架，想象力便可被推向极致——并产生最丰富的思想。如若给予完全的自由，作品便可能蔓延无当。

——T. S. 艾略特