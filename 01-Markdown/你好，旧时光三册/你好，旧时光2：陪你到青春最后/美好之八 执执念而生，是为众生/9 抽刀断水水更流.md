## 9 抽刀断水水更流

流言四窜。

余周周坐在桌子前一边翻着《中国国家地理》漂亮的彩页，一边揪着盘子里面的葡萄。她刚刚挂下陈桉的电话，脑子里一片混乱。

她笑着问陈桉，当初她因为躲避周沈然等人的阴影笼罩，也为了躲避奥数，选择了十三中这个悬崖潜心修炼，那么为什么女侠重出江湖的时候，还会陷入同一个怪圈里？

也许是她爸爸的那通电话和“一起过年”的邀约惹的祸。在分校一直安分守己的周沈然突然重新出手，这一次他的身份只是神秘知情人，不是余周周同父异母的弟弟。只是余周周实在没有兴趣以毒攻毒，把他也拉下水。

陈桉笑了：“小学时候你会因为这个哭鼻子，现在不会了，这就是区别。”

余周周愣了愣：“也许是因为我妈妈已经不在了。”

电话那端沉默很久。

“周周，你就那么不把自己当回事吗？”

她不知道怎么样回答。

余周周的确不在乎。当有女生神神秘秘地拉着刚刚返校的米乔八卦这件事情的时候，米乔伸手就是一巴掌：“除了嚼别人舌根还会点儿别的吗？他妈的不知道真假的事情就能四处传，还‘别告诉别人’——你自己先做到再说吧！”

后来米乔向她道歉，因为她的冲动把事情闹大了，反而让被打的女生更愤怒，以“评评理”的名义将事情传播得更广。

余周周笑了，轻轻揉了揉米乔的头发，心里说：“打得好，不怪你。”

“周周？”陈桉轻声喊她的名字，余周周从自己的思绪中清醒过来。

“其实不是这样的，”余周周慢慢地说，“我没有像你担心的那样破罐子破摔。我只是想起，小学时，我觉得没有爸爸这件事情压得我都受不了了，生怕别人知道，一心想要躲开。”

“而我的确躲开了，”她顿了顿，“在你的庇护下。”

“之后我才发现，那些当初会影响到我情绪的同学老师，其实早就已经淡出了我的生活，而且，他们都不再记得我。即使记得我的那些人，就像凌翔茜、蒋川，他们自己也在成长，也会知道什么是对的什么是不对的。所以这一次，重来的一次，我只要装作一切都不知道就好了，反正总有一天，这些和我坐在同一个教室里面八卦我的人，都会从我的生活中消失，就好像从来都没出现过。”

余周周甚至被自己临时编造出来的理由说服了。

然而，她仿佛听到了陈桉的摇头声。

“周周，我宁肯你什么都看不开，然后跟我哭诉问我怎么办，再然后由我来安慰你，这至少证明你还是爱惜自己名誉的，还是有在乎的事情的，还是像个孩子的。你告诉我，你真的是这样想的吗？还是你只是已经觉得都无所谓了？”

不，还是有所谓的。她想起那个寒冷的夏天，陈桉温暖的怀抱。

余周周握住听筒，忍耐了半天，才把要说的话吞了回去。

“陈桉，”她转移了话题，“工作忙吗？”

陈桉从来面对她的打岔都无动于衷，只是这一次，他顿了顿，突然很明朗地笑了。

“累死了，要学的东西太多了，累得像狗一样。对了，周周，我过年的时候没休假，都攒到夏天了。有个亲戚在泰国，我想去曼谷玩，你愿意跟我一起吗？”

余周周心都颤了一下，想也没想就回答：“好，好！”

雀跃得像个孩子。

然后冷静下来：“陈桉，我没钱。”

关于余周周的谣言肆虐到极致的时候，林杨正在东边的滨海城市参加物理和数学联赛的集训。

打给她的电话，对方一直不接。但是每天晚上，都会收到一条例行短信，告诉他：“晚安，好好休息，加油。”

从一开始的感动到此刻，这个短信越来越像一种例行安抚。林杨突然开始怀疑自己剃头挑子一头热的爱情。

路宇宁的电话在某天早晨降临：“喂，少爷，你知道你们家那位……”

“什么我们家那位？”林杨挠挠头，“现在还不是呢……过几年再允许你这么叫。”

“好，别废话，你们家未来的那位，出什么事了，你知道吗？”

林杨的心好像被一只手攥紧了。

“什么事？”

路宇宁絮絮地讲着，末了加上一句：“不过都是三八的传闻，你别太当真，我想了半天觉得不应该告诉你，省得你在那边分心，不过……唉，你自己看着办吧。”

林杨挂下电话，立即给余周周的手机打过去，可是和往常一样，她根本不接电话。

是因为心烦意乱，不愿意理他？

还是根本不需要别人关心？

林杨放下手机，苦笑了一下。说到底，其实他还是别人吧！

夏天就这样来了。

凌翔茜抬起手挡住眼前炽烈的光芒，时间久了，手臂发酸。

早上的升旗仪式刚刚进行一半，太阳光就迎面暴晒着她们。天亮得越来越早了，常常在醒来的时候发现天光大亮，再也没有初春的暧昧。

站在讲台上进行升旗演讲的是高一的学弟，声音平板、语气僵硬，凌翔茜想起楚天阔，那个人已经跑去集训一整个月了。

她记得在晚自习的时候，他们在一片漆黑的行政区顶层牵着手说话，她对他倾诉自己的烦恼，却又时时小心着说话时留些余地，抱怨得很“优雅”，很大度很有分寸。他在背后抱住她，轻轻地蹭着她柔顺的长发，给她讲些其实她自己也很明白的大道理——可是被他说出来，那些道理听起来就不一样，很不一样。

凌翔茜忽然觉得很讽刺。

她一遍遍地告诉自己，如果这个人毁容了，再联想到他的心胸气度，你就一定不喜欢他了，对吧，对吧。

然而就是戒不掉，想起来那个模糊的轮廓，还是会下意识地想要摆出一脸虚伪、殷勤的笑。

不算恋爱的恋爱，不算分手的分手。

还是想念，想得睡不着。晚上会心疼到哭醒。

广场上黑压压一片，凌翔茜突然觉得自己很孤独。她知道李静园一边和自己一起吃午饭，一边却和别人一起八卦自己。那些传言，她略知一二，一边告诉自己没必要找气受，一边又忍不住想要知道他们都说什么。

你的敌人与你的朋友的合作将会彻彻底底地将你击垮——一个负责造谣诽谤，一个负责将这些谣言和它造成的毁灭性影响一一告诉你。

李静园将所有八卦倾倒给她，毫无保留，事无巨细，还要装出一副多么义愤填膺的语气。

凌翔茜不愿意再搭理李静园，然而她在午饭中的沉默通通印证了李静园的想法——她被楚天阔甩了，还在纠缠对方，以至于茶饭不思、沉默寡言。同时又觊觎第一名，苦于得不到，更加抑郁。

升旗仪式结束，大家纷纷朝着教学楼走过去，凌翔茜忽然发现走在自己身边不远处的正是余周周。

“天开始热起来了。”她说。

“是啊。”

“升旗仪式的时候太阳晒得我头痛。”“的确很晒。”

凌翔茜笑了：“我是不是挺无聊的？”

余周周摇摇头。

“你想林杨吗？”

女孩诧异地扬眉，凌翔茜不知道那个表情代表什么，“你为什么这么问”还是“我为什么要想念林杨”？

凌翔茜和余周周一直不熟悉，然而这些天来的压抑让她发疯一样地想要倾诉。

“可是我想念一个人。”凌翔茜大方地开口，笑容惨淡。

余周周仿佛知道凌翔茜在想什么，轻声说：“他应该快要回来了。”

“原来你也听说了那些传言。”凌翔茜继续笑。

“什么传言？”

余周周的表情一点儿都不像在说谎。凌翔茜愣了一下，摇摇头：“没什么。”

“我身上也有传言，”余周周笑，“而且传言说的都是真的。”

凌翔茜扭过头。

她身上的传言，几乎也都是真的。她还在小心翼翼地给楚天阔发短信，她也想夺回第一，虽然因为楚天阔的关系她已经对那个位置产生了生理性厌恶，可是她需要第一，她需要唯一的证书来获准隔绝自己和周围那种恶心的流言气氛，她也需要它来治愈她妈妈左脸的抽搐。

从来没有什么时候像此刻一样厌恶自己。

“周周，”凌翔茜低着头，声音有些颤抖，“没有人知道，其实我过得好辛苦。”

期末考试，辛锐又是第一名。她知道，有时候名次这种东西是认主人的，你保住了这个名次，不出意外，连贯性也会保护你。

舆论让她快乐，只是这种快乐过后是更大的空虚。别人的苦痛和妒忌——哪怕那妒忌是她自己通过流言制造出来的——会让她觉得自己存在得更有意义、更成功。

只是，余周周和凌翔茜愈加对她无视。

余周周已经很久不和她一起回家。辛锐有时候一个人站在站台前会回想起当初她们两个并肩发呆的时光，只是恍然回头的时候，却想不起来她们究竟是因为什么不再一同回家。

也想过要给她发个短信，问问要不要一起走——只是心底有个地方让自己不敢面对她。

辛锐没有告诉过任何人，她是害怕余周周的。除了何瑶瑶，余周周是振华里面唯一知道她叫辛美香的人，余周周知道她偷书，余周周知道她家开食杂店，她妈妈四处追打她爸爸，余周周知道她曾经在课堂上站起来就无法开口说话，被徐志强扯着领子欺负……

余周周只要说出来，她就万劫不复。

余周周是新世界里面唯一的故人。

当周沈然告诉她那一切的时候，她甚至都没有过求证的打算，一瞬间就相信了。尽管周沈然个子小小，驼着背，还抖脚——可是她相信他说的是实话。

或者说，她希望他说的是实话。

她希望余周周的背后和她一样不堪，那个笑容甜美的小公主血统并不纯正。尽管余周周曾经拯救、保护了自己——甚至就连曾经的这些救助，辛锐也总是会告诉自己，那不是余周周的功劳，那只是自己足够争气、足够勇敢，并不是借助外界的任何人。

即使余周周没有伤害过她。

公主们最大的错在于，她们是公主。

辛锐抬头去看斜前方的凌翔茜的背影。

然后笑了。

凌翔茜觉得被深深地侮辱了。

饭桌上，妈妈竟然神经兮兮地问：“要不要重新练钢琴，尽快恢复到初一时候的十级水平，然后考艺术特长生？”

“为什么？”她放下饭碗。

“可以加分啊。”妈妈笑得有些怪，“几十分的加分，有备无患。”

凌翔茜听见书房里隐隐的谈话声。自从妈妈因为第三者的事情大闹之后，父亲留在家里面的时间越来越长，平时在外饭局上能谈的话都挪到书房里。

可是她知道，这只是一种安抚而已。直觉告诉她，父亲对母亲的厌烦已经让他不惜做戏来耍她。

“我为什么要加分？”凌翔茜有些颤抖，她只是这次发挥失常而已，年级第十一，数学两道大题思路一片空白，只是失常，只是失常。

失常的究竟是她还是她妈妈？

她站起身要回房间，被妈妈抓住了胳膊。

“你以为我不知道你在学校什么状态？你们老师都说了，你和那个男生的事情……你自己不争气，我们只能替你想办法，这是一条路，至少能保底。”

凌翔茜冷笑。武文陆已经跟她谈过了。不知道是谁多嘴举报给了老师。

她拒不承认自己与楚天阔之间的事情，她相信，即使楚天阔被问到，也一定什么都不会说。

可是她错了。楚天阔在武文陆说起“有人看见过你们常常在一起”的时候，轻描淡写地说：“只是关系还不错的同学，不过她有没有别的想法我就不知道了，我已经在和她保持距离了，毕竟是关键时期，老师，你知道，我也不会分不清轻重缓急。”

不过她有没有别的想法我就不知道了。

不过她有没有别的想法我就不知道了。

武文陆转述这句话的时候，凌翔茜瞬间绽放出一脸灿烂到凄惨的笑容。

“我的确没有什么别的想法啊！”

从现在开始。

“我不考艺术特长生，我没有任何问题，妈妈，你保护好你的脸，别总是东想西想的，别管我。”

她听不见妈妈在背后都说了些什么，回到自己房间闩上门，戴上MP3，将音量调到最大。

亨德尔的某部交响乐。某部。

凌翔茜从来都不喜欢古典音乐，虽然她自己学钢琴，可只是把考级的每首曲子都练得很熟练，至今也不知道门德尔松到底是谁。

只是因为楚天阔，只是因为楚天阔，她开始听德沃夏克的《自新大陆》，开始揣摩《四季》里面到底哪一季更富有表现力——只是为了某个能够延续的话题。

他们活得一点儿都不高雅，听什么高雅音乐？

林杨整个人坐在走廊窗台的背阴儿处，盯着手边那一方边缘完整的阳光。

米乔拍拍他的肩膀：“其实她只是出去玩了嘛，你干吗一副人家把你给甩了的丧门星表情？我猜她是心情不好，散散心也正常啊。”

林杨笑了：“她心情不好的时候，根本也没有告诉我。”

“怕你分心啊，你集训，多重要的事情啊，事关前途啊前途。你又不是不了解余周周，她是那种分不清轻重缓急、不体谅别人的女生吗？她这是为你好，这是关心你的表现。”

林杨侧过脸看她：“你说这种话，自己相信吗？”

米乔咳嗽了两声：“不信。”

“传言就是关于她的身世？”

“还有人说她受刺激了，性格大变，精神不正常，至少是抑郁症。”

“放他大爷的狗屁！”

米乔激动地鼓掌：“行啊你，乖乖宝的外表下掩藏着一颗如此粗犷的爷们儿心，骂得真顺口！”

林杨偏过头没有说话。

米乔拍拍他：“你也别太介意，她自己都一点儿也不上心，你说你激动什么。恋爱中的人就是矫情，人家难过你会担心，人家不难过你又失落，折腾个什么劲啊！”

林杨歪过头：“米乔，你为什么要帮我？”

米乔愣了一下，嘿嘿一笑：“闲着也是闲着，保媒一桩胜造七级浮屠。给自己攒阴德。”

“真的吗？”

米乔刚要回答，忽然一口气提不上来，剧烈地咳了几声，身体都缩成一团，好像是在拼命地把什么往外呕吐一样，面色通红，满脸泪水。

林杨慌张地跳下阳台，米乔声音减弱，倒在他怀里面的时候，轻得仿佛一片羽毛。

她太瘦了，肩胛骨硌得林杨胸口生疼，安安静静的样子，仿佛已经死了。