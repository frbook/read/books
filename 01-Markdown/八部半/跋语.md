  

## 跋语

直到写完第八个故事，我仍然不确定自己是否真正进入了小说家的角色。人生的前四十年已经习惯于当一个称职的出版人、勤奋的翻译者、自觉的评论者以及挑剔的读书人，这些角色仿佛被一只看不见的手，捏合成一个贴身跟踪的影子，须臾不曾放过我。

如是，出一本集子，或许可以被视为某种策略。无论如何，故事就在这里，或生涩或笨拙或意外地抓住了某些真相——它们都已经被打包收进了一本书，成为过去时。把这本书交给我的影子，批判得失，计算优劣，承受褒贬，那是她的事。我可以腾出空来，轻减行囊，重新上路。

八个虚构故事大多发表在最近三年里，各有其独特的、不可复制的动机与路径（具体可参见收入附录的几篇创作谈）。不过，把它们归拢到一起，多少也能看出下笔时自觉或不自觉关注的几个方向。观察当代都市基于互联网发展的新型人际关系和欲望结构，关注欺骗和自我欺骗、角色与角色错位，潜入城市中产者脆弱且内外交困的梦境，追问小说（故事）在未来的命运——凡此种种，一旦付之标签，就索然无味了。如果一定要给它们总结一点共性，或许从技术层面讲会显得更清晰一点。我希望它们都构成对我虚构能力的考验，而我的答卷至少能在完成度上达到及格线；我希望我能压制作者的倾诉欲，附身于人物，通过他们的感官，重新体察我熟悉的世界，获得某种可贵的陌生感。

回溯个人虚构史的起点，我总是想起五六年前发表的散文《海外关系》。正是在那篇写到瓶颈时，我开始尝试在虚构与非虚构的边境线上来回跳跃。闯进陌生疆界的兴奋记忆，至今鲜活如昨。在结集的最后关头，我将它折算成半篇小说，与八个故事合在一起，方才觉得这幅拼图被填上了最初的那一块。

这样便凑成了《八部半》。借用意大利电影大师费里尼的名作标题，当然是为了增加一点传播效果，但我还是忍不住生出些许牵强的浮想来。打碎现代都市人的生活现状和心理困境，重组成具有审美意义的艺术奇观，这是费里尼早就做到的事，或许也是我想要做到的事？无论如何，有目标——哪怕暂时悬在空中——也比没有目标好。

虽然是本小书，终究费了诸般心思，离不开一路上各位师友襄助。感谢《人民文学》的徐则臣、《上海文学》的崔欣和《小说界》的沈大成的约稿、修正和发表，感谢浙江文艺出版社上海分社曹元勇社长早在我刚开始虚构时就约定了这本书的出版计划，感谢责任编辑周语耐心细致地把这些故事组装成完全吻合我想象的形状，感谢陈村、孙甘露、小白、毛尖、张莉、徐则臣、黄德海、陆晶靖、赵振杰等各位老师和朋友——于我，他们不仅仅是赞助了几声推荐，而且在我这三年的虚构历程中，一直是我最早的读者。尤其要感谢李敬泽老师慷慨赐序，让我从这本小书的出版中得到无可替代的乐趣——被懂得。

黄昱宁

2018年6月10日14时30分

![](Image00000.gif)  
  

The End

  
![[/八部半/images/Image00001.jpg]]  

浙江出版联合集团旗下电子书出版机构  
http://www.bookdna.cn  
  
新浪微博：[[http://weibo.com/bookdnacb\|@BookDNA本唐在线出版]]  
微信公众号：本唐在线出版  
![[/八部半/images/Image00002.jpg]]

如您发现本书内容错讹，敬请发送邮件至 cb@bookdna.cn 指正。

![](Image00000.gif)  
  
  
  

成为作者，只需一步

To be an author, just one click.

  

[[http://www.bookdna.cn\|BookDNA.cn]]