   

人名地名索引之一

|   |   |   |
|---|---|---|
|中文译名|希腊文原名|英文译名|
|**三　画**|   |   |
|马叙阿斯|M![[/理想国/images/00488.jpeg]] V|Marsyas|
|**四　画**|   |   |
|巴拉米德斯|![[/理想国/images/00489.jpeg]] V|Palamedes|
|厄洛斯|’K![[/理想国/images/00490.jpeg]] V|Er|
|厄佩俄斯|’E![[/理想国/images/00491.jpeg]] V|Epeius|
|文艺女神|M![[/理想国/images/00492.jpeg]]|Muses|
|月神|![[/理想国/images/00493.jpeg]] V|Moon|
|比雷埃夫斯港|![[/理想国/images/00494.jpeg]] V|Peiraeus|
|开奥斯岛|K![[/理想国/images/00495.jpeg]] V　或　K![[/理想国/images/00496.jpeg]] V|Ceos|
|开俄斯岛|X![[/理想国/images/00497.jpeg]] V|Chios|
|乌拉诺斯|O![[/理想国/images/00498.jpeg]] V|Uranus|
|**五　画**|   |   |
|古各斯|![[/理想国/images/00499.jpeg]] V|Gygc|
|“必然”|’A![[/理想国/images/00500.jpeg]]|Necessity|
|皮塔科斯|![[/理想国/images/00501.jpeg]] V|Pittacus|
|卡克冬|X![[/理想国/images/00502.jpeg]]|Chalcedon|
|尼克拉托斯|N![[/理想国/images/00503.jpeg]] V|Niceratus|
|尼客阿斯|N![[/理想国/images/00504.jpeg]] V|Nicias|
|尼俄珀|N![[/理想国/images/00505.jpeg]]|Niobe|
|**六　画**|   |   |
|吕底亚|![[/理想国/images/00506.jpeg]]|Lydia|
|吕西阿斯|![[/理想国/images/00507.jpeg]] V|Lysias|
|吕萨略斯|![[/理想国/images/00508.jpeg]] V|Lysanias|
|伊达山的|’I![[/理想国/images/00509.jpeg]] V|Idás|
|伊里翁（特洛伊）|![[/理想国/images/00510.jpeg]]|Ilion|
|伊纳霍斯|’I![[/理想国/images/00511.jpeg]] V|Inachus|
|伊塔卡|’I![[/理想国/images/00512.jpeg]]|Ithaca|
|伊斯梅尼阿|’I![[/理想国/images/00513.jpeg]] V|Ismenias|
|伊奥尼亚|’I![[/理想国/images/00514.jpeg]]|Ionia|
|多利亚|![[/理想国/images/00515.jpeg]] V|Doria|
|毕阿斯|B![[/理想国/images/00516.jpeg]] V|Bias|
|毕达哥拉斯派|![[/理想国/images/00517.jpeg]]|Pythagoreans|
|色雷斯|![[/理想国/images/00518.jpeg]]|Thracia|
|色雷斯人|![[/理想国/images/00519.jpeg]] V|Thracian|
|色拉叙马霍斯|![[/理想国/images/00520.jpeg]] V|Thrasymachus|
|色弥斯托克勒|![[/理想国/images/00521.jpeg]] V|Themistocles|
|西西里的|![[/理想国/images/00522.jpeg]] V|Sicilian|
|西蒙尼得|![[/理想国/images/00523.jpeg]] V|Simonides|
|**七　画**|   |   |
|麦加拉人|M![[/理想国/images/00524.jpeg]]|Megaran|
|克尔贝洛斯|K![[/理想国/images/00525.jpeg]] V|Cerberus|
|克迈拉|X![[/理想国/images/00526.jpeg]]|Chimaera|
|克法洛斯|K![[/理想国/images/00527.jpeg]] V|Cephalus|
|克勒托丰|K![[/理想国/images/00528.jpeg]]|Kleitophon|
|克洛索|K![[/理想国/images/00529.jpeg]]|Clotho|
|克罗诺斯|K![[/理想国/images/00530.jpeg]] V|Cronos|
|庇西亚|![[/理想国/images/00531.jpeg]]|Poythia|
|忒拜人|![[/理想国/images/00532.jpeg]] V|Theban|
|佛里其亚|![[/理想国/images/00533.jpeg]]|Phrygia|
|玛摩斯|M![[/理想国/images/00534.jpeg]] V|Momus|
|苏格拉底|![[/理想国/images/00535.jpeg]] V|Socrates|
|阿得曼托斯|’A![[/理想国/images/00536.jpeg]] V|Adeimantus|
|阿雅斯|A![[/理想国/images/00537.jpeg]] V|Ajax|
|阿克琉斯|’A![[/理想国/images/00538.jpeg]] V|Achilles|
|阿里斯同|’A![[/理想国/images/00539.jpeg]]|Ariston|
|阿里斯托纽摩斯|’A![[/理想国/images/00540.jpeg]] V|Aristonymus|
|阿尔戈斯|’'A![[/理想国/images/00541.jpeg]] V|Argos|
|阿尔蒂阿依俄斯|’A![[/理想国/images/00542.jpeg]] V|Ardiaεο|
|阿尔刻诺斯|’A![[/理想国/images/00543.jpeg]] V|Alcinous|
|阿尔米纽斯|’A![[/理想国/images/00544.jpeg]] V|Armenius|
|阿尔赫洛霍斯|’A![[/理想国/images/00545.jpeg]] V|Archilochus|
|阿斯克勒比斯|’A![[/理想国/images/00546.jpeg]] V|Aesculapius，Asclepius|
|阿泰兰泰|’A![[/理想国/images/00547.jpeg]]|Atalanta|
|阿特瑞得斯|’A![[/理想国/images/00548.jpeg]] V|Atreus’sons|
|阿特洛泊斯|’'A![[/理想国/images/00549.jpeg]] V|Atropos|
|阿米勒斯河，“忘记”之河|’A![[/理想国/images/00550.jpeg]] V|River of Forgetfulness|
|阿波罗|’A![[/理想国/images/00551.jpeg]]|Apollo|
|阿革俄斯|’A![[/理想国/images/00552.jpeg]] V|Argive|
|阿布德拉城|’'A![[/理想国/images/00553.jpeg]]|Abdera|
|阿伽门农|’A![[/理想国/images/00554.jpeg]]|Agamemnon|
|阿格莱翁|’A![[/理想国/images/00555.jpeg]]|Aglaion|
|**八　画**|   |   |
|拉赫西斯|![[/理想国/images/00556.jpeg]] V|Lachesis|
|佩里索斯|![[/理想国/images/00557.jpeg]] V|Peirithous|
|佩洛匹达|![[/理想国/images/00558.jpeg]] V|Pelopidae|
|佩狄卡|![[/理想国/images/00559.jpeg]] V|Perdiccas|
|佩里安得罗|![[/理想国/images/00560.jpeg]] V|Periander|
|佩莱新|![[/理想国/images/00561.jpeg]] V|Peleus|
|戴蒙|![[/理想国/images/00562.jpeg]]|Damon|
|戴达罗斯|![[/理想国/images/00563.jpeg]] V|Daedalus|
|宙斯|![[/理想国/images/00564.jpeg]] V|Zeus|
|泽尔泽斯|![[/理想国/images/00565.jpeg]] V|Xerxes|
|迪俄墨得斯|![[/理想国/images/00566.jpeg]] V|Diomede|
|命运三女神|M![[/理想国/images/00567.jpeg]]|Fates|
|朋迪斯节|B![[/理想国/images/00568.jpeg]] V|Bendis|
|弥达斯|M![[/理想国/images/00569.jpeg]] V|Midas|
|欧若得摩|E![[/理想国/images/00570.jpeg]] V|Euthydemus|
|欧律皮吕|E![[/理想国/images/00571.jpeg]] V|Eurypylus|
|**九　画**|   |   |
|品达|![[/理想国/images/00572.jpeg]] V|Pindar|
|科库托斯|K![[/理想国/images/00573.jpeg]] V|Cocytus|
|俄尔菲|’O![[/理想国/images/00574.jpeg]] V|Orpheus|
|叙拉古的|![[/理想国/images/00575.jpeg]] V|Syracusan|
|堵勒马霍斯|![[/理想国/images/00576.jpeg]] V|Polemarchus|
|派尼亚|![[/理想国/images/00577.jpeg]]|Paeania|
|派特罗克洛斯|![[/理想国/images/00578.jpeg]] V|Patroclus|
|哈得斯，冥国|![[/理想国/images/00579.jpeg]] V|Hades，house of Hades|
|哈曼提得斯|X![[/理想国/images/00580.jpeg]] V|Charmantides|
|哈朗德斯|X![[/理想国/images/00581.jpeg]] V|Charondas|
|**十　画**|   |   |
|埃及|![[/理想国/images/00582.jpeg]] V|Egypt|
|埃斯库洛斯|A![[/理想国/images/00583.jpeg]] V|Aeschylus|
|格劳卡斯|![[/理想国/images/00584.jpeg]] V|Glaucus|
|格劳孔|![[/理想国/images/00585.jpeg]]|Glaucon|
|泰米斯|![[/理想国/images/00586.jpeg]] V|Themis|
|特洛伊战争|T![[/理想国/images/00587.jpeg]]||
|索福克勒斯|![[/理想国/images/00588.jpeg]] V|Sophocles|
|海神波塞顿|![[/理想国/images/00589.jpeg]]|Poseidon|
|海女歌妖|![[/理想国/images/00590.jpeg]] V|Siren|
|荷马|![[/理想国/images/00591.jpeg]] V|Homer|
|浦吕达马斯|![[/理想国/images/00592.jpeg]] V|Polydamas|
|**十一画**|   |   |
|梅诺提阿德|M![[/理想国/images/00593.jpeg]] V|Menoitius’offspring|
|勒翁提俄斯|![[/理想国/images/00594.jpeg]] V|Leontius|
|勒塞，“忘记”女神|![[/理想国/images/00595.jpeg]]|Oblivion|
|菲尼克斯|![[/理想国/images/00596.jpeg]]|Phoenix|
|萨尔佩冬|![[/理想国/images/00597.jpeg]]|Sarpedon|
|**十二画**|   |   |
|雅典人|’A![[/理想国/images/00598.jpeg]] V|Athenian|
|雅典娜|’A![[/理想国/images/00599.jpeg]]|Athena|
|普洛蒂卡斯|![[/理想国/images/00600.jpeg]] V|Prodicus|
|普罗塔戈拉|![[/理想国/images/00601.jpeg]] V|Protagoras|
|普罗图斯|![[/理想国/images/00602.jpeg]] V|Proteus|
|普拉纳酒|![[/理想国/images/00603.jpeg]] V|Pramnian wine|
|《奥德赛》|’O![[/理想国/images/00604.jpeg]]|Odyssey|
|奥德修斯|’O![[/理想国/images/00605.jpeg]] V|Odysseus|
|奥托吕科斯|A![[/理想国/images/00606.jpeg]] V|Autolycus|
|提修斯|![[/理想国/images/00607.jpeg]] V|Theseus|
|斯特锡霍洛斯|![[/理想国/images/00608.jpeg]] V|Stesichorus|
|斯土克斯|![[/理想国/images/00609.jpeg]]|Styx|
|斯基泰人|![[/理想国/images/00610.jpeg]] V|Scythian|
|斯库拉|![[/理想国/images/00611.jpeg]]|Scylla|
|腓尼基|![[/理想国/images/00612.jpeg]]|Phoenicia|
|**十三画**|   |   |
|福库利得斯|![[/理想国/images/00613.jpeg]] V|Phocylides|
|塞里福斯人|![[/理想国/images/00614.jpeg]] V|Seriphus|
|塞尔息特斯|![[/理想国/images/00615.jpeg]] V|Thersites|
|塞蒂斯|![[/理想国/images/00616.jpeg]] V|Thetis|
|塞亚格斯|![[/理想国/images/00617.jpeg]] V|Theages|
|**十四画**|   |   |
|赛缪洛斯|![[/理想国/images/00618.jpeg]] V|Thamyras|
|赫拉|![[/理想国/images/00619.jpeg]]|Hera|
|赫拉克勒斯|![[/理想国/images/00620.jpeg]] V|Heracles|
|赫律塞斯|X![[/理想国/images/00621.jpeg]] V|Chryses|
|赫戎|X![[/理想国/images/00622.jpeg]]|Cheiron|
|赫西俄德|‘H![[/理想国/images/00623.jpeg]] V|Hesiod|
|赫淮斯托斯|![[/理想国/images/00624.jpeg]] V|Hephaestus|
|赫勒斯滂特|![[/理想国/images/00625.jpeg]] V|Hellespont|
|赫克托|![[/理想国/images/00626.jpeg]] V|Hector|
|赫罗迪科斯|![[/理想国/images/00627.jpeg]] V|Herodicus|
|**十五画**|   |   |
|墨涅拉俄斯|M![[/理想国/images/00628.jpeg]] V|Menelaus|
|潘菲里亚|![[/理想国/images/00629.jpeg]]|Pamphylia|
|潘达洛斯|![[/理想国/images/00630.jpeg]] V|Pandarus|
|**十六画**|   |   |
|默塞俄斯|M![[/理想国/images/00631.jpeg]] V|Musaeua|