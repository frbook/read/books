   

# 第十二章 美国日渐衰落的活力

本书反复证明了一点，美国最大的相对优势在于其创造性破坏的天赋。甘冒极大风险来追求更好生活的拓荒者和冒险家开垦并锤炼出了独特的美国。阿尔若·克拉梅尔（Arjo Klamer）曾称美国为“大篷车”社会，与欧洲的“堡垒”社会截然相反：美国人总是不停地迁徙，寻找新的机会，而欧洲人则建造堡垒来保护已有的财富。 [^1] 19世纪下半叶，近2/3的30岁以上的美国人曾跨越国界。相比之下，只有1/4的英国人走出了他们狭小的岛国。 [^2] 林肯任期内的司法部部长爱德华·贝茨曾在1849年写道：“我们中很少有人是这个国家的原住民，我们都是远道而来的冒险家，希望在此发财致富和扬名立万。” [^3] 虽然弗雷德里克·杰克逊·特纳曾担忧美国的拓荒精神会在1893年美国宣布封锁边界时消失终结，但实际上美国对流动性的热情丝毫未减。

美国理所应当地自认为是培养企业家的沃土，相比其他地方，在这里更容易创立公司，并且如果你足够幸运且意志坚定，还能将公司发展为商业巨头。很多伟大的美国企业家都曾是无名之辈，但他们后来创建了商业帝国：安德鲁·卡内基曾是一名身无分文的移民，洛克菲勒是一个推销蛇油的流动商贩的儿子。美国很多杰出的成功商人都是通过满足普通民众的需求积累财富的：想一想西尔斯和罗巴克为方便给地处偏僻的农民送货而创建的巨型邮购系统，或者雷·克罗克在面包和汉堡包上建立的商业帝国。在英国，伟大的企业家功成名就后便逐步减少业务并购买地产和贵族头衔，而在美国，没有比企业家更高等的贵族。

与此同时，美国曾在基础设施建设方面一路领先，这是实现现代资本主义经济所必需的重要元素。公路和运河的修建为工业腾飞奠定了基础，而且美国曾在建设现代铁路和高速公路方面处于世界领先地位。美国还是首个以相对廉价的综合国内航线覆盖全国的国家。

在很大程度上，美国的繁荣在于它认可毁灭是创造的必经之路。美国拥有全球最自由的破产法，它允许公司倒闭；全球最大的国内市场允许人们搬迁到合适的地方，获得最丰厚的工作回报。美国能够接受，在发展的进程中，不得不废弃一些城镇和关闭一些工厂。

时至今日，这样的“经典美国”仍然具有重要的影响力。在撰写本书时，全球最具价值的公司前三名（苹果、谷歌和微软），以及排在第六位和第七位的亚马逊和脸书都是美国的科技公司。美国公司为全球61%的社交媒体用户及其91%的搜索量提供服务，而且它们发明的操作系统供全球99%的智能手机用户使用。谷歌每天要处理多达40亿次的搜索量。此外，美国公司还控制着信息经济的基础设施。亚马逊几乎占据了云计算市场份额的1/3，其云服务部门在2017年扩大了50%以上。

同时，美国占据了全球金融的制高点。 [^4] 在欧洲公司萎靡不振、亚洲雄心壮志的企业家举步维艰时，华尔街投资银行的全球市场份额已增至50%。美国基金经理管理的全球资产从10年前的44%增至55%。交易所交易基金和抵押贷款证券这类最复杂的新型金融工具都是美国所创。

美国拥有世界20所顶尖大学中的15所、超过60%的全球风险资本存量。美国在全球专利中的份额已从里根当选总统时的10%增至现在的20%。尽管人们都在谈论中国崛起，但是中国最具创新力的公司，如阿里巴巴，在纽约证券交易所而非上海证券交易所上市。

除了技术和金融领域外，美国拥有数量最多的全球最佳公司：科氏工业集团、宝洁公司和强生公司可以匹敌世界上任何一家顶级公司。美国公司在经历了20世纪80—90年代的大换血后变得更强大，并通过多轮裁员和重组精简了机构。它们把低附加值的工作外包到国外，并把过去30年最具影响力的两位商业思想家的理念总结为一个制胜之道。20世纪末，时任通用电气公司首席执行官杰克·韦尔奇曾建议企业离开它们没有占据主导地位的市场。21世纪最著名的投资家沃伦·巴菲特赞扬那些建立起“护城河”的公司，这是提供稳定性和定价能力的屏障。

然而，美国这个生产力高度发达的国家却停滞不前。创造性破坏的各项衡量标准（从地域流动性、创立公司到社会对破坏的包容度方面）都呈下降趋势。美国和其他缓慢增长的成熟经济体（如欧洲和日本）对待创造性破坏的方式正日趋相同：用克拉梅尔的话来说就是一个“堡垒社会”，而且其中的大部分要素都变得萧条。

美国人口普查局的报告指出，过去30年来地域流动性一直在减弱。州际移民率从20世纪80年代起持续下降，当前的比例相比1948—1971年的平均水平下降了51%。同期的跨县流动率降低了31%，县内流动率降低了38%。流动性不足在非洲裔美国人中表现尤为显著：自20世纪上半叶从南方大量向北迁徙后，他们正在新居住地扎根。2010年，76%的非洲裔美国女性和她们的母亲在同一州生儿育女，白人女性的同一比例为65%。一项针对4800名出生于1952—1982年的非洲裔美国人的研究显示，69%的人在成年后留在了同一个县，82%的人留在同一个州，90%的人留在同一个地区，而他们上一代人的相应比例分别为50%、65%和74%。

普通美国人迁移到经济热点地区已经变得越来越难。现在，一个典型的纽约人把全美平均工资的约84%花在房租上。这使一个普通人不太可能从堪萨斯州搬去曼哈顿。在财富聚集的高地，房价也总是更高，因为很多人都想住在那里。今天的这些创意之都（尤其是旧金山）同时也是“邻避主义”之都，充斥着各种规则和限制，导致建造新房屋或创办新企业变得更加困难。据谢长泰（Chang-Tai Hsieh）和恩里科·莫雷蒂（Enrico Moretti）预测，如果搬到美国具有高生产力的城市所需的成本变得更低，那么得益于更好的工作带来的收入，美国的GDP将会提高9.5%。 [^5]

流动性下降也表现在其他方面。向上晋升变得越来越困难：斯坦福大学的拉杰·切蒂（Raj Chetty）基于对大量税收记录的研究得出，30岁的人比其父母在同年龄时收入还要高的概率，从40年前的86%降至现在的51%。 [^6] 三位美联储经济学家和他们在圣母大学的一名同事于2015年进行的一项研究表明，几十年来，人们换工作的频率一直在持续下降。其中一个原因是，解雇员工变得越来越困难（事实上，在公共部门几乎不可能这样做），而且雇主一开始就不太倾向于贸然雇用他们。尽管和很多欧洲国家相比，美国的劳动力市场仍更具流动性，但它依然可能重蹈欧洲的覆辙。一部分受保护的工人能长期保住工作，而越来越多的人徘徊在正规劳动力市场之外。

美国也正在丧失其标榜的拓荒精神。1850年，赫尔曼·梅尔维尔（Herman Melville）曾夸口道：“我们是世界的开拓者和先锋，被派往无人涉足的荒野，在新世界中开辟出一条新的道路。” [^7] 今天，在这些拓荒者的后代中，大部分人因过于害怕失败而不敢踏上任何新道路。这个问题源于学校。2013年，马里兰州的一个学区禁止大力推动儿童荡秋千、把自制食品带入学校和在校园里送生日聚会请柬等行为。 [^8] 这种现象在大学里依然可见，教授因“安全空间”和“触发警告”发起指控。类似的情况在日常生活中也屡见不鲜。麦当劳把警告标志印在咖啡杯上，注明“小心热饮烫嘴”。丘吉尔曾对他的同胞们说：“我们不曾跨越世纪，不曾漂洋过海，不曾翻山越岭，不曾踏过草原，因为我们是一群‘一碰就碎的人’。” [^9] 如今，诉讼、监管和教学方式的组合害得“一碰就碎的人”无处不在。

荆棘与坦途

交通运输方面的公共投资占GDP的比例从20世纪60年代的2.3%下降至今天的1.7%左右，该值低于欧洲，并且远低于中国。道路坑洼不平，特别是东北部地区和加利福尼亚州。与上海浦东国际机场相比，纽约的约翰·F.肯尼迪国际机场简直像个贫民窟；与中国的高铁相比，美国的火车就是蹒跚的“老爷车”。

《美国土木工程师协会2017年工作报告》（2017 Report Card from the American Society of Civil Engineers）中的数据更是加深了这种整体印象。美国9万座水坝的平均使用年限为56年。由于人口密度的不断增加，“高危”水坝的数量已上升至至少15500座。估计每年有24万根主水管破裂，浪费超过2万亿加仑的净化水。机场拥堵和航班延误每年所造成的损失将近220亿美元。美国61.4万座桥梁中有40%的建筑年龄已超过50年，1/9的桥梁存在结构缺陷。美国一半以上的船闸已使用超过50年，通过这些船闸的船只中有将近一半会出现延误。电力变压器已平均使用了40年。电力系统的布线过于陈旧，以至有时无法将剩余的电力从东北地区输送到南方。 [^10]

21世纪，材料技术和工程技术的进步让我们能够突破物理极限，见证一些令人叹为观止的伟大建筑。2008年竣工的迪拜哈利法塔高2716英尺，是世界上最高的建筑。迪拜还在修建世界上最大的机场——迪拜世界中心，预计能容纳2亿多名乘客。2005年竣工的东海大桥全长20英里，连接上海和洋山深水港，是世界上最长的桥梁之一，然而，中国人已经在规划修建第二座大桥，以应对日益增长的交通流量。令人尴尬的是，这些工程奇迹几乎很少发生在美国。

美国人发现，相比上一代人，他们创办新公司更加艰难，而且即使创办了新公司也很难将其发展壮大。新公司（成立时间在5年之内）在所有企业中的占比从1978年的14.6%下降至2011年的8.3%，而倒闭企业的比例一直维持在8%~10%之间。新公司贡献的总就业人数的比例从20世纪80年代的18.9%下降至大萧条前的13.5%。在私营公司持有股份的30岁以下人士的比例从1989年的10.6%下降至2014年的3.6%。 [^11]

公司创办比例的下降甚至蔓延至科技行业。新兴技术公司的数量在2000年达到峰值后就持续下降。首次公开发行的企业数量锐减——从20世纪90年代的每年547家降至近期的每年192家。20世纪90年代，技术型企业家常常梦想让公司上市，自己成为下一个比尔·盖茨。现在，他们梦想把公司（或至少是创意）卖给一家科技巨头。他们成了既定秩序的屈从者，而非大胆的革新者。

与此同时，最大规模的公司正在巩固其对经济制高点的控制权。苹果、谷歌、亚马逊和它们的同行主导着当今的经济，正如美国钢铁公司、标准石油公司和西尔斯–罗巴克公司曾一度主导罗斯福时代的经济一样。《财富》100强企业收入在《财富》500强企业收入中所占的比例从1994年的57%增至2013年的63%。

大公司的扩张和新公司创办比例的下降意味着经济变得更加集中。1997—2013年，美国上市公司的数量几乎减半，从6797家减至3485家。2013年，上市公司的销售额中位数是20年前的三倍。《经济学人》将经济划分为美国经济普查所涵盖的900多个行业，其中有2/3的行业在1997—2012年变得更加集中。各行各业排名前4位领军企业的加权平均份额从26%上升至32%。这种集中在知识密集型行业表现得最为明显。 [^12]

自20世纪80年代以来公司创办比例的下降并不一定表明企业家精神的消退：很多小公司都是跟风型企业，幻想着什么都不投入也能提高生产力。美国曾见证一些初创企业（如微软、亚马逊和谷歌）风起云涌，它们致力于实现行业变革。约翰·迪尔等老牌大公司也变得更具企业家精神。集中化也不一定意味着掠夺性的垄断。熊彼特认为，集中化既是成功的原因，也是成功的结果。成功的公司超越了竞争对手，才能获取暂时的垄断地位。它们把从暂时垄断中获得的超额利润投入更多的研发中，以继续在竞争中保持领先。正如熊彼特所说，大公司“在很大程度上创造了它们的优势”。

即便如此，我们仍有理由深感忧虑。这些企业设有各种“高墙”和“护城河”来保护自己免受竞争威胁。这一点尤其适用于科技巨头。它们利用网络效应来主导市场：越多人使用你的网络，这些网络就变得越有价值。它们还利用便利性来排挤潜在的竞争对手，例如苹果手机很容易和苹果平板电脑搭配使用。在购买专利和起诉竞争对手侵犯专利时，他们表现得非常积极。 [^13]

越来越多的证据表明，固化正在减缓创新在整个经济中的传播速度。熊彼特认为，资本主义充满活力的原因之一在于，成功企业“脚下没有稳固的基石”。反应迅速的追随者总是在“窃取”你的机密并加以改进。这会让领先的公司没有安全感，但对整个社会是有利的，因为这意味着新思想在整个经济中能够迅速传播。令人担忧的是，经济合作与发展组织的一组研究人员丹·安德鲁斯（Dan Andrews）、基娅拉·克里斯库奥洛（Chiar Criscuolo）和彼得·盖尔（Peter Gal）认为，好点子的传播速度比过去更慢了。 [^14] 跻身前5%的精英公司被称为“前沿公司”，和过去相比，它们能在更长的时间内保持领先地位，并提高其生产力，而其余95%的公司则停滞不前。信息技术行业正孕育出一批“超级前沿公司”：与其他精英公司相比，前2%的信息技术公司的生产力已经有所提高。与此同时，技术传播一直停滞不前的部分原因是前沿公司能聘请到顶尖的精英，并与最好的大学和咨询公司建立联系。

死于绝望

社会的病态导致大量失业，底层民众饱受其苦。在某些地方，特别是曾经作为工业革命摇篮的地区，无业成了一种生活方式。在宾夕法尼亚州的斯克兰顿，18岁以上的人中有41%已经退出了劳动力市场。在纽约州的锡拉丘兹，这一比例高达42.4%。 [^15] 无业常常导致人们走向轻度犯罪和吸毒成瘾的生活：鸦片制剂和冰毒的流行进一步缩短了人们的寿命，并加重了社会的病态。

近年来最受关注的变化之一就是，过去主要与美国黑人有关的社会问题现在正向美国白人蔓延。白人高中毕业生中，非婚生育的比例从1982年的4%上升至2008年的34%。白人高中辍学者的比例从21%上升至42%。黑人非婚生育的比例从1982年的48%上升至2008年的70%，高中辍学率从76%上升至96%。破碎的家庭产生了“贫困循环”：在缺失父亲的家庭中长大的孩子更有可能经历辍学、未婚生育和犯罪。美国的监禁率是欧洲大国的8~10倍，很大原因在于其执行严格的毒品法，判定较低级别的毒品犯罪为重罪，并将违法者长期囚禁。监禁除了每年花费美国纳税人740亿美元外，还有令人绝望的后续效应：让人无法完成学业，只能和其他囚犯交往，并给他们的声誉留下永远的污点。一项研究发现，60%的囚犯在获释一年后处于无业状态。

普林斯顿大学的安格斯·迪顿（Angus Deaton）和安妮·凯斯（Anne Case）指出，事实上，美国白人工薪阶层的预期寿命已经开始缩短，这种情况在工业革命以来首次出现。 [^16] 预期寿命的缩短是由于“死于绝望”人数的增加。毒品、与酒精相关的肝病和自杀在死亡原因中的占比正在上升，而心脏病和癌症等“中年杀手”的占比在下降。作者认为，对这一切最合理的解释是，由于高薪工作的消失和社会矛盾的积累，在经历过20世纪70年代初的鼎盛期后，“高中学历的白人工人阶层正在逐渐崩溃”。在黄金时代，美国工薪阶层可以期待稳定的生活和长期的进步。现在，他们正在被逐步边缘化，随着其健康状况的恶化，还会增加社会福利计划的负担。

停滞的原因

为什么美国曾引以为傲的活力消退了？有三种盛行的解释。第一种解释是，美国正在丧失其长久以来拥有的经济领导力。美国引领世界进行了三次伟大的教育革命——在19世纪创建了公共小学教育体系，并在20世纪创建了公共高中和大学教育体系。完成高中教育的17岁学生的比例从1900年的6.4%上升至1970年的77%。考上大学的高中毕业生的比例从1960年的45%上升至2000年的63%。据哈佛大学的克劳迪娅·戈尔丁和劳伦斯·卡茨（Lawrence Katz）估计，在1890—1970年的80年间，每10年民众的教育程度平均增长0.8年，且每年教育程度的提升为生产力和人均产出的增长贡献0.35个百分点。

从1980年起，美国逐渐失去了其教育优势，高中毕业的美国人的比例一直呈停滞或下降之势，这两种结果取决于衡量方式的不同［詹姆斯·赫克曼（James Heckman）发现，获得“真正”学校文凭的18岁学生的比例在2000年降至74%］。目前，美国的高中毕业率在发达国家中排名第11。虽然从4年制大学获得学士学位的25~34岁学生的比例从25%上升至32%，但这种上升掩盖了许多重大问题，例如，美国曾是18~24岁大学生的比例排名第一的国家，现在这一排名已下滑至第15以后。如果你查看教育程度，而不只是受教育的时间，美国的指标更加令人沮丧。根据经济合作与发展组织2013年度的学生能力国际评估计划（PISA）测试结果，美国15岁学生在阅读上排名第17，在科学上排名第20，在数学上排名第27。

从不同年龄组的比较结果可以看出，美国的相对地位在降低。在经济合作与发展组织34个成员中，55~64岁的美国人相比其他国家的同龄人完成高中学业的比例更高。24~34岁的美国人和其他4个成员的同龄人在高中毕业率排名上并列第9。美国也是唯一一个24~34岁人群的毕业率没有高于55~64岁人群的国家。

在美国教育体系的积极特性被削弱的同时，其消极特性正变得更加明显。该系统在培训非院校学生从事职业工作方面表现不佳。二战之前，纽约市的高中曾要求学生完成“作坊”课程，包括学习木工和焊接电线的技能。后来，尽管有公司抱怨缺乏熟练的体力劳动者，这些课程还是逐步被淘汰了。该系统也未能做好成本控制。从1950年起，高等教育的成本增加至原来的10倍，学生负债越来越重：目前，学生贷款债务已接近1.5万亿美元，超过了信用卡未销账款或汽车贷款的债务。

在历史上大多数时期，美国一直是全球人才最向往的国家。截至2010年，《财富》500强名单中有18%的公司（包括美国电话电报公司、杜邦、易贝、谷歌、卡夫、亨氏和宝洁）都是由移民创立的。如果加上移民子女所创立的公司，该比例应为40%。2012年，占美国人口13%左右的移民建立了硅谷52%的初创企业，对全球专利的贡献率在25%以上。移民在拥有学士学位的科学家和工程师中占比25%，在拥有博士学位的上述人才中占比47%。然而，美国对移民不断增强的敌对态度与其他国家和地区持续涌现的机会，正在阻碍未来企业家和专业人才的供应。其他富裕国家（如加拿大和澳大利亚）正在积极吸引高素质移民。印度和中国的毕业生现如今也能在国内找到更多机会。

上述内容反映了很多道理。美国在国际比赛中的排名无疑下滑了，它的众多学校也表现欠佳。然而，我们也无法指望美国保有二战结束时享有的世界主导地位，这不太现实。美国在高等教育方面依然领衔世界，世界20所顶尖大学中有15所位于美国，也比大多数国家更乐于给人第二次机会。没有证据表明如果有更多人了上大学，经济就会更景气：约有40%的大学毕业生都找不到要求大学教育背景的工作。美国并不需要更多有学士学位的咖啡师。

第二种解释是，与过去的技术驱动型革命相比，信息技术革命令人失望。19世纪末的第二次工业革命带来了广泛的创新，改变了人们生活的方方面面：汽车取代了马匹，飞机取代了热气球，电灯取代了煤油和天然气。有观点认为，信息技术革命只在狭义范围内影响了一些活动。

这缺乏说服力。信息技术革命正在触及日常生活的更多方面。苹果手机可以完成数以千计的工作：它可以帮你找到想去的地方，充当你的虚拟秘书，为你整理书籍和报纸。优步用信息革新了出租车行业。爱彼迎用信息实现了酒店行业的变革。亚马逊允许我们从海量虚拟目录中订购，并在几天甚至几小时内交付货物。据摩根士丹利估计，无人驾驶汽车每年能让美国实现5070亿美元的生产力提升，主要是因为此后人们可以只看笔记本电脑，而无须再盯着马路。

信息技术革命为我们将在制造业习以为常的生产力提升进一步延伸到服务业提供了机会。IBM和贝勒医学院开发了一套名为“知识集成工具包”（KnIT）的系统，可扫描医学文献，并为解答研究课题提出新的假设。从专利纠纷到美国最高法院审理的案件，各种各样的软件在预测法院判决结果方面通常优于法律专家。新技术正让机器和辅助专业人员能接手专业人员的很多日常任务。由初创公司肯硕开发的程序能为金融方面的问题提供解答，例如，当出现隐私泄露恐慌时，科技股票会发生什么状况？在计算机和诊断工具的辅助作用下，护士和医师助理正承担着越来越多曾经只有医生才能做的工作。在线服务和智能手机应用程序让外行无须依赖专业人士，或至少能提高他们的议价能力。在美国，每个月有1.9亿人访问互联网医疗健康信息服务平台WebMD网站——比看普通医生的次数还多。在苹果的应用商店中，教育类应用程序是除游戏之外第二大热门类别，并且慕课大规模在线开放课程（MOOC）也正吸引着数以百万计的学生。法官和律师越来越多地通过“电子裁决”处理小额索赔，这也是易贝为解决用户每年6000多万起分歧而采用的技术之一。威廉·鲍莫尔（William Baumol）等经济学家曾认为，服务行业的生产力增长在本质上低于制造业的生产力增长。与此类担忧相反的是，现在生产力的增长不再受限于市场构成（如制造业对服务业），而是取决于创新者开发新技术的能力。值得注意的是，保罗·戴维认为，在20世纪20年代企业重组工厂之前，电力对生产力的影响并不显著。信息技术革命对生产力的影响可能才刚刚开始，尤其是对服务业的生产力而言更是如此。

第三种解释是，劳动力的增长速度正在放缓。新生代工人浪潮的到来曾一再推动了美国经济的发展——先有一批放弃农场到城市从事更高薪工作的农民，再有一批放下家庭无偿工作加入劳动力大军的女性。现在，我们面临的问题正好相反：工人纷纷离开劳动力队伍，申领养老金。达到退休年龄的人口占总人口的比例从1940年的6.8%增至1980年的11.3%，2010年又增至13.1%，并将在下一个25年内迅速增长。

这甚至比信息技术的论点更缺乏说服力。最大的争议问题是婴儿潮一代开始步入退休年龄，另外还有一个更微妙的问题是：人们的工作时间比过去的工作时间更长，一部分原因是因为他们的寿命延长了，还有一部分原因是因为工作不像过去对体力的要求那么高。一些国家（如瑞典和英国）正根据人口寿命的延长逐步提高退休年龄。

那么，国家为什么会停滞不前？最重要的原因是权益支出抑制了生产力的发展，即所有美国人都有权享受这套社会福利（主要是社会保险、国家老年人医疗保险制度和医疗补助计划）。在1935年引入社会福利后的30年内，权益支出除了在二战后出现过一次暴涨外，其余时间增长都比较平稳，随后开始猛增：1965—2016年以年均9%的增长率增长。用于投入社会福利的GDP份额也从4.6%跃升至14.6%，发生了巨大的改变。

现在，福利支出让美国负重前行。55%的美国家庭至少能从一项主要的联邦福利项目中获得现金或物质援助。几乎所有65岁以上的美国人都享有社会保险和医疗保险。80%的生活在单身母亲家庭的美国公民享有权利收益，58%的美国儿童有权申领权益金。约1.2亿美国人（2/3的受助者）能从两项以上的项目中申领权益金，约4600万人（近1/3的受助者）从三项以上的项目中申领权益金。

该权益制度和需求的关联不大：超过90%的社会保险救助都用于根据年龄而非需求划分出来的单一人群，即65岁以上的老年人。政府每年为社会保险和老年人医疗保险拨款5万美元，给那类典型的在2016年满66岁退休的已婚夫妇，这只比美国普通中等收入的家庭少6000美元。而且这些退休人员经历过美国历史上最繁荣的时期，预期寿命也比过去退休的人更长。供养这荣耀一代的重担将落在当代工人的身上，他们的机会比长辈少得多，同时还得养育自己的子女。

大部分权益支出具有自动生成的性质：根据固定公式，有人登记支出就增长。因此，权益支出按固定比率增长，与经济运行状况或谁入主白宫无关，总统可以随其心意去谈论小政府的好处。核心权益项目的规模必然会随着人口年龄、物价和医疗支出的增长而扩大。三大基本权益项目（社会保险、老年人医疗保险和医疗补助）现在占联邦预算的近50%。未来无论哪个党派取得政治优势，该比例预计还会上升。

然而，总统可以对增长速度产生影响。从1965年起，权益支出在共和党总统执政期间（年增幅10.7%）反而比在民主党总统执政期间（年增幅7.3%）增长更快。克林顿不仅比里根把社会福利支出管控得更好（克林顿时期的增幅是4.6%，里根时期的增幅为7.3%），他还在福利体系方面进行了大刀阔斧的改革（虽然不可否认这一改变是在共和党的支持下进行的）。小布什在没有提供资助手段的情况下增加了新药物福利。像克林顿这样的财政保守型总统绝不会做这样的事。两党都参与了竞争激烈的拉票（一些共和党人为愿意花费公款辩护，称如果他们不花，这些钱也会被民主党人花掉）。即使自视为小政府保守主义者的选民在权益问题上也很坚持——茶党积极分子让奥巴马不要插手“他们的”医疗保险，最能表明他们的立场。

这就指出了权益的一个矛盾之处，也是它难以进行改革的原因。美国人倾向于认为他们“赚到了”自己的权益金：他们只是在拿回自己放在信托基金里的钱和利息。他们在脑海中把受纳税人资助的“补贴”（可以中断）和“拿回属于自己的东西”（神圣的行为）区分得非常清楚。在美国退休人员协会（AARP）的一则广告中，一名退休人员声明：“我赚得了自己的老年医疗保险和医疗补助。”这其实是一种幻觉。美国人整体上取出的钱比存入的多，要补齐实际差额就得给第三方永久性增税，或者让第四方的福利永久性减少。 [^17] 没有这样的改变，社会保险信托基金到2034年就会透支，医疗保险基金也会在2029年用尽。“拿回自己的钱”是一种极强烈的幻觉，让改革变得几乎不可能。雨果曾说，在政治上，没有什么比一个恰逢其时的信念更加有力。他错了：在政治上，最有力的是一种高度依赖补贴，却让领受者相信已由自己完全支付的福利。

更重要的是，联邦福利支出正在挤压可自由支配的支出。施托伊尔勒–勒佩尔财政民主指数用于衡量自动生成和可自由裁量的美国财务决策的数量。1962年，约2/3的联邦支出都是可自由裁量的。在20世纪60年代中期，由于约翰逊福利计划，该比例开始剧烈缩减，在1982年降至30%以下，2014年，该比例维持在20%左右，但预计会在2022年降至10%以下。

联邦权益支出正在挤占国内储蓄。图12–1展现了惊人的统计稳定性：人均社会福利（权益总支出）与国内储蓄总额（两者都以占GDP的比例表示）之和的曲线在1965年后走势不明。权益总支出在GDP中所占比例的稳步上升，与国内储蓄总额在GDP中所占比例的总体下降相对应。这意味着权益支出不仅在挤占国内储蓄，而且是以金额对等的方式实现的。

![[/繁荣与衰退_艾伦・格林斯潘/images/00027.jpeg]]

图12-1 储蓄和政府社会福利季度图表（1965年第一季度至2017年第四季度）

生产力（每小时劳动产出）的主要驱动力是资本存量（或累计投资净额）。国内投资总额（投资净额加折旧）由（1）国内储蓄总额和（2）1992年以来贷自海外的净储蓄（主要是美国的经常账户赤字）提供资金。从海外借款不能无限期持续下去：截至2016年第二季度，此类债务已累积至8万亿美元。国内投资最终必须依赖国家在储蓄和资本存量投资上的意愿，但这种意愿正在削弱。令人担忧的是，有大量统计证据表明，福利支出的激增大部分来源于政府通过税收抢占私人储蓄进行资助，该储蓄本可为国内资本投资和生产力的提升提供资金。

商业信心和投资意愿最重要的衡量指标之一就是资本支出比率，即公司选择把多少比例的流动现金转化为非流动的设备或建筑资产。有些令人惊讶的是，我们发现有两个金融统计数据可以“解释”未来两个季度内资本支出比率近3/4的变化，而两个季度与投资拨款和其实际支出的时间差大约相等。第一个是周期性调整的联邦预算赤字或盈余，可以衡量私人投资支出的挤入和挤出程度。第二个是美国财政30年期国债收益率与5年期国债收益率之差。这代表与永久性资产的实物资本投资相关的不确定性不断增长，例如，软件的预期寿命是3~5年，工业设备的预期寿命是19年。从1970年起，联邦预算赤字或盈余在统计上占资本支出比率变化的一半，另一半在收益率差价和其他未知因子间均匀分布。此外，由于资本存量是生产力（以每小时劳动产出来衡量）的主要决定因素，如果用于资本投资的储蓄继续被转用于社会福利支出，生产力提升将进一步受损。

比起20世纪30年代以来的大多数时间（不包括二战时的非常规环境），现在，企业更不愿意做长期投资。不确定性增长的因素有几点：美国增长的赤字、愤怒的政治活动、令人失望的经济增长率。权益危机加重了赤字，减缓了生产力增长速度，让不确定性雪上加霜，结果阻碍了GDP的增长和政治的发展（见图12–2）。

更糟的还在后面：在未来20年，65岁及以上的美国人数量会增加3000万，而适龄（18~64岁）美国工人的数量预计只会增加1400万。庞大的退休人群加上数十年权益自由化和扩张所遗留的问题，会让美国遭遇前所未有的财政挑战。过去，高额的联邦支出和债务的增加大部分是战争导致的，战争总会结束，债务也会随着军事花费的萎缩而减少，但美国即将进入一段由透支的权益支出引发的高额联邦支出和债务的时期，这一定会发生，在不远的将来无可避免。如果任其发展，我们在未来就会面临债务的增长和反复的财务危机。

第三个问题是监管的加强，表现为对企业家的两种最具价值的资源征税：尝试新事物的时间和能力。20世纪50年代，列出所有新条例的《联邦公报》（Federal Register ）以年均11000页的速度更新。21世纪的头10年中，它年均增加73000页。联邦法律法规现在增至1亿多字。国家和地方监管条例又另外增加了20亿字。《多德–弗兰克法案》有2319页。2010年出台的《平价医疗法案》有2700页，包含28字对“高中”一词的定义。医疗保险有14万个赔偿类别，包括针对“宇宙飞船失事”的21个单独类别。此外，美国免税代码包含340万字。这意味着自由之地已经变为世界上监管最多的社会，例如，2013年，在经济合作与发展组织35个成员中，美国的产品市场监管排在第27位。

![[/繁荣与衰退_艾伦・格林斯潘/images/00015.jpeg]]

图12-2 资本存量和生产力（1948-2016年）

![[/繁荣与衰退_艾伦・格林斯潘/images/00019.jpeg]]

图12-3 现金流业务转为固定资产的比例（1929-2017年年度绘图，萧条期用阴影表示）

![[/繁荣与衰退_艾伦・格林斯潘/images/00007.jpeg]]

图12-4 美国联邦法规页数（1975-2016年）

2001年安然公司的倒闭让美国的过度监管进一步加重：从20世纪70年代末开始一直普遍存在的解除管制的呼声变得不合时宜。2002年，在安然公司倒闭后，《萨班斯-奥克斯利法案》修订了一般公司的治理章程。2010年的《多德-弗兰克法案》试图对金融服务业进行更缜密的监管，详细监管章程有上千页。在最近经济减速的整段时期内，监管机构不断壮大，变得更加劳民伤财。证监会的预算从1995年的3亿美元升至2018年的16亿美元，司法部相比2000年前更频繁地使用1977年的《反海外腐败法》来监管在海外有可疑行为的公司。根据该法案生成决议的平均花费从2005年的720万美元增至2014年的1.57亿美元。

过度监管让美国无法展现其解决问题和鼓励创新的社会形象。大多数基础设施工程延期数年，因为官员不得不突破重重关卡（尤其是现今在环境保护方面的关卡）。在大萧条时期，修建金门大桥耗时4年。今天，开展更大的高速公路项目需要花10年扫清各种官僚障碍，才能让工人正式施工。纽约港务局决定重修连接斯塔滕岛和新泽西州的贝永大桥，让大型邮轮可以从桥下驶过。做出该决定后，他们得去19个不同的政府部门完成47项审批，从2009年一直忙到2013年年中。经历了整个过程的港务局官员琼·帕帕耶奥吉斯（Joann Papageorgis）说道：“这个过程不是要解决问题，而是要制造问题。说‘不’能为他们省去很多麻烦。” [^18]

过度监管迫使企业创始人忍受噩梦般的经历，去拜访不同的政府部门，无止境地填写复杂的表格。比如，要想在纽约开个餐馆，你得和11个不同的市政机构打交道，这花费了美国人大量的时间和金钱。一半美国人雇用专业人士来处理税收，相比之下，只有少数英国人这样做。美国甚至把赚钱做慈善的孩子变成罪犯。2011年，乡镇官员在马里兰州贝塞斯达美国高尔夫公开赛附近关闭了一个儿童开设的柠檬汁售卖点，因为这些孩子为儿科癌症患者筹款，却没有销售许可证。 [^19]

公司监管不可避免地给小企业强加了难以承受的负担，因为合规需要很高的固定成本。拉斐特学院的妮科尔·克雷思（Nicole Crain）和马克·克雷恩（Mark Crain）计算得出，对雇员人数在19人以下的企业，联邦法规遵从的单个雇员花费是10585美元；而雇员人数在500人以上的，相应的花费为7755美元。美国系统的复杂性也对小公司不利。大型机构能雇得起专家，可以在这些法规的高山中循路绕行，的确，《多德–弗兰克法案》很快被称为“‘律师和咨询师’的完全就业法案”。通用电气公司的税务部有900名雇员。2010年，它几乎没交税。小公司不得不花钱从外面请律师，且总是担心与国内收入署自相矛盾的条款发生冲突。世界经济论坛基于一项针对小企业的调研，在合规简易度方面把美国排在第29位，居然在沙特阿拉伯之后。

尽管过度监管能为大公司带来短期利益，却会阻碍它们的长远发展，让其变得更加官僚化，缺乏创新。知名公司会扩大其处理合规业务的部门规模而非创新部门规模，雇用高级经理来花时间谄媚政客、拉拢官僚主义者而非改善产品。监管最大的代价在于它招致资本主义的官僚化，从而扼杀了创业创新精神。

一个监管方面尤其令人沮丧的例子就是许可证制度的兴起。1950年，只有5%的工作需要许可证。到2016年，该比例已升至30%（英国的相应比例是13%）。许可证制度把触角伸向了对健康和安全不存在潜在威胁的职业，比如花商、手工艺者、摔跤选手、导游、冷冻甜食卖家、二手书商和室内设计师。 [^20] 获取许可证非常耗时。在得克萨斯州，想成为理发师的人要用一年多的时间学习理发，想成为假发制作师的要上300个小时的课，并通过笔试和实作考试。亚拉巴马州迫使美甲师在参加实作考试前听完750个小时的介绍。在佛罗里达州，如果你没有拿到4年制大学的学位，完成两年的学徒期，并通过历时两天的考试，就不能成为室内设计师。明尼苏达大学的莫里斯·克莱纳（Morris Kleiner）计算得出，许可证能为其持有人带来约15%的收入增长，换句话说，它对工资的影响和工会成员资格差不多（对于获得许可证的工会成员，时薪能增加24%）。克莱纳还认为，许可证制度减缓了行业创新：通过比较在一些州受到监管而在另一些州不受监管的职业，他发现，1990—2000年，无监管的职业比有监管的职业新增就业岗位多出20%。职业许可证的增长也减少了地域流动性，因为取得新许可证需要付出大量的时间和努力。

监管的兴起要追溯至新政时期，罗斯福的智囊团热切地相信政府应该在经济决策中拥有更大的控制权，但其发展被证明只是自我强化：新“监管者”团体迅速发现需要解决的（真实的或想象的）“问题”，而后这些受政府资助的解决方案需要更多官员来进行监管。如此这般，成了永无止境的循环。

特朗普的出现

停滞不可避免地让美国民众的情绪消沉，并波及政局。在2008年金融危机以来的所有调研中，大多数选民都告诉民意调查人员国家正行进在错误的轨道上。像茶党这样的异见政治运动平地而起，抓住了公众的想象力。2016年，从未涉足公职的房地产大亨特朗普以“让美国再次伟大”的竞选口号击败了经验最丰富的政治家之一希拉里，当选美国总统，震惊了全美、全世界，或许还有他自己。在历史上经历与特朗普最接近的是安德鲁·杰克逊总统，他是被公众对“普通人”的热情和对贵族统治集团的厌恶浪潮推上总统之位的，但杰克逊的平民主义与他对金本位制的坚定支持并存。事实上，他是纸币的反对者（众所周知，他是美国第二银行的敌人），他要求在购买政府土地时必须支付实物。特朗普的平民主义不包含这样的秩序。

在特朗普当选后，经济开始从近10年的停滞中复苏。股市在他获胜后立刻创下新高，投资者预期会出现更好的营商环境。失业率持续下降。蓝领工资增长超过了其他群体。财富效应开始显现：房价的持续上涨伴随着股价和商业资产价格的激涨，给GDP带来了大幅提升。特朗普解决了商界一些最为紧迫的问题。一方面，联邦机构几乎停止发布新的监管条例，尽管在很大程度上这能否成为成熟的政策及总统的管理会不会产生失败的结果依旧不明朗。他的税法于2017年12月22日由共和党主导的国会通过并签署成为法律。该法大大降低了公司税税率。特朗普的税法似乎受到了爱尔兰共和国的启发，爱尔兰把公司税税率从1998年的32%减至2003年的12.5%。另一方面，他在贸易上采取了一个危险的政策，退出跨太平洋伙伴关系，对从各国（尤其是中国）进口的钢材增收25%的关税，对进口的铝材增收10%的关税，并威胁称要对从中国进口的货物再增收1500亿美元的关税。

国家的深层问题还在发酵。现有公司继续为自己架设重重防护，尤其是因为特朗普罕见地未能抽干他们所掌控的“沼泽地”。监管依然繁重。只有当投资者能拿到合理的收益率时，被特朗普的税务改革激起的资本撤回才能促进国内资本投资，否则，增加的现金流会最终成为股东的红利和增加的流动资本。特朗普同时减税和刺激消费（尤其是在基础设施建设方面）的政策一旦实行，终将增加债务，迫使政策制定者刹车，尤其是白宫对解决国家日益膨胀的权益支出问题表现得漠不关心（见图12-5）。

![[/繁荣与衰退_艾伦・格林斯潘/images/00005.jpeg]]

图12-5 公众持有的联邦债务（1789-2017年,国会预算局预测2018-2037年）

在本书撰写过程中，有越来越多的迹象显示美国正处于滞涨初期，这是停滞和通货膨胀的危险结合，一开始看起来充满活力，最后会成为一片死寂，就像20世纪70年代曾发生过的一样。创造历史的低失业率正带来工资方面的压力，但历史最低生产力的遗留问题依然尾随。2011—2016年，非农经济每小时劳动产出的年增长率不到1%。尽管目前的情况有所好转，美国活力日渐衰落的深层原因依然不明。

[^1]:  Deirdre Nansen McCloskey, Bourgeois Equality: How Ideas, Not Capital or Institutions,En riched the World (Chicago: University of Chicago Press, 2016), 500.

[^2]:  Tyler Cowen, The Complacent Class: The Self-Defeating Quest for the American Dream(New York: St. Martin’s Press, 2017). 25. Cowen’s book has been an invaluable source of data and references for this chapter.

[^3]:  Oscar Handlin and Lilian Handlin, Liberty in Expansion 1760–1850 (New York: Harper &Row, 1989), 13.

[^4]:  See Patrick Foulis, “The Sticky Superpower,” Economist, October 3, 2016.

[^5]:  Chang-Tai Hsieh and Enrico Moretti, “Why Do Cities Matter? Local Growth and Aggregate Growth,” NBER Working Paper No. 21154, National Bureau of Economic Research, May 2015; Cowen, The Complacent Class, 8.

[^6]:  Raj Chetty et al., “The Fading American Dream: Trends in Absolute Income Mobility Since 1940,” NBER Working Paper No. 22910, National Bureau of Economic Research, March 2017.

[^7]:  Handlin and Handlin, Liberty in Expansion, 141.

[^8]:  Philip K. Howard, The Rule of Nobody: Saving America from Dead Laws and Broken Govern ment (New York: W. W. Norton, 2014), 33.

[^9]:  Thomas Friedman and Michael Mandelbaum, “That Used to Be Us”: What Went Wrong with America and How It Can Come Back (New York: Little, Brown, 2011), 26.

[^10]:  Howard, The Rule of Nobody, 13.

[^11]:  Robert J. Gordon, The Rise and Fall of American Growth: The U.S. Standard of Living Since the Civil War (Princeton, NJ: Princeton University Press, 2016), 585.

[^12]:  “Too Much of a Good Thing,” Economist, March 26, 2016.

[^13]:  Adrian Wooldridge, “The Rise of the Superstars,” Economist, Special Report, September 17, 2016.

[^14]:  Dan Andrews, Chiara Criscuolo, and Peter Gal, Frontier Firms, Technology Diffusion and Public Policy: Micro Evidence from OECD Countries, OECD Productivity Working Paper,2015.

[^15]:  Gordon, The Rise and Fall of American Growth, 629.

[^16]:  Anne Case and Angus Deaton, “Rising Morbidity and Mortality in Mid-Life Among White Non-Hispanic Americans in the 21st Century,” Proceedings of the National Academy of the United States 112, no. 49; Anne Case and Angus Deaton, “Mortality and Morbidity in the 21st Century,” Brookings Institution, Brookings Paper on Economic Activity, March 23, 2017.

[^17]:  The 2017 Annual Report of the Board of Trustees of the Federal Old-Age and Survivors Insur ance and Federal Disability Insurance Trust Funds, 199.

[^18]:  Howard, The Rule of Nobody, 8.

[^19]:  Ibid., 21.

[^20]:  “Rules for Fools,” Economist, May 12, 2011.