## 五光十色的国家（代序）

一

我出生的城市阿雷基帕[^1]位于秘鲁南部安第斯山的一座山谷里。这座城市以其教权主义、造反精神、律师、火山、一望无云的蓝天、美味大虾、地方主义以及“下雪”而名扬全国。所谓下雪，是神经官能症的一种暂时性的症状。某天，一个最为温顺的阿雷基帕人突然会对人家的问候不予理睬；连续几个小时把脸拉得长长的；能干出最古怪的事，能说出最古怪的话；意见稍微不一致，他就有可能在最好朋友的颈后砍上一刀。对此，没有人感到奇怪，也没有人生气，因为大家都知道那个人正在“下雪”，而他明天又会像往常那样成为温和而无害的人。虽说在我出生的第二年，我的家人就带我离开了阿雷基帕，而且从此以后我再也没有在阿雷基帕居住过，但我一直认为我是一个阿雷基帕人。我也认为那些在秘鲁流传的针对我们的玩笑，譬如说我们骄傲自大、令人反感甚至发疯，都出于嫉妒心。我们讲的西班牙语非常纯正；我们的建筑奇迹圣卡达莉娜修道院曾吸引了五百名妇女移居；我们的城市曾是秘鲁历史上大规模地震和多次革命的舞台。难道不是这样吗？

从一岁到十岁，我住在玻利维亚的柯恰潘巴市。在这座城市，我既天真又幸福。我还记得我所干的事和我认识的人，但最难忘的是我阅读过的书籍：山道坎[^2]的故事，诺查丹玛斯[^3]的作品，《三个火枪手》，卡略斯特罗[^4]的作品，《汤姆·索耶历险记》，《辛巴达航海旅行记》，海盗、冒险家和匪徒的故事，浪漫的爱情故事，还有我母亲藏在床头柜中的那些诗歌（其实我并不懂，只是视为禁果的诱惑）。阅读这些书籍是我最美好的时刻。我把喜欢的书全看完了，这太令人难熬了。有时我自己想出某些新的篇章，或是改变某一作品的结局。这种对他人作品进行的“续作”或“补充”就是我最初的写作，也是我写故事才能的最初表现。

像所有移居他乡的家庭一样，侨居异国增强了我们的爱国心。直到十岁，我一直坚信生为秘鲁人是我最大的幸运。我头脑中的秘鲁与其说是同现实的秘鲁相联系着的，不如说是同印加帝王和征服者的那个国家相联系着的。只是到了1946年，我才真正地认识了现实的秘鲁，那一年，我们家从柯恰潘巴迁到了皮乌拉[^5]，因为我的祖父被任命为该市的地方长官。我们走的是陆路，在阿雷基帕作了短暂停留。我还记得，在踏上故乡土地的那一刻，我是那么激动；我也还记得，我的伯父对我是那么宠爱。伯父叫埃德华多，是个单身汉，也是一位对宗教极为虔诚的律师。他有个女仆，叫伊诺森西娅。他的生活犹如西班牙内地的绅士，穿着整齐，有条不紊。在那旧式家具、古老挂像和古旧杂物中间，他也渐渐地衰老了。我也记得在卡玛纳奇耶，当我第一次看到大海时的那股兴奋劲儿。我的祖父母被我磨得没办法，不得不把汽车停下来，让我在那荒凉的海滩上扎了个猛子。但那次海浴洗礼并不成功，因为一只螃蟹夹了我一下。尽管如此，对秘鲁海岸这份一见钟情式的情谊却延续了下来。这三千公里的沿海沙漠几乎没有被从安第斯山流下的河水渗浸过，只是被太平洋的海水冲刷着，却受到了一些人的恶意中伤。那些誓死捍卫印第安传统的人，那些仇恨一切西班牙事物的人，都指责我国的沿海地区轻浮、媚外。他们认为秘鲁的政治经济中心由山区转移到沿海（即由库斯科[^6]转移到利马）是一种不幸，因为这一转移产生了令人窒息的中央集权主义，使得秘鲁变成了一只大蜘蛛：这个国家的头部（即首都）硕大无朋，四肢却细小羸弱。一位历史学家把利马和沿海称做“反秘鲁”。而我，作为一个阿雷基帕人，也就是说，作为一个“山区佬”，在这场争论中本应该站在安第斯山一边来反对沿海沙漠地区的。然而，如果让我在沿海地区、安第斯山区和亚马孙森林地区（按经度划分的秘鲁三个地区）三者之中进行选择的话，很可能我是站在沙漠和海浪一边的。

沿海是印加帝国的外围地区，其文明也是从库斯科辐射过来的。虽说印加文明在西班牙征服之前不是唯一的秘鲁文化，但可以说是一种最强有力的文化，它从秘鲁一直延伸到玻利维亚、厄瓜多尔以及智利、哥伦比亚和阿根廷的一部分。它只短短地存在了一个半世纪。在此期间，印加帝王们征服了几十座城镇，修建了道路和灌溉工程，修筑了城堡和要塞，并且建立了行政系统，使得产出足以养活所有秘鲁人。这一点，以后的任何政权都未能做到。尽管如此，我从未对印加帝王们有过好感。虽说他们留下的纪念性建筑物，譬如马丘比丘[^7]和萨克萨瓦曼[^8]使我惊服，但我一直在想，秘鲁的愁苦（即我们性格的突出特点）也正是根源于印加帝国。那是一个军团化的官僚主义社会，人像蚂蚁一样被组织起来，一台无所不能的压缩机粉碎了人们的一切个性。

印加帝王们为了控制其统治下的人民，施展了最狡猾的诡计：他们自命为神，分封诸侯，移民他乡，把村镇居民迁离故土，“嫁接”在遥远的异乡。流传至今的用克楚亚语[^9]写成的最古老的诗歌就是表现这些人在异乡感到惆怅而怀念自己失去的故土的悲歌。远在苏联大百科全书和乔治·奥威尔的小说《1984》出版前五个世纪，印加帝国就施行了政治上的古为今用：每个印加帝王登上宝座时，都伴有一批“阿矛塔”（即学者），他们负责修改历史，以证明印加的历史是在当今帝王的统治下才达到高峰的，先帝们的一切丰功伟绩都归功于现今的帝王，导致要想恢复被神秘歪曲了的历史就成了不可能。印加帝王们有一种相当精密的计数法，即结绳计数法，但他们没有文字。有一种理论认为，这些帝王们根本不想有文字，因为文字对他们那样的社会将构成一种危险。我一直认为这一理论是有道理的。印加帝王们的艺术是严峻的、冰冷的，既缺乏想象力，也没有印加帝国之前各种文化（如纳斯卡[^10]文化和帕拉卡斯[^11]文化）所表现出的技艺。精致非凡的羽毛毯、图案神秘的纺织品就是上述两个文化的产物，它们至今仍然保持着鲜艳的色彩和魅力。

继印加帝国之后，压榨秘鲁人民的另一台压缩机是西班牙的统治，征服者把他们的语言和宗教带到了秘鲁，通行至今。对殖民统治不加区别地备加颂扬与对印加帝国加以理想化，都是荒谬的。殖民统治把秘鲁变成了包括若干共和国在内的总督府所在地，把利马变成了拥有豪华宫廷、重要学术活动和典礼活动的首都，但也意味着宗教愚昧主义，即宗教裁判和检查制度。这一制度甚至禁止阅读某种体裁的文学作品，即小说，且对无神论者和异教徒进行迫害。所谓无神论者和异教徒，在许多情况下不过是那些敢于思考的人罢了。殖民统治同时也意味着剥削印第安人和黑人，以及建立一个在经济上拥有特权的阶层。这一阶层至今仍然存在，并把秘鲁变成了一个贫富极端分化的国家。独立仅仅是政治上的现象，而这个少数人享有现代化生活的特权、多数人愚昧贫困的社会毫无改变。印加帝国、殖民统治和共和国这三个时期的历史使我认识到，我们生活在其统治下的历届政权根本无力把秘鲁人之间的两极分化缩小到可以容忍的程度。这一创伤是不可能用任何纪念性的建筑物、显赫的战功和辉煌的宫廷加以补偿的。

当然，我刚从玻利维亚返国时根本没有想过上述的一切。我家的习惯是按《圣经》办事，每次搬家都是全体一道搬，叔伯、姑婶、堂兄弟姐妹跟在家庭的支柱即祖父母的后面一道搬。就这样，我们到了皮乌拉，这是一座四郊都是荒漠的城市。这是我在秘鲁的第一次经历。在萨雷斯教派中学里，我的同学们嘲笑我，因为我的口音是山区人口音，发r和s两个音时，口中嘘嘘作响，也因为我相信婴儿是白鹳从巴黎衔来的。他们向我解释说，这种事不会在空中发生。

我的脑海中充满了我在皮乌拉生活的那些岁月中的形象。皮乌拉人是外向型的，很外向，爱开玩笑，也很热情。那时，皮乌拉人喝的是质量很好的玉米酒，跳的是当地的丹德罗舞。乔洛和白人之间的关系比起其他地区来也不那么紧张。皮乌拉人不拘礼节、喜好热闹的性格缩短了人与人之间的社交距离。恋人们在姑娘的阳台下奉献小夜曲，遭到阻挠的就把姑娘抢走，把她劫持到某个庄园住上一两天。等双方家庭和解之后，就是一个幸福的结局：敲敲打打地在教堂里举行宗教婚礼。抢亲是事先宣布的，也是受到庆贺的，就像庆贺河水来临一样，因为皮乌拉的河水一年只来那么几个月，给种棉花的庄园带来了生命。

皮乌拉这个美好的城市，充满了各种奇闻轶事，足以点燃人们的想象力。曼加切利亚[^12]区都是些泥巴茅草盖的房子，玉米酒的质量最好；加依纳塞拉区则位于河流和屠场区之间。两个居民区互相仇恨，甚至有时双方进行“野战”。还有“绿房子”，那是该市的第一家妓院，是在荒漠中建立起来的，每晚灯火辉煌，人声嘈杂，人影憧憧。萨雷斯教派的神父对“绿房子”的存在大发雷霆，我却感到惊异，受到吸引。我连续几个小时地谈论它，偷看它，对里面发生的事进行想象。“绿房子”是不结实的木质结构，曼加切利亚区的一支乐队在里面演奏，皮乌拉人前去吃饭、听音乐、谈生意，还有做爱。一对对的男女就在露天里、星光下、沙地上做爱。这是我童年时代最富有诱惑力的回忆，《绿房子》就是从这个回忆中产生的。在这部小说中，我企图通过妓院在皮乌拉人的生活和想象中所引起的混乱和一群冒险家在亚马孙河流域的所作所为及其不幸遭遇，以虚构的方式把秘鲁两个相距遥远、差别很大的地区——沙漠地区和森林地区——联结起来。我的第一部作品《首领们》中的九个故事也是受到了我对皮乌拉的回忆的驱使而写的。这部短篇小说集出版之后，有些评论家认为，书中存在“欺弱凌小”这一具有拉美特点的影子。是否如此，我也不知道。但我知道，与我同龄的秘鲁人是在温柔的暴力或粗暴的柔情中成长的。我只是企图把这一点在我最初的几个故事中再现出来。

二

我是在童年刚刚结束的时候到达利马的，从一开始我就厌恶这座城市，因为在这个城市里，我是相当不幸的。我的父母早就分开了，十年后重归于好。同父亲住在一起，就得同祖父母和叔伯们分开，就得服从一个我并不熟悉而又极为严厉的人强加给我的规矩。我对利马最初的回忆，总是同不愉快的经历联系着。我们当时住在玛格达雷娜区，那是典型的中产阶级居民区。每当我考了好分数，就到住在观花埠[^13]的叔伯们的家里去度周末，这是对我的奖赏。观花埠位于海边，也更为繁华，在那里，我结识了一群与我同龄的男孩和女孩，我同他们一起做少年时代的游戏，这就是所谓“有了自己的天地”。我等于另外有了一个家庭，它就位于街角处。我们一起踢足球，偷着吸烟，学跳曼波舞，还向女孩求爱。同我们后面几代的人相比，我们可以说老实得像天使。今天的利马青年第一次领圣餐后就立即做爱，尚在变声期间就吸了第一口大麻，而我们当时根本不知道毒品的存在。我们干的调皮事只不过是偷偷地去看被禁的影片（即被教会检查制度列为“不适合女士”的影片），或是在星期六家庭聚会之前在街角的店铺里喝上一杯“上尉”（即开胃酒与皮斯科酒的混合液，被认为是有毒的），因为家庭聚会上从来不供应烈性酒。我还记得，我们这些十四五岁的男孩曾经进行过一次很严肃的讨论，讨论在星期天下午场的电影院里如何正正经经地吻自己的恋人。被吉亚柯莫·卡萨诺瓦[^14]大言不惭地称之为“意大利式”的接吻方式（即用舌头接吻），作为不赦之罪被一致排除了。

当时（20世纪40年代末）的利马还是一个不大的城市，安全，静谧，但注重表面。人们住在互不往来的居民区里：富人、有钱人住在奥兰迪亚区和圣伊西德罗区；收入较高的中产阶级住在观花埠；收入较低的中产阶级住在玛格达雷娜区、圣米格尔区和巴兰科区；穷人则住在维多利亚区、林塞区、桥下区和波尔维尼尔区。我们这些特权阶层的孩子从来见不到穷苦的孩子，甚至连他们的存在都未发觉。穷孩子们住在别处，住在自己的区里，住在远郊那些犯罪层出不穷的危险地方。我们圈子里的孩子如果不离开利马，可能一生都认为自己是住在一个只讲西班牙语的国家，一个只有白人和印欧混血人的国家，根本不知道还有几百万（全国人口的三分之一）印第安人讲克楚亚语，过着完全两样的生活。

我很幸运能在某些方面打破这个界限。我现在认为这是一种幸运，但是在当时（1950年）简直是一场悲剧。我父亲早就发现了我在写诗，因而很为我的前途担心，因为他认为一个诗人注定要饿死，同时也为我是否缺少“男子气”而担心。因为某些圈子认为，所有的诗人都搞同性恋。这一说法流传颇广。为了防止我陷入此种险境，他认为最理想的抗毒素就是莱昂修·普拉多军事学校，于是我在这个军校里住校学习了两年。莱昂修·普拉多是秘鲁社会的一个缩影，进此学校的有特权阶层的孩子（因而他们的父母等于把他们送进了教养所），有中等阶层的孩子（他们想学得军事专业），也有贫困阶层的孩子（因为军校有助学金制度，这为最贫穷人家的孩子打开了大门）。这是秘鲁为数不多的富人、穷人、不富不穷的人兼收，白人、乔洛、印第安人、黑人、华人、利马人、外省人并蓄的学校之一。与外界隔离，遵行军事纪律，还有那粗野、凶暴、以强凌弱的气氛，对我来说都是难以忍受的。然而我觉得，我在这两年中学会了如何认识真正的秘鲁社会，包括上述的两极分化、穷人和富人之间的紧张关系、偏见、胡作非为和不满情绪。这样，一个观花埠的孩子就不会怀疑上述事物的存在了。我感谢莱昂修·普拉多军事学校还由于另外一个原因，就是它提供给我的经历成了我第一部长篇小说的素材。尽管其中有许多虚构的成分，但《城市与狗》再现了秘鲁这个微型世界的生活。这部小说受到了引人瞩目的虐待：有一千册在军校的庭院中被当众焚烧，还有一些将军对小说进行了粗暴的攻击，其中一位将军说，这部小说只有一个头脑堕落的人才能写得出。还有一位将军，他的想象力更为丰富，说这部小说得到了厄瓜多尔政府的资助，以此来诋毁秘鲁陆军的名声。作品取得了很大的成功，但我怀疑起这到底是由于它的优秀还是由于这些丑闻。

最近二十年来，几百万山区人口流入利马，在贫民区（为了好听，人们称之为“新建区”）定居下来。这些贫民区包围了原来的市区。与我们那时不同，在现在的利马，中产阶级的子弟们一打开自家的窗子就能发现秘鲁的现实。到处是穷人，他们有的沿街叫卖，也有的流浪街头；有的坐地乞讨，也有的当众抢劫。利马拥有五百万人口（也许是六百万），问题成堆：垃圾成山、交通不便、住房紧张、犯罪不断……这使它失去不少往日的魅力，譬如殖民时代的住宅区、带有百叶窗的阳台、怡人的宁静和互相泼水喧闹的狂欢节。现在全秘鲁的人口、全秘鲁的所有问题都集中在利马了，它确实成了名副其实的一国之都了。

有人说，恨与爱常相混淆，这大概有道理。我一生都在谈论利马的弊病，但这个城市也有许多使我动情的东西，譬如利马的细雾。这种薄纱般的雾从五月到十一月一直蒙在利马城上空。麦尔维尔[^15]路过此地时，这薄纱般的细雾也给他留下了深刻的印象。他在《白鲸》中称利马为“想象中最为愁苦、怪异的城市”，因为“蒙上一层白色的轻纱，它那可怕的愁苦反而有增无减”。我喜欢利马那蒙蒙的细雨，这细雨肉眼看不见，却使人感到宛如蜘蛛脚在脸上搔弄，使得一切都是潮渍渍的，使得我们这些市民在冬日里觉得自己是个两栖动物。我喜欢利马那海水冰冷、白浪滔滔的海边，那是冲浪运动的理想之地。我喜欢利马那古老的体育场，常去观看足球赛，为大学体育队鼓掌叫好。然而，我知道这些只是我个人的喜好，因为我的祖国最美好的事物并不在利马，而在内地，在沙漠，在安第斯山，在森林地带。

秘鲁的一位超现实主义作家塞萨尔·莫罗在他的一首诗的下面狠狠地写上了“写于可怕的利马”的字样。几年之后，另一位作家塞巴斯蒂安·萨拉萨尔·蓬迪以此侮辱性的字眼为题，写了一篇散文，专门用来打破利马神话，粉碎故事、传说和当地白人音乐中对这座城市的理想化，并展示这座城市虚假的一面与现实的一面的鲜明对比。前者富有摩尔和安达露西亚情调：精细的百叶窗后面，神秘而邪恶的美女半遮半露，正在勾引往假发上撒香粉的绅士；后者则是问题重重，肮脏不堪，充满仇恨。整个秘鲁文学可分为两大倾向，一是吹捧利马，一是诅咒利马。而真正的利马很可能既不像有些人描绘的那么美好，也不像有些人讲述的那么可怕。

总的说来，利马是一座没有个性的城市，却有着某些吸引人的地方，如广场、修道院和教堂，作为斗牛场地的阿乔广场更是其中的瑰宝。自殖民时代以来，利马一直保持着对斗牛的爱好，一个喜好观看斗牛的利马人同西班牙人或墨西哥人一样内行。我就是个斗牛迷，在十月集市期间总是力图不漏掉任何一场斗牛。这个爱好是我舅舅胡安培养出来的，他是我的母系众多亲戚中的一员。他的父亲是著名斗牛士胡安·贝尔蒙特的朋友，这位斗牛士曾把自己在利马斗牛时穿的服装赠送给了我舅舅的父亲。这套服装一直像文物似的保存在我舅舅胡安的家里，只在重大节日时，才拿出来给我们这些孩子们看。

同斗牛一样，军事独裁也是利马所特有的。我这一代的秘鲁人，在暴力政权下度过的时光要长于在民主政权下度过的时光。我亲身经历的第一个独裁政权就是曼努埃尔·阿图罗·奥德里亚将军[^16]从1948年到1956年的独裁。这一时期，正是我这一代的秘鲁人从孩提到成年的时期。奥德里亚将军推翻了阿雷基帕籍的律师何塞·路易斯·布斯塔曼特[^17]，他是我祖父的表兄弟，我们在柯恰潘巴居住的时候，我就认识了他。他那时曾来我祖父母家住过一段时间。我还记得他讲话很吸引人，我们都把嘴巴张得大大的听他讲话。我也还记得，他在离去时往我手里塞了零花钱。布斯塔曼特在1945年大选中是民主阵线的候选人。民主阵线是一个联合阵线，其中拉乌尔·阿亚·德·拉托雷的阿普拉党[^18]占大多数。作为左派中心的阿普拉党人曾被历届独裁政权镇压过。布斯塔曼特是无党派人士，阿普拉党提名他为候选人是因为他们提不出自己的人选。布斯塔曼特获多数票当选之后，阿普拉党就立即开始了活动，想把他当作自己的傀儡。与此同时，野蛮反动的秘鲁右派也对布斯塔曼特充满了刻骨的仇恨，他们认为布斯塔曼特是黑色畜生（指阿普拉党）的工具。布斯塔曼特维护了自己的独立性，顶住了来自左右两种势力的压力。他在任期内尊重言论自由和工会活动，也尊重各个政党。然而由于街头骚乱、政治犯罪和各种暴动，他只在任三个年头就被奥德里亚发动的政变推翻了。我小的时候很钦佩这位打着蝴蝶领结、走路似卓别林的布斯塔曼特先生，现在仍然钦佩，因为人们说他身上发生过我国历届总统身上从未曾发生过的怪事：他离任时比上任时更穷；为了不给人以口实说他偏心，他待对手宽容，对自己人却很严厉；他极为尊重法律，以致造成了政治上的自杀。

随着奥德里亚将军的上台，在秘鲁，野蛮统治恢复了。虽然奥德里亚也屠杀、监禁和流放了为数不少的秘鲁人，但其血腥的程度仍比不上同时代的南美其他独裁政权。然而，作为一种补偿，他却更为腐化。这不仅由于政府头头们中饱私囊，还由于其他一些更为严重的事件：造谣生事、卖官鬻爵、敲诈勒索、出卖告密、胡作非为……成了普遍的社会现象，污染了国内生活。

我正是在那个时期（1953年）考上圣马可大学的。我学的是法律和文学。我的家庭本来希望我考天主教大学，那是一所当时所谓“体面人家”子弟上的大学。但是我在十四五岁时就失去了信仰，不愿意当个阔少爷。早在军校的最后一年，我就发现了一些社会问题。当时我是以一个小孩子的浪漫方式发现了社会上的偏见和不平等。我愿意同穷人一样，希望搞一次革命，给秘鲁人带来正义。圣马可是一所不信神的国立大学，有着不妥协的传统，这一点与它的学术成就同样吸引着我。

那时，独裁政权已经捣毁了圣马可大学，许多教授流亡国外。在我入学的前一年（1952年），一次大搜捕把几十名学生投进了监狱或流放异国。互相猜疑的气氛笼罩了教室，因为独裁政权派了许多警察假扮学生注册学习，各政党被宣布为非法，阿普拉党和共产党（当时双方还很对立）只能在地下活动。

考进圣马可不久后，我就开始参加卡魏德的活动。起这个名字是为了恢复被独裁政权破坏得相当厉害的共产党组织。我们这些人参加它的活动，对独裁政权并不构成一种威胁。我们分成小小的支部进行秘密集会，学习马克思主义，印刷反政府的传单，同阿普拉党进行斗争，商量如何让圣马可支持工人的斗争。我们的一大功绩是促成了圣马可举行声援电车工人的罢课。当时是斯大林时期，在文学领域中，党的官方美学理论是社会主义现实主义。我想这就是首先使我对卡魏德失望的东西。尽管我有所保留地（这也是由于我所相信的萨特对我的“反影响”）不得已接受了辩证唯物主义和历史唯物主义，但我永远不能接受那种扼杀了幻想、把文学创作变成一系列宣传手法的理论。我们的争论没完没了，在一次争论中，我把尼古拉·奥斯特洛夫斯基的《钢铁是怎样炼成的》说成一部乏味的小说，却对腐朽的安德烈·纪德的《人间食物》加以辩护。我的一个同志说：“你是个头脑不发达的人。”

从某种意义上讲，我确实是个“头脑不发达”的人。我当时贪婪地、以不断增强着的敬佩之情阅读着一系列被当时的马克思主义者称之为“西方文化的掘墓人”的作家们的作品，其中有亨利·米勒、乔伊斯、海明威、普鲁斯特、玛尔劳、塞利纳[^19]、博尔赫斯，特别是福克纳等人的作品。在大学期间，我记得最牢的也许并不是在课堂上学的，而是从小说中学到的，从讲述约克纳帕塔法郡的故事中学到的。我还记得我那时手执纸笔阅读《八月之光》《野蛮的棕榈》《我弥留之际》《喧嚣与骚动》时那种心醉神迷的样子。在阅读中，我学到了一个故事可能拥有无数复杂的风格和不同的音韵以及结构和概念上的丰富多彩。我了解到，要很好地描述一个故事，就要求作者有着魔术师般的技巧。我青年时代的文学榜样如萨特渐渐地褪了色，我现在不可能再去重读他的作品了，但是福克纳对我来说仍然是第一流的作家。每当我重读他的作品，我都愈加相信，他的作品是“小说写作技巧大全”，足以媲美伟大的古典作品。在20世纪50年代，我们拉美人喜欢阅读欧美人的作品，几乎不问津自己人的作品。现在，这种情况改变了，拉丁美洲的读者发现了自己的作家。与此同时，世界其他地区的读者也发现了拉美的作家。

三

在那几年中，有一件事对我来说相当重要，那就是我认识了独裁政权中负责保安工作的头头。除了奥德里亚本人以外，他是最遭人恨的人物了。我当时是圣马可大学学生联合会的代表，当时许多圣马可的学生被关在监狱里。我们了解到，这些学生就睡在牢房的地上，既无垫子也无毯子，于是我们进行了募捐，买了毯子。但是当我们想送进去的时候，监狱（当时的监狱就是现在舍拉顿饭店的所在地，据说当年在牢房里受到刑讯的冤魂仍在游荡）里的人对我们说，只有内政部办公厅主任堂阿历杭德罗·埃斯帕萨·萨尼亚图才有权同意把毯子交给犯人。于是联合会做了个决议，派五名代表去会见此人。我就是五人代表之一。

我至今仍然保留着在位于意大利广场的内政部里近距离看到这个可怕人物时给我留下的深刻印象：他是个矮小的人，五十多岁，面似羊皮，令人生厌。他仿佛从水中看着我们，我们的话他根本没听，任凭我们讲。当我们声音颤抖着讲完之后，他仍然死盯着我们看，一言不发，好像在嘲笑我们那副困惑的样子。随后，他打开写字台的抽屉，拿出几期小报《卡魏德》，那是我们秘密印刷的油印小报。当然，我们在报纸上对他进行了攻击。他说：“哪篇文章是你们中的哪个人写的，你们在何处集会油印小报，你们的支部进行何种密谋，我都了如指掌。”实际上确是如此，他似乎无所不在。然而，与此同时，他又给人一种可怜虫、碌碌无为的庸人印象。在那次会见中见到他，使我第一次产生了写《酒吧长谈》这部小说的想法。十五年后，这部小说才写成。我想在小说中描写奥德里亚八年统治下的独裁政权给人们的日常生活以及学习、工作、爱情、梦想和志向所留下的影响。我费了好长时间才找到了一条贯穿众多人物和情节的总线，这就是一个在独裁政权中当过保镖和密探的人，同一个依靠独裁政权而发迹者的儿子（后来又当了记者）偶然相遇，以及二人之间进行的贯穿整部小说的谈话。小说出版后，那位已经退出政治舞台、致力于慈善事业的前内政部办公厅主任评论说：“如果巴尔加斯·略萨早点儿来找我，我还可以提供给他一些更为有趣的素材。”

正如莱昂修·普拉多军事学校使我认识了我的国家那样，新闻工作帮助我打开了我的国家许多方面的大门。这一职业促使我深入探索各个领域、各个阶层、各种地方和各种活动。早在十五岁的时候，我就从事新闻工作了，那是在中学四年级的假期里，我先是作为地方版后来又作为侦破版的编辑在《纪事报》工作。夜间跑警察局，打听发生了什么样的犯罪、偷盗、抢劫和交通事故，调查类似“夜蝴蝶”那样引人注目的案件。这一切都是非常令人着迷的。譬如，在波尔维尼尔区，一名妓女被刺身亡，这案子促使我来往于利马各个妓院、下等舞厅、龟奴和同性恋者常去的酒吧。那时，报人同流氓（起码也是名声最糟的流浪汉）之间的界限很难划清。一天的工作下来，照例跟同事们钻进某个咖啡馆（这种咖啡馆很赚钱，一般说来都由华人招待，地上铺满锯末以掩盖醉汉们的呕吐物），然后再到妓院去。妓院为了遮掩丑闻，给侦破版的记者优惠待遇。

在大学的最后几年里，我在一家电台（即泛美电台）里工作过，负责写新闻稿。在电台里，我有机会了解到广播剧的制作，那是一个令人眼花缭乱的世界，多愁善感又恐怖瘆人。巧妙的偶然加上无限的装模作样，简直就是19世纪“报屁股文学”的现代翻版，却拥有众多的听众。据说，一个行人走在利马的任何一条大街上都能听到费利克斯·B.凯赫特《生的权利》的章节，收听这部广播剧的不止一户人家。这个令人眼花缭乱而又奇特的小小世界为我的另一部小说提供了题材，那就是《胡利娅姨妈和作家》。表面上看，那是一部描写广播剧制作和情节剧写作的小说，但实质上是对我自己为什么要写作、什么是写作这些疑问的描述。这些问题一直缠绕着我，我也为此奉献了自己的大半生，却从来未弄明白。从小时候起，我就想把我经历的种种事情写成故事。我一直摆脱不掉这一诱惑，甚至有时有这样一种印象，即我一生的所作所为或是别人对我的所作所为都只不过是为了编故事而找的借口。不断地把现实移植为故事，其背后到底是什么？是企图把某段偏爱的经历从飞逝的时间之口中拯救出来吗？是想通过改头换面的手法使痛苦而可怕的事实变得轻松一些吗？也许仅仅是一种游戏，是文字与幻想的杂乱堆砌？我越写下去，这个问题就越难以回答。

1957年，我大学毕业，第二年交了论文，并获得去马德里攻读博士学位的奖学金。到欧洲去，想办法再到巴黎去，是我阅读大仲马、凡尔纳和雨果的作品以来一直梦寐以求的事。我幸福地整装待发，正在此时，一个偶然的事件为我提供了去亚马孙地区旅行的机会。一个叫做胡安·柯玛斯的墨西哥人类学家想周游上玛拉尼昂河[^20]，因为阿瓜鲁纳人[^21]和汪毕萨人[^22]的部落就在那里。考察队中还有一个空缺，于是在朋友的帮助下，我去了。

在上玛拉尼昂河流域的那几个星期里，我们参观了部落、屯子和村镇。那是一次难忘的经历，因为那次旅行向我展示了我的国家的另外一个天地（正如我所见到的那样，秘鲁是一个五光十色的国家）。从利马到奇凯斯和乌拉库萨这两个屯子，等于从20世纪跳回石器时代，等于同赤身裸体生活在最原始状态之中并受着非常残酷的剥削的同胞们进行接触。剥削者们也都是些可怜的赤脚的半文盲商人，他们用以低得可笑的价钱从部落里购得橡胶和皮毛，印第安人如果有任何摆脱他们的企图，他们便野蛮地加以惩罚。我们到达乌拉库萨的时候，酋长出来迎接我们。酋长是阿瓜鲁纳人，叫胡姆。见到他、听他讲述自己的经历是非常令人震惊的，因为此人不久前由于企图建立一间合作社而痛遭毒打。在上玛拉尼昂河地区的遥远村落里，我看到并亲身体验到了我国人民为生存而斗争的残酷现实。然而，亚马孙地区并不仅仅意味着苦难、暴行及不同思想、不同历史时期的秘鲁人困苦的共居生活，它也是一个繁茂的世界，一个拥有不可思议的力量的世界。从城市里来的人在那里可以发现尚未被驯服、尚未被洗劫的大自然：壮观湍急的河流、原始林莽、仿佛从神话中走出来的动物以及过着冒险生活却自由自在的男男女女。他们过着就像我在孩提时代欣喜地阅读过的探险小说中的主人公所过的那种生活。我想我还从来没有做过收获如此丰富的旅行。我在1958年的那次旅行中的所闻、所见、所做的许多事情后来都被我酿成了故事。

在那次旅行中，我第一次直觉到被以赛亚·伯林[^23]称之为“矛盾着的真理”的东西。在圣玛利亚·德·涅瓦那小小的镇子里，于20世纪40年代建立了一个传教所，修女们为部落的女孩们开办了一所学校。但是由于女孩们不愿意上学，修女们就求助于警察，强迫女孩们上学。有些女孩在传教所里待上一段时间后，就同家人失去了联系，不能返家重过以前的生活了。那么这些女孩们怎么办呢？于是她们被托付给代表“文明”的人们，即路经圣玛利亚·德·涅瓦镇的工程师、军人和商人，等等。这些人却把女孩们带走当了用人。富于戏剧性的是，修女们不仅没有发觉这一善举的恶果，反而还要把这一善举坚持到底，以此来证明她们那真正的英雄主义。实际上，修女们的生活条件也相当艰苦，在河流涨水的几个月中，她们处于完全与世隔绝的困苦状态。用世界上最美好的愿望，作出无限的牺牲，却给人们造成了如此巨大的灾难，这就是我要永远记取的教训。这一教训使我明白了善与恶之间的界限是可以双向的。这一教训也向我指明，在判断人们的行为时，在对社会问题决定加以解决时，你如果想使你所采取的手段不会导致比疾病更有害的恶果，那么你就必须慎重行事。

我到了欧洲，直到1974年才返国定居。我二十五岁出国，三十八岁回国，在这期间发生了许多事。我回国后在很多方面变成了另外一个人，但在我与自己国家的关系上，我想我还是保持着童年时的那种看法。这种关系不能用概念而只能用比喻加以说明。我认为秘鲁本身就是一个不治之症，我同它的关系是紧张的、冷酷的，但充满着以粗暴为特点的激情。小说家胡安·卡洛斯·奥内蒂[^24]有一次曾说，作为作家，我与他的区别是，我与文学关系是夫妇关系，而他与文学的关系是通奸关系。我觉得我与秘鲁的关系与其说是夫妇关系，不如说也是一种通奸关系，也就是说，充满了疑惧、迷恋与狂热。我对各种形式的“民族主义”的反对是自觉的，我认为“民族主义”是人类最大的缺点之一，是为最糟糕的走私行为打掩护的。然而事实是，我国的事物，有的使我恼火，也有的使我兴奋。在我国正在发生着的或发生过的事都不可避免地与我密切相关。如果能作一番衡量，其结果很可能是在我写作的时刻，离我的眼前最近的正是秘鲁的缺点。也许我对困扰着秘鲁的各种问题批评得过于严厉，从而有失公允了。不过，我认为在这种批评的后面有着深深的休戚相关之情。虽然我恨秘鲁，但是，正如塞萨尔·巴列霍[^25]的诗句所说，这种恨总是浸渗着柔情。

略萨

1989年4月

   

谨将此书以最诚挚的感情献给住在佩蒂·杜阿路上的博尔赫斯研究者路易斯·洛埃萨[^26]和“海豚”阿维拉尔多·奥肯多[^27]。

你们永远的兄弟　小萨特[^28]