七

这儿只有一点点光亮，全是从我们房间透过浴室帘子照过来的。我能看到阿克利正躺在床上，我他妈太清楚了，他肯定是万分清醒。“阿克利？”我叫他，“你醒着吧？”

“对。”

里面很黑，我踩到地板上不知道是谁的鞋上，差点儿他妈的摔了个跟头。阿克利在床上坐起来，撑着胳膊。他脸上抹了很多白乎乎的东西，治粉刺的，黑暗里有几分可怕。“你他妈在干吗？”我问他。

“什么他妈的我在干吗？我正想睡觉呢，你们这两个家伙搞出那么大动静。他妈的到底为什么打架？”

“灯呢？”我找不到开关，手在墙上摸来摸去。

“干吗开灯？……就在你手边。”

我终于找到开关把灯打开，阿克利这厮抬手遮光，免得刺眼。

“天哪！”他叫了一声，“你他妈到底怎么了？”他是说我流了那么多血。

“跟他妈斯特拉雷德比划了两下。”我说着坐到了地板上。他们的房间里从来没有椅子，也不知道把他妈的椅子都弄哪儿去了。“喂，”我说，“我们玩会儿扑克怎么样？”他可是个扑克迷。

“你还在流血呢，岂有此理。你最好上点儿东西。”

“会止住的。喂，你到底想不想玩会儿扑克？”

“扑克，岂有此理，你脑子里有没有一点儿谱，这会儿几点了？”

“不晚，才十一点左右，十一点半吧。”

“才！”阿克利说，“喂，我明天上午还要去做弥撒，岂有此理。已经他妈的半夜了，你们又吵又打——到底他妈的为什么？”

“说来话长，阿克利，我也不想烦你，是为你好。”我告诉他。我从来不跟他说我自己的事，第一个原因就是他比斯特拉雷德还蠢，跟阿克利比起来，斯特拉雷德可以说是他妈的天才了。“嗨，”我说，“我今天晚上睡埃利的床行不行？他明天晚上才回来，对不对？”我他妈知道得一清二楚，埃利几乎他妈的每个周末都回家。

“我不知道他他妈到底什么时候回来。”阿克利说。

乖乖，那可真让我来气。“你他妈什么意思不知道他什么时候回来？他从来不到星期日晚上不回来，不是吗？”

“对，可是岂有此理，我也不能就这么跟别人说，想睡就可以睡到他的破床上。”

他这样把我气死了。我坐在地板上伸手拍拍他的破肩膀。“你是个大好人，阿克利小孩儿，”我说，“你知道吗？”

“不，我是说实话——我不能跟别人说他可以睡到——”

“你是个真正的大好人，是个绅士，还是个文化人呢，小孩儿。”我说，他真的是。“你有没有烟？你要说没有，我可要立马断气了。”

“我没有，这是实话。喂，到底他妈的为什么打架？”

我没理他，只是起身走过去往窗外看。突然，我感到很孤独，几乎希望自己死掉算了。

“到底他妈的为什么打架？”阿克利问，他已经问了有五十遍，纠缠这个真让人腻烦。

“跟你有关。”我说。

“岂有此理，跟我有关？”

“对，我是为了保护你的破名誉。斯拉特雷德说你为人很差，说这话我可跟他没完。”

这句话让他精神了。“他真的说过？不是开玩笑吧？他真的说过？”

我告诉他我只是开玩笑，然后走过去躺倒在埃利的床上。乖乖，我感觉真是糟透了，感到他妈的孤独至极。

“这个房间里有臭味，”我说，“我从这儿就能闻到你的臭袜子，你从来不洗吗？”

“你不乐意的话，知道自己可以怎么着。”阿克利说，他可真会说话啊，“把他妈灯关掉好不好？”

可我没有马上把灯关掉，只是躺在埃利的床上，脑子里想着简，还有别的事。想起她和斯特拉雷德在肥屁股埃德·班基的车里待着，能把我逼得彻底疯掉。每次一想起这件事，我就想从窗口跳下去。首先，你不了解斯特拉雷德，我可是了解他。在潘西，多数人只是一天到晚嘴里念叨跟女孩儿性交——就像阿克利那样——可斯特拉雷德是动真格的，我自己就跟至少两个和他干过的女孩儿熟，这是事实。

“给我说说你精彩纷呈的这辈子里有什么故事吧，阿克利小孩儿。”我说。

“把他妈灯关了好不好？明天上午我还得去做弥撒呢。”

我起身把灯关了，只要能让他开心，然后又躺到埃利的床上。

“你准备怎么着——睡埃利的床？”阿克利问我。乖乖，他可真会招待人。

“可能，也可能不，别担心。”

“我不是担心，只是很他妈的不想看到埃利突然回来，让他看到别人——”

“放心吧，我不睡这儿，我不会辜负你他妈的盛情款待。”

几分钟后，他就呼噜打得震天响了。总之，我继续躺在那儿，就在黑暗中，努力不去想简这妞儿和斯特拉雷德待在混蛋埃德·班基的汽车里，但几乎不可能。问题是，我知道斯特拉雷德的招数，这可真是添乱。我们一块跟女孩儿约会过，就在埃德·班基的汽车里，他和女朋友坐后排，我们坐前排。这家伙真有两招。他一开始是很低声地、用一本正经的腔调跟他女朋友说话——就好像他不仅是个靓仔，还是个讨人喜欢、一本正经的家伙。我听他说话差点儿没他妈吐出来。他的女朋友一直在说：“别——请你别这样。请别。请你别这样。”可是斯特拉雷德这厮还是用林肯总统般一本正经的腔调跟她说话。到后来，车后座那儿静得出奇，真是太让人难堪了。我想他那天晚上没能跟那个女孩儿干成事儿，不过也他妈差不离，他妈的差不离。

我正躺在那儿努力什么也不想时，听到斯特拉雷德这厮从厕所回到我们的房间里。我能听到他放下他的破盥洗用具什么的，还打开了窗户，他是个新鲜空气狂。后来很快他就关灯了，根本没找一下我去了哪儿。

就连外面的街道也让人沮丧，根本再也听不到什么汽车声。我感觉很孤独，很糟糕，甚至想把阿克利叫醒。

“嗨，阿克利。”我叫他，有点儿压着嗓子，免得让斯特拉雷德隔着浴室帘子听见。

可是阿克利没听到。

“嗨，阿克利！”

他还是没听到，睡得像块石头。

“嗨，阿克利！”

好，这下他听见了。

“你他妈的怎么回事？”他说，“我睡着了，岂有此理。”

“喂，进修道院得办什么手续？”我问他，我多少在琢磨这个念头，“是不是非得是天主教徒什么的才可以？”

“当然非得是天主教徒。你这个杂种，把我弄醒就为问这个蠢问题——”

“啊，你继续睡觉吧，反正我也不打算进。我这人倒霉就倒霉在很可能会进了一间修道院，里面的教士却跟我不是同一类人，全是些蠢杂种，要么只是杂种。”

我说完后，阿克利这厮他妈的直挺挺地坐了起来。“听着，”他说，“你说我什么我都无所谓，可是你他妈敢拿我的信仰开玩笑，岂有此理——”

“放心，”我说，“没人拿你他妈的信仰开玩笑。”我从埃利的床上下来往门口走去，我不想在这个破环境里再待下去了。我停了一下抓起阿克利的手，跟他假惺惺地大握特握。他把我的手甩开。“什么意思？”他问。

“没什么意思，看到你他妈真是个大好人，只是想谢谢你，如此而已。”我说，用的是一本正经的口气。“你出类拔萃，阿克利小孩儿，”我说，“知道吗？”

“小聪明，总有一天有人会把你揍——”

我根本懒得听他说完，关上破门就到了走廊上。

人们全睡了或者出去了，要么回家度周末，走廊上很静很静，令人沮丧。莱希和霍夫曼的房门外，有个考利诺斯牌子的牙膏盒，向楼梯走去时，我用脚上的羊皮边的拖鞋一路踢着它。我在想该干吗，想到也许可以下去看看马尔·布罗萨德那厮在干吗。我突然改了主意，一下子，我又想好了真的该干吗，我他妈要离开潘西——就在当天夜里，我是说我不等到星期三还是怎么样，我只是不想再逗留了，这儿让我感觉难过万分，寂寞万分。我决定去纽约的旅馆租个房间——很廉价的那种——然后优哉游哉地过到星期三。到了星期三，我便会休息充分、容光焕发地回家。我估计我爸妈在星期二或星期三之前，很可能不会收到老瑟默的信，那封信会告诉他们我被开除了。我不想在他们得知这个消息并将其充分消化前到家，我不想在他们一得知这一消息时，就在他们跟前出现。我妈会变得歇斯底里，但是当她充分消化了一件事后，就不会太糟糕。更何况，我也多少需要小小地休个假，我的精神太紧张了，真的。

总之，那就是我决定要做的事。我回到房间打开灯，开始收拾行李，我已经打点好不少东西。斯特拉雷德这厮根本没醒。我点了一支烟，穿好衣服，然后把两只格拉斯顿牌手提箱收拾好，总共只花了两分钟左右。在收拾东西方面，我算是个快手。

但在收拾东西时，有件事让我有点沮丧：我得把我妈事实上就在没几天前寄给我的新滑冰鞋装进去，那让我感到沮丧。我想象得出我妈走进斯伯丁商店，向售货员问了无数个傻乎乎的问题——可现在我又被学校开除了，这让我挺难受的。她给我买的型号不对——我想要速滑比赛用的，她却给我买了玩冰球的那种——但同样让我难受。几乎每次别人送我礼物，到头来都让我觉得难受。

收拾完以后，我随便点了一下我有多少钞票，不记得确切是多少，可也不算少。大约一星期前，我奶奶刚给过我一沓钱。我这个奶奶在钞票上很大方。她的记性全没了——她老得要命——每年总要给我寄四次左右的钱，算是生日礼物。虽然我的钞票不少，但我觉得多点儿钱总会用得着，谁也说不准。我就走下楼，叫醒了弗雷德里克·伍德拉夫，我的打字机就是借给了这个家伙。我问他愿意出多少钱买我的打字机。他是个有钱佬，可他说不知道，不太想买。最后他还是买了。这台打字机我买时花了约九十块钱，卖给他只卖了二十块。因为被我叫醒，他还不高兴呢。

收拾完东西准备走时，我在楼梯口站了一会儿，最后看了一眼这条破走廊。我有点儿在哭，也不知道为什么。我戴上我的红色猎帽，就像我喜欢的那样，把帽檐拉到后面，然后用他妈最大的嗓门喊了一声：“好好睡吧，你们这帮蠢蛋！”我敢说，这层楼上的混蛋全让我吵醒了，然后我他妈就走了。不知道哪个笨蛋往楼梯上扔了些花生壳，差点儿没他妈让我摔断我的破脖子。