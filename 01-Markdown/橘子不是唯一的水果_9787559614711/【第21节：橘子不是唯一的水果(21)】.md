  

### 【第21节：橘子不是唯一的水果(21)】

　　"如果上帝带你回来，"她因胆结石而入院时曾对玫说，"你就会明白，那是因为他还有工作要让你去做。"我趴在床单下，祈祷自己能被带回来。

　　手术当天的大清早，护士们笑眯眯地又理了一次床，还把碗里的橘子堆出匀称的形状。两条汗毛浓密的手臂拖我起来，把我绑在冰凉的手推车里。脚轮咯吱咯吱地响，推车的男人走得太快了。走廊，对开门，露在密实的白面具上的两只眼睛。一个护士抓住我的手，与此同时还有一个罩子扣在我的嘴巴和鼻子上。我吸入了一口，看到一整排滑水的人随波跌落，没再浮起，然后我就什么都看不到了。

　　"珍妮特，小果冻。"

　　我就知道是这样！我已经死了，天使们在发我果冻吃。我睁开眼睛，还指望看到一双翅膀呢。

　　"来，吃一点。"那个声音在鼓动我。

　　"你是天使吗？"我带着希冀问。

　　"算不上，我是医生。但她是个天使，护士小姐，是不是？"

　　天使羞红了脸。

　　"我听见了。"我说，不是特意对谁说的。

　　"吃你的果冻吧。"护士说。

　　要不是艾尔西发现我身在何处，并且来看望我，我很可能衰弱无力地独自挨过余下的一周。我母亲得等到周末才能来，我知道，因为她在等管道工检查她的装修。艾尔西每天都来，讲笑话逗我笑，还讲故事，让我精神多了。她说，故事能帮助我理解世界。等我感觉好些了，她承诺会从基础知识教起，以后就能帮她数字占卦了。一阵激动油然而生，因为我知道母亲肯定不同意。她说过，占卦几乎就算得上发疯了。

　　"甭担心，"艾尔西说，"占卦可管用啦。"

　　我们过得挺开心的，就我们俩，不停地计划等我病好了该干什么。

　　"你多大了，艾尔西？"我很想知道。

　　"我记得大战，我只能说这么多啦。"随后她就开始说，她怎么驾驶一辆没有手闸也没有脚闸的救护车。

　　周末，我母亲来得挺勤的，但那是一年里教堂最忙活的季节。他们都在安排圣诞活动。她不能脱身时就让父亲来，他通常都会捎来一封信和几只橘子。

　　"唯一的水果。"她总那么说。

　　水果沙拉、水果派、水果奶油杯 、果汁潘趣酒。恶魔果、激情果、烂果子、礼拜日的水果。

　　橘子就是唯一的水果。我剥下的橘子皮填满了小垃圾桶，护士们去倒垃圾时都不情不愿的。我把橘子皮藏在枕头底下，护士们又责骂我，还叹气。

　　艾尔西·诺里斯和我每天都分吃一个橘子，一人一半。艾尔西没有牙，所以她先吸吮，再揉烂了吃。我假装在吃牡蛎，把橘子瓣放在后舌根。人们会看我们，但我们不在乎。