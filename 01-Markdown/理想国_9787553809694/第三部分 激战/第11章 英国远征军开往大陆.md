   

## 第11章　英国远征军开往大陆

由于英国人发生了一场争执和意见分歧，英军没有及时前来掩护朗勒扎克将军暴露的左翼，彼处战线原定是由英方防守的。8月5日，英方宣战后的第一天，因亨利·威尔逊详细制订的总参谋部计划，必须首先得到不列颠帝国国防委员会的批准，而不是像大陆国家的作战计划那样能自动生效，该委员会于这天下午4时召集了一次作战会议，与会者照常是那几位文职和军方领袖，另外还有一位既是文官又是军人的显赫人物第一次参加这次会议。

陆军元帅基钦纳勋爵就任了陆军大臣。他本人对于这一任命不感愉快，同僚们对于由他出长陆军部所感到的不愉快也不相上下。政府也为自效忠于查理二世的蒙克（Monk）将军以来，基钦纳作为第一个现役军人进入内阁而忐忑不安。使将军们担心的是，他有可能利用他的地位或者为政府所利用，来干扰派遣赴法远征军的决定。他们的担心确非杞人忧天。基钦纳不久就对英法计划指定英军所必须执行的战略、方针和任务等等表现了极端的轻视。

由于他处于双重地位，他的具体的职权范围是不完全清楚的。英国参战之初，人们模模糊糊地认为最高权力在首相手中，至于首相应听从什么人的建议或者以谁的建议为准，则缺乏明确的安排。在军内，战地军官轻视参谋人员，认为他们“既无头脑，又要故作姿态”。而这两种人又都同样厌恶那些被称为“大礼服”的文官大臣的干扰。文官反过来也把军人称为“笨蛋”。出席8月5日举行的作战委员会的文官是阿斯奎斯、格雷、丘吉尔与霍尔丹，陆军方面是十一名将级军官：内定出任远征军总司令的陆军元帅约翰·弗伦奇爵士，远征军的两位军长，即道格拉斯·黑格（Douglas Haig）爵士和詹姆斯·格里尔森爵士，远征军参谋长阿奇博尔德·默里（Archibald Murray）爵士，他们都是中将；其次还有副参谋长亨利·威尔逊少将，他的个性易树政敌，在克拉危机中表现得非常充分，以致栽了跟斗，失去了一个更高的职位。在文武官员之间，基钦纳勋爵究竟代表何方，谁都不十分清楚。他对远征军的目的非常怀疑，对其总司令很不赏识。如果说基钦纳在表达自己的思想感情时不如海军上将费希尔那样来得暴烈，至少他对总参谋部的计划把英国军队“钉在”法国战略的尾巴上的做法，则已开始流露出同样的蔑视。

基钦纳没有亲自参与制订大陆作战计划，因而能够正确地评估远征军的作用，他根本不相信在70个德国师与70个法国师之间迫在眉睫的冲突中，远征军的6个师能对战局产生多大影响。基钦纳在出任喀土穆战役的指挥官时，克罗默（Cromer）勋爵曾说过，“他是我一生中碰到的最有能力的人”。他虽然是个职业军人，近几年来处理的事务却都在宏观层面。他所关注的只以印度、埃及、帝国等大事为限。人们从未见他与士兵交谈过，或注意过他们。与克劳塞维茨一样，他把战争看作政策的延续，并在这个意义上看待战争。他与亨利·威尔逊以及总参谋部不同，不埋首于制订登陆日期、铁路时刻表、马匹及营房等计划表。他站在一个比较超脱的地位观察战争，因此能够从各个强国之间的关系出发纵观战争的全貌。同时他能够看到，为扩充国家军事实力，应付即将开始的长期抗衡，该作出多么巨大的努力。

他宣称：“我们必须准备好把数以百万计的军人投入战场并维持数年。”他的听众大吃一惊，觉得难以置信，基钦纳却是铁石心肠。为了参与并赢得一场欧战，英国必须拥有一支与大陆国家旗鼓相当的70个师的兵力。他估计过，这样一支军队要到战争的第三个年头才能配备足额，这就意味着人们可以从中得出一种令人震惊的推论，即这场战争就将持续这么长的时间。他还认为，现有的正规军及其职业军官，特别是士官，是培训他心目中那支大军的一批可贵的必不可少的核心力量。如果把这支常规部队投入到他认为处于不利形势下的眼前的战役中，或把它部署在从长远角度考虑是不能起决定性作用的地方，他都认为是犯罪的愚蠢行为。在他看来，一旦这支部队完蛋，就没有经过严格训练的部队来代替它了。

英国不实行征兵制是英国与大陆国家的军队之间一切差别中最显著的一个方面。常规部队的建立旨在执行海外任务，而不是保卫本土的安全；保卫本土的职责由本土军承担。威灵顿公爵当年说过，派赴海外服役的新兵“必须是志愿兵”，从此以后这就成了一条不可更易的金科玉律，英国也就全靠志愿部队进行战争，因此也就弄得其他国家无法肯定究竟英国已承担或愿意承担多大的义务。已过七十高龄的陆军元帅罗伯茨勋爵，多年来一直力主实行征兵制，他在内阁中唯一的支持者不用说就是温斯顿·丘吉尔。可是工人阶级强烈反对，同时也没有一届政府甘冒倒台的风险去支持征兵法案。英国的军事建制，其本土诸岛的正规军（Regular Army）为6个师和一个骑兵师，另有派驻海外的4个正规师（6万人）和14个本土师（Territorials）。后备役约30万人，分为两类：一类是特别后备役（Special Reserve），这一部分仅够勉强补充正规部队，使之达到作战实力，能在战场上支持得住最初几个星期的作战；另一类是为本土军提供补充的国民后备役（National Reserve）。按基钦纳的标准，本土军是一批未经训练、无用的“业余军人”。对于本土军的看法，他跟法国人对他们的后备军一样，是完全蔑视的，是不公正的，认为它们的作用等于零。

基钦纳二十岁时，曾在法国军队中充当志愿兵参与1870年的战争，讲一口流利的法语。无论他是否因此而对法国特别同情，他绝非法国军事战略的最狂热的支持者。在阿加迪尔危机期间，他曾告诉帝国国防委员会，他预期德国人将会“像鹧鸪一样”穿越法国；他拒绝邀请，不愿插手作出委员会认为适当的任何决定。据伊舍记载，他曾捎信给委员会，表示“如果委员们设想他将指挥在法国的部队，他就要他们自己先见鬼去吧”。

英国政府1914年让他主管陆军部，从而任命了唯一的一个准备坚持组织长期作战的人，倒不是出于他的见解，而是因为他有声望。他不擅长主管一个政府部门所需的官僚手腕，内阁会议的那套“议事程序”又不配他的胃口，他做惯了殖民地总督，一向只知道简简单单地叫人“照我说的办”。基钦纳尽他力之所能摆脱命运的安排。他的超人的洞察力，并不如他性格上的缺点那样为英国政府和将军们所了解，因此他们都巴不得让他回埃及去，无奈他们又少不了他。他被任命为陆军大臣，这不是因为考虑到他的高见为他人所望尘莫及，而是因为他的名声乃是“安定民心”之所需。

喀土穆战役以后，举国上下都对基钦纳怀有一种近乎宗教徒的虔诚。在他和公众之间存在着一种后来在法国人民和“霞飞老爹”之间或在德国人民和兴登堡（Hindenburg）之间发展起来的不可思议的内心的息息相通。“喀土穆的基钦纳”，两个词的第一个字母（K of K）成为具有魔力的徽号，他的一把宽阔而威武庄严的胡子也成了英国的民族象征，犹如红裤子是法国的象征一样。基钦纳身材高大，肩膀宽阔，浓浓的胡须，一副大权在握的神态，乍看起来俨然是狮心王理查（Richard the Lionhearted）出现在维多利亚时代的形象，所不同的只是在他严肃的、炯炯的目光背后，隐藏着一种令人莫测高深的神情。从8月7日起，一份著名的征兵招贴出现在街头，画中的那髭须、那眼睛以及那手指“祖国需要你”的形象，都深深射进每个英国公民的心灵。英国要是在没有基钦纳的情况下参战，就会像礼拜天没有教堂一样不可思议。

可是这时候，人人所想的都是把六个师派往法国这个眼前的问题，作战委员会也不把他的先见之明当作一回事。格雷在很久以后带着也许大可不必如此迷惑不解的语调写道，“从未透露过他是怎样或是根据什么推理过程而对战争的长期性作出这一预测的。”是不是因为基钦纳是对的而别人都错了，或者是因为老百姓难以相信军人也具有思维，还是因为基钦纳从来未能或从来不屑于阐明自己的理由，但不管怎样，正如格雷所说那样，他的同僚和同辈人无不认为“他不是凭推理，而是凭直觉中一闪而过的灵机”作出他的结论的。

不管经历了什么样的过程，基钦纳还预言了德国即将在默兹河西岸所采取的进攻模式。据一位总参谋部的官员说，人们后来同样认为，他之能一语中的，应归功于他的“某种料事如神的天才”而不是出于他“对时间和距离的了解”。实际上，基钦纳与阿尔贝国王一样，已看出对列日的袭击预示着施利芬右翼的包抄行动。他认为，德国侵犯比利时和把英国卷入对德作战，并非像劳合·乔治所说那样，是为了通过阿登山区而对比利时的中立进行“小小的侵犯”。基钦纳拒绝对战前的计划承担责任，可是，他现在也不能建议扣下这六个师。不过，他认为根本没有必要让这六个师去莫伯日那样远在前方的地点面临覆灭的风险，他预料它们在莫伯日将承受德国侵略军的全部压力。他建议把它们集结的地点改为亚眠（Amiens），也就是退后70英里。

计划的急剧改变激怒了众将军。在他们眼中，这显得是临阵畏怯，从而证实了他们原来的最坏估计。即将上阵挂帅的，身材矮胖、面色红润的约翰·弗伦奇爵士正处于骁勇好斗状态的高峰。他平时那种中了风似的呆滞神色，加上系得紧紧的用以代替衣领及领带的骑兵硬领巾，始终给人一种濒于窒息的印象。而事实上，他的确是经常感到窒息，如果不是肉体上，至少在情绪上是如此。1912年，他被任命为帝国总参谋长后，就立即通知亨利·威尔逊，说他打算使军队作好对德作战的准备，因为他认为这是“势所必然”，自此以后，在名义上他负责与法国共同制订联合作战计划，尽管事实上他对法国的作战计划就像他对德国的作战计划那样一无所知。跟霞飞一样，在被任命为总参谋长时，他既没有任何参谋阅历，又没有参谋学院的学历。

他的中选，跟基钦纳的出长陆军部一样，主要是由于他的军阶和声誉，而他的内在素质倒在其次。在几次给英国建树了军事声誉的殖民地战争中，约翰爵士表现得勇敢而机智，并像一位权威人士所称誉的那样“切实掌握中小局面的战术”。在布尔战争中，作为一名骑兵将领，他的功绩中最为脍炙人口的是他急驰穿过布尔人的防线，援救被围的金伯利城（Kimberley）这一传奇式的行动。这些功绩为他赢得乐于担当风险的勇敢的指挥官的声誉，并在大众中为他博得了几乎与罗伯茨和基钦纳相埒的美名。由于英国在与既未经训练又缺乏现代武器装备的对手的较量中未能取得怎样辉煌的成就，此时出了一位英雄，部队高兴，国家感激。弗伦奇的英勇善战，加上他在社交界的赫赫名声，使他扶摇直上。跟海军上将米尔恩一样，他也是爱德华七世治下的显贵人物。身为骑兵军官，他知道自己是陆军中的精英。他与伊舍勋爵之间的友谊更对他有益无害；同时，在政治上，他与自由党人结好，该党于1906年执政。1907年，他任本土防卫军总监（Inspector General）；1908年，他代表陆军，陪同国王爱德华到雷维尔对沙皇进行国事访问；1912年就任英帝国参谋长；1913年被提升为陆军元帅；到六十二岁时，他是级别仅次于基钦纳的现役军官。他比基钦纳小两岁，虽然外表显得比基钦纳老些。普遍认为，如果战争爆发，他将指挥远征军。

1914年3月克拉兵变发生后，军队首脑受到冲击之猛烈，犹如参孙倾覆神室[^1]一般，弗伦奇引咎辞职，看上去像堂吉诃德一样突然中断了他的职业生涯。然而，政府对他的宠爱反而加深，因为在政府的心目中，这次兵变是反对党策划的。“弗伦奇是一个勇敢的人，我喜欢他。”格雷不胜惋惜地写道。四个月后，当危急关头到来时，他又再度受到重用。7月30日被指定在英国参战时出任总司令。

由于缺乏学习方面的训练，又因天性不喜读书，弗伦奇之所以成名，与其说是由于他智力过人，不如说是由于他急躁易怒，至少在他早期立下汗马功劳之后是如此。“我并不认为他特别聪明，”国王乔治五世向其叔父透露过，“而且他的脾气坏得惊人。”就像在海峡彼岸的法军司令一样，弗伦奇也不是一个凭理智行事的军人。但他们之间有着根本的不同：霞飞的突出品质是坚定不移，而弗伦奇则是极易受压力、人和他人成见影响。有人说过，他具有“爱尔兰人和骑兵普遍具有的那种反复无常的气质”。霞飞在各种处境下都很沉着；而约翰爵士却是顺利时盛气凌人，不顺利时垂头丧气。他容易感情冲动并易为流言蜚语所左右；在伊舍勋爵看来，他有“一颗爱作奇想的稚子之心”。有一次，他赠给他以前在布尔战争中的参谋长一只刻有“我们的友谊久经考验，同甘共苦永不变”字样的金瓶作为纪念。这位久经考验的朋友就是那位不像他那样易动感情的道格拉斯·黑格。也就是这位黑格，1914年8月在日记中写道：“从内心来说，我认为在我国历史上这个生死存亡的时刻，让弗伦奇担任这个举足轻重的职务是不太适宜的。”黑格内心的这种看法跟他的某种意识不是没有联系的，那就是最合适的人选就是他本人。他这个人是指挥权不到手绝不肯罢休的。

基钦纳重新开启了关于英国远征军的目的地——因此也牵涉其目标——等问题的讨论。按照亨利·威尔逊的看法，委员会里“大多数人对问题一窍不通……他们犹如白痴一样讨论着战略问题”。这时，约翰·弗伦奇爵士突然“插进了一个荒谬的建议，要把部队开往安特卫普”，说什么英国的动员既然落后于预定的时间，那就得考虑与比军合作的可能。黑格也像威尔逊那样有记日记的习惯，他在日记中写道，他对他上司改变计划的“那种不顾后果的方式感到震惊”。新上任的英帝国总参谋长查尔斯·道格拉斯（Charles Douglas）爵士也同样压抑不住内心的激动，他说：鉴于在法国登陆的事情全都安排妥当，同时法国已拨出运输车辆准备往前方输送军队，因此，在这最后时刻，任何改变都将导致“严重的后果”。

最使总参谋部感到烦恼的莫如法英两国火车车厢容载人数不同这个不幸的问题。要把载运的部队从一种车厢转到另一种车厢，牵涉到一个极其复杂的数学上的排列问题。难怪负责运输的官员听到计划行将改变时会感到担忧。

幸而丘吉尔否决了把部队转向安特卫普的决定，使得负责运输的官员们得以放下心来。两个月之后，丘吉尔亲自到安特卫普去了一趟，计划派两旅海军陆战队和一师本土军去那里作一次大胆的、孤注一掷的登陆，为拯救这个重要的比利时港口作一番最后而又徒劳的努力。不过在8月5日，他说海军不可能保护运兵船队作横跨北海到比境内的斯海尔德河（Scheldt）这样长途的航行，但可绝对保证船队安全通过多佛尔海峡。由于海军已有充分时间作了横渡海峡的准备，他声称时机业已成熟，并主张立即将六个师全部派遣过去。霍尔丹支持，罗伯茨勋爵也支持。接着又产生了究应派几个师去的问题，争论着在本土军有更多时间进行训练或从印度调回接替部队之前，是否得留下一个或几个师。

基钦纳又提出了他那个在亚眠集结的想法，并得到他的朋友、未来的加利波利战役的指挥官伊恩·汉密尔顿爵士的支持。后者感到不管怎样，应让英国远征军尽快到达那里。格里尔森发言支持“在要害地点部署优势兵力”这一观点。走在激进派最前列的约翰·弗伦奇爵士提出了“我们应该立即渡过海峡，随后再决定目的地”的建议。最后，一致同意马上调集运输舰船把六个师全部运过去，目的地待法国参谋部的代表到达后再协商决定。在基钦纳的坚持下，已向法国参谋部提出紧急要求，请派一名代表前来就法国战略问题作进一步讨论。

由于一夜之间出现的入侵恐慌闹得人心惶惶，委员会在二十四小时内改变了自己的主意，把六个师减为四个师。由于讨论远征军人数的消息有所外传，自由党的喉舌、有影响的《威斯敏斯特报》谴责了这种削弱本土防务的“鲁莽”行为。对立阵营的诺思克利夫（Northcliffe）勋爵也来反对派遣一兵一卒。虽然海军部重申了帝国国防委员会1909年所作的不可能有严重入侵的结论，但仍不能消除人们头脑中敌人会在东海岸登陆的想法。亨利·威尔逊感到极端厌恶的是，这位目前对英国安危负有重任的基钦纳，竟把原来安排好直接从爱尔兰开往法国的一个师抽调回国，并且从别的师中又抽调了两旅兵力去守卫东海岸，从而“把我们的计划搞得一塌糊涂”。因为最后的决定是：立即派遣四个师和骑兵部队——8月9日起开始上船——然后再派遣第四师而将第六师留在国内。休会时，基钦纳以为大家都已同意把亚眠作为集结地，但是别的将军却没有这样的想法。

法国总参谋部火速派来了陆军上校于盖。他一到达，威尔逊就将出发的时间告诉了他。虽然这不是一件需要对远征军的法国东道主保密的事情，但威尔逊却惹怒了基钦纳，他指责威尔逊泄密。威尔逊“顶了嘴”，他写道，他“不想受基钦纳的气”，“尤其在今天像他这样胡说八道的时候”。于是他们之间产生了，也可说加深了对远征军毫无益处的敌对情绪。在所有的英国军官中，威尔逊与法国人民的关系最为密切，约翰·弗伦奇爵士也最能听取他的意见。然而，基钦纳却认为他傲慢放肆，就此不理睬他。而威尔逊也宣称，他认为基钦纳是个“疯子”，并且认为他“对英国的危害不亚于毛奇”，他还把他的偏见灌输到那位生性好疑、易于激动的总司令的头脑中去。

8月6日到10日期间，正当列日的德军在等待攻城炮和法国得而复失米卢斯的时候，配备有军马3万匹、野战炮315门和机枪125挺的8万名英国远征军在南安普敦和朴次茅斯集结。军官们的指挥刀都是刚磨过的，闪闪发光，这是他们奉命一律在动员的第三天送修械所磨的。但这些军刀除在检阅时用以致敬外，别无其他用场。不过，据这支部队的军史纂修人所述，这支部队，除了这种偶见的骑士遗风的举止外，确是“历来踏上征途的英军中训练、组织和装配得最好的”。

8月9日，部队开始登舰，运输船每隔十分钟开出一艘。每艘船离开码头时，港内其他船只汽笛和喇叭齐鸣，甲板上人人欢呼致意。喧闹声震耳欲聋，在一位军官看来，远在列日城外的冯·克卢克将军也不可能听不到。不管怎样，海军深信他们已把海峡封锁起来，可以安全横渡海峡，而无遭受袭击之虞。运输船队在没有护航的情况下在夜间渡海。一个在凌晨4时30分醒来的士兵感到大吃一惊，他发现整个运输船队漂浮在平静如镜的海面上，发动机全都停了，附近看不见一艘驱逐舰；原来是在等候其他港口开出的船队前来在海峡中途会合。

第一批部队在鲁昂登岸，受到狂热的欢迎，一位在场的法国人说，仿佛他们是来为圣女贞德举行赎罪仪式似的。在布洛涅，另几批在高耸的拿破仑纪念碑脚下登陆，拿破仑当年便是计划从这座圆柱形纪念碑坐落所在誓师出发入侵英国的。其他运输船只进入勒阿弗尔时，当地的法国驻军爬上营房屋顶，为在强烈的阳光下走下舷梯的盟军狂热欢呼。当晚，远处传来隆隆的雷声，残阳如血，冉冉西下。

第二天，在布鲁塞尔，人们终于看见了英国同盟者，尽管仅仅是一瞥而已。美国公使馆秘书休·吉布森（Hugh Gibson）带着一项使命去找英国武官，他未经通报就步入武官的房间。吉布森发现一个脏乎乎、胡子满面、身穿野战军服的英国军官在伏案书写。武官连忙把他推出室外，后者则不客气地问其余的英国部队是否都藏在这座大楼内。事实上，英军登陆地点的机密保护得如此之好，德军在蒙斯第一次碰上他们前，完全不知道英国远征军已开抵何处和在何时到达。

与此同时，在英国，各指挥官之间的互不相容日益表面化。国王在巡视时向与宫廷关系密切的黑格询及他对约翰·弗伦奇爵士任总司令有何看法。黑格认为他有责任这样回答：“我非常怀疑，他是否具有足够平和的性情和足够高深的军事学识，使他能够胜任指挥官的职责。”国王离去后，黑格在他的日记中写道，约翰爵士在布尔战争期间的军事思想“常常使我震惊”；接着他又写下了他对阿奇博尔德·默里爵士的“看法”：默里是个“老太婆”，为了避免跟约翰爵士发生争执，他总是“姑息迁就”，明知命令谬误，还是执行不违。黑格认为两人“全都不适宜于担任他们现在的职务”。他告诉另一位军官说，约翰爵士将不愿倾听默里的意见而“宁愿信任威尔逊，这反而更为坏事”。威尔逊不是一位军人，而是一个“政客”；对于“政客”一词，黑格解释说，与“不正当的交易和错误的生活准则同义”。

黑格这个人态度温和，举止文雅，看不出有什么缺点，凡是可能于他有帮助的地方，他都有朋友。行年五十又三，生平事业一向无往不利，如今他倾吐这一番衷曲，是要为更上一层楼创造条件。在苏丹战役中，身为一名军官，他就已惯于养尊处优，在跟随他一起穿越沙漠的私人包裹驮载队中就有“一只满驮着红葡萄酒的骆驼”。

8月11日，在启程赴法的前三天，约翰·弗伦奇爵士第一次获悉一些使他感兴趣的有关德国部队的实情。他和作战处副处长卡尔韦尔（Callwell）将军一起拜访了情报处。情报处长开始告诉他们一些有关德国运用后备兵役制的情况。卡尔韦尔写道：“他不断搞出一批批新的后备师和额外后备师，就像一个魔术师从口袋里掏出一缸又一缸金鱼那样。而且，他似乎是故意为之，让人感到恼火。”这些情况，法国情报部门的第二处于1914年春获悉，为时太晚，已来不及说服总参谋部改变它对德军右翼的判断。要改变英国人的想法也为时过晚。一种新的想法，如要深入人心并从根本上改变既定的战略以及更动部署上无穷的具体细节，就得需要时间，而余下的时间却远远不够。

在下一天举行的委员会的最后一次会议上，基钦纳和将领们为了战略问题展开了一场激战。到会的除基钦纳外，还有约翰·弗伦奇、默里、威尔逊、于盖和另两名法国军官。基钦纳除非凭着心灵的耳朵，他当然听不到打通穿过列日的道路的420毫米大炮炮弹的爆炸声。虽然如此，他断言德国的“强大兵力”将从默兹河彼岸过来，他并挥动手臂，在墙头的大地图上比划了德军的包围阵势。他振振有词地说，倘若远征军集结在莫伯日，在做好战斗准备之前就有陷于重围被迫后撤之虞，这次作战是克里米亚战争以来第一次与一个欧洲国家的交锋，被迫后撤会给远征军的士气带来灾难性的后果。为了取得回旋余地，他坚持要以亚眠这个更后一些的地方作为基地。

他的六个对手，三名英国军官及三名法国军官，都同样毫不动摇地坚持原来的方案。约翰·弗伦奇爵士本人原来建议向安特卫普转移，而今在威尔逊的授意下，则坚决表示任何变化都将“打乱”法国的作战计划，仍旧主张向莫伯日进军。法国军官强调了填补其战线左翼末端空白地区的必要。威尔逊对于把部队集中在亚眠的这种“懦夫之见”则是五内俱焚。基钦纳说，法国的作战计划是危险的；他说，他们不应采取攻势，他“完全反对”这样做，而应等到德军发动进攻时予以反击。争吵持续三个小时，最后基钦纳被迫逐步作出让步，尽管他还没有被说服。作战计划早已存在，五年来，他一直知道这个计划，而且根本不赞成。如今，部队已经上船出海，他只能接受这个计划，因为已没有时间再拟定新计划了。

最后，基钦纳作了一个无可奈何的姿态——或者是一种旨在开脱自己责任的姿态——他带领约翰·弗伦奇爵士一起去向首相汇报争论情况。正如威尔逊在其日记中所吐露的那样，阿斯奎斯“对这事根本不懂”。他所作的决定，不出人们所料。他在听取基钦纳陈述他跟联合总参谋部的专家们一致意见相左的看法后，表示同意总参谋部的意见。远征军由六个师减为四个师，按原计划行动。这种按既定计划办事的势头又一次获得了胜利。

然而，与法德两国的陆军大臣不同，基钦纳仍保留有指挥本国军务的大权。他现在给约翰·弗伦奇爵士发出的有关远征军在法国行动的命令，反映了他意图限制远征军在战争初期所应承担的责任。丘吉尔预见到英国海军行将担负的重任，因而命令地中海舰队既要同“格本”号交战又要避开敌人的“优势火力”。跟丘吉尔一样，基钦纳现在预见到他必须建立起一支数百万人的大军，因而给远征军规定了不相协调的方针和任务。

他写道：“你所统率的部队，其主要目的是支持和配合法国陆军……并协助法军阻止或击退德军入侵法国或比利时领土。”他带着某种乐观情绪继续写道：“并最终恢复比利时的中立”——这个计划好比要为姑娘恢复童贞。鉴于“英军及其配属的增援部队的兵力非常有限”，必须“经常牢记”，“尽最大努力把死亡和损耗减到最低限度”乃属必要。基钦纳的命令反映了他不赞成法国的进攻战略。命令指出，在被要求参加任何“前方调动”时，如果在此调动中，法军并未投入大量兵力，或有使英军“过分暴露易受敌军攻击”之虞，约翰爵士应当首先请示本国政府，同时还必须“清楚地了解，你的指挥权是完全独立的，在任何情况下，在任何意义上，你都不受任何盟军将领的节制”。

这番话毫无模棱两可之处。基钦纳已经一笔勾销了统一指挥的原则。他的动机是把英军作为未来的核心力量来保存。而这样做的后果，对一个具有约翰爵士那样气质的指挥官来说，实际上是取消“支持与配合”法军的命令。这种思想，即使是在约翰爵士去职和基钦纳本人去世之后很长一段时间内，还不时露头，影响盟国在战事上作出努力。

8月14日，约翰·弗伦奇爵士、默里、威尔逊以及一位取了一个振奋人心的名字的陆军少校参谋赫里沃德·韦克（Hereward Wake）爵士，一起到达亚眠。英军在这儿下火车，然后继续前往勒卡托（Le Cateau）和莫伯日周围的集结地区。英军开始出发那天，克卢克的部队也开始从列日向南移动。英国远征军高高兴兴地朝着通往勒卡托和蒙斯的道路前进，沿途人群不断报以“英国人万岁！”的热烈欢呼声。这种欢乐气氛，使基钦纳勋爵向全军发布的使人扫兴的通告增添了说服力，通告指出他的军队可能会“遇到美酒和女色的诱惑”，全军必须“一律抵制”。英国军队越往北走，欢迎的热情越高涨。人们纷纷飨以热吻，赠以鲜花。摆设盛放食物和饮料的台子，招待英军，分文不收。有一处栏杆上挂了一块红台布，上面缝着一些白布条，权代英国国旗上的圣安德鲁十字的图案。士兵们把部队臂章、帽子和皮带抛给那些索取纪念物的笑容满面的姑娘及其他景慕他们的人。不久，英国部队便只好头戴农民的花呢帽子，用绳子束住裤子开向前方。一个骑兵军官后来写道，一路上“我们受到人们的盛宴款待和热烈欢呼，但不消多久，他们便要看见我们向后败退了”。如今回顾当时的情景，他记忆中的英国远征军进军蒙斯，真是“一路春风得意”。

---

注释

[^1]:  参孙倾覆神室（Samson's temple），系基督教《圣经》故事，见《旧约·士师记》第十六章。参孙，以身强力大著称，为了报非利士人剜他双眼的仇，抱住托房的那两根柱子，尽力屈身，房子倒塌，压住首领和房内的众人。这样，参孙死时所杀的人，比活着所杀的还多。——译注

---

Unless otherwise noted, facts about the BEF are from Edmonds, and all quotations from Sir Henry Wilson and Haig are from their diaries, edited by Callwell and Blake, respectively.

“既无头脑，又要故作姿态”：Philip Gibbs, Now It Can Be Told.

“大礼服”、“笨蛋”：Childs, 134.

基钦纳蔑视把英国军队“钉在”法国战略尾巴上的计划：qtd. F. Maurice, Life of General Rawlinson, 95.

“我们必须准备好”：qtd. Magnus, 284. On Kitchener's views, see also Esher, Tragedy, 31, 38–9.

威灵顿公爵：qtd. Hurd, British Fleet, 29.

“像鹧鸪一样”：qtd. Magnus, 279.

“他们自己先见鬼去吧”：Esher, Journals, III, 58.

“安定民心”：Arthur, 13.

“他不是凭推理，而是凭直觉中一闪而过的灵机”：Grey, II, 69.

对德作战“势所必然”：Wilson, 112.

“切实掌握中小局面的战术”：article on French, DNB.

“弗伦奇是一个勇敢的人”：qtd. Trevelyan, 198.

“我并不认为他特别聪明”：to the Duke of Connaught, May 23, 1915, qtd. Nicolson, George V, 266.

“反复无常的气质”：qtd. Cruttwell, 23.

“一颗爱作奇想的稚子之心”：Esher, Tragedy, 43.

8月5日的作战委员会会议：Churchill, 248–55; Haldane, 296; Wilson, 158–9; Blake, 68–9; Esher, Tragedy, 24.

“对英国的危害不亚于毛奇”：td. Magnus, 302.

军官们的指挥刀都是刚磨过的：Memoirs of Field Marshal Montgomery of Alamein, New York, 1958, 30.

“历来踏上征途的英军中训练、组织和装配得最好的”：Edmonds, 11.

在一位军官看来，远在列日城外的冯·克卢克将军都能听到英国的欢呼声：Childs, 115.

鲁昂的目击者：qtd. Poincaré, III, 31.

隆隆的雷声和如血的残阳：Childs, 117.

黑格告诉另一位军官：Charteris, 11.

弗伦奇爵士和卡尔韦尔将军拜访情报处：Callwell, Dug-Out, 17.

8月12日的作战委员会会议：Huguet, 41–2; Wilson, 162–3; Arthur, Kitchener, 22.

基钦纳的指令：Edmonds, Appendix 8.

“美酒和女色的诱惑”：text in Spears, Appendix XIII.

沿途的法国民众招待英军：Corbett-Smith, 32.

“一路春风得意”：Bridges, 75.