   

## 第182回 赫尔利大办牛仔外交 宋美龄小试纵横说辞

话说赫尔利送走史迪威后就开始办他心目中的一件头等大事——拉拢国共进行和谈。

以下是驻延安观察组组长包大卫上校的一段描述：

延安是没有什么飞机的，每次当重庆有飞机到达延安时，总是被人们看做一件大事。1944年11月7日，我和周恩来、我们组的全体人员以及一大批中国人都到飞机场去迎接一架来自重庆的飞机。飞机停了下来，机门打开了，第一个走出来的是一位个子高高的、满头灰发、富有军人气概的标准美男子。他那一套军服是我生平所见过的剪裁得最漂亮的军服。军服上佩戴了各种奖章和绶带，大概美国内战以来所有各次战争的奖章都有了。这是美国总统特派代表帕特里克·赫尔利少将。没有人通知我们说他要来访问延安。他的目的(我是事后才知道的)是充当国共两党的调停人。

显然，周恩来对来了这么一位神气十足的将军感到十分意外，他问我这位贵宾是谁。我告诉他说，这是赫尔利将军，当他在胡佛内阁内任陆军部长的时候我曾在纽约见过他一面。周恩来立即对我说：“劳你驾把他缠住一会儿，我去请毛主席来。”说完就走了。

没有一会儿(比我设想的快得多)，毛和周乘了一辆汽车来了。这是一辆装了篷的旧卡车，是我所见过的延安的唯一一辆机动车。汽车后面又来了一个连的步兵，显然这是临时召集起来的。毛正式对赫尔利表示欢迎，这一连人就站在一旁作为仪仗队接受检阅。赫尔利在检阅完毕之后，突然抖擞精神，学印第安人大叫三声。我完全注意到，毛和周见此情况弄得莫名其妙。

接着，毛主席和赫尔利就坐进了这辆古老的汽车，我挤在他们身旁作翻译。这是一次十分困难的翻译，因为赫尔利讲的全是不着边际的话，他看到农村景象就大谈其俄克拉何马州的老朋友。路旁有一位老农在赶毛驴，汽车惊了毛驴，不听指挥，赫尔利又一次出其不意地大叫“查利，给它左边一鞭子”，又一次把毛、周两人弄糊涂了。

赫尔利的宾馆也是窑洞，对此他倒没有任何难色。但他一开始就对包大卫吹嘘说，当年他当陆军部长任满后，曾代表辛克莱石油公司跟墨西哥政府进行谈判，因为后者扬言要没收辛克莱公司的油田。结果谈判成功，他获得了100万美元的报酬。在他心目中，国共谈判也将如此简单地获得解决。

在第一次正式谈判中，赫尔利向毛泽东提出了国民党的谈判草案，共有五条：国民政府和中国共产党将共同努力，统一中国的全部军队，以便迅速击败日本和建设中国；中国共产党的军队将服从和执行国民政府和军事委员会的命令；国民政府和中国共产党将支持三民主义，以此建立一个民有、民治、民享的政府，两党将采取旨在促进进步和民主的政策；只能有一个全国政府和一个军队，共产党的军队在获得中央政府承认以后，其所有的官兵将获得政府官兵同样之薪给和待遇，其部队之供应也将与政府部队一样；国民政府承认共产党，并将认为它是一个合法的政党。所有其他政党也将获得合法地位。

![[/美国演义(第6版)(插图本) - 曹德谦/images/00287.jpeg]]

1944年11月，毛泽东、朱德在延安会见罗斯福的私人代表赫尔利。赫尔利是第一个在政治问题上与中国共产党打交道的美国人。从某种意义上说，赫尔利来华代表着一个新的美国对华政策时代。然而事实似乎证明，赫尔利是个“爱出风头、无知而又傲慢的家伙”，“对中国政治、历史和文化可以说基本上全无了解”。

在第二次谈判中，毛泽东询问赫尔利：美国政府是否真正希望民主，世界范围内的民主?举例说，它是否认为中国应当搞民主？它是否希望中国政府是一个真正代表中国人民的政府?它是否注意到它现在所承认的中国政府不是一个合法的政府，它并不代表中国人民。蒋介石是国民党一党选出来的，而且只是90名国民党代表选出来的，连自己的党员恐怕也不能代表。希特勒可比他强，希特勒毕竟是人民选的，而且他还有一个联邦议会。美国是否注意到中国人民不信任国民党，不支持国民党?当然，问题关键不在于美国政府是否看到这一事实，而是它是否愿意促进中国之民主。毋庸讳言，国民党必须改革，现政府必须改组。按现在这个样子，它是不可能打胜仗的。即使靠美国来打胜仗，打胜以后也仍然将是一个烂摊子。政府应当扩大基础，把所有各党各派都包括进去。

赫尔利说，蒋介石为了表示愿与共产党合作，他已同意共产党派一名委员参加军事委员会。毛大笑说，这个建议一个钱也不值。赫说，共产党至少可以伸一只脚进去。毛说，如果一个人的双手被捆住了，进去一只脚又有什么用!赫说，既然共产党不能接受国民党的条件，那就请共产党提自己的条件。

在第三次谈判中，毛泽东提出了自己的条件，赫尔利看了后很愉快地说：“在我看来，你们的条件完全是合理的，我觉得你们提得还不够。如果毛主席不介意的话，我想带回去研究一下，提一些建议，明天早上再谈。”

在第四次谈判中，赫尔利把他增补过的共产党条件交还给毛泽东。这些条件的具体措辞就成了这样：

国民政府、中国国民党和中国共产党将共同努力统一中国境内所有之武装部队，以便迅速击败日本和建设中国。

国民政府应改组为联合国民政府，包括一切抗日政党和无党派团体之代表。应颁布和执行一种新的政策，规定对军事、政治、经济和文化进行改革。同时，军事委员会应改组为统一军事委员会，包括一切抗日军队之代表。

联合国民政府将支持三民主义，以此建立一个民有、民治、民享的政府。联合国民政府将采取各项政策以求促进进步和民主，确立正义、信仰自由、新闻自由、言论自由、集会自由、请愿自由、居住自由以及人身保护法。联合国民政府也将采取政策实现免于恐惧之自由及免于贫匮之自由。

一切抗日部队将服从和执行联合国民政府和统一军事委员会的命令。一切抗日部队将获得国民政府和军事委员会的承认。来自外国的援助将作平等的分配。

联合国民政府承认中国国民党、中国共产党和一切抗日政党为合法政党。

赫尔利把修改过的中共原稿交毛周阅毕后，问道：“毛主席，你认为怎么样?”毛泽东毫不迟疑地回答说：“可以。”

赫尔利大喜，以为事情已经办成，马上趁热打铁地说：“那么，我们是不是可以在上面签字为凭?”毛泽东又毫不迟疑地说：“可以。”

毛泽东在签字时故意把第一位的空白留了下来，自己签在第二位的空白上，赫尔利签在第三位的空白上。不用说，那第一位空白是留给蒋介石的。

在赫尔利看来，蒋介石的签名是不成问题的。第二天，他就带包大卫一道返回重庆，并要求中共派一代表前往，毛泽东就派了周恩来同机前往。在飞机上，包大卫就同周恩来聊了起来。包大卫能操相当流利的华语，所以双方的谈话是不成问题的。突然间，这位美国上校提了一个尖锐的问题：“周将军，你认为苏联的民主与美国的民主相比，到底哪一个好些?”周恩来用了3秒钟的时间进行思考，然后回答说：“苏联的民主是最高级的民主，中国现在还望尘莫及。因此，如果我们中国人能做到美国的民主，那就是中国人民的莫大幸福。”周恩来的这一回答，既没有伤害苏联，同时又使包大卫听了觉得美滋滋的。

赫尔利到达重庆后就去见蒋介石，在这类问题上，蒋介石要比赫尔利高明一百倍。他看了赫尔利交给他的稿子后，仍然春风满面，笑容可掬地说：“将军，今天我们是纯粹为你洗尘，不谈政治，改天我们将作一长谈。”赫尔利没有办法，只好留下来共进晚餐，然后返寓。

赫尔利刚出门，蒋介石马上召集宋子文、张群、何应钦、戴笠开紧急会议，当然还少不了蒋夫人宋美龄。宋子文是美国问题专家，免不了要首先发言。他说：“我一眼看这个稿子，总觉得这是出于赫尔利的手笔。请看‘人身保护法’这个词儿，这是一个典型的美国词儿，我们中国人从来不使用这个词儿，延安也从来不使用这个词儿，我猜中共恐怕根本不知道有这个词儿的存在。”他又故意刺戴笠一下说：“雨农兄，你说我们中国人有‘人身保护法’这个词儿吗?”戴笠只好狼狈地答曰：“没有吧!”宋子文接着说：“我还可以举一个例子说明赫尔利是个笨蛋。他来华之时途经莫斯科，会见了斯大林。你们知道他跟我说什么?他说：‘子文，我给你带来了一个好消息。斯大林已经拍着胸脯向我保证，俄国人只承认国民政府，绝不承认延安。俄国的一切援助都将给重庆国民政府绝对不会给中国共产党。他说他不喜欢中国共产党，他喜欢蒋介石，只有蒋介石能安定中国。’我问他相信斯大林的话吗?他说他当然相信。他说他有生以来不骗人，也从来没有被人骗。现在要解决这个问题是不难的。不过还得有劳美龄去跑一下。很简单，你到赫尔利公馆去，可以给他狠狠的两个耳光，然后再亲他一下，说：‘我的宝贝，你受骗了。’事情就可以解决。”大家禁不住哈哈大笑。

这时，戴笠却有机会了。他说：“我已经掌握材料，可以说明美国大使馆的参赞范宣德、一秘戴维斯、二秘谢伟思都是共产党同路人。史迪威虽走，史迪威的这个班子还在，与其说赫尔利受毛泽东骗，还不如说受大使馆班子之骗。”

宋子文又说：“对。牛仔一贯自以为正直，他也要求对方正直。一旦发现自己受骗，立即就可以由绵羊变为斗牛，疯狂地向红巾扑过去，至死而后已。”

最后由蒋介石收场说：“辛苦大家了。看来还是要由夫人来扭转乾坤了。请大家回家吧!”

原来，蒋介石认为他一生中的一个大杰作就是跟宋美龄结婚。这自然是一种自我吹嘘和自我欣赏。但从历史角度来看，也不能说全无道理。西安事件上，宋美龄飞西安跟中共代表进行谈判。在开罗会议上，她充当了蒋介石的翻译，跟罗斯福和丘吉尔进行了会谈。特别是在珍珠港事变后，她充当蒋介石私人代表，住进白宫，到处演讲，大吹中国抗战，把美国人弄得神魂颠倒，风靡一时。

第二天，宋美龄拿了戴签所收集的情报，去见赫尔利。她开门见山地对赫尔利说：“将军，我希望你记得，破译日本密电码的不是别人，是我们中国的谍报人员。现在我们要告诉你，你手下的人都是共产党的同路人，这不是瞎说的，我们不能随便诬赖好人，我们是有凭有据说这些话的。”说着，她就打开材料，一五一十向赫尔利念。当然，我们不知道她念的是什么情报，但无疑的是，这个炮弹真正打中了赫尔利。从那以后，赫尔利就不再充当公证人，而成了蒋介石的代言人。他马上撤回了对中共五点建议的支持，转而支持蒋介石提出的新三点。这新三点的要点其实也只有一点，那就是：中共的军队必须完全受国民政府军委员会的指挥。

赫尔利命包大卫带了这样三点回延安与毛泽东商量。包大卫再一次会见了毛泽东和周恩来。包汇报了情况，并把新三点交给了毛泽东。毛泽东给蒋介石以迎头痛击，说到兴奋处，他站了起来说道：“蒋介石的军队是镇压人民的，我们的军队是保卫人民的，怎么可听他的指挥，蒋介石一贯是欺压人民，他早就该下台了。”

包大卫看到毛泽东大发脾气，不知如何是好，只得细声细气地说：“主席，我是否可以提醒一下，站在你前面的是包大卫，不是蒋介石。”这时，周恩来乃插话说：“Take it easy，Colonel。”最后，毛泽东又坐下来说：“但是，赫尔利将军曾经签字支持五点，如果有必要，我们可以把一切事实公之于世。”

包大卫碰了一个钉子，又空手返回了重庆，向赫尔利作了汇报，并且说：“弄得不好，共产党可能要公布你的签字。”这一点，又触动了赫尔利的神经。因为一名牛仔最怕的就是有人说他不诚实，说他背信弃义。在牛仔看来，不诚实比死还要难堪。因此，他又禁不住作印第安人大吼三声，并怒叫曰：“他妈的，共产党抓了我的辫子了。我可不是怕抓辫子的。”包大卫又一次不知如何是好，只得又一次细声细气地说：“将军，是不是可以允许我提醒一下，站在你面前的是包大卫上校，不是毛泽东。”

就这样，美国调停国共关系的第一幕演出宣告结束。第二幕就有待马歇尔将军来担任主角了。当然，国共关系是内在因素决定的，失败或成功不在于美国，更不在于赫尔利，赫尔利只是在舞台上跑了一下龙套而已。

正是：

兴败成亡有定局，山姆岂能挽回天。

牛仔徒劳三板斧，怎抵书生多远见。