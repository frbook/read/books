   

## 第220回 侵古巴美国总统丢尽脸皮 撤导弹赫鲁晓夫出足洋相

卡斯特罗在古巴建立革命政权后，就有一大批反革命分子逃往美国。中央情报局头子艾伦·杜勒斯把他们收留起来并加以组织，成立了所谓的古巴旅，准备随时伺机返回古巴搞反革命复辟。他们还特地在危地马拉设置了一个训练基地。

肯尼迪就职还不到一个星期，中央情报局就来找他，要他立即派古巴旅发动进攻。肯尼迪刚上任，还没有弄清楚事情的底细，他要求给予充分的时间进行考虑，但艾伦·杜勒斯欺肯尼迪没有经验，竟大摆老资格，向他进行了一次出奇的“推销”。

杜勒斯说：现在是古巴旅取胜的唯一机会，错过这个机会以后就难办了，因为卡斯特罗很快就会从俄国人那儿得到一大批米格式飞机，估计在6月1日以前，卡斯特罗就能装备起足够数目的飞机编入现役，由正在捷克斯洛伐克受训的古巴飞行员驾驶。那时古巴旅就没有可能在海滩登陆建立滩头阵地了。杜勒斯还说：危地马拉总统伊迪哥拉斯已通知，在马德雷山区受训的古巴人到4月以后就不能再在那儿呆下去了，因为雨季一来，那儿将成一片大沼泽，不可能进行训练。现在箭在弦上，势在必发，我们准备充分，势在必胜，古巴的解放可以说万事俱备，只欠东风，那东风就是总统的点头。

肯尼迪仍半信半疑，拿不定主意。于是杜勒斯二次进言曰：“如果总统不批准这个计划，就等于不容许那些爱好自由的流亡者从共产党独裁政权下解放自己的祖国，等于鼓励古巴去颠覆拉丁美洲各国的民主政府，并且为1964年的总统选举制造一个难题，因为感到幻灭的古巴旅必然会在共和党资助之下，指责总统背弃他们，姑息卡斯特罗。”

肯尼迪问道：“那么，你看有成功的把握吗？”

这个问题正中杜勒斯下怀，他立即口沫四溅，大吹起来：“胜利是没有问题的，我们已在1954年的危地马拉事件中获得丰富的经验。我可以坦白地说，与危地马拉事件相比，我们现在的力量要比那时至少大一倍有余，准备的时间也至少充裕得一倍有余。我告诉你，当时我就站在这儿，站在艾森豪威尔总统的办公桌旁边，我拍着胸脯向艾克担保我们的危地马拉行动计划一定会成功。艾克完全相信我的话，我们的计划一丝不差地取得圆满成功。总统先生，我可以说，现在我们古巴旅的计划的前景要比那时更好得多。”

肯尼迪又向参谋长联席会议征询意见，参谋长联席会议主席兰尼兹将军回答说，他已看了这个计划，他认为成功是不成问题的。

肯尼迪最后派了一名代表，到危地马拉训练基地去做一次实地考察。这位代表报告说：“我的观察增强了我的信心，使我认为这支部队不仅能够完成初期的作战任务，而且有能力实现推翻卡斯特罗这一最终目的。他们说，他们深知其本国人民，认为只要给对方的军队一次沉重的打击，那些军队就会土崩瓦解，立即抛弃他们本来无意支持的卡斯特罗。不管卡斯特罗能使出多大力量，他们对胜利是有绝对把握的。”

肯尼迪终于让步，批准了行动计划。

1961年4月，中央情报局用卡车把古巴旅送到尼加拉瓜的卡贝萨斯港，由那儿上船出发。目标是：在猪湾占领三个滩头阵地，同时由古巴旅的伞兵夺取位于古巴本岛和大海之间的萨帕塔大沼泽地上的几个据点。

4月16日晚，古巴旅的5艘运兵船和两艘护航舰到达了猪湾，抛锚停泊。晚上11点，司令官佩佩·圣罗曼到甲板去透透气，一看可把他吓呆了。他原听说海岸是空无一人的，现在却是一片灯火。原来向他介绍情况的人所说的猪湾是三年前卡斯特罗政权以前的猪湾。现在这一带已建成公园。中央情报局认为无法通行的沼泽地带早已修起了现代化公路。三个旅游中心的修建工程接近完成，其中之一就高耸在古巴旅的第一个目标吉隆镇上。汽车旅馆、快餐馆和浴室，全部近200所建筑物，都已快完工。从1月份以来，每逢周末总有成千旅客从哈瓦那乘车来参观。当古巴旅的运兵船开始看到陆地时，最后一批游客的车子刚刚离开几个小时。

第一批登陆的叛军是潜水员，他们的任务是设置登陆指示灯。当他们刚安放第一盏引导部队登陆的指示灯时，海滩上就亮起了吉普车的头灯。这是一支民兵巡逻队。吉普车一转头停了下来，车灯直照在潜水员身上。后者向车子开火，一辆载着武装民兵的卡车迅速开来支援吉普车。尽管运兵船上的炮火消灭了卡车，但出其不意的袭击因素已告吹了，炮声向整个古巴敲响了警报。

卡斯特罗从睡梦中惊醒，他立即做出了反击的布置。在事后的报告中，卡斯特罗描绘这次战斗道：

雇佣军想建立据点的地方正是吉隆滩，正是沼泽地区，这个我们已经建了许多公路的地方。但他们不了解也完全不考虑他们要去的地方的居民思想。为什么？因为从地理上说来，那个地方是适合他们的计划的。

美国把最有效、最现代化的武器大量地交给他们。诸位请想一想，三天内，我们缴获了60支火箭筒。他们有8门4.2口径的重迫击炮，好几门75毫米口径的无后坐力反坦克炮、57毫米口径的无后坐力反坦克炮，5辆恭尔曼坦克或M-41坦克，10辆装甲汽车，上面装备着机关枪。他们认为完全掌握制空权是毫无问题的。完全掌握了制空权再加上控制了沼泽地区的入口，他们就可以处于必胜的地位了。于是靠着他们所占的这块土地，他们就可以处于必胜的地位了。于是靠着他们所占的这块土地，他们就可以开始公开对我国进行消耗战。这是他们的计划，能够设想得到的地方他们都周密地设想过了，但正如世界一切反动派一样，反动派总是要做错误的估计。其错误之一就是对古巴空军的估计。古巴旅的内部通知说：“古巴空军已经完全解体，空军中已经没有有能力的飞行员和熟悉维修与通讯的专门人才。由于维修不良和缺乏零件，大部分的飞机都已陈旧不堪，不能作战，能够使用的几架飞机，也只是能起飞而已，不完全能够作战。古巴空军作战的能力几乎等于零。”

但正是古巴的那几架飞机，在勇敢的爱国驾驶员操作之下，炸沉了古巴旅的船只，使叛军进无所据，败无所退，只好成批地当上了俘虏。

不过，反动派更大的错误估计是对民心的估计。艾伦·杜勒斯曾吹嘘：对卡斯特罗感到失望的古巴人多得很，足以保证这次登陆成功。已有2500名古巴人参加了各种反卡斯特罗组织，另有2万人同情反卡斯特罗运动。只要古巴旅建立滩头阵地，古巴人民就将群起而拥护之。但事实证明，古巴人民都是爱国的人民，在反对侵略这一点上，古巴人民永远是支持政府抵抗外来侵略势力的。

![[/美国演义(第6版)(插图本) - 曹德谦/images/00339.jpeg]]

预谋武装颠覆卡斯特罗政权的古巴旅被古巴军队成队俘虏，由美国策划的“猪湾行动”宣告失败。这次未成功的进攻不但是一次军事上的失败，而且也是一次政治上的失误。刚刚上任90天的约翰·肯尼迪政府为此大失信誉，相反的卡斯特罗政权和古巴革命则被巩固。

入侵古巴的失败使刚上任的肯尼迪大丢其脸。他后悔地说：“我当时怎么会这样轻率呢？我一生从来不迷信专家。我怎么会这样愚蠢，竟让他们放手去干呢！”肯尼迪对这次失败耿耿于怀，随时想寻找机会，拉平比分，而赫鲁晓夫果真不久就赐了他一个良机。

1962年秋，赫鲁晓夫以帮助古巴为名，要在古巴设置地对地导弹。卡斯特罗虽然在开头表示反对，但经不住赫鲁晓夫的劝说和利诱，到底还是点了头。于是苏联导弹就陆续地、秘密地由俄国运至古巴。中央情报局也探听到了一些似可信似不可信的风声。

9月初，肯尼迪的弟弟、司法部长罗伯特·肯尼迪召苏联驻美大使多勃雷宁至司法部谈话。多勃雷宁对罗伯特说，赫鲁晓夫已指示他要肯尼迪总统放心，苏联绝不会在古巴设置地对地导弹或其他进攻性武器。他还说，赫鲁晓夫喜欢肯尼迪总统，他不想使总统为难。

多勃雷宁的话一点没有解除肯尼迪的疑虑。总统在他弟弟的建议下，于9月4日发表声明，表示美国绝不容忍把进攻性的地对地导弹或任何其他进攻性武器引进古巴。9月11日，苏联大使馆人员交给罗伯特·肯尼迪一封信，托他转交给总统。这是赫鲁晓夫的亲笔信。赫鲁晓夫在信中明确地说，他希望总统放心，苏联绝不会把地对地导弹运往古巴。

1962年10月16日，中央情报局对间谍飞机U-2在古巴上空所摄的照片进行了仔细的分析，发现在古巴的圣克里斯托瓦正在建筑导弹发射场。肯尼迪接到情报局报告后立即召开内阁会议，商议对策。总统决定进一步进行侦察。10月17日，情报局提供新的照片，表明圣克里斯托瓦确有中程导弹发射场外，还发现在圣克里斯托瓦和哈瓦那之间的瓜纳哈伊地区以及古巴东部的雷梅迪奥斯，设有远中程导弹发射场。苏联用这些设备可以向美国本土的目标一次集中发射40枚核弹头。

总统安全顾问邦迪和各位参谋长主张采取“外科手术”，也就是对基地实行突然的空袭。国防部长麦克纳马拉表示反对，他主张采取封锁或曰隔离的办法，逼苏联撤回导弹。但他又说，他已在准备飞机、人员和炸弹以供轰炸之用，如果最后作出的决定是轰炸的话。

10月22日，肯尼迪在下午7时发表电视演说，他说，在过去的一周里，已有确凿的证据表明在古巴正在修建一系列进攻性的导弹发射场。修建这些基地的目的只可能是为了提供向西半球进攻的核打击能力。他说，为此，美国将对古巴实行封锁，直至导弹撤走为止。他还说，封锁是初步措施，他已命令五角大楼为进一步的军事行动做好一切必要的准备。

麦克纳马拉已做好计划，列出战备需要：25万兵员，对古巴各种目标进行2000架次出击，9万名海军陆战队和空降部队的入侵力量。部队正迅速开往美国东南部，他们都已装备齐全，并做好了准备。已开始把100多艘军舰集中起来，供入侵之用。

10月23日，罗伯特·肯尼迪往苏联大使馆会晤多勃雷宁，后者竟再一次一口咬定说，在古巴没有导弹。

10月24日，封锁早已开始了，但在上午10时，麦克纳马拉报告，有两艘俄国船——“加加林号”和“科米莱斯号”，已经距离封锁线只有几海里了。可能在华盛顿时间正午以前需要对其进行拦截。同时，海军方面又报告说，有一艘俄国潜水艇穿插于两艘俄国船之间。海军决定派埃塞克斯号航空母舰前往阻拦，它将用声纳信号通知该潜艇浮出水面以证明身份。如果对方拒绝这样做，将使用小型的深水炸弹使之浮出水面。

![[/美国演义(第6版)(插图本) - 曹德谦/images/00340.jpeg]]

1962年10月24日，肯尼迪面对电视镜头，宣布对古巴实行封锁。

这时，肯尼迪陷入了最紧张的状态，他弟弟这样描写哥哥：

我认为这几分钟是总统最为关注的时刻。世界已处于大规模毁灭的边缘了吗？这是我们的过错吗？还有什么该做的事或者说不应该做的事吗？他把一只手伸到脸上，蒙住了嘴。他把拳头放开了又捏拢。他的脸好像拉长了。他的眼睛有痛苦的神情，简直变成灰色。

我模模糊糊地想到他过去生病，病得快要死的情况；想到他失去孩子的情况；想到我们获悉大哥死亡时的情况。我仿佛不知道自己的存在。然后，我听到总统说：“有没有办法可以使我们不首先同一艘俄国潜艇交火呢？除此之外，简直就没有别的办法了吗？”麦克纳马拉回答说：“别无其他办法。已经指示司令官要尽可能避免发生战争。但是，我们也必须预料到会发生战争。”

我们已处于进退不得的悬崖的边缘。决定就在此时此刻。不是明天，那样的话我们或许还可开个会商量商量；也不是8小时之内，那样的话，我们还可以给赫鲁晓夫拍个电报。总统已把这事态开了个头，但他已无法控制事态的发展。现在只有听天由命了。时间的分分秒秒，都显得太慢了。

然后，信童突然送来了一个信息：俄国的舰只已停止了前进。

危机暂时缓和了，但远未结束。10月25日，赫鲁晓夫终于送来了一封和解的信，信中说：

如果战争果真爆发了，那么，这场战争将不是我们的力量所能制止的，战争的逻辑就是这样。我参加过两次大战，知道战争要到它碾过了许多城市和乡村，到处播下了死亡和破坏的种子之后才会结束。

如果美国总统做出保证说，他绝不参与进攻古巴并解除封锁的话，那么撤除和销毁古巴导弹基地的问题就是一个完全不同的问题了。

这就是我的建议：绝不再向古巴运送武器，已经在古巴的那些武器，则加以撤除，你们则以解除封锁和答应不入侵古巴作为交换。

赫鲁晓夫在信中已承认苏联在古巴设置了导弹，这是他自己打自己的耳光，但毕竟是一大进步。

可第二天，苏联外交部又给了美国一封信，内称:我们将从古巴撤走我们的导弹，而你们应从土耳其撤走你们的导弹。苏联将保证不入侵或干涉土耳其的内政；美国也应该对古巴做出同样的保证。

这是与赫鲁晓夫的亲笔信相矛盾的，国务院的谋士们做出决定：美国将接受赫鲁晓夫的信的建议，并据此作答，同时假装不知道有第二封信。

赫鲁晓夫无可奈何，只好答应在适当监督和核查之下，撤走导弹。当撤走导弹的俄国船只驶进大洋时，美国飞机在上空飞翔，要求打开货舱察看所载导弹。苏联舰长也只好乖乖地打开货舱，任美国人员在空中察看。

赫鲁晓夫在比赛“两人相对看谁先眨眼”中显然打了一个败仗，但他在自传中不但不以为耻，反以为荣。他写道：“我们赢得了美国不入侵古巴的保证，这是一个伟大的胜利。”

赫鲁晓夫做出撤走导弹的决定前没有同卡斯特罗商议，这引起了古巴人的极大反感，古苏关系一度出现阴云。赫鲁晓夫急派米高扬前往古巴进行安抚，并答应增加经济援助。一场小风波才算平息。

10年以后，卡斯特罗自我检讨说：事实证明，赫鲁晓夫的决定是正确的，我当时反对撤除导弹是错误的。

正是:

两霸大玩rou-le-tte，亿万生灵视等闲。

更有肿脸充胖子，苦丸卖作蜜糖般。