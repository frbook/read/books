 

![[/哈利·波特全集[套装全7册]/images/logo.png]]

来自 J.K. Rowling

“Pottermore”是J.K.Rowling的电子出版、电子商务、娱乐消息及新闻相关公司，也是哈利·波特系列及J.K. Rowling魔法世界的全球电子出版商。Pottermore.com作为J.K. Rowling魔法世界的数字发行核心，致力于解放想象力。除了提供新闻、特辑、与文章之外，也将公开J.K. Rowling先前未曾发布的作品。

请访问 [[http://www.pottermore.com\|pottermore.com]]