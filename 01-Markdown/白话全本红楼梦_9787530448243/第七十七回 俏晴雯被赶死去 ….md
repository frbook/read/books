﻿    

## 第七十七回 俏晴雯被赶死去 …

　　中秋过去了，熙凤的病也比原先好多了，虽然还没痊愈，但已经可以出门走动了。王夫人让大夫仍旧每天诊脉，又开了丸药方子来配调经养荣丸。药方需要上等人参二两，王夫人回家去拿，翻找了半天，只在小匣子里找到了几枝簪子粗细的。王夫人看了嫌不好，命令再找去，又找了一大包须末出来。王夫人恼火地说：“用不着偏有，但用着了，又找不着。我天天说叫你们查一查，都归拢在一起。你们根本不听，就随手乱放。你们不知它的好处，用起来多少钱买来还不一定好用呢。”彩云赶紧委婉地提醒：“应该是没有了，就只有这个。上次那边的太太来找了一些去，太太都给她了。”王夫人什么也听不进去了：“没有这样的事儿，你再仔细找找。”不管是谁，一出了问题，就会很着急，就会怪别人，领导或者长辈可能更明显。没办法，彩云只好又去找，拿了几包药材来说：“我们不认识这个，请太太自己看看。除这个再也没有了。”人参她能不认识？这是故意的吧。王夫人打开一看，也忘了都是什么药，但没有一枝人参。她又派人去问熙凤，熙凤说：“虽然有几枝，也不是好的，每天还要煎药用呢。”王夫人听了，只好派人到邢夫人那里问去。邢夫人说：“因为上次没了，才到你那里找，早已经用完了。”王夫人没办法，只好又亲自去问贾母。贾母忙叫鸳鸯去拿，竟然还有一大包，都有手指头粗细，就称了二两给王夫人。王夫人出来交给周瑞媳妇拿去，让小仆人送到医生家里，又叫把那几包不认识的药也带去，让医生认认，各包都写上名字。

　　过了不久，周瑞媳妇拿着东西回来说：“这几包药都写上名字了。这一包人参确实是上等的，但时间太长了。这东西和别的东西不同，不管是怎样好的，过一百年，自己就成了灰了。现在这个虽然还没化成灰，然而已成了腐朽的烂木头，没有什么药效了。请太太收起这个，不拘粗细，再换些新鲜的就好。”王夫人听了，低头没说话，半天才说：“这可没办法了，只好去买二两来吧。”她也没心再看着老人参了，摆摆手说：“都收起来吧。”她又对周瑞媳妇说：“你就去说给外边，挑好的买二两来。如果老太太问起，你们只说用的是老太太的，不要多说别的。”宝钗正好在这里，她笑着说：“姨娘先别着急。外边卖的人参都没有好的。就算有一两枝根须齐全的，他们也一定会截成两三段，镶嵌上芦泡的须枝，掺匀了再卖，不能只看粗细。我们铺子里常和参行打交道，现在我去和妈说说，叫哥哥去托个伙计过去和参行商量一下，请他们把没加工的原枝好参称二两来。不妨咱们多花几两银子，也得到好的了。”王夫人笑着说：“还是你明白。就难为你亲自走一趟吧。”宝钗就回去了。过了半天，她回来说：“已经派人去了，到晚上就有回信的。明天一早去配也不迟。”王夫人当然高兴了，不过她又解释说：“这真是，‘卖头油的老板娘用水洗头’。本来家里有好的，不知给了别人多少。现在轮到自己用了，反倒各处求人去了。”她又长叹一声。贾府过去确实有很多人参的，贾母保存的一大包，都快一百年了。不过，好汉不提当年勇啊，贾府现在连根人参都没有了。王夫人是怕别人笑话啊。宝钗多懂事儿啊，赶紧帮着解释：“这东西虽然值钱，但不过就是药材，本来就应该救济他人的。咱们和那些没见过世面的人家不一样，他们得到了这个东西，还不赶紧珍藏起来。”王夫人点头说：“这话说得对。”宝钗真会给人找借口、送台阶。贾府会随便把人参送人？鬼才信呢，又不是胡萝卜。贾母珍藏人参都快藏成灰了，和农村老太太也没什么区别啊！

　　不一会儿，宝钗告辞走了。见屋里没有了别人，王夫人就叫周瑞媳妇来问前天搜查园子的结果。周瑞媳妇早已经和熙凤她们商量好了，一点儿也不必隐瞒的，所以就详细地汇报给王夫人。王夫人听了，又吃惊有生气，同时又很为难。她想到司棋是迎春的丫环，都是那边的人，就想派人先去向邢夫人通报一下。周瑞媳妇赶紧说：“前天那边太太生气王善保媳妇多事，打了她几个嘴巴子，现在她也装病在家，不肯出头了。再说又是她外孙女儿，自己打了嘴，她只好装忘了，等国些日子，事情过去了再说。如果现在我们过去提起这事，倒好像是咱们多事。不如直接把司棋带过去，连带罪证都给那边太太看看，不过就是把她打一顿许配了人，再另外派个丫头来，那不就省事了吗。现在如果去说，那边太太可能有说了：‘既然这样，你太太就该料理，又来说什么。’这样不就耽误事了吗。如果那个丫头自杀了，就更不好了。现在已经派人看了她两三天，如果一时看不到，那不就闹出事儿来了。”周瑞媳妇好像很着急着要把司棋赶走啊。王夫人想了想，就说：“这话也对。快处理了这件事，再处理咱们家的那些妖精。”

　　周瑞媳妇又叫来那几个媳妇，先到迎春那里，禀告迎春说：“太太们说了，司棋大了，她娘也多次求太太，太太已赏给她娘去许配人，今天就叫她出去，另挑好的给姑娘使唤。”接着，她就命令司棋收拾东西走人。迎春听了，眼含着泪花，好像恋恋不舍。前天夜里，她已经听其他丫环汇报了这件事，知道虽然有多年的感情，但男女关系可是涉及到道德问题啊，自己也不好再说什么了。司棋也求过迎春，实指望迎春能够拼命把她保下来，可是迎春笨嘴拙腮，耳软心活，根本不能做主。司棋一看，也知道已经没救了，就哭着说：“姑娘好狠心啊！哄了我这两天，到现在了怎么连一句话也没有？”周瑞媳妇她们马上都吆喝：“你难道还要姑娘留你吗？就算留下，你也没脸再见园子里人了了。听我们的好劝，也别哭了，人不知鬼不觉地走吧，大家脸上都好看些。”迎春含着泪说：“我知道你做了大错事了，我再去说情留下，哪不连我也完了。你看看入画也是呆了几年的人，怎么说走就走了。也不止你两个人，这园子里年龄大的都要走呢。按我说，将来终归是要分开的，不如你们各自走吧。”周瑞媳妇说：“还是姑娘明白。明天还有要走的人呢，你放心吧。”她知道内情啊，王夫人真要动手了。司棋也没办法，流着泪给迎春磕了头，又和姐妹们告别。她又凑到迎春耳朵上说：“听说我要受罪，替我说个情儿，好歹主仆一场！”迎春含着泪答应说：“你放心吧。”答应归答应，她又能做什么呢。

　　周瑞媳妇叫人带着司棋出了院门，又命领两个老婆子拿着司棋的东西。走了没几步，绣桔追上来，递给司棋一个绸子包，边擦眼泪边说：“这是姑娘给你的。主仆一场，现在一下子分开，这个给你作个纪念吧。”司棋接过去，又和绣桔大哭了一回。周瑞媳妇不耐烦，只管催促，二人只得分了手。司棋又哭着哀求：“婶子大娘们，求你们照顾一下，让我去向好姐妹们告个别吧。” 周瑞媳妇她们早就对这些大丫环有气，现在又怎么会发善心呢，就冷笑着说：“劝你还是好好走吧，别拉拉扯扯的了。我们还有正经事呢。她们又不是和你从一娘肚子里爬出来的，和她们告什么别，她们看你的笑话还看不完呢。快走吧。”她们边说边拽着走，把她带着往后角门走。司棋也不敢再说社么了，只好跟着出来。

　　宝玉正巧从外边进来，一见带着司棋出去，又见后面抱着些东西，估计她再也不能回来了。他也大致听说了那天的事情，晴雯的病也是那天加重的，他想细细地问问晴雯，可晴雯又不说为了什么。他见入画已经被赶出去了，又见司棋也要走，立刻就失魂落魄了，赶忙拦住就问：“这是到哪里去啊？”周瑞媳妇她们了解宝玉的脾气，又担心唠叨个没完耽误事儿，就笑着说：“和你没什么关系，快读书去吧。”宝玉笑着说：“好姐姐们，先等一等，我有事情。”周瑞媳妇马上说：“太太一会儿也不准耽误。我们只知道听太太的话，管不了许多了。”司棋见了宝玉，就拉住他，哭着说：“她们做不了主，你好歹去求求太太吧。”宝玉禁不住也伤心了，含着泪说：“我不知你做了什么大事，晴雯也病了，如今你又走了。都要走了，这可怎么办才好啊。”周瑞媳妇对着司棋发了火：“你现在也不是副小姐了，你不听话，我就能打你。别光想着过去了，那时有姑娘护着，任由你们瞎折腾。还不好好走，和小爷们拉拉扯扯，成个什么样子！”那几个媳妇不由分说，拉着司棋就出去了。

　　宝玉害怕她们去告状，所以什么都不敢说了，只是恨恨地瞪着她们。看她们走远了，才指着她们，气呼呼地说：“奇怪，奇怪，怎么这些人只要一出嫁，沾染了男人的气味，就这样混蛋起来，比男人更可恨，更该杀了！”守园子门的老婆子听了，觉得非常好笑，就故意问他：“这样说，凡是女孩个个都是好的，女人个个都是坏的了？”宝玉使劲儿点点头：“不错，不错！”老婆子们笑着说：“还有一句话我们糊涂弄不明白，倒要问一问了。”刚要说，只见几个老婆子走过来，着急地说：“你们小心，都叫来伺候着。太太已经亲自来园子里了，在那里查人呢。只怕还查到这里来呢。又命令人快叫怡红院的晴雯姑娘的哥嫂来，在这里等着领出他妹妹去。”看门的老婆子笑着说：“阿弥陀佛！老天终于睁开眼了，把这一个祸害妖精解决了，大家都清净些。”宝玉一听王夫人要来清查，就知道晴雯也保不住了，早已经飞一样赶回去了，后边的话就没听到这些幸灾乐祸的话 。

　　等到了怡红院，宝玉就见一群人在那里，王夫人在屋里坐着，一脸怒色，见宝玉也不理。晴雯四五天水米没沾牙，病殃殃的，现在被从炕上拉了下来，蓬头垢面，两个女人刚架起她来走了。王夫人下命令，只准把她贴身衣服的拿出去，剩下的好衣服都留下给好丫环们穿。又叫人把这里所有的丫环们都叫来一一过目。原来那天王善保媳妇趁机告了晴雯，那些和园子里丫环们不和睦的人，也都帮忙说了很多坏话。王夫人可是都清清楚楚地记在心里了。因过节事儿多，她先忍了两天，今天是来亲自调查来了。

　　有人说宝玉他大了，已经明白很多事儿了，都被屋里不好的丫环们教坏了。这事可比一个晴雯严重多了，她就从袭人开始，一直到干粗活的小丫环，个个亲自看了一遍。她又生气地问：“谁是和宝玉一天的生日？”这时候谁敢说话啊，老婆子用手指指说：“这一个蕙香，又叫四儿的，和宝玉同一天生日。”王夫人仔细打量打量，看她虽然比不上晴雯的一半，但也很清秀。再看看行动举止，觉得她的聪明劲儿都露在外边，打扮得也与众不同。王夫人好像本来就特别讨厌太机灵的人，她本身好像就不是很机灵。聪明劲儿露在外边，经常惹得别人嫉妒或者厌烦的，要不古人讲究“大智若愚”呢。她冷笑着说道“这也是个不怕害臊的。她背地里说的，同一天生日就是夫妻。这是你说的八？还以为我隔得远，什么都不知道吧。你知道我身子虽然不大来，我的心、耳朵时时都在这里。我只有一个宝玉，能放心让你们勾引坏了吗！”这四儿听见王夫人说到她平日和宝玉的悄悄话，脸一下红了，低头流泪。王夫人马上就命令也快把她家的人叫来，领出去许配给人。

　　接着，她又大声地问，“谁是耶律雄奴？”老妈妈们马上就把芳官指出来。王夫人马上宣判：“唱戏的女孩子，当然都是狐狸精了！上次放你们，你们不愿出去，那就该安分守己才对啊。你还成精了，还折腾起来，教唆着宝玉什么事儿都干。”芳官笑着辩解：“我没敢教唆什么的。”王夫人故意笑着说：“你还犟嘴。我先问问你，前年我们到皇陵上去，是谁教唆着宝玉要柳家的丫头五儿了？幸亏那丫头短命死了，不然进来了，你们又会拉帮结伙糟蹋这园子了。你连你干娘都欺负倒了，更何况别人！”她大声吆喝：叫她干娘来领走，就赏她到外边自找个女婿去吧。把她的东西一概都给她干娘。”接着，她又命令凡是姑娘们分的唱戏的女孩子们，一概不准留在园子里，都让她们各自的干娘领出去，由干娘做主许配人。这群干娘可高兴了，得了便宜，还报了私仇，大家马上集合起来去给王夫人磕头，收拾东西，带人。王夫人又满屋里检查宝玉的东西。凡是有些眼生的，一概要求收的收，卷的卷，让人拿到自己的屋里去。随后，她略微轻松地说：“这样才干净，省得别人说三道四的。”她又要求袭人、麝月等人：“你们小心！往后再有一点儿违反规定的，我一概不饶恕。因为找人算卦了，今年不适合搬家，先挨过今年去，明年全都给我仍旧搬出去。”说完，她茶也不吃，匆匆地带着人又往别的地方去检查了。

　　宝玉还以为王夫人只不过是来检查检查，没什么大事，谁知竟然这样电闪雷鸣地发起火来。她说到的都是平常大家说的话，一字也不错，宝玉估计这事儿没救了。他虽然心里难受，恨不得马上就死，但碰上王夫人发了这么大的火，自然不敢多说一句，多动一步，一直跟着送王夫人到了沁芳亭。王夫人非常严肃地说：“回去好好念书，小心明天问你话。刚才已经在发狠了。”谁在发狠？好像是说贾政。这是王夫人在吓唬他吧？宝玉垂头丧气地往回走，边走边想：“谁去告的状？这里的事别人也不应该知道啊，怎么说得这么准呢。”走进屋，只见袭人正在那里抹眼泪呢，他伤心得倒在床上也哭起来。

　　袭人知他心里难受，别人走了还可以忍受，晴雯走了可是第一痛苦的事啊。她就推推他说：“哭也不没用了。你起来我告诉你，晴雯的病已经好了，她回了家，倒可以静心地养几天。你要真舍不得她，等太太气消了，你再求求老太太，慢慢地叫进来也不难。不过是太太偶然信了别人的坏话，一时在气头上才这样做的。”宝玉哭着说：“我也搞不清楚晴雯到底犯了什么要杀头的大罪啊！”袭人说：“太太只嫌她长得太好了，做事有些轻佻。太太认为这样的美人一定不老实，所以就讨厌她，像我们这粗粗笨笨的倒好。”宝玉抬起头来说：“这也算了。可咱们私下里的玩笑话她怎么也知道了？又没有外人走漏风声，这可就奇怪了。”袭人说：“你平时又不注意 ，一时高兴了，你就不管有人无人了。我使过眼色，也递过暗号，别人都看出来了，你反而没发觉。”宝玉又疑惑地问：“怎么人人的错误太太都知道，单不挑你和麝月、秋纹的错呢？”袭人听了这话，心里一动，低着头半天，也没想好怎么回答，就笑着说：“正是呢。我们也有开玩笑不留心犯错误的时候啊，怎么太太都忘了？可能是还有别的事，等办完了再处理我们吧。”

　　袭人确实很可疑，她的确也曾经去王夫人那里告过状。不过呢，她的心好像还没黑到这种地步啊，她也没必要把丫环们全都收拾了啊。当然，她浑身是嘴也辩不清了。为什么？她是大丫环，而且受到王夫人的信任，这次还是少数幸免遇难的人之一。其实，在中国是很难保密的，故意告状的人不少，无意传闲话的人更多，所以还真搞不清到底应该怪谁。比如在这里，有很多老婆子，专门偷听、打探隐私，然后就是在背后到处宣传，逮着机会就告状。这次晴雯被赶走，主要还是老婆子使得坏。

　　宝玉笑着说：“你是头一个出了名的善良、贤惠的人，她两个又是你带出来的，怎么可能犯什么错误呢！只是芳官还小，过于伶俐了，不免就会有些仗势欺人，招人讨厌啊。四儿是我害了她，还是那年我和你拌嘴的那天起，叫她上来干些细活的，所以才有今天。只是晴雯也是和你一样，从小儿在老太太屋里过来的，虽然她长得比人强，也没什么大妨碍啊。就是她的性格爽快，说话尖刻，也没有得罪过你们。还是因为她长得太好，反而被拖累了。”说完，他又哭起来。袭人细细想想他的话，好像宝玉有怀疑她的意思，也就不好再劝了，叹口气说：“只有天知道是怎么一回事了。这时候也查不出人来了，白哭一场也没什么用的。倒是应该养着精神，等老太太心情好的时候，再请示要她回来才是正理啊。”宝玉冷笑着说：“你不用宽我的心。等到太太火下去，她的病就等不及了。她从小来了以后，也是娇生惯养，没受过一天的委屈。她这次回去了，就好像一盆刚抽出嫩箭来的兰花被送到猪窝里去一样。再说又是一身重病，一肚子的闷气。她又没有亲爹亲娘，只有一个醉泥鳅一样的姑舅表哥。她这一去，一会儿都没法生活，哪里还等得了几天。也不知道还能不能见再见她一面了！”说着，他就更伤心了。

　　袭人笑着说：“你还是‘只许州官放火，不许百姓点灯’。我们偶然说一句不太好的话，你就嫌不吉利。你现在好好的就诅咒她，难道应该吗！就算她比别人较弱些，也不至于这样啊。”宝玉擦擦眼泪，一本正经地说：“不是我诅咒她，今年春天已经有兆头了。”袭人忙问什么兆头。宝玉说：“这台阶下好好的一棵海棠花，竟然无缘无故地死了半边，我就知有怪事，果然应在她身上。”袭人听了，又笑起来，拉着他说：“我想不说，又撑不住，你也太婆婆妈妈的了。这样的话，可不是你们读书的男人说的。草木怎又关系到人了呢？如果不是婆婆妈妈的，那就真成了个呆子了。”

　　宝玉长叹一口气说：“你们哪里知道，不但草木，凡是天下的东西，都是有情有理的，也和人一样，有了知己，就非常灵验的。如果用大题目作比，就有孔子庙前的桧柏，坟前的蓍草，诸葛祠前的柏树，岳武穆坟前的松树。这都是堂堂正正跟随人的正气，前年都不会消失的东西。世道乱了就枯萎衰败，世道好了就生机勃勃，几百几千年了，枯萎了又重新发芽生长好几次。这难道不是预兆吗？用小题目作比，就有杨太真沉香亭的木芍药，端正楼的相思树，王昭君冢上的青草，难道不灵验吗？所以这海棠也预兆着人要死了，所以先就死了半边。”自然和人息息相关，但还没有一一对应的现象。传说用孔子坟前的蓍草占卜最灵验。占卜，就是算卦。岳武穆，就是岳飞，“武穆”是他死后皇帝给的荣誉称号。传说，他坟前的松树树枝都朝南生长，代表岳飞心向南宋朝廷。杨太真，就是杨玉环杨贵妃，她曾做过道士，“太真”是她的道号。唐明皇和杨贵妃曾经在沉香亭赏牡丹，木芍药就是牡丹。端正楼在华清宫，是杨贵妃梳妆的地方。王昭君的坟地上长满了青草，所以叫“青冢”。宝玉的杂书真没有白读啊！

　　袭人也没读过什么书，更没有多愁善感到这个程度，所以就好像听了一段疯话，觉得又可笑，又可叹，就笑着说：“这些话把我的气都说起来了。那晴雯是个什么东西，竟然让你费这样心思，都拿这些正经人来比较了！还有，她纵然好，也不能到我前面去。就说这海棠，也应该先来比我啊，也还轮不到她。应该是我要死了。”宝玉听了，忙捂住她的嘴，劝她说：“这是何苦呢！一个还没解决呢，你又这样了。算了，再别提这事，别弄得走了三个，又搭上一个。”袭人心里暗暗高兴：“如果不这样说，你也没有个完啊。”宝玉又自我安慰说：“从今以后不要再提了，就当她们三个死了。再说死了的事儿也有过，也没有见我怎么样，这是一样的道理。先说现在的事，应该把她的东西，偷偷地派人送出去给她。另外，咱们平时积攒下的钱，拿出一些给她养病，也算是你姐妹好了一场。”袭人笑着说：“你把我们看得也太小器、太没人心了。这话还等你说，我刚才已经把她所有的衣裳和东西打点好了，都放在那里。白天人多眼杂，等到了晚上，悄悄地叫宋妈给她拿出去。我攒下的几吊钱也给她吧。”宝玉听了，感谢不尽。袭人笑着说：“我已经是出了名的贤人了，连这一点儿好名声还不会买吗！”宝玉听出她有些生气了，连忙陪着笑安慰了几句。晚上，袭人果然派宋妈把东西送去了。

　　宝玉把其他人稳住，找了个机会出了后角门，央求一个老婆子带他到晴雯家去看看。刚开始，这老婆子怎么也不肯，只说怕人知道丢了饭碗。无奈宝玉死活央求，又需给她很多钱，她就带他去了。晴雯是过去赖大媳妇买来的，那时晴雯才有十岁。因为她常跟赖妈妈进贾府来，贾母见她长得标致，聪明伶俐，就非常喜爱。所以，赖妈妈就把她孝敬给了贾母使用，后来她又到了宝玉那里。晴雯进来的时候，也不记得家乡父母，只知有个姑舅表哥，会炒菜做饭的手艺，也沦落在外边，所以就又求了赖大媳妇把他买进来做工。赖大媳妇见晴雯虽然到贾母跟前，很受信任、宠爱，却倒还不忘过去，所以也就把她表哥买了进来，把家里一个女孩子配给了他。谁知她表哥已过上这样安逸的生活，就知道死喝酒，把家里人都忘了。偏偏他的老婆是一个多情的美女，见他不懂感情，只知道喝酒，就常常感叹自己命不好，感到特别的寂寞。这种情况在古代叫“蒹葭倚玉树”。 蒹葭，就是芦苇，很低贱。玉树，很高贵。这两种东西放在一块儿，当然就很不般配了。现在有句话意思差不多，就是“一朵鲜花插在牛粪上”。不过，这表哥肚量很大，从来不知道吃醋。所以，这媳妇就放开了手脚，在贾府里到处找情人，上上下下，主子奴才竟然有一半人和她有过关系。这对夫妻是谁啊？就是上回贾琏接见的多浑虫和灯姑娘。晴雯只有这一门亲戚，所以出来就住在他家了。

　　这时候，多浑虫到外边去了，灯姑娘吃了饭去串门子，只剩下晴雯一人，在外间屋里趴着。宝玉让那老婆子在院门口放哨，他独自掀起草帘子进来，一眼就看见晴雯睡在芦席土炕上，幸亏被子、褥子还是过去她用的。他走上来，含着泪伸手拉住她，轻轻地叫了两声。晴雯因为受了风，又挨了哥嫂的一顿训斥、讽刺，病上加病，咳嗽了一天，刚刚朦朦胧胧地睡着了。听到有人叫自己，她抢睁开双眼，一见是宝玉，又惊又喜，又悲又痛，忙一把死死地攥住他的手。哽咽了半天，才说出半句话来：“我只当见不到你了。”接着，她又咳嗽个不停。宝玉也只剩哽咽了。

　　晴雯又说：“阿弥陀佛，你来的正好，先倒半碗茶给我喝。渴了这半天，半个人也叫不着。”宝玉抹抹眼泪问：“茶在哪里？”晴雯说：“那炉台上就是。”宝玉一看，虽然有个黑沙的水壶，但不像个茶壶。他在桌上拿了一个碗，很大很粗糙，不像个茶碗，还没拿到手里，先就闻见油腥味儿。宝玉先拿些水洗了两次，复又用水冲了冲，这才提起沙壶倒了半碗。看一看，黑红的颜色，也太不像茶了。晴雯扶在枕头上说：“快给我喝一口吧！这就是茶了。哪里能和咱们茶比呢！”宝玉先自己尝了一尝，没有什么清香味儿，也没有茶味，只有苦涩的味道。他递给晴雯，只见她好像拿到了玉液琼浆，一口气都灌下去了。这就叫饥不择食，可不择水啊！宝玉心里想：“往常那样的好茶，她还常常不满意，今天却这样了。看来，古人说‘饱饫烹宰，饥餍糟糠’，又说‘饭饱弄粥’，都没有错啊。”“饱饫烹宰，饥餍糟糠”大致意思：吃饱了就讨厌大鱼大肉；饿极了吃米糠都很满足。饭饱弄粥，好像是说吃饱了饭，闲着没事儿，就再做些粥喝。他流着眼泪问：“你有什么要说的，趁着没人快告诉我。”晴雯哭着说：“还有什么可说的！我知道用不了三五天，这命就没有了。我现在是挨一天算一天了天。只是一件，我死也不甘心的：我虽然长得比别人略好些，但并没有勾引过你啊，为什么一口咬定了我是个狐狸精！我太不服了。现在白担了个罪名，而且就要死了，不是我说一句后悔的话，早知道这样，我当时也另有个打算啊。我是一片痴情，只说是大家反正是在一起。没想到凭空出了这事儿，我有冤无处诉啊。”说完，她又大哭起来。晴雯过去是不是一直认为能陪伴宝玉一辈子的？那就是做姨太太了。如果知道有今天，她会怎么样？是躲开宝玉，还是不顾一切地与宝玉爱上一场？过去的女孩子啊，快被封建的绳索勒死了，又怎么可能真正地轰轰烈烈地活一回呢。

　　宝玉拉着她的手，只觉得骨瘦如柴，手腕上还戴着四个银镯，就哭着说：“先摘下来，等好了再戴上吧。”他帮着摘下来，塞在枕头底下。他又说：“可惜这两个指甲，好容易长了二寸长，这一病，又会弄伤好些。”晴雯擦擦眼泪，伸手拿了把了刀，把左手上两根葱管一般的指甲齐根剪下，又伸手在被子内把贴身穿着的一件旧红绫袄脱下，全都交给宝玉，强打精神说：“这个你拿着，以后就就像见了我一样。快把你的袄儿脱下来我穿。我将来在棺材里独自躺着，也就像还在怡红院的一样了。论理不该如此，只是已经白担了个罪名，我可也没有办法了。”宝玉忙换上袄儿，收起指甲。晴雯又哭着说：“回去她们看见了要问，不用撒谎，就说是我的。既然都这样了，干脆不用隐瞒了。”真可怜哪，她这就是要一个心理安慰，也算是与相爱的人在一起了。

　　这里话还没说完，她嫂子突然笑嘻嘻掀帘子进来，大声地嚷嚷：“好呀，你两个的话，我已经都听见了。”她又对宝玉说：“你一个作主子的，跑到下人屋里干什么？看我年轻又俊俏，就来调戏我吗？”宝玉吓得赶紧央求：“好姐姐，别嚷嚷。她伺候我一场，我私自来看看她。”灯姑娘拉着宝玉就进了里间屋，笑着说：“不嚷嚷也行，只是你要答应我一件事。”说着，她就坐在炕沿上，却紧紧地把宝玉搂在怀里。宝玉哪里见过这样的猛女，心里突突地乱跳，急得满面涨得通红，又害羞又害怕，嘴里不停地说：“好姐姐，别闹了。”灯姑娘斜着醉眼，笑着说：“呸！常听人说你是风月场里的高手，怎么今天反倒害羞了。”宝玉脸更红了，赶紧说：“姐姐放手吧，有话咱们好说。外边有老妈妈，听见了就没意思了。”灯姑娘还以为是遇到知音了，去不知道，虽然都是风流的人，但却大不一样啊。

　　灯姑娘笑着说：“我早进来了，已经叫老婆子到园门等着呢。我像等什么似的，今天终于等到了你。虽然听说过名声，但怎么也不如见面。你空长了一个好模样儿，竟然是没药性的炮竹，倒比我还怕羞。看来，人的嘴一概不能听啊。就比如刚才，我们姑娘下来，我也是认为平日一定有偷鸡摸狗的事儿。我进来现在窗户下听了听，屋内只有你们两个人，却什么事都没有。看来天下被委屈事也不少。我也后悔错怪你们了。既然这样，你就放心吧。以后你只管来，我也不?嗦你了。”宝玉这才放下心来，起身整理整理衣服，又央求说：“好姐姐，你千万要费心照看她两天。我先走了。”他出来，又告诉晴雯。晴雯知道宝玉舍不得走，就用被子蒙着头，不再搭理他了，宝玉这才出来。他还向去看看芳官、四儿，无奈天已经黑了，出来了半天，担心里面人找不到自己，又会闹出事儿来，就先进了园子，准备明天再去。赶到后角门，就见里边老婆子们正在查人，如果晚一步，门就关上了。

　　宝玉到了自己的住处，只告诉袭人说到薛姨妈家去了。铺床的时候，袭人就问他今天怎么睡。宝玉随口说：“怎么睡都行。”原来，这两年，袭人因为王夫人看重自己，所以就更自重了，不大和宝玉亲近了，比小时候更疏远了些。再说，忙针线活也很劳累，她就经常咳嗽，痰里还带着血，所以最近夜里就不住在宝玉房间了。宝玉夜间常醒，又很胆小，腥了一定叫人。因为晴雯睡觉比较警醒，而且动作轻柔，夜里伺候就全靠她一个人，所以宝玉外边床上只有她睡。现在她走了，袭人只好问一问。宝玉说怎么都行，袭人只好按原来的做法，自己搬铺盖去睡了。

　　宝玉发了一晚上呆。催他睡下后，袭人她们也都睡下。袭人听着宝玉在枕头上长吁短叹，复去翻来，直到三更以后。他渐渐地安顿了，还有了鼾声。袭人这才放了心，也就朦朦胧胧地睡着了。没喝半杯茶的工夫，只听宝玉叫“晴雯”。袭人忙睁开眼连声答应，问干什么。宝玉想要喝茶。袭人忙下去洗了手，从暖壶里倒了半杯茶来。宝玉笑着说：“我最近叫惯了她，却忘了是你。”袭人倒也不在意，笑着说：“她刚来的时候，你曾经也在睡梦里直叫我，半年后才改了。我知道这晴雯人虽然走了，这两个字只怕是不能走的。”说着，大家又睡下。宝玉又翻腾了一两个小时，至五更才睡着了。他恍恍惚惚地看见晴雯从外边走进来，仍然是过去的样子，笑着对宝玉说：“你们好好过吧，我要告别了。”说完，她转身就走了。宝玉赶忙使劲地叫，又把袭人叫醒了。袭人还以为他习惯了在乱叫，上去一看，却见宝玉哭了，嘴里还说：“晴雯死了。”袭人笑着说：“这是哪里的话！你就知道胡闹，被人听着有什么意思。”宝玉哪里肯听劝，恨不得一下子天亮了就派人就探望。

　　天刚亮，就有王夫人房里的小丫环立等着叫开前角门传王夫人的话：“‘马上叫起宝玉，快洗脸，换了衣裳快来，因为今天有人请老爷赏看桂花，老爷因为喜欢他前天写的诗好，所以要带他们去。’这都是太太的话，一句别错了。你们快飞跑着告诉他去，立刻叫他快来，老爷在上屋里还等他吃面茶呢。环哥儿已经来了。快跑，快跑。再找一个人去叫兰哥儿，也要这样说。”里面的老婆子听一句，应一句，一面扣扭子，一面开门。早有两三个人一边扣衣，一边分头通知去了。

　　袭人听到有人敲院门，就猜到有事。听到通知，她马上叫小丫环来舀了面汤，又催宝玉起来洗漱。她又去拿衣服，想到是要跟着贾政出门，就没拿很新鲜的衣服、鞋子，只拿了旧一点儿的来。宝玉只好匆匆忙忙地赶到前边。贾政果然已经在那里喝茶了，心情也很好。宝玉忙行了省晨的礼节。省晨，就是早晨看望问候父母等长辈。贾环和贾兰两个人也都向宝玉问好。贾政命令他坐下喝茶，又对贾环和贾兰说：“宝玉虽然读书不如你两个，但说到题写对联和写诗的才能，你们都不如他。今天去了，可能就要你们写诗，宝玉必须找机会帮助他们两个。”王夫人她们从来没听到过贾政这样夸奖宝玉，所以感到特别的高兴。

　　贾政他们走了，王夫人正想到贾母这边来，芳官她们三个人的干娘们走过来，请示说说：“从前天太太开恩把她们放出去，芳官就像疯了似的，茶也不喝，饭也不吃，勾引上藕官、蕊官，三个人要死要活，只要剪了头发做尼姑去。我只当是小孩子家一时出去不习惯，估计过两天就好了。谁知道越闹越凶，打骂着也不怕了。实在没办法，所以来求太太，或者就答应她们做尼姑去，或教训她们一顿，赏给别人作女儿去吧，我们也没这福气。”芳官她们落到早就和干娘闹翻了，现在落到她们手里会有好吗？这群女孩子，唱戏唱得，对生活、爱情也有了自己的一些理想，但当时的现实允许她们追求自己的生活吗？被人胡乱安排嫁个人，所有的美好想象就全破灭了。对她们来说，比死亡更好的选择，或者说自己还能选择的，就只有出家当尼姑了。

　　王夫人听了，勃然大怒：“胡说！那还能让她们做主了，佛门也是随便谁都能进去的！每人打一顿，看还闹不闹了！”八月十五日各庙里都上供，各庙里的尼姑也都有来送供尖的惯例。供尖，其实就是贡品，它的意思是供品的尖儿。王夫人十五这天就留下水月庵的智通和地藏庵的圆心住两天，到今天还没回去。她们听到这个情况，巴不得再拐两个女孩子回去干活呢，就都对王夫人说：“咱们府上到底是慈善的人家。因太太好做善事，所以感应得这些小姑娘们都这样。虽说佛门轻易难入，也要知道佛法平等。一切生灵，无论鸡犬，我佛都要度它，无奈人们痴迷不醒。如果有善根能醒悟，就可以超脱轮回。所以经书上虎狼蛇虫得道的就不少。如今这两三个姑娘既然没有了父母，家乡又远，她们从小命苦，后来又进了唱戏这个风流的职业，将来也不知道怎么样，所以苦海回头，出家修修来世，也是她们的一片真心。太太可不要限制了她们的善念啊。”

　　王夫人本来就喜欢做善事，刚才也是说的气话，现在听两个尼姑讲的很有道理。最近家里事情也很多：邢夫人派人来通知，明天接迎春回家住两天，准备让人家相看；另外，有媒婆来为探春说亲。她心里很烦躁，哪里还有闲心管这些小事，所以就笑着说：“你两个既然这样说，你们就带去做徒弟吧？”两个尼姑听了，赶紧说：“阿弥陀佛，善哉！善哉！如果这样，你老人家阴德可不小啊 。”说完，她们又磕头拜谢。王夫人说：“你们去问她们去。如果是真心，就马上来当着我拜了师父吧。”这三个女人听了出去，果然把她们三人带来了。王夫人问了问，她们三个人已经打定了主意，于是就给两个尼姑磕了头，又磕头向王夫人告辞。王夫人见她们这样坚定，知道不能勉强了，反倒又伤心可怜起她们来，赶忙叫人拿了些东西来赏给她们，又送了两个尼姑好些礼物。芳官跟了水月庵的智通，蕊官、藕官两个人跟了地藏庵的圆心，就这样出家了。

　　这两个尼姑好像也不是本分的出家人，芳官她们不会受什么折磨吧？

　　请看下回。

　　............................................................................................................................................