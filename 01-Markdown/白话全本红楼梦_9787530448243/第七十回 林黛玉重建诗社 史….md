﻿    

## 第七十回 林黛玉重建诗社 史…

　　贾琏自己在梨香院里陪伴了七天七夜，天天和尚和道士不断做佛事。贾母叫了他去，吩咐不准往家庙送。贾琏没办法，只好又商量了一下，在尤三姐的坟墓上边修墓埋葬。送殡那天，只不过同族的几个人、王信夫妇和尤氏婆媳罢了。熙凤一概都不管不问任凭他自己去办理。在过去，娶二奶根本没错误，熙凤这样对待贾琏就不对了。

　　这天，林之孝开了一个人名单子来，共有八个二十五岁的单身男仆人应该娶妻成家，就等里面有该放出去的丫环了。熙凤看了，先来问贾母和王夫人。大家商量，虽然有几个应该家人的，但又都有不能出嫁的原因。第一个是鸳鸯，她发誓不嫁。自从那天发了毒誓，从来不和宝玉说话，也不特别地打扮。大家见她意志坚定，也不好勉强了。第二个是琥珀，又生了病。彩云呢，最近和贾环闹翻了，也得了重病。只有熙凤和李纨屋里干粗活的几个大丫环能出嫁，其余的年龄都还小。于是，贾母她们就下令让他们到外边自己找媳妇儿。

　　很长的一段时间了，因为熙凤病了，李纨和探春料理家务没了空闲，再加上过年过节，乱七八糟的事情，诗社的活动就停止了。现在是仲春时节，大观园不需要什么春耕、春种，所以都很有搞活动的时间。但是，诗社的主要办事人员宝玉却没有情绪，所以，诗社的活动一直没恢复。柳湘莲出家了，尤三姐自刎了，尤二姐吞进了，柳五儿气病了，接二连三的事情，对别人来说，可能就是茶余饭后谈笑的话题罢了，但对宝玉来说可就闲愁乱恨，一次又一次的心理打击了。宝玉又变得痴痴呆呆了，说话也颠三倒四、莫名其妙的。袭人她们非常担心，但又不敢随便就去报告贾母，只好想办法逗着他欢笑。照顾精神病不容易，伺候情痴也很难啊。

　　这天清晨，宝玉就听到外间屋里嘻嘻哈哈地笑声不断。袭人过来笑着说：“你快出去救人吧，晴雯和麝月两个人按住温都里那正胳肢呢。”宝玉听了，忙披上灰鼠袄子出来看，只见她们三个人被褥都还没有叠起，大衣也没穿。那晴雯只穿葱绿院绸小袄，红小衣红睡鞋，披散着头发，骑在雄奴身上。还记得吗？温都里那就是雄奴，也就是芳官。麝月是红绫抹胸，披着一身旧衣，在那里抓雄奴的胳肢窝。雄奴仰倒在炕上，穿着撒花紧身儿衣服，红裤绿袜，两脚乱蹬，笑得喘不过气来。宝玉忙上前，笑着说：“两个大的欺负一个小的，让我来拔刀相助吧。”说着，他就上床来胳肢晴雯。晴雯怕痒，笑得忙丢下雄奴，和宝玉对着抓。雄奴趁势又把晴雯按倒，向她的肋下抓去。袭人笑着说：“小心冻着了。”看他们四人闹在一处，袭人也笑个不停。她好像不她喜欢胡闹，现在这样做，应该是为宝玉专门设计安排的吧。这帮小丫头，用心良苦啊。女人啊，关心起人来，那真叫无微不至啊。当然，如果她恨你的话，也会想尽千方百计、无所不为的。

　　这时，李纨派碧月来说：“昨天晚上奶奶在这里把块手帕子忘了，不知是不是在这里啊？”小燕说：“有，有，有，我在地下拾了起来，不知是哪一位的，刚洗了出来晾着，还未干呢。”碧月见他们四个人乱滚，就笑着说：“倒是这里热闹，大清早就热热闹闹地玩儿到一起了。”宝玉笑着说：“你们那里人也不少，怎么不玩？”碧月说：“我们奶奶不玩，把两个姨娘和琴姑娘也束缚住了。现在琴姑娘又跟着老太太到前头去了，我们就更寂寞了。两个姨娘过了今年，到明年冬天都走了，会更寂寞呢。你看宝姑娘那里，走了一个香菱，就冷清了很多，云姑娘就落了单了。”过去的女孩儿，结了婚就不会再那样玩儿了，更何况李纨这样的寡妇呢。宝钗，是性格关系，也可能是自我要求太严格，所以，不太喜欢过于随便地玩儿。不过，她也不是过于死板，还是挺随和的。不过，湘云这样的性格，如果说到做游戏，可能只有宝玉适合做她的拍档了。

　　正说着，只见湘云又派人翠缕来说：“请二爷快出去看好诗。”宝玉听了，忙问：“哪里的好诗？”翠缕笑着说：“姑娘们都在沁芳亭上，你去了就知道了。”宝玉听了，忙梳洗了一下，奔了出来，果然看见黛玉、宝钗、湘云、宝琴、探春都在那里，手里拿着一篇诗在看。见他来了，大家都笑着说：“天都这么晚了，还不起来，咱们的诗社停了一年，也没有人发动。现在正好是初春时节，万物更新，应该重新建立起来才好。”湘云笑着说：“建立诗社时是秋天，本来就不应该发达。现在是万物逢春，蓬勃生长。再说这首桃花诗又好，就把海棠社改成桃花社吧。”宝玉听了，点头说：“很好。”他又忙着要过来欣赏。大家都又说：“咱们去找稻香老农去，大家商量商量怎么活动。”说着，大家就都去了稻香村。宝玉一边走，一边看那纸上写着的《桃花行》。诗是这样：

　　桃花帘外东风软，桃花帘内晨妆懒。

　　帘外桃花帘内人，人与桃花隔不远。

　　东风有意揭帘栊，花欲窥人帘不卷。

　　桃花帘外开仍旧，帘中人比桃花瘦。

　　花解怜人花也愁，隔帘消息风吹透。

　　风透湘帘花满庭，庭前春色倍伤情。

　　闲苔院落门空掩，斜日栏杆人自凭。

　　凭栏人向东风泣，茜裙偷傍桃花立。

　　桃花桃叶乱纷纷，花绽新红叶凝碧。

　　雾裹烟封一万株，烘楼照壁红模糊。

　　天机烧破鸳鸯锦，春酣欲醒移珊枕。

　　侍女金盆进水来，香泉影蘸胭脂冷。

　　胭脂鲜艳何相类，花之颜色人之泪，

　　若将人泪比桃花，泪自长流花自媚。

　　泪眼观花泪易干，泪干春尽花憔悴。

　　憔悴花遮憔悴人，花飞人倦易黄昏。

　　一声杜宇春归尽，寂寞帘栊空月痕！

　　诗，本质上说是不能翻译的，一翻译就变了味道，甚至彻底失去了味道。不过，为了好理解，简单地把主要意思说说吧：

　　桃花在外人在内，

　　人比桃花更憔悴。

　　春天过去花凋谢，

　　眼泪流干人伤悲。

　　宝玉看了，并没有称赞，却流下泪来，怕别人看见，又忙自己擦了。他就问：“你们怎么得到的？”宝琴笑着说：“你猜是谁写的吧？”宝玉笑了：“当然是潇湘妃子了。”宝琴笑着说：“我说是我写的呢。”宝玉笑了：“我不相信。这声调口气，一点儿也不像蘅芜的形式，所以不相信。”蘅芜，是宝钗写诗的号啊。宝钗笑着说：“你这样说就是不通诗了。难道杜工部每首诗都只写‘丛菊两开他日泪”这样的句子吗！他一样也有‘红绽雨肥梅’‘水荇牵风翠带长’这样美艳的句子啊。”宝钗分析得很好，人的性格是多重的，写作风格也是多样的。宝玉笑着说：“固然这样说。但我知道姐姐是决不会允许妹妹有这样伤感的语句。妹妹虽然有这样的才华，也是绝对不会写的。比不得林妹妹经历过人生大痛苦啊，才写了这样非常悲凉的诗篇。”大家听了，都笑了。看来，还真是黛玉写的。性格也好，风格也罢，总有一种是主要的、突出的。

　　大家到了稻香村，把诗拿给李纨看了，她当然是不停地称赞了。说起诗社，大家商量决定：明天是三月初二，就重新开始诗社活动，就把“海棠社”改成“桃花社”，黛玉做了这次的社主，也就是活动主持人了。第二天吃了饭，大家都带来到潇湘馆。先是拟题。黛玉就提议说 “大家就要写桃花诗一百韵。”宝钗说：“不能这样做。自古以来桃花诗最多，即使写了也一定回落了俗套，比不了你这一首古风。必须重新设个题目。” 古风，就是古体诗。正说着，有人来通报：“舅太太来了。姑娘出去请安吧。”大家就都到前边来拜见王子腾的夫人，陪着说话。吃完饭，又都陪着到园子历来，各处游玩了一遍。晚饭以后掌灯的时候，她们才回去了。

　　第二天，是探春的生日，元春早派两个小太监送了几件玩意儿。其他人也都送了寿礼，不一一介绍了。饭后，探春换了礼服，到各处行礼。黛玉笑这对大家说：“我这一次诗社活动真不凑巧，偏偏忘了这两天是他的生日。虽不摆酒唱戏的，少不得都要陪他在老太太、太太跟前顽笑一日，如何能得闲空儿。”因此，大家就把活动改在了初五。

　　这天，姐妹们伺候贾母吃完早饭，贾政的书信到了。宝玉赶紧行礼，把书信里请安的话念给贾母听，上面不过是请安的话，说六月中进京。其余的家务事等，有贾琏和王夫人去读。大家听说他六七月回京，都很高兴。正巧，这几天王子腾的女儿许给保宁侯的儿子，定在五月初十这天过门，熙凤当然又忙着张罗了，常常三五日不在家。这天王子腾的夫人又来接熙凤，顺便邀请姐妹们去了乐呵一天。贾母和王夫人就让宝玉、探春、黛玉、宝钗四个人和熙凤一起去。大家也不敢说什么，赶紧回去打扮起来，去玩了一天，到掌灯的时候才回来。

　　宝玉回到怡红院，歇了一会儿，袭人趁机劝他收一收心，闲了的时候读读书，好好准备一下。准备什么？当然是准备着给贾政汇报了。宝玉屈指算一算说：“还早着呢。”袭人提醒说：“书是第一件，字是第二件。到那时你就算有了书，你的字在哪里呢？”宝玉笑着说：“我平时也写了好些，难道都没收起来？”袭人说：“怎么没收。你昨天不在家，我就拿出来数了一数，只有五六十篇。这三四年的工夫，难道只写了这几张字吗。依我说，从明天起，把别的心全收起来，天天快临临摹几张字补上。虽说不能按日子都补上，也要大概看得过去。”宝玉听了，忙自己又数了一遍，看实在是糊弄不过去了，就说：“从明天开始，一天写一百字才好。”说完，大家就都睡了。

　　第二天，起来洗刷完，宝玉就开始在窗户底下，认认真真地临摹正楷。贾母因为没见他，只当他病了，忙派人来问。宝玉这才去请安，说清晨的工夫先练字，所以来晚了。贾母听了，非常高兴，嘱咐他说：“以后只管写字念书，不用出来也可以。你也去告诉太太去。”中国的家长啊，一听孩子读书就高兴。宝玉又到王夫人屋里说了说。王夫人说：“临阵磨枪，也不中用。现在才知道着急，如果天天写写念念，有多少完不了啊？这一赶，又赶出病来怎么办。”宝玉回答说没事的。这里贾母也说怕急出病来。探春、宝钗等人都笑着说：“老太太不用着急。书虽然不能替他，字却是能替的。我们每人每天临一篇给他，搪塞过这一步就完了。一来老爷到家不生气，二来他也不会急不出病来。”这好啊，两全其美啊，贾母当然非常高兴了。唉，这欺上瞒下的，还有了冠冕堂皇的理由，还得到了领导的认可。

　　黛玉听说贾政回家，一定会问宝玉的功课，宝玉如果分了心，当时候肯定吃亏。因此，她就装作不耐烦，不再提诗社的活动了。探春和宝钗两个人每天也临摹一篇楷书字给宝玉，宝玉自己每天也加班加点，写个二百三百字。到三月下旬，凑出了很多的作业。这天，他算了算，再有五十篇，也就混得过去了。不想紫鹃来了，送了一卷东西给宝玉。他拆开一看，原来是在老油竹纸上临的钟王蝇头小楷，字迹和自己十分相似。宝玉高兴得赶紧给紫鹃作了一个揖，又亲自来道谢。湘云和宝琴两个人也临摹了几篇送来。这下宝玉放了心，又把该读的书，又温习几遍。他乱正忙活呢，又传来消息，说沿海一带出现了海啸，财产生命损失惨重。地方官就赶紧上奏皇帝，皇上命令贾政顺路查看灾情、组织赈灾，然后再回家。这样一算，贾政差不多过了冬才能回来。宝玉听了，又把书和字扔到一边，继续逍遥自在了。

　　这时候到了晚春时节，湘云觉得很无聊，见到柳花飘舞，就写了一首很短的词，也叫“小令”，词牌是《如梦令》。这样写道：

　　岂是绣绒残吐，卷起半帘香雾，纤手自拈来，空使啼燕妒。且住，且住！莫使春光别去。

　　大致意思：柳絮就像女孩随口吐出的线绒，它独占了春光，真让春鸟都嫉妒啊，真不愿意把春光放走啊。

　　自己写完，有些得意，就拿给宝钗看了，又来找黛玉指正。黛玉看完，笑着说：“太好了，也新鲜有趣。我写不出来。”湘云笑着说：“咱们诗社从来没有填过词。你明天何不组织一次填词活动，改个样儿，不就新鲜了嘛。”黛玉听了，也来了兴趣，就说：“说得对啊。我现在就请他们去。”她马上命令准备几样点心，又派人分头去请姐妹们和宝玉。她们两个在这里二以及确定了柳絮这个写作主题，又限定了出几个词牌，都写出来挂在了墙上。

　　大家很快就都赶来了，先看了看要求。又看了湘云的词，都称赏了一回。宝玉笑着说：“我写词的水平不行，不过也得胡诌一下了。”于是，大家就抓阄，宝钗抓了《临江仙》，宝琴抓了《西江月》，探春抓了《南柯子》，黛玉抓了《唐多令》，宝玉抓了《蝶恋花》。紫鹃点了一支梦甜香，大家都构思起来。

　　不一会儿，黛玉就写完了。接着宝琴、宝钗也都写好了。她们三个人就互相欣赏，宝钗笑着说：“我先看完了你们的，再看我的。”探春也笑着说：“哎呀，今天这香怎么着得这样快，只剩下三分了。我才写了半首。”她又问宝玉写出来了没有。宝玉虽然写了些，但自己嫌不好，又都抹了，像另外再写，回头看看香，已经快烧完了。宝玉也算有才了，怎么每次在诗社的表现都不好呢？一是他总是看这个、又瞧那个的分神了，二是才思还是不如黛玉、宝钗她们敏捷。这也提醒我们两点：一是男女同学可能影响学习，二是女孩们的文学水平一般高于男孩子。不过，在过去，女孩子是没有办法施展文学才能的。李纨笑着说：“你这就算输了。蕉丫头的半首先写出来。”探春听了，忙写了出来。大家一看，上面只有半首《南柯子》：

　　空挂纤纤缕，徒垂络络丝，也难绾系也难羁，一任东南北各分离。

　　这半首词的意思好像预兆了探春远嫁他乡。李纨笑着说：“这很不错的，为什么不续上？”宝玉见香没了，情愿认输，不肯糊弄过去，就把笔放下，来看这半首词。见还没写完，反倒有了思路，来了诗兴，于是提笔续写：

　　落去君休惜，飞来我自知。莺愁蝶倦晚芳时，纵是明春再见隔年期！

　　大致意思：柳絮飘飞不要伤心，自然景物就是这样的，以后还能见面，只是要等待一年。一年一见的就是牛郎织女了。这是否也预兆了宝玉以后的遭遇呢？

　　大家都笑着说：“你自己的不会写，现在偏偏写出来了。再好也不能算数的”说着，大家又都看黛玉的《唐多令》：

　　粉堕百花州，香残燕子楼。一团团逐对成求。飘泊亦如人命薄，空缱绻，说风流。

　　草木也知愁，韶华竟白头！叹今生谁舍谁收？嫁与东风春不管，凭尔去，忍淹留。

　　大致意思：柳絮飘飞，就像人世漂泊；草木也有感觉，也会为青春年华的消逝悲伤；柳絮飘飞谁来收拾啊？既然想嫁给东风一样，跟着它飞走了，春天就不管了，就这样忍心看它漂泊在外了。

　　读完，大家都点头感叹说：“好是很好，就是太悲伤了。”接着又看宝琴的《西江月》：

　　汉苑零星有限，隋堤点缀无穷。三春事业付东风，明月梅花一梦。

　　几处落红庭院，谁家香雪帘栊？江南江北一般同，偏是离人恨重！

　　大致意思：汉家园林的柳絮不多，隋堤上却是无穷的；无论在哪里，柳絮都会引起离愁别恨的。

　　大家都笑着说：“还是她的有力量些。‘几处’‘谁家’两句最妙。”宝钗笑着说：“还是有些悲伤的意思。我想，柳絮本来是一种轻薄而且没根的东西，按我的想法，偏要把它说好了，才不落俗套。所以我诌了一首来，不一定合乎你们的意思。”大家都说：“不要太谦虚了。我们先欣赏欣赏吧，肯定写得不错。”大家就都看她的《临江仙》，头一句：白玉堂前春解舞，东风卷得均匀。湘云抢着说：“好一个‘东风卷得均匀’！这一句就比别人强了。”又都看下面：

　　蜂团蝶阵乱纷纷。几曾随逝水，岂必委芳尘。万缕千丝终不改，任他随聚随分。韶华休笑本无根，好风频借力，送我上青云！

　　大致意思：白玉建造的房屋前，柳絮随风飘扬，那么优美和谐，就像蜜蜂蝴蝶在飞舞；何曾随波逐流，何必一定要落在污泥中；千条万条的柳枝不改自己的姿态，任它随缘而去；不要笑我无根无基，借着东风，我就能直上青云的。

　　宝钗这首词充满了开朗乐观的情绪，而且见解独特、新颖。很多人觉得“送我上青云”就表现出宝钗是个“野心家”，一心向上爬。有这个意思吗？可能有，也可能够没有吧。

　　大家拍案叫绝，都说：“写的太好了，这首词应该是第一。缠绵悲戚，应该数潇湘妃子；妩媚有情，就数枕霞了；小薛与蕉客今天失败了，是要受罚的。”宝琴笑着说：“我们当然应该受罚，但不知到交白卷子的又该怎么罚？” 枕霞，就是湘云。蕉客，就是蕉下客探春了。交白卷的就是宝玉了。宝琴毕竟是走过南闯过北的人，熟悉了环境以后，也开始和宝玉开玩笑了。李纨郑重其事地说：“不要忙，这次一定要重重罚他。下次为例。”

　　下次为例，意思是下次就照这次的办法来处理。这样好，立刻做出了惩罚，做事有了参考，也避免了犯错误的侥幸心理。我们熟悉的成语是“下不为例”，意思是下次不可以这样做了，表示只通融或饶恕这一次。现在，很多错误就是在“下不为例”的借口或托词保护下，一犯再犯，三犯四犯，不停地犯下去了。

　　李纨的话还没说完，就听窗外竹子上一声响，好像窗扇掉下来一样，大家都被吓了一跳。帘子外的丫环们喊：“一个大蝴蝶风筝挂在竹梢上了。好漂亮的风筝啊！不知是谁家放断了绳，拿下来吧。”宝玉他们听了，也都出来看。宝玉笑着说：“我认识这风筝。这是大老爷那边院子里娇红姑娘放的，拿下来给她送过去吧。”紫鹃笑着说：“难道天下没有一样的风筝，单单他有这个吗？我不管，我先拿起来。”探春说：“紫鹃也学小气了。你们一样也有的，现在捡了别人的，也不怕犯忌讳。”黛玉笑着说：“可不是吗，你知道是谁放晦气的，快扔出去吧。把咱们的拿出来，咱们也放放晦气。”紫鹃听了，赶忙命令小丫环们把这风筝送出给而园子门口值班的老婆子去了，预备有人来找，好交还给他们。古代放风筝有的为了敬献神灵，祈求消灾降福，所以叫“放晦气”。这有点像送瘟神。古代人总是期望着能改变命运，于是想尽办法要把晦气，也就是不吉利、倒霉给送走，除了放风筝外，还有的把熬的草药渣倒在外边，让踩着的人把病给带走，至于晦气会不会传染给别人，那就管不了许多了。

　　小丫环们早就摩拳擦掌了，一听到命令，立刻七手八脚地去拿出个美人风筝来。也有搬高凳的，也有捆剪子股的，也有拔?子的。剪子股，?子，都是放风筝的工具。宝钗等人都站在院门前，让丫环们在院外宽敞的地方放去。宝琴笑着说：“你这个不大好看，不如三姐姐的那一个软翅子大凤凰好。”宝钗笑着点点头：“可不是吗。”于是，她回头对翠墨说：“你把你们的也拿来放放。”翠墨笑嘻嘻地去拿了。

　　宝玉也来了兴趣，也派一个小丫环回家：“把昨天赖大娘送我的那个大鱼取来。”小丫环去了半天，却空手回来，笑着说：“晴姑娘昨天放走了。”宝玉有点着急地说：“我还一次也没放呢。”探春笑着说：“反正都是给你放晦气了。”宝玉点点头：“也是啊。再去把那个大螃蟹拿来吧。”小丫环走了，不一会儿就和几个人扛了一个美人风筝和?子来，汇报说：“袭姑娘说，昨天把螃蟹给三爷了。这一个是林大娘才送来的，放这一个吧。”宝玉仔细地看了看，见这美人风筝做的十分精致，就乐呵呵叫人放起来。

　　这时，探春的也拿来了，翠墨带着几个小丫环在那边山坡上已放了起来。宝琴也让人把自己的一个大红蝙蝠也拿来。宝钗兴致也很高，也让人拿来一个，是七个大雁连成串的，都放了起来。唯独宝玉的美人风筝放不起来。宝玉说丫环们不会放，自己放了半天，只飞起房子那么高，就落下来了。宝玉急得头上直冒汗，大家却都咯咯地笑了，宝玉恼火地把风筝扔到地下，指着就喊：“如果不看你是个美人，我早就把你踩个稀巴烂了。”黛玉笑着说：“那是顶线不好，拿出去另外打个顶线就好了。”宝玉就让人拿去打顶线，另外拿了一个来放。大家都仰头往天上看，就见几个风筝都飞到半空中去了。

　　不管是谁闹了笑话，或犯了错误，我们最好是上前去帮个忙，或者问候一下，最好别笑，起码不要哈哈地大笑。中国人，可能活的太无聊了吧，所以只能从看别人摔跤、耍弄别人等事情上找点儿乐子。真是不容易啊。人家小朋友皮皮觉悟就很高，他就说：“今天有个小朋友掉进坑里了，同学们都笑他，就我没笑。”多好孩子啊，是谁掉坑里了？他低着头说：“那就是我。”听听，掉坑里的人是笑不出来的，除非他是个疯子。

　　一会儿，丫环们又拿来许多各式各样的送饭的来，大家痛快地玩了一回。“送饭的”就是放风筝时附加的东西，把它挂在线上，它就随风鼓起来，沿着线往上去，有的上面系着鞭炮，有的系着各种彩带等。古代的人是很会玩儿的，花样特别的多，不像现在的人，就会和电视、电脑较劲儿，一旦是停了电，大人和孩子都像傻了一样。

　　紫鹃笑着说：“这会儿劲很大，姑娘来放吧。”黛玉就拿手帕垫着手，顿了一顿，果然风紧力大。她接过?子来，随着风筝把?子一松，只听一阵哗啦啦地响，?子上的线到了头。黛玉就叫别人来放走。大家都笑着说：“各人都有，你先请吧。”黛玉笑着说：“这一放虽然有趣，只是不大忍心啊。”李纨说：“放风筝图的就是这一乐，所以又叫放晦气，你更该多放些了，把你的病根儿都带走就好了。”紫鹃笑着说：“我们姑娘越来越小气了。哪一年不放几个啊，现在怎么又心疼了。姑娘不放，等我放。”说着，她就从雪雁手里接过一把西洋小银剪子来，从线的最头上，“咯登”一声铰断了，然后笑着说：“这一下把病根儿可都带走了。”那风筝飘飘摇摇，快速地想后边去了，一会儿就只有鸡蛋大小了，转眼间只剩了一个黑点了，接着就不见了。大家都仰着头，边看边说：“有趣，太有趣了。”

　　宝玉感慨说：“可惜不知落到哪里去了。如果落在有人的地方，被小孩子捡到了还好，如果落到荒郊野外，那它就太寂寞了。还是把我这个也放去，让它们两个作伴儿吧。”于是，她也用剪子剪断，放走了风筝。探春正要剪自己的凤凰，见天上也有一个凤凰，就说：“这也不知是谁家的。”大家都笑着说：“先别剪你的，看他的倒像是要来缠上的。”说着，就见那只凤凰渐渐靠近了，就和这只凤凰缠在了一起。大家正要往下收线，那一家也要收线，正不开交呢，又见一个门扇大的玲珑喜字带响鞭的风筝，在半空中噼里啪啦地响着，也靠了过来。大家笑着说：“这一个也要来缠上了。先别收线，让它们呢三个缠在一起，那才叫有趣呢。”正说着，那个喜字风筝果然和这两个凤凰缠在了一起。三处都在收线，三拽两拽，就都断了线，三个风筝飘飘摇摇都飞走了。大家高兴地直拍手，笑着说：“太有趣了，不知道那个喜字是谁家的，太会捉弄人了。”，可不是嘛，人家弄两个凤凰，他就马上弄个喜字来凑热闹。黛玉说：“我的风筝也放走了，我也累了，我也要回去休息了。”宝钗说：“等我们放走了吧，大家一起回去。”说着，她们看姐妹们都放去了，大家才散了。黛玉回房间马上歪着休息。

　　两个凤凰加一个喜字，究竟代表什么？吉祥不吉祥呢？黛玉肯定是累了，但另外是不是又联想到什么了？

　　请看下回。

　　............................................................................................................................................