   

## 《横祸》

摘自里果夫斯科-切尔诺烈倩斯基银行的大事记

最初发表在一八八二年十一月二十日《花絮》杂志第四十七期上，署名“安托沙·契洪捷”。目前保存着从杂志上剪下的小说，经契诃夫修改过，但未完工。

在剪下的小说上，删去了副标题，并有增补。契诃夫已开始修改，但未完工，并且在上面写道：“不收入全集。安·契诃夫。”

契诃夫在这家杂志上多年写稿，是从这一篇开始的。杂志主编列依金一八八二年十一月十四日写信给契诃夫说：“先生，我荣幸地通知您，您分两次寄来的小小说，我已经收到，并且从中选出三篇供《花絮》杂志刊登：《演说和小皮带》、《不顺当的拜访》和《横祸》。多承赐稿，无任感铭。然而您的小说《教会了》和《顺便提到》随信奉还。总的来说，您为本刊写稿，我感到愉快。您那些短小的散文作品，如果符合本刊方针的话，我是永远乐于发表的。要是以后您有可供小《花絮》栏刊登的小东西或图画说明词，也请赐下。”在十二月三日所写的下一封信里，列依金通知契诃夫说，他的稿费已经定为每行八戈比。