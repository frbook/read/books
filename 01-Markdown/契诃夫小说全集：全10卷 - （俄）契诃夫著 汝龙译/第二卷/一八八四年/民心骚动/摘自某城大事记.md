摘自某城大事记

人间活像一个火炉。午后的骄阳晒得那么起劲，就连税务员办公室里挂着的列氏寒暑表也张皇失措，水银柱一直升到三十五点八度，然后迟疑不定地停住不动。……市民们大汗淋漓，不亚于跑累的马，听凭脸上的汗自己干掉，懒得去擦了。

在市集的大广场上，那些关紧百叶窗的房屋前面，有两个市民在走动，一个是地方金库司库员波切希兴，一个是诉讼代理人奥普契莫夫（他又是《祖国之子》的老资格记者）。两个人走着，由于炎热而沉默不语。奥普契莫夫见到市集的广场上尘土飞扬，颇不洁净，本想把市政机关批评一番，然而他知道他的同伴秉性和平，思想温和，就没开口。

在广场中央，波切希兴忽然站住，抬起头来瞧着天空。

“您瞧什么，叶甫普尔·谢拉皮奥内奇？”

“椋鸟飞来了。我瞧它们飞到什么地方去。像黑压压的乌云似的！假定我们开枪打它们，随后把它们拾起来……假定……它们停在大司祭的花园里了！”

“根本不对，叶甫普尔·谢拉皮奥内奇。不是停在大司祭家里，而是停在助祭符拉托阿多夫家里。要是从这个地方放枪，一只鸟也打不到。散弹的颗粒小，飞到半路上就没有力量了。再者，您想想吧，何必打死它们呢？这种鸟对浆果有害，这是实在的，不过它们毕竟是动物，是上帝创造的活物啊。比方说，椋鸟会歌唱。……那么，请问，椋鸟歌唱的目的何在？为了赞美上帝。‘你们各种活物啊，赞美上帝吧。’啊，不对！看样子，它们是停在大司祭家里了！”

有三个年老的女香客，身后背着行囊，脚上穿着树皮鞋，静悄悄地走过交谈的人身旁。她们用疑问的眼光瞧着波切希兴和奥普契莫夫，看见他们不知什么缘故瞅着大司祭的房屋，就悄悄走开，在离他们不远的地方站住，再看看两个朋友，然后自己也瞧着大司祭的房屋。

“对，您说得对，它们是停在大司祭家里了，”奥普契莫夫继续说，“现在他家的樱桃成熟了，所以它们飞到那儿去啄食。”

大司祭沃斯米司契谢夫本人从他花园的旁门里走出来，同他在一起的还有诵经士叶甫斯契格涅依。大司祭看见人家往他这边瞧，不明白人家瞧什么，就停下来，同诵经士一起也抬起头观看，想弄明白是怎么回事。

“巴伊西神甫多半是去给人家行圣礼，”波切希兴说，“上帝保佑他吧！”

在两个朋友和大司祭之间那一大块空地上，有些刚在河里洗过澡的工人走过去，是商人普罗夫工厂的。这些人看见巴伊西神甫举眼望着高空，又看见那些女香客站在那儿不动，也往上看，就停住脚，也往那边瞧。照这样做的还有一个男孩，他本来领着一个瞎眼的乞丐走路，另外还有个汉子，提着一小桶腐烂的青鱼走来，准备倒在广场上。

“大概是出了什么事吧，”波切希兴说，“莫非是起火了？可是不对，没看见黑烟呀！喂，库兹玛！”他对伫立观望的汉子说，“出了什么事？”

汉子回答了一句什么话，可是波切希兴和奥普契莫夫什么也没听清。在所有的店铺门口，带着睡意的店员们纷纷出现。有几个抹灰工人本来在粉刷商人费尔契库林的粮食店，这时候从梯子上爬下来，同那些工人站在一起。一个消防队员本来在瞭望台上光着脚转圈子，这时候停住，看一会儿，从上边走下来。瞭望台上变得空荡荡。这就显得可疑了。

“真是有什么地方起火了吗？可是您别推我呀！该死的猪！”

“您看见哪儿起了火？哪有什么火灾？诸位先生，你们散开吧！我客气地请求你们！”

“多半是屋子里边起火了！”

“他嘴上说客气地请求，可是他乱推乱搡！您不要抡胳膊！虽然您是长官先生，不过您根本没有权利放任您那两只手！”

“踩痛我的鸡眼了！啊，巴不得叫你咽了气才好！”

“谁咽气了？伙计，有人咽气了！”

“干什么聚着这么一群人？这究竟有什么必要？”

“有人咽气了，官长！”

“在哪儿？你们散开！诸位先生，我客气地请求你们！我客气地请求你，笨蛋！”

“你要推就去推乡巴佬，上流人可不准你碰！你别碰我！”

“难道这些人也算是人？难道他们这些魔鬼也懂得好话？西多罗夫，你跑一趟，去把阿基木·丹尼雷奇找来！快去！诸位先生，你们不会有好下场！阿基木·丹尼雷奇一来，你们就要倒霉！巴尔芬，你也在这儿？！你还是个瞎子，而且那么大的年纪！他什么也看不见，却跟着人家跑到这儿来凑热闹，不听管束！斯米尔诺夫，把巴尔芬的名字记下来！”

“是！请问，普罗夫工厂的那些工人也记下吗？喏，那个肥脸蛋的家伙就是普罗夫的人！”

“普罗夫工厂的暂时不用记。……明天普罗夫过生日！”

那群椋鸟飞上大司祭花园的天空，像是一团乌云，可是波切希兴和奥普契莫夫已经顾不上看它们。他们站在那儿，一直往上看，极力要弄明白这样一群人聚在这儿干什么，往哪儿看。阿基木·丹尼雷奇来了。他嘴里嚼着东西，擦着嘴唇，哇哇地嚷，冲进人群里。

“消防队员们，准备好！你们散开！奥普契莫夫先生，您走开，要不然您就会遭殃！您与其在报纸上写出各式各样的文章批评正派人，还不如自己多守点规矩的好！报纸不会教人做好事！”

“我请求您不要提到报刊！”奥普契莫夫发脾气说，“我是文人，我不允许您提到报刊，虽说我按照公民的责任，是尊重您，把您看作父亲和恩人的！”

“消防队员们，拿水浇他们！”

“没有水，官长！”

“少说废话！坐着马车去取水来！快去！”

“没有马车可坐，官长。少校坐着消防队的马车送他姑母去了！”

“你们散开！你往后退，见你的鬼！……你要自讨苦吃吗？把他记下来，魔鬼！……”

“铅笔丢了，官长。……”

人群越聚越大。……要不是格烈希金的小饭铺里有人想起试一试前几天从莫斯科寄来的新风琴，那么上帝才知道这群人会增长到什么规模。这群人一听见奏起《射击手》这支曲子，就叫一声啊呀，纷纷往小饭铺里涌去。因此，这群人什么缘故聚集在一起，谁也没弄明白，至于那些椋鸟，这次事故真正的罪魁祸首，奥普契莫夫和波切希兴却已经忘了。过了一个钟头，全城已经太平而安静，所能看到的只有一个人，也就是在瞭望台上走来走去的消防队员。……

当天傍晚，阿基木·丹尼雷奇在费尔契库林的食品杂货店里坐着，喝那搀白兰地酒的**柠檬汽水**[^1]，写道：“大人，除正式呈文外，本人略抒己见，作为补充。父亲和恩人啊！多亏您贤惠的夫人住在本城附近一个有益于身心的别墅里虔诚祈祷，本案才没发展到不可收拾的地步！今天我遭到种种艰难困苦，实非笔墨所能形容。克鲁宪斯基和消防队少校波尔土彼耶夫亲临现场，指挥若定，其才干颇难找到适当的词句加以描述也。我国有如此称职的公仆，本人引以为荣！我虽尽力而为，无奈我是软弱的人，一心为人谋幸福，此外别无所求。目前本人坐在温暖家庭之中，眼内含着泪水，一心感谢上苍未使本案酿成流血惨剧。至于犯罪人等，因罪证不足，暂时在押，不过我打算过一星期就予以释放。他们愚昧无知，故触犯神诫[^2]也！”

---

[^1]:  原文为法语。

[^2]:  指《圣经》所载神的“十诫”。按此处应说“法律”。