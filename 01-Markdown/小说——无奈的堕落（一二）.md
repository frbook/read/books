> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [zhuanlan.zhihu.com](https://zhuanlan.zhihu.com/p/633499225?utm_medium=social&utm_psn=1757998519167033344&utm_source=wechat_session)

> 第一章在办完丈夫的丧礼后，若兰开始思考继续活下去的意义，为了不辜负丈夫的期望，为了照顾孩子，还是为了自己的人生？自己的父母早就已经不在了，接下来看来要和刚上高中的女儿相依为命，以后她就是若兰的一切。…

第一章
---

在办完丈夫的丧礼后，若兰开始思考继续活下去的意义，为了不辜负丈夫的期望，为了照顾孩子，还是为了自己的人生？自己的父母早就已经不在了，接下来看来要和刚上高中的女儿相依为命，以后她就是若兰的一切。

还没来得及从丈夫意外过世的悲伤中脱离出来，一件件的事情又接踵而至，她还有六年的房屋贷款需要交，每个月的还款都是一笔普通家庭难以负担的开支。丈夫创业半途没有成功，只留下了一笔存款和一笔更大的债务给她，这些债务像失去了承重墙的高楼，重重的压在了若兰的肩上。

她一个小小的公司会计，挣得的薪水不多，只刚好够母女两人生活，勉强可以负担女儿的学费和日常开销。四十出头的她原本保养的很好，皮肤白皙红润，身材高挑，像三十岁的少妇。可现在经受了如此打击，气色倒像一个中年妇女，平时要靠化妆才能维持神色。

她的闺蜜见她这样也不是办法，便想方设法帮她找财路渡过难关。成琳是朋友中最好的一个，但与贤惠的若兰不同，成琳喜欢混迹在夜场，街上的酒吧街经常出现她的身影，她的前夫还因为她的这个习惯和她经常闹矛盾，后来还离婚了，她也在为若兰想办法找工作，但她物色到的自然是夜场工作居多。

“帮你找了个工作，薪资待遇很好，你要不要去看看？” 成琳找到若兰，把这个好消息告诉她。“真的吗？什么工作？” 若兰眼里放光。“我听说夜魅酒吧正在招模特，月薪保底两万，还有不小的提成，怎么样？” 若兰显然有些为难，她上次去那种场合已经是几年前，瞒着老公去了一会儿还没敢喝酒。成琳又劝她：“机会可不等人，这么好的机会就赶紧去啦。磨磨唧唧的我看着都着急。”“嗯，” 若兰心想为了女儿的生活和学业，她就走出这一步吧，“我明天晚上去应聘看看吧。”

第二章
---

第二天傍晚下班一回家，没来得及把身上的职业装换下，若兰就在厨房准备晚餐，她心想要先把女儿放学回来的晚饭准备好，然后再去酒吧应聘。她给女儿的手机发了消息，告诉她妈妈今天在外面加班，要很晚才会回来，让她自己吃饭，然后写完作业早点睡觉。

若兰根据地址打车来到了夜魅酒吧，此时才刚过九点，夜场还没有热闹起来，她绕过大厅，来到了公司后台。敲了敲门，里面的 hr 便让她进去。若兰坐在了 hr 对面的椅子上，她仔细观察了一下，hr 很年轻，而且身材纤细，穿着和她一样端庄优雅的职业装，只是手上似乎带了手套一样油光发亮，双腿也是油亮光泽的，她感到奇怪，但也没有多想。

经过一番简单的面试，hr 便同意她入职，在签署合同前，hr 还问了她一句：“我们公司要求员工穿着统一服装，也就是紧身衣，请问可以接受吗？” 若兰没有多想，觉得酒吧夜店要穿性感服装去展示也是正常的事情，便回答可以接受，接着在合同上签了字。

不一会儿有两名穿紧身连衣裙的服务生开门进来，对她说 “这边请，带您去更衣。” 若兰心想对新员工还挺周到，没多想就跟着她们来到了另一个房间。

房间中也有两个桌子和几张椅子，还有一张床，“请把衣服脱下，躺在床上。” 若兰便从外套开始，接着衬衫，包臀裙，一件件脱下，还讲丝袜卷起来塞到高跟鞋里，坐在了床上，“内衣内裤也要脱哦～” 其中一名小姐告诉她，她便讲内衣内裤也脱下了，一边躺下还一边疑惑，突然两名小姐双手涂满乳液，在若兰的身上涂抹，她们告诉她，这是脱毛乳液，不仅可以脱毛，还可以延缓毛发的生长，只需要用一次，一年都不会再长毛了。十分钟后又用热毛巾将她的身体擦干净，开始涂抹另一种液体，“这是润滑液，方便穿后面的服装的。” 一名小姐说。涂抹完毕，还没等若兰开始忐忑不安，乳胶袜就开始往她的脚上套，她们的手法很娴熟，看来已经不是一次两次做这个了。

乳胶衣一点一点的往上吞没若兰的身体，好紧！当乳胶衣穿到大腿的时候，若兰便忍受不住了，虽然之前也经常穿着塑身衣，但这乳胶衣比她穿过的所有塑身衣都要紧几倍，若兰坐起身，看着自己的下半身，只见双腿已经在一层透明乳胶的包裹之中，光滑发亮，腿也瘦了一圈，还剩下没穿上的部分耷拉在腰间，乳胶衣晶莹剔透，看起来很薄，也很有弹性。“请躺好，我们的员工都统一需要穿着乳胶紧身衣的，尤其像您这样的模特职位，不仅要穿着乳胶紧身衣，还要穿高跟鞋和束腰。” 说着还指了指一旁的桌子，上面是什么若兰也没有看清，只顾着抗拒乳胶紧身衣的包裹，还说着：“怎么会有这样的员工服装，快给我脱下来。” 还伸出去想要将乳胶扯下，还没能碰到乳胶衣，手就被另一位小姐挽住，她们还拿来了一张合同的复印件，告诉她这些都是刚刚自己同意的，不可以违约，否则需要缴纳违约金。

若兰听到违约金便垂下了头，她又重新躺下，任由两名小姐摆布。没多久这一件只有脖子有开口的乳胶紧身衣便穿在了她的身上。她坐在床边，感受到身体被紧紧包裹的不适，从脖子一直到脚尖的窒息感，呼吸也不太舒畅，幸亏之前时常穿塑身衣，不然穿上这一身还不得崩溃了，若兰心里想着。服务小姐又拿着一双高跟短靴，蹲在她身前，抓起她的脚就往里面塞，因为短靴也很紧，加上没有涂润滑液的乳胶皮会有点涩，费了好大劲才把两只脚的高跟鞋都穿好。当她以为可以下床时，只听见 “咔哒” 一声，她低头看到，一把精致的小锁挂在了她右脚短靴的鞋帮上，服务小姐又娴熟的拿着另一把小锁，扣在了她的左脚上。

“这是……” 若兰还没来得及惊讶，也或是她已经不知所措不知说什么，一件束腰围在了她的腰上，是一件黑色宫廷束腰，从胸部一直包裹到胯，扣好了前面的几个扣子，身后的小姐便开始用力拉背后的绳子，若兰一下瞪大了眼睛，肺里的空气很快都被挤了出去，她只能小口小口的喘气，但身后的小姐还不满意，继续一点一点把绳子往外拉，一点点夺走了若兰胸腔和腹腔活动的空间。直到她有了一阵窒息感和头晕时，才停了下来，被拉出来的绳子在若兰的背后打了一个优雅的蝴蝶结。这还没有完，一个闪着金属光泽的腰带围在了束腰最细的地方，和双脚一样，一把漂亮的小锁扣在了腰带上。“为什么…… 还要上锁？” 若兰不适应忽然出现的强力束缚，只得在束腰狭小的空间里争取着呼吸空气，说话也有些喘。“我们为了防止员工任意脱下，影响公司形象，也影响模特身材的塑造，给每套束腰和高跟鞋都要严格上锁，即使回家也要一直穿着。只有每天下班时可以脱下洗澡，然后重新穿上才能离开。”

“这……” 若兰想要大声指责，但有限的空气让她无法大声说话，情绪激动让她一阵眩晕。没等若兰抱怨，两名服务小姐便推门出去了，临走前还告诉她：“你先在这里适应一会儿，半小时后带你去参加培训。”