   

## 简化你的电子产品

技术和数字通信的迅速发展的确带来了许多好处。信息技术确实让我们的生活变得更容易、更快捷、更有效率。但是，随着我们对数字设备的投入增加，回报却在逐渐减少。

我们变得沉迷于科技，而科技则影响着我们生活的方方面面。我们变成了那些原本应该简化我们生活的小玩意儿的奴隶，比起现实生活中的互动与经验，我们更喜欢那些快而短的信息和低质量的娱乐。

我们花大量的时间浏览社交媒体，电脑收件箱总是满的，桌面上放满了文件，笔记本里有大量文档、照片、下载文件，它们吸食着我们的生命。

数字“杂物”会以一种你难以察觉的方式占据你的时间，让你做一些没用的事情，就像你房间里的那些真实存在的杂物一样，它们都会让你感到焦虑和压力。

在《10分钟数字整理：消除过量信息的简单习惯》里，巴里和斯蒂夫提到：

如果你每天花很多的时间在数码设备上，那么虚拟世界恐怕比你的配偶、孩子、朋友与你更亲近。你知道生活的平衡出了一些问题，发现自己一旦有空闲时间，就会翻开笔记本的盖子，要不然就盯着你的手机。有时候甚至在并不空闲的时候，仍会这样。这真的是你想要的生活吗？

在这本书中，我们提供了几种方法，帮助你来养成精神层面的整理习惯。

### 你是怎么用电子产品的？

真实地观察自己是怎么在数码设备上花费时间的。当然，你的生活中有许多方面都需要上网来支持。除此之外，你是否花了许多时间上网、玩游戏、浏览社交网站呢？

花一点时间回顾一下你的一天，把那些做不必要事情的时间加起来。最好能记录你一整天使用数码设备的情况。你会惊讶于你在虚拟体验中花费了多少时间。

这些虚拟的刺激会让你感到兴奋，它们还有成瘾的效果，把你拖离那些可以充实自己而不是耗尽能量的有意义的爱好。

那么如何才能回到正轨？

每天抽出神圣而自由的一小时，让自己不受数字时代的侵扰。关掉你的电脑，把手机扔进抽屉里，如果不沉溺于数字的消遣，你能做些什么？

我们建议你：

·看一本书

·长距离地散步

·锻炼身体

·与朋友交谈

·与配偶和子女共度美好时光

·做一些有创造力的东西，比如写作或画画

·学习一个新的技能

·冥想

·听音乐

·骑自行车

·完成一项工作

做一些切实的、当下的、积极的事情，这样既能够避免沉溺于虚拟的数字世界，也能避免因在无意义的事情上花费太多时间而感到内疚和焦虑。

### 你的网络空间有多杂乱？

数字杂物不像你房间里的杂物一样能客观地被看见，它悄悄地找上你，在你意识到之前，你的桌面上就充满了各种图标，你的收件箱充满了邮件，你的文档塞在各个角落，以至于你不得不依靠查询软件来找到自己想找到的东西。

如果你和我们一样，生活全拴在电脑上，这听起来似乎挺不可思议，但如果你把所有重要的个人文件和工作文档都放在电脑里，你就知道它对你的生活有多么重要了。

电脑很容易成为数码仓库，你应该试着去找到那些浪费你时间、让你感到沮丧焦虑的文档和文件。

手机是另一个“迷你电脑”，你会把它放在口袋或包里随身携带。这也是另一个储藏“数码杂物”的地方，过量的应用、照片、新闻、游戏都会让你沉迷。

如果你的数码设备已经像这样“撑爆了”，你开始感到有负担，那么每天花十分钟整理这些杂物，你会开始感到轻松和自由。

我们建议从你能获得最大收益的部分开始，如果你每天都因为找不到文件而沮丧，那么就从整理文件开始；如果你每天都因为邮箱里的数千封邮件而心悸，那么就从处理邮件开始。关键是一定要开始行动。

### 你的数字化思维模式是什么？

众所周知，电子设备，或更准确地说是里面的内容，给你带来了大量精神压力，让你的精神持续兴奋。没人愿意承认这一点，但我们都无法否认，数字世界对我们生活的影响在当今是多么普遍。

数码产品的兴起并不是会随时间而消失的流行产品。它将会一直存续，并且随着时代的发展，与我们生活的联系也将越发地紧密。如何应对数字时代的干扰，如何应对它对我们精神健康的影响，都取决于你的行动。无论如何，主动地在数字时代做出符合自己准则的选择，是非常重要的。

通过建立关于数字时代的“价值系统”，来管理你的时间和杂念，被认为是非常有效的。你可以问自己下面几个问题，来帮助你树立自己的准则：

·我每天因工作花费在电子设备上的时间，有多少是绝对必要的？

·我的工作要求我待在电脑前的时间是否超出了我的期望？

·我如何能更频繁地与工作伙伴面对面交流？

·我希望花费多少时间用自家的电脑工作？

·我想花多长时间查看社交媒体（以娱乐为目的）？

·我想玩多长时间的手机？

·什么情境下，电话交流与面谈要比短信交流更合适？

·我忽视了和谁的友谊，又想如何重新培养这段友谊？

·在与家人和朋友共处时，使用手机、平板电脑、笔记本电脑的时候，我们应该达成什么样的共识？

·在哪些特定场合或家庭时间（比如一起吃晚饭）里，你希望杜绝电子设备的出现？

·对孩子使用电子设备，应该设定什么限制或规则？

·在这些规则面前，我们应该如何为孩子们树立榜样？

·如果我的电脑死机了，利用这段时间的最好的五种方法是什么？

·我该如何应对想要上网和浏览社交媒体的强烈冲动？

·我要如何保证处理这些数字垃圾，避免它们失控？

根据你对这些问题的答案，写下你关于减少在电子设备上花费时间的承诺和准则。无论这期间失败多少次，你都需要重新站起来！