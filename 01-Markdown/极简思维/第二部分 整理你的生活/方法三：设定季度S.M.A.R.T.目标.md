   

## 方法三：设定季度S.M.A.R.T.目标

设定S.M.A.R.T.目标能有效地帮你找到你人生中真正重要的东西。这些目标短期内就能够实现。你需要在每个季度（每三个月）设定一次目标，而不是像年度目标那样，总是让你脱离现实状况。

让我们先简单地讲解一下S.M.A.R.T.目标

乔治·多兰（George Doran）是S.M.A.R.T.法的创始人。该方法最早发布在1981年的《管理评论》（Management Review）上。五个首字母分别代表着：具体的（Specific）、可衡量的（Measurable）、可达成的（Attainable）、相关联的（Relevant）和有时限的（Time-bound）。

下面让我们详细介绍一下每个组成部分：

S：具体的（Specific）

具体的目标将回答你六个以W字母开头的问题：谁（who）、什么（what）、哪里（where）、什么时候（when）、哪一个（which）、为什么（why）。

当我们能够分辨答案里的每一种元素后，我们就知道用哪种工具和行动可以帮我们达成目标了。

·何人：目标涉及了哪些人？

·何事：你想完成什么目标？

·何处：你准备在哪里完成你的目标？

·何时：你打算什么时候开始做？

·何物：什么东西可能会妨碍你？

·何因：你为什么要这样做？

当你能够分别完成每一个元素所代表的任务后（比如何时、何地、何事），你就会知道，你的目标已经完成了。

M：可衡量的（Measurable）

可衡量的目标是通过精确的时间、数量或其他单位来定义的。本质上是那些可以用来量化目标实现程度的指标。

建立可衡量的目标会使做决定变得更加容易，同时也能验证你前进的方向是否正确。一般来说，一个可衡量的目标可以通过问自己“怎么做”“做多少”“做的速度”来确定。

A：可达成的（Attainable）

可达成的目标会延伸你所认为的可能性的极限。它们并非不可完成，但总是充满了挑战。设立可达成目标的关键点在于，创造一个恰好超越你现在能力所能达成的目标。这样做，即使做失败了，你仍旧能够完成一些有意义的事情。

R：相关联的（Relevant）

相关联的目标的关键在于你真正地渴望。它与你人生中重要的一切事物和谐共生，从事业成功到家庭幸福，都能够成为相关联的目标。

T：有时限的（Time-bound）

有时限的目标会有明确的截止日期。你需要在目标时间前完成你想达成的结果。有时限的目标是具有挑战性而又脚踏实地的。完成的时间可以是当日、几周后、几个月内甚至几年。建立有时限目标的关键是，设定截止日期，然后倒着推论你要做的事情，并养成定时完成的习惯。

S.M.A.R.T.目标是明确而容易确定的。你想达成的结果是确定的，而到了截止日期，是否完成了目标也变得一目了然。

下面这个例子讲述了S.M.A.R.T.目标是如何与之前讲述的人生中的七个重要部分产生联系的：

1.事业：“我将在两个月内通过推介、网络和社交媒体营销活动为我的网页设计顾问提供五个新项目。”

2.家庭：“每六个月至少带我家人去旅行一次，加强我和家庭的联系。每个月我会留出一个小时来规划未来的旅行。”

3.婚姻（或情侣）：“我要从我的伴侣身上找到我最爱的三个点，然后在周五的晚上向她表白。我会在周四晚上抽30分钟来完成这件事，这样我就可以边回顾我们所度过的美好时光边完成这项任务了。”

4.精神/个人成长/个人发展：“我打算每天花五分钟来感谢我生命中一切美好的事物。说做就做，在午饭前我就要开始做这件事情，并且养成习惯。”

5.休闲/社交：“我决定每周花三个小时练习水彩画。这样我需要减少一些不重要的习惯所耗费的时间，比如看电视。”

6.生活管理：“每次发工资，我都要把其中的十分之一存起来，然后用它们来投资基金。”

7.健康与健身：“12月31日前，我要完成每周三次，每次至少半小时的训练。”

希望这七个例子能够让你了解如何创建S.M.A.R.T.目标，开始一种平衡的生活。现在让我们完成以下六个步骤，将上述知识转化为行动：

### 第一步：确定对你来说重要的事情

关注你生活中的所有领域并不能帮你完成对你来说有意义的目标。原因很简单：如果你总想在所做的事情中找到意义，那么当你看着你那长长的目标列表，你很快就会感到不堪重负了。展望未来是非常重要的，但你也需要足够的时间来活在当下。我们的建议是，只关注你生活中的三到四个方面。你可以通过比较我们讨论过的七个方面来确定现在对你而言最重要的是什么。从你认为最重要的几个方面创建目标，挖掘那些会让你感到有挑战性的令人兴奋的目标。

### 第二步：制定三月计划

斯蒂夫的经验教训表明，长期目标是很容易产生变化的。今天看起来非常紧急的事情下个月可能就不再重要了。对他而言有效的策略是把最优先的目标分解成三个月（或季度）的目标。

我们为什么要这么做呢？

你的生活节奏很快，并且很容易发生变化。为了及时跟上这些变化，制定短期目标就显得极为重要。这些目标能帮助你持续不断地努力，并且保持高动机。

斯蒂夫的经验还告诉他，一个长期目标（比如超过六个月）往往会让人失去动力。假如你知道几个月后才能看到结果，那么重复的行为就很容易让你产生拖延。你会不断地推迟你的计划，承诺你会在下周完成它。然后你发现，一年过去了你什么都没有完成。

所以，为了使事情变得简单好操作，我们建议你确定此时你生命中最重要的3~4个部分，然后在每个部分都设定一个你希望在未来三个月内能达成的S.M.A.R.T.目标。

### 第三步：每周总结以及时间表的制定

当你还有大量其他事情需要处理的时候，按部就班地完成目标似乎不那么容易。好在我们有一个简单的方法能解决这个难题：每周做一次总结，并为接下来的七天做一个新的行动计划。

周总结是一个非常棒的概念。戴维·艾伦（David Allen）在他的书《把事做完》（Getting Things Done）中讲述了如何去做周总结。这是一个很简单的过程，每周选择一天（斯蒂夫选择的是周日），在这一天里展望未来七天，然后给那些需要完成的事情做一个时间表。

你可以通过以下三个步骤来完成你的时间表：

1.回答三个问题：仔细地预想未来七天可能发生的事情，然后回答下面三个问题：我的义务是什么？我需要优先去处理的事情是什么？我有多少时间？你的回答是非常重要的，这决定了你这七天里有多少时间可以支配来完成自己的目标。你不应该用数以百计的琐事来填充你的时间表。这样做最容易导致精神杂念的产生。相反，你最好能提前地预估可以用来实现重要目标的实际时间。

2.制定计划：回答完这三个问题，开始规划接下来的七天。你可以边看目标列表，边规划时间，确保那些最重要的任务能够被完成。

3.完成你的计划： 如果你和巴里、斯蒂夫一样，每周都会产生大量的关于完成目标的极佳点子，那么你可能会面临下面的问题：如何才能实现这些点子呢？我建议你可以记录你的点子，并做如下处理：1.确定是否要立即去执行；2.如果不立即执行，那么设定一个执行的时间。

下面我们来看看这个方法是如何运作的：

如果这个想法是立即可行的，那么就制定一个循序渐进的计划来确定你该怎么做。简单地记录下你将执行的一系列任务，然后把这些任务安排到你的周工作计划中。

如果这个想法是不可行的，那么把这个想法放到月总结的存档文件夹里。如果每产生一个想法都这样去做，你就不会忘记在适当的时候完成它们。

周总结在实现目标的过程中是非常重要的。它能帮助你建立危机感，让你能够更好地完成每个目标。同时周总结还能帮你制定每日的行动计划，你将知道每天有哪些任务切实可行。

### 第四步：行动起来

不行动你永远不可能达成目标。达成目标的诀窍在于把行动分解在每周的任务中。我们建议你采取以下几种行动：

·把目标转化为任务：查看目标需要被完成的日期，然后倒着推导你的行动。当你确认了你要完成的任务是什么，最简单的方法是把它们按步骤记录下来，这样你就能按部就班地完成目标了。

·计划目标时间：你在每个目标上花多少时间取决于每个任务需要多少时间。有些任务每周只需要几分钟，而有些任务需要你每天都付出努力（这就是为什么要坚持完成每个目标的时间承诺）。计算出每个任务需要多少时间，并将它们安排在你一周的工作计划内。

·目标的优先级：我们每天都有大量的事情要处理，它们彼此之间甚至可能产生冲突。怎么解决这个问题？我们建议把最重要的事情放在早上，或一天中任意让你感到精力最为充沛的时间去完成。

·安排单独任务的时间：许多人都会陷入一种误区，那就是随时去处理那些重要但不紧急的任务。解决这个问题的好方法是，每周安排固定的时间，来处理这些单独的任务。

斯蒂夫用手机应用ToDoist来帮他计划单独的任务。每次他在制定三月计划的时候，他都会用ToDoist创建一个新的计划表，然后把那些需要完成的单独任务添加在计划表里。这样他就能清楚地确定自己接下来每天要完成的明确的任务。

### 第五步：回顾的你目标

完成任何目标都需要连贯性，确保你的行为前后是一致的。这也是为什么我们需要去回顾自己的目标，确保自己完成了每一个重要的任务。我们建议你建立一种衡量标准来量化你目标的完成度。在周总结的时候查看完成度，来确保你循序渐进地完成目标。

不管你有多么忙碌，都该留出回顾总结的时间。如果每天不进行总结，那么你就不太可能获得成功了。

生活常常会在你完成目标时给你出难题，这些难题会让你感到沮丧，对目标失去动力。我们建议你每天都要反复地确认自己的目标，频率在2~3次。通过这种方式，你能够把你的目标放在你意识的最前端，并提醒自己是为了什么而做每天的工作的。

### 第六步：评估季度目标

每天，你都在为了目标努力工作。每天每周你都会回顾你的努力，并做出总结。但问题是，你可能会忘记回头看看藏在每个目标背后的“为什么”。换句话说，人们常常忘了去重新审视自己的目标，看看它们是否仍旧值得去追求。季度评估是非常重要的，它能确保你的行为与人生目标的一致性，并且能根据你新学到的事物来设立新的目标。

通过回答下面几个问题，你可以完成你的季度评估：

·我是否收获了预期的结果？

·什么策略导致了成功或不成功？

·为了完成目标，我付出了百分之百的努力吗？如果没有，为什么？

·我是否取得了与我的努力相一致的结果？

·下个季度我是否还需要制定一个类似的目标？

·我应该消除或改变什么目标？

·有没有什么我愿意尝试的新事物出现？

虽然你需要花几个小时来完成季度评估，但这是非常有必要的。季度评估在最大的程度上防止了你在与你计划不一致的目标上浪费时间。短短的几小时与你可能会浪费的时间相比实在不算什么。

以上就是对S.M.A.R.T.目标的简介。现在，你拥有了设定目标和制定计划的方法，而确保你能真正去设定目标的最佳方式，是把目标与个人激情联系起来。在下一节和最后一节中，我们将向你展示如何做到这一点。