   

## 充分敞开身体和心灵

卡夫卡本人如何看待自己的写作者角色？这符合他的从事写作的理想：没入一个形象流之中，以便在尽可能没有控制和规划的情况下，把摆放在他想象中的东西当作似乎自动形成的计划加以激活。这个过程对他来说达到一种具体的性爱的深度。1911年10月3日他提到，创造力必须在他内心爆发，为了不致自身毁灭。在这一点上它们像他青春期时在与他的家庭女教师的关系上压制下去的那种“倾泻”，致使它们受到“反冲”而在他自己体内消散。一个月后他谈到了写作行为中的“振奋”。1912年1月在一个写作失败、单调无聊的星期天之后，他在日记中写道，他曾经“想倾泻进”写作之中。1912年9月25日在文学创作毫无所获的一个夜晚之后，一则日记抱怨这时间“徒劳地流过”。“好朋友，倾泻出来吧。”1915年11月初的绝对命令如是说，一次晚间散步越过卡尔桥想到写作时，他发出了这道命令。

认为他只由文学组成的这个独特观点方面，卡夫卡已略带几分自嘲地看透。它表现在这一尝试中：在涉及存在的写作深度时，自我陶醉式地培养孤寂者角色并通过孤独获得自我享受。1912年3月2日他记下：“谁向我证实我只是由于我的文学创作，而对别的事漠不关心，并因此而冷酷无情这件事的真实性或可能性。”他因写作而“消瘦了”，因为他没有空闲的力量去吃、喝、做哲学思考、性爱和欣赏音乐，1912年1月3日他这样断言。这也是一种自我想象的形式，因为“自我”在实验的现场废除自己，以便单纯作为“作家自我”而存在，只要语言建造并同时严密地保护“自我”，那么对于卡夫卡而言，语言就仍然是超然社会的——交际的——规模之外的一种协调一致的媒质。这样的身份总是带有脆弱的特性，因为它只是一瞬间，在极度兴奋的写作体验的瞬间发展起来。语言正如《一场战斗的描写》已经显示的那样，拥有一种创造性的和一种破坏性的潜能。这种潜能产生一种也可以在身体上体验到的协调一致，但终究不能给予互相理解，因为它受到总是变化不定的分派的束缚。这种潜能的有效成果同时产生一种基本的忐忑不安：世界如何能够被说成是亲切友好的，如果语言以不断变化无常的形式产生并以这样的方式总是虚构这种潜能的话？每一个比喻——卡夫卡分明知道——在怀疑现实的客观意义之时便是。这件语言礼物是受到困难困扰的，因为它迫使人对自己的现实经验的统一性产生怀疑。

文学写作对于卡夫卡来说，仍然是具有犹如存在的特性的目的本身。不是对作品的喜爱，而是对写作的乐趣掌控着对作家角色的理解。这是不想生儿育女，却想实施同房的儿子的乐趣。“创作的甜蜜，”1911年12月9日他引用画家卡尔·施陶费尔—伯恩的话，“你使你看不见它的绝对价值。”出版一本书的喜悦虽然自从开始和库尔特·沃尔夫合作以来他就一直有，然而这种喜悦并不处于中心地位。卡夫卡那尽人皆知的与文化企业的距离，他对出版社流言蜚语和对图书市场投机活动的冷淡均符合这一估量。马克斯·布罗德作为布拉格文化生活活跃的积极分子不断地从事公关活动，而卡夫卡却是个自我满足的夜间写作风云人物。在这后面的并不是苦行，而是经创作而得到的性爱满足，这显得比最终的（常常残缺不全的）产品更重要。虽然正如卡夫卡过了好些年后在日记中透露的那样，他了解阅读自己作品的校样带来的那种巨大享受。但是这难以掩盖真正的优先权：写作占有比公众效应优先的地位。他对文学写作的兴趣像许多则日记所透露的那样源自这种内心感受：要把自己的身体引进文字之中，让身体在一种使“自我”获得一种对其自身的综合体验的定义的游戏中定型。文字——远比印出来的作品强大——是一种满足的源泉，它使感官的和精神的体验相聚在一起。写作对于卡夫卡来说，意味着进入一种简直是开辟宗教能量的循环流动之中。格斯霍姆·绍莱姆在犹太教神秘教义及其象征性意义的研究论文中指明了这种传统观点：在图拉经文中“字母表现一种神性的神秘壳体”。在卡夫卡的自述中文字也显示出一种仿佛是宗教的深度。夜间的写作是向心神和感观的一种绝对关系的接近，这种接近在别处是不会有的。他对文学创作精神性质的认识在著名的1920年笔记直接表现出来了：“写作作为祈祷的形式。”这一简单明了的表达形式是与犹太人的“造词神学”相吻合的，这种神学认为在原始的（人类始祖的）人类言语中语言和世界实现了统一。就像上帝通过命名创造事物，人类能够在提高了宗教虔诚的瞬间用其语言工具穿透现实的秘密（却不可以经久不变地领会这个秘密）。

卡夫卡的写作观很接近于这种语言神学。然而它的宗教性质却包含与一种感同身受的作品理解以及与计划的公开的作为作家的自画像保持距离。由于仅仅是写作过程的命名行为，而不是其结果使人有可能接近一种冥想状态，所以文学产品本身就退居次要地位。所以阿多诺的《美学理论》中的不容争辩的意见只说对了一半：“艺术品的概念不适宜于卡夫卡，就像宗教的概念也向来不适宜于他那样。”看来恰恰正是写作那带有宗教色彩的力量，是它一开始就用它那诉讼上的逻辑排除了作品的僵化规章。它的诸多吸引人的方面中的一个，就是连卡夫卡的已完成的作品也都保持其暂时的性质：它们避开了一种单义意义的长久存在。按照德里达的思考，那些已完成的作品也是纯粹的“文字”，也就是一种通过阅读在一再翻新的形态中产生的开放结构的模式。

卡夫卡通常都未经仔细规划就着手写作。他通常会极其迂腐，可是在写作上他却讨厌循规蹈矩。托马斯·曼的小气的写作节俭作风——这在日记中有所反映——与这样一种写作方式形成鲜明对照。很能反映卡夫卡的性格特征的，不是创造性行为的节俭的自我限定，不是这种严格确定写作开始和写作中断的自我限定，而是这种自发的写作形式。属于这种写作形式的有一再重新做出的尝试：尽可能高度浓缩写完一件作品的一些文段，还有就是作为这种做法的反面的，这种患有中断病的行为。这种中断跟托马斯·曼的情况不一样，它们不是形成自我规定的写作策略的有机组成部分，而是对创造力的巨大干扰。与不受控制的写作相称的是，卡夫卡不习惯做分段要点和写作提纲。文学虚构必定由一种自发的联想游戏而产生。这种工作方式的一面镜子就是那张通常未经整理的写字台，它在一则1910年12月25日的日记中被描写为剧院舞台。在这个舞台上各种日常用品——直尺、信件、衣刷、小钱包、钥匙串、领带、刮须镜——扮演着在想象中的观众前登台演出的演员的角色。空间上的秩序成为作家迷宫似的创作活动的象征，这种创作活动像在高烧性谵妄中进行。它反映出幻想的一次次长途跋涉，为了找到他的作品的合适结构，他不得不进行长途跋涉。在这写字台舞台上他漫游一个想象的世界。“文字对于作家而言，”德里达这样写道，“是一次必不可少的和冷酷无情的航行。”

在1912年9月22日晚上很晚的时刻，在这个未来的妹夫约瑟夫·波拉克一家人到尼克拉斯街来做客的、经历贫乏的星期天行将结束的时候，卡夫卡着手写作短篇小说《判决》。那是赎罪日，犹太人的赎罪日，他在过去的几年里常常在犹太教会堂庆祝这个节日（约瑟夫·罗特后来强调，为了恰如其分地领会赎罪日的意义，人们就得先说说“和解日”）。他拘谨地坐在结下情同手足情谊的两家人之间，在度过了这个不能令他满意的庆典之后，卡夫卡急于坐到写字台前。晚上10点左右，他进入了他的故事之中，起先没有具体的设想。

在这一宿，他将他的故事快速向前推进。第二天清晨6点，当窗户前摩尔道桥上方渐渐泛白时，他已经完成初稿。小说透出一股强大的、简直可以通过形式上的节奏感觉得到的勃勃生气。这个在日记中流传下来的文本几乎没有任何改动，后来刊印时的校样极少有与手稿不一致之处。卡夫卡在最后一个段落下面记下了发表手稿预计所需的印张数：这是默认作品定会获得成功的意思。

读过第一遍后，《判决》中远近配置的协调和反映出夜间创作浓缩狂喜的推动活力风格便令卡夫卡感到满意。写作过程被体验为一种神秘行为，身体和精神聚集，致使“最陌生的想法”也在一种灵感之“火”中“消失并再生”：“只有这样才能写作，只有在这样一种关联中，才能充分撇开身体和心灵。”这件已完成的作品，这件让外部的刺激——通过阅读和体验——变为特有之物的作品在回溯时作为“正规的诞生”出现，它在同“污秽和黏液”隔离的情况下从他之中“脱颖而出”。八年后他还在对米莱娜·波拉克解释，说是整个故事的“音乐”与“恐惧”有关联，这“恐惧”的“伤口”在“一个漫漫长夜中”第一次裂开了。这种令人诧异的音乐和恐惧之间的同盟对卡夫卡的文学自我认识一直具有典型意义。与此相应地，艺术只能来自精神疾病的宝库。他不见得是偶然地把产生《判决》的这个9月之夜看作他的真正的作家自我的“诞生日”。

在写下小说之后的次日早晨卡夫卡无法去公司上班并开始从事这一星期平淡乏味的公务活动，好像什么事也没发生似的。他在自己的名片上写了几句并不令人觉得可信的话，向自己的上司欧根·普福尔请假，这几句话读起来就像是对《判决》所做的第一篇短评：“今天清晨我有点儿头晕，还有点儿发烧。所以我待在家里。但是这肯定没什么事，我今天一定还会来上班，不过也许要在12点以后才来。”在标志着狂喜的身体上的振奋之后，危机便随之而来：一道运动轨迹，它在作者的心理状态中反映出小说主人公格奥尔格·本德曼的命运。由于身体上的和心理上的原因他不能随时都重做类似的夜间值勤：这一点卡夫卡是清楚的。持续不断的关联中的写作往往也意味着在一个超然于社会的空间中的无条件孤独的压力：身体上的和心理上的接受状况。1913年7月底他对决定他的文学写作的强制性禁锢做出说明：“我必须经常一人独处。我已经做的，仅仅是一种一人独处的成果。”

在写下小说的次日卡夫卡就在奥斯卡·鲍姆的寓所朗读《判决》。他获得的巨大反响向他证实了“小说的毋庸置疑”。一如常有的那样，对他来说朗诵会才是检验作品质量的关键。1912年12月4日晚上他应维利·哈斯的邀请，在两年前建立的赫德尔协会的一个布拉格作家晚会上，在文策尔广场旁边的青春艺术风格高级饭店“大公爵施特凡”的一座镜厅里给少量听众朗诵这篇小说。朗诵期间隔壁房间在举办室内音乐会，他这样向菲莉丝·鲍尔报告说，他没费什么力气稍稍提高一下嗓门就顶住了。他读到小说结尾时，情绪越来越激动，以致竟然压皱了一张印有一张菲莉丝的照片的风景明信片，他先前把手搁在这张明信片上了。最后，就像在鲍姆家第一次展示这件作品时那样，他眼里噙着泪水。这些泪水显示出一种基本情感，它一再决定卡夫卡与他的作品的关系。是那种自我享受，是他不无狂妄地因意识到自己可以在理想的情况下在文学创作上取得自己一直想取得的成就而被激发起来。这种解除一切阻塞，没有任何阻力的写作自《判决》起便比任何时候都更清晰地作为理想浮现在卡夫卡眼前。这使得他即便在对自己怒不可遏时也能控制住他的神秘自信。