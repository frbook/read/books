   

# 第九章　夜班文学写作(1912—1913)

## 心理分析的秘密

在开始和菲莉丝·鲍尔通信的时候，卡夫卡还是一个经常出席报告会的人。他对弗洛伊德的学说的研究也与这种情况有关联，这种研究并非源于大部头作品的研读，而是溯源于通过专题报告、座谈会和报刊文章所获得的信息。譬如1912—1913年间旧城环行路上的凡塔屋里就举办过多次以心理分析为题的活动。卡夫卡至少间或参加过这类活动（布罗德和韦尔奇在他们那篇与此同时发表的论文《观点和概念》中参考的弗洛伊德读物形成一种反射）。估计他也听了阿尔弗雷德·阿德勒的报告，此人1913年1月初在布拉格做了一个专题报告，详细介绍他的书《神经质的性格》。然而这时候他没有奋力研究弗洛伊德，因为他知道弗洛伊德的作品中有一个自成一体的理论体系，自卢浮宫圈的那些日子起这种体系便使他感到厌恶。1912年7月他从容博尔恩写信给维利·哈斯：“从弗洛伊德那儿人们能读到闻所未闻的东西，这个我信。可惜我对他了解少，对他的弟子们倒了解得多（……）。”

这里承认的间接“弟子”尤其是一种对期刊的明显的兴趣的结果。作为《新周报》的读者，卡夫卡是熟悉阐述弗洛伊德学说的文化、伦理和医学意义的有现实意义的论文的。譬如1910年他无疑读过南德精神病科医生维利·黑尔帕赫的论述心理分析的文章，该文用通俗易懂的表达方式对弗洛伊德的作品在治疗和文化上的重要意义作了全面评价。第二个源泉是《行动》，卡夫卡自己没订，却常常在咖啡馆里读它。在1913年这一年里奥托·格鲁斯，犯罪学专家汉斯·格鲁斯的儿子，在该刊物上发表了六篇论述心理分析问题的小论文：《克服文化危机》《路德维希·鲁宾纳尔的〈心理分析〉》《心理分析或我们门诊医生》《一般对个别的影响》《一种新伦理学述评》以及《关系论》。1917年卡夫卡才结识在柏林当精神病科医生的格鲁斯本人，当时格鲁斯征求卡夫卡对合作办一份他策划的心理分析期刊的意见。格鲁斯把弗洛伊德学说理解为对推翻一种权威的父权制的贡献，而且格鲁斯对弗洛伊德学说所做的具有对现存社会制度批判色彩的研究颇有其独特性，对这种独特性，卡夫卡，如同他将在1920年6月致米莱娜·波拉克信中所写的那样，至少是一一隐约捉摸到了的。

1913年6月中他对菲莉丝·鲍尔声称，他会“一点儿评价人，并与人感同身受”。考虑到这样一番表白对于他的往往是消极的自画像来说多么不寻常，这番表白就有了特殊的意义。从对待陌生体验的敏感性中势必产生出一种对心理分析的较强烈的兴趣，然而卡夫卡却把心理分析的治疗要求视为一种自我苛求的表现，这种自我苛求起因于单一因果的说明模式，却没有充分把握现代个体综合病理学。1920年11月在一封致米莱娜·波拉克的信中，卡夫卡认为，克服精神上的缺陷的意愿来源于一个“笨拙的错误”（他避而不谈，正是弗洛伊德对经久治愈神经症和精神病感染的前景持怀疑态度）。所有精神上的疾病说到底都是“处于困境中的人下锚泊在慈母般的土地上”，卡夫卡作如是说——通过1920年年底他记下一些笔记阐述出的一种假设，从而试图着重指出其意义。

卡夫卡把心理分析终生看作对理解现代精神的有才智的贡献，他曾从原则上研究过现代精神之领悟，虽然他并不确信它在医学上的成就。1917年10月他在曲劳记下：“心理分析是阅读一种倒写体，所以是很费劲的，并且就其永远正确的结果而言是富有成果的，可是确确实实什么事也没发生。作为诠释左右颠倒的下意识文字的注释学行为，对于卡夫卡来说心理认识具有物质的价值，然而它的治疗是否有成效却一直是一件没有把握的事。尼采就已经在《善与恶的彼岸》中表明了这样的观点：心理学已经“停留在道德偏见和担忧上”并且“没敢进入深处”。“从心理学中只产生出分析”，卡西米尔·埃德施米德在他的1918年刊印在《新周报》的纲领性短评《诗中的表现主义》中这样说。“仇恨对积极的自我观察。”卡夫卡的1913年12月9日的日记作如是说，并因此而遵循同样的对心理学研究方法的只是分析倡议的反感。四年后一则诠释日记试图提出一种理由从而对这一公式做了补充：“心灵观察者不能深入心灵之中，但是分明有一条边线，他就在这条边线上与心灵相交。这一相交使人认识到，心灵也对自己茫然无知。所以心灵必定是为人所不知的。”这样一份鉴定直指心理分析实践的核心，人的内心世界便是应该借助于这种实践经验受到探听，并且如福柯所说，被迫做出供认。按照这则曲劳日记的判断，心灵在一种疾病发病前的情况中所泄露的东西只是心灵陷入沉思而忘记周围一切的一种表现：如果它对自己“茫然无知”，那么就是分析家用他自己的理论充满空位。

就卡夫卡所接受的那部分而言，他把弗洛伊德的学说理解为阐明那个时代的纲领，并从而将其理解为一种承担着历史义务的注疏学体系。然而他是否较为缜密地注意到了战前时代的主要作品——《梦的解析》《性学三论》《论日常生活的精神病理学》《图腾与禁忌》，这可能还是个未知数。在1917年前的这几年里恰恰是他的专业知识不够，是这——尽管原则上怀着好奇——狐疑不决地塑造着他与心理分析的关系。对弗洛伊德的作品，1912年7月他这样对维利·哈斯说，他“只有一种高尚而空洞的敬意”。心理分析的威信——如果人们认真看待这一说法——对于卡夫卡来说产生自因不知情而形成的软弱无能。弗洛伊德本人从而也就升为安乐椅里的父亲形象，其影响作用仅仅是基于儿子们甘愿让身体和精神屈从家长制的法则。如果说1912年9月23日的日记表述说，小说《判决》写作时是“想着弗洛伊德”的，那么这并非就意味着，这件作品吸纳了心理分析的诠释成分。正是鉴于那封致哈斯的信在这之前两个月所使用的“高尚而空洞的敬意”这一用语，所以才不排除弗洛伊德的名字在这里作为那个父亲——世界的代号存在：在小说本身中，一如我们还要说明的那样，那个父亲世界达到了一种神话的程度。

在日记中除了前面已提到的谈《判决》的那段话以外，只有一处提到卡夫卡1912年7月在容博尔恩自然疗法疗养院和一位九年制高级中学教师进行的一次有关心理分析的谈话，可是没提及这位教师的具体头衔。估计1917—1918年冬季在曲劳休养度假期间，除阅读克尔凯郭尔之外他也从事弗洛伊德研究。在这一阶段他最后读完的，是汉斯·布吕厄尔对男性社会中性爱作用所做的学术上不知名的阐述，如同一个梦所透露的那样——此梦处理了对此书的分析。与弗洛伊德学说的关系一直带有双重性，这一点显示在他对弗兰茨·韦弗尔的剧本《沉默者》的批评之中，这一批评是他在1922年12月做出的。这则批评谈到韦弗尔的庸俗心理学长篇大论创造出来了这“三幕烂泥”，并最后说道：“与心理分析打交道不是什么愉快的事，我避之唯恐不及，但是它至少像这一代人那样存在着。犹太教几乎在创造出附属的拉席注释的同时也产生了它的痛苦和愉快，心理分析也是如此。”

在卡夫卡死后15年出版的弗洛伊德的晚期作品《摩西其人及一神论宗教》中，弗洛伊德众所周知地持有这一——已经在《图腾和禁忌》(1912—1913)中勾画的——观点：犹太教的弥赛亚信仰可比作人由于摆脱父亲而产生的罪责感。而卡夫卡却认为这种使宗教和心理学相聚的尝试——他通过阅读期刊了解这一尝试的纲领性要求——是一条错误的道路：“（……）所以心理分析也认为宗教的根本原因尽是些在它看来是个人的“疾病”的东西（……）。”卡夫卡不能把他个人向一种其根源在现代主义石头下面的信仰传统的靠拢解释为恢复父亲权威的心理行为。对他来说宗教意味着恢复近代个性的形成过程已经埋掉了的生命力，从而同时也意味着疏远赫尔曼·卡夫卡代表的那种现实感。宗教的经验可以表明母亲的土地——但是绝表明不了父亲的规章，卡夫卡正企盼着离开这父亲的家宅。

卡夫卡对心理分析的治疗要求和对为它奠定基础的“笨拙的错误”的批评中的一个重要的异议是针对弗洛伊德的。作为自然疗法的追随者，卡夫卡意识到，现代人的疾病也由具有典型文明特征的约束引起的。可是弗洛伊德的治疗方法只是在语言上有根据，却没有能力撤消这种束缚。心理分析虽然用治疗性谈话疏导不可用宣传教育排除的性欲，却从而也就使这种性欲受理念的力量和要求揭开它的神秘真实面目的权利的监管。分析家是一个说话算数的人，他通过语言的媒介管教病人，从而谋求对病人体格诊断的控制。这种情况在弗洛伊德的《癔病的研究》中表现得尤其明显，这种皈依癔病被视为一种现象，它会让身体显示在代表有待从语言上解决的精神上的冲突中出现的症状。

弗洛伊德开除肉体的教籍，他把它置于语言的法则之下。正是这种做法让卡夫卡觉得可疑，因为他认为这种做法象征地反映但没有克服文明缺陷的症状。精神疾病产生自一些个人的根深蒂固的关系，它们也决定了个人的身体素质：“但是这种有现实根基的固定下来的东西却并不是人的单个的可替换的所有物，而是在人的天性中预先形成的并在事后继续形成着他的本性（以及他的身体）。你要在这里治好什么病吗？”弗洛伊德的学说不是下降到迷宫之中并废除在语言的那一边的禁令，而是仍然沉溺于语言。它并不向前推进至精神疾病的更深的层面，因为它想治疗本身就是一种综合症状学的一部分的东西。心理分析如同哈罗德·布鲁姆所说的，向现代精神指明心灵的地图，可是卡夫卡将用自己的作品说明心理分析知识丝毫无助于拯救我们。