   

# 感谢

每一个卡夫卡研究者都要面对一大堆旧有的论著，这些著作激励他，但是有时也会因其浩如烟海而望而却步。对卡夫卡的作品的学术研究今天可以依据比20世纪70年代和80年代更好的先决条件。汉斯—格尔德·科赫(Hans-GerdKoch)领导的乌珀塔尔研究所正在拟订一个第一次符合现代标准的卡夫卡版本出版计划。人们可以怀着内心的激动期待尚且是未完成的书信版本编纂的完成。使用者不会把在罗兰德·罗伊斯(Roland Reuβ)和彼德·施滕勒(Peter Staengle)领导下，在手抄本基础上产生的施特勒姆费尔德出版社的历史评注版看作与同类企业进行的竞争，而是看作令人高兴的补充。它——也通过其真迹复制——提供了这样的可能性：比以前任何时候都更精确地阐明卡夫卡作品的渊源及其文学结构方面的个人特色。

克劳斯·瓦根巴赫(Klaus Wagenbach)撰写了历时数十年之久一直是权威性的青年时代卡夫卡传，它直至今日一直为研究卡夫卡的受教育史以及这种教育的环境特定条件提供重要的基础。哈特穆特·宾德尔(Hartmut Binder)的百科全书式的论著——尤其是他的卡夫卡述评、卡夫卡手册以及布拉格文化生活论著——极大地丰富了我们对这位作家的知识。汉斯·齐施勒(Hanns Zischler)在将近10年前给卡夫卡的形象添上了一层新的色彩，他在精确的来源追述的基础上呈现出这样一位电影观众：此人的文学创作受到这一新传媒的幻觉效应的极大推动。最近20年，在无数的研究论著中，代特莱夫·克雷默尔(Detlef Kremer)、盖哈尔德·库尔茨(Gerhard Kurz)、盖哈尔德·诺伊曼(Gerhard Neumann)和里特希·罗伯特松(Ritchie Robertson)的著作使我本人受益匪浅，因为它们在卡夫卡的时代关联及其文化特色层面上研究他，却与此同时没有使他失去他的个人特色。

我要感谢我的波鸿的以及我后来的维尔茨堡教席的同事们，他们进行组织协调、调查研究、校对更改，支持了这一多年的研究项目。（以下略去众多人名）

柏林和维尔茨堡，2005年4月