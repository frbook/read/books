   

# 人名译名索引

Adler, Alfred 阿尔弗雷德·阿德勒

Adler, Friedrich 弗里德里希·阿德勒

Adorno, Theodor Wiesengrund 特奥多尔·维森格伦德·阿多诺

Agamben, Giorgio 乔治·阿甘本

Alexander der Große 亚历山大大帝

Altenberg, Peter 彼得·阿尔滕贝格

Ambrožorá, Jarmila, s. Reinerová, Jarmila 娅尔米拉·安布罗若娃, 即娅尔米拉·赖内罗娃

Arleth, Emil 埃米尔·阿尔雷特

Arlosoroff, Chaim 哈伊姆·阿洛索罗夫

Asch, Scholem 朔莱姆·阿施

Assmann, Jan 扬·阿斯曼

Aurel, Marc 马克·奥雷尔

Avenarius, Richard 里夏德·阿芬纳里乌斯

Baader, Franz Xaver von 弗兰茨·克萨韦尔·冯·巴德尔

Bab, Julius 尤利乌斯·巴勃

Bachofen, Johan Jacob 约翰·雅各布·巴赫奥芬

Badeni, Kasimir 卡西米尔·巴代尼

Baeck, Leo 莱奥·贝克

Bailly, Louise 路易丝·贝吕

Baioni, Giuliano 朱利亚诺·巴约尼

Balzac, Honoré de 奥诺雷·德·巴尔扎克

Barthes, Roland 罗兰德·巴尔泰斯

Bašik, František Xaver 弗朗齐歇克·克萨韦尔·巴舍克

Bassermann, Albert 阿尔伯特·巴瑟尔曼

Basserwitz, Grafen Gerdt von 格拉夫·格尔特·冯·巴塞尔维茨

Bataille Georges 乔治·巴塔耶

Batka, Richard 里夏德·巴特卡

Baudelaire, Charles 夏尔·波德莱尔

Bauer, Anna 安娜·鲍尔

Bauer, Carl 卡尔·鲍尔

Bauer, Elisabeth, s. Braun, Elisabeth 伊丽莎白·鲍尔, 见伊丽莎白·布劳恩

Bauer, Erna 艾尔娜·鲍尔

Bauer, Felice 菲莉丝·鲍尔

Bauer, Ferdinand 费迪南德·鲍尔

Bauer, Toni 托妮·鲍尔

Baum, Oskar 奥斯卡·鲍姆

Bäuml, Max 马克斯·博伊姆尔

Becher, Johannes Robert 约翰内斯·罗伯特·贝歇尔

Beck, Matthias 马蒂亚斯·贝克

Beck, Oscar 奥斯卡尔·贝克

Beckett, Samuel 塞缪尔·贝克特

Beer-Hofmann, Richard 里查德·贝尔—霍夫曼

Beilhack Hans 汉斯·拜尔哈克

Beneš, Edvard 爱德华德·贝内斯

Benjamin, Walter 瓦尔特, 本雅明

Benn, Gottfried 戈特弗里德·本

Ben-Tovim, Puah 普阿·本—托维姆

Bergmann, Arthur 阿图尔·贝格曼

Bergmann, Else 埃尔泽·贝格曼

Bergmann, Hugo 胡戈·贝格曼

Bergson, Henri 亨利·贝格松

Bie, Oskar 奥斯卡·比

Bin-Gorion, Micha Josef 米夏·约瑟夫·宾—戈里昂

Bizet, Georges 乔治·比才

Blanchot, Maurice 莫里斯·布朗绍

Blaß, Ernst 恩斯特·布拉斯

Blei, Franz 弗兰茨·布莱

Bloch, Ernst 恩斯特·布洛赫

Bloch, Hans 汉斯·布洛赫

Bloch, Joseph 约瑟夫·布洛赫

Bloch, Louis 路易·布洛赫

Bloch, Margarethe(Grete)玛加蕾特（格蕾特）·布洛赫

Bloom, Harold 哈罗德·布鲁姆

Blüher, Hans 汉斯·布吕厄尔

Blumenberg, Hans 汉斯·布鲁门贝格

Blumenfeld, Kurt 库尔特·布鲁门费尔德

Böcklin, Arnold 阿诺尔德·伯克林

Böhm, Adolf 阿道尔夫·伯姆

Böhme, Jakob 雅各布·伯梅

Bohrer, Karl Heinz 卡尔·海因茨·博勒尔

Bösche, Wilhelm 威廉·伯尔舍

Börne, Ludwig 路德维希·伯尔纳

Bourget, Paul 保尔·博尔盖特

Brahm, Otto 奥托·勃拉姆

Brand, Karl(d. i. Karl Müller)卡尔·勃兰德（即卡尔·米勒）

Braun, Bernat 贝尔纳特·布劳恩

Braun, Lily 莉吕·布劳恩

Brecht, Bertolt 贝托尔德·布莱希特

Brentano, Clemens 克莱门斯·布伦塔诺

Brentano, Franzvon 弗兰茨·冯·布伦塔诺

Brenner, Josef Chajim 约瑟夫·夏吉姆·布兰纳

Brešnovsky, Václav 瓦茨拉夫·布雷斯诺夫斯基

Broch, Hermann 赫尔曼·布罗赫

Brod, Adolf 阿道尔夫·布罗德

Brod, Elsa 艾尔莎·布罗德

Brod, Max 马克斯·布罗德

Brod, Otto 奥托·布罗德

Brod, sophie 索菲·布罗德

Bronnen, Amolt 阿诺尔特·布洛嫩

Bruckner, Ferdinand, s. Tagger, Theodor 费迪南德·布鲁克纳, 即台奥多尔·塔格尔

Buber, Martin 马丁·布贝尔

Bugsch, Irene 伊雷妮·布格施

Bürger, Gottfried August 戈特弗里德·奥古斯特·比格尔

Busse, Carl 卡尔·博塞

Byron, George Gordon Noel, Lord 乔治·戈登·拜伦

Camus, Albert 阿尔伯特·卡莫斯

Canetti, Elias 艾里亚斯·卡奈蒂

Carco, Francis 弗兰西斯·卡尔科

Carus, Carl Gustav 卡尔·古斯塔夫·卡鲁斯

Cassirer, Ernst 恩斯特·卡西勒尔

Cassirer, Paul 保尔·卡西勒尔

Celan, Paul 保尔·策赖

Chain, Emst Boris 恩斯特·博里斯·夏恩

Chamberlain, Houston Stewart 休斯顿·斯图尔特·张伯伦

Chaplin, Charlie Spencer 查理·卓别林

Claudius, Matthias 马蒂亚斯·克劳迪乌斯

Cohen, Hermann 赫尔曼·科恩

Courbet, Gustave 古斯塔维·库尔贝

Čuchalová, Anna 安娜·丘哈洛娃

Danziger, Ema 艾尔娜·丹齐格

Darwin, Charles 查尔斯·达尔文

Daumier, Honoré 胡诺雷·道米尔

Dauthendey, Max 马克斯·道滕代

Darid, Jacques-Louis 雅克维斯—路易·大卫

David, Josef 约瑟夫·大卫

Degas, Edgar 埃德加·德加

Dehmel, Richard 里夏德·德默尔

Deleuze, Gilles 吉勒斯·德洛伊策

Derrida, Jacques 雅克·德里达

Detter, Ferdinand 费迪南德·德特尔

Diamant, Dora 多拉·迪阿曼特

Dickens, Charles 查尔斯·狄更斯

Dilthey, Wilhelm 威廉·迪尔泰

Döblin, Alfred 阿尔弗雷德·德布林

Dostojevskij, Fjodor Michailowitsch 费奥多尔·米哈伊洛维奇·陀思妥耶夫斯基

Dreyfus, Alfred 阿尔弗雷德·德雷福斯

Du Bois-Reymond, Emil 埃米尔·杜·鲍伊斯—雷伊蒙德

Dühring, Eugen 欧根·迪林

Eckhart, gen. Meister Eckhart 迈斯特尔·埃克哈尔特

Edison, Thomas Alva 托马斯·阿尔瓦·爱迪生

Edschmid, Kasimir 卡西米尔·埃德施米德

Eduardowa, Jewgenja 叶夫根耶·艾杜阿多娃

Ehrenfels, Christian von 克里斯蒂安·冯·艾伦费尔斯

Ehrenstein, Albert 阿尔伯特·埃伦施泰因

Ehrenstein, Carl 卡尔·埃伦施泰因

Einstein, Albert 阿尔伯特·爰因斯坦

Einstein, Carl 卡尔·爰因斯坦

Eisler, Norbert 诺贝特·艾斯勒

Eisner, Ernst 恩斯特·艾斯讷

Eisner, Minze 明策·艾斯讷

Eißfeldt, Otto 奥托·艾斯费尔特

Eliasberg, Alexander 亚历山大·艾里亚斯贝格

Emrich, Wilhelm 威廉·埃姆里希

Ernst, Paul 保尔·恩斯特

Eucken, Rudolf 鲁道夫·欧肯

Ewers, Harhns Heinz 汉斯·海因茨·艾韦尔斯

Eysoldt, Gertrud 格特鲁德·艾索尔特

Falke, Gustav 古斯塔夫·法尔克

Fanta, Else, s. Bergmann, Else 埃尔泽·凡塔, 见埃尔泽·贝格曼

Fechner, Gustav Theodor 古斯塔夫·特奥多尔·费希纳尔

Fehling, Jürgen 于尔根·费林

Feigl, Friedrich 弗里德里希·法伊格尔

Feigl, Karl 卡尔·法伊格尔

Feinmann, Sigmuncl 西格蒙德·法伊恩曼

Fichte, Johann Gottlieb 约翰·戈特利布·费希特

Fiebig, Paul 保尔·菲比希

Fischer, Samuel 萨穆埃尔·费舍尔

Flake, Otto 奥托·弗拉克

Flaubert, Gustave 居斯塔夫·福楼拜

Fleischmann, Siegmund 西格蒙德·弗莱施曼

Fleming, Alexander 亚历山大·弗雷明

Florey, Howard 霍华德·弗洛赖

Foerster, Friedrich Wilhelm 弗里德里希·威廉·弗尔斯特

Fontane, Theodor 特奥多尔·冯塔诺

Forchheimer, Otto 奥托·福尔希海默

Foucault, Michel 米歇尔·福柯

Frankl, Otto 奥托·弗兰克尔

Freud, Sigmund 西格蒙德·弗洛伊德

Friedlaender, Salomo 萨洛莫·弗里德伦德尔

Friedländer, Moriz 莫里茨·弗里德兰德勒

Friedmann, Max 马克斯·弗里德曼

Friedrich, Paul 保尔·弗里德里希

Frisch, Efraim 埃富雷姆·弗里施

Fromer, Jakob 雅各布·弗罗默尔

Fromm, Erich 埃里希·弗洛姆

Fuchs, Eugen 欧根·福克斯

Fuchs, Rudolf 鲁道夫·福克斯

Geiger, Abraham 亚伯拉罕·盖格尔

George, Stefan 斯蒂芬·格奥尔格

Gerstenberg, Heinrich Wilhelm von 海因里希·威廉·冯·格斯登贝格

Gibian, Camill 卡米尔·吉比安

Girbert, Jean 让·吉尔贝特

Glaube(Dentist)格劳伯尔（牙医）

Goethe, Johann Wolfgang von 约翰·沃尔夫冈·封·歌德

Gogol, Nikolaj Wassiljewitsch 尼古拉·瓦西里耶维奇·果戈理

Goldfaden, Abraham 亚伯拉罕姆·戈尔德法登

Goldschmiedt, Guido 吉多·戈尔德施米特

Goltz, Hans 汉斯·戈尔茨

Gomperz, Heinrich 海因里希·戈姆佩尔茨

Gordin, Jakob 雅各布·戈尔丁

Gordon, Aharon David 阿哈龙·大卫·戈尔东

Gorki, Maxim 马克西姆·高尔基

Gottsched, Hermann 赫尔曼·戈特谢特

Gottwald, Adolf 阿道尔夫·戈特瓦尔特

Grabbe, Christian Dietrich 克里斯蒂安·迪特里希·格拉贝

Graetz, Heinrich 海因里希·格雷茨

Greco, El(d. i. Domenico Theotocopuli)艾尔·格雷科

Gregori, Ferdinand 费迪南德·格雷戈里

Grillparzer, Franz 弗兰茨·格里尔帕策

Groß, Hans 汉斯·格鲁斯

Groß, Otto 奥托·格罗斯

Grün, Nathan 纳旦·格林

Grünberg, Abraham 亚伯拉罕·格林贝格

Gschwind, Emil 埃米尔·克施温特

Guattari, Félix 费利克斯·古阿塔里

Gütling, Alois 阿洛伊斯·居特林

Haas, Willy 维利·哈斯

Hadwiger, Victor 维克托, 哈德维格尔

Haeckel, Ernst 恩斯特·海克尔

Hajek, Markus 马尔库斯·哈耶克

Hamsun, Knut 克努特·哈姆松

Handl, Willi 维利·汉德尔

Hanish, Otto 奥托·哈尼施

Hardt, Ludwig 路德维希·哈尔特

Hasenclever, Walter 瓦尔特, 哈森克勒弗尔

Hašek, Jaroslav 雅洛斯拉夫·哈赛克

Hauptmann, Carl 卡尔·霍普特曼

Hauptmann, Gerhart 格哈德·霍普特曼

Hebbel, Friedrich 弗里德里希·黑贝尔

Hebel, Johann Peter 约翰·彼得·黑贝尔

Hecht, Hugo 胡戈·黑希特

Hegel, Georg Wilhelm Friedrich 格奥尔格·威廉·弗里德里希·黑格尔

Heidegger, Martin 马丁·海德格尔

Heilborn, Lydia 吕迪娅·海尔博恩

Heilmann, Hans 汉斯·海尔曼

Heimann, Moritz 莫里茨·海曼

Heindl, Robert 罗伯特·海因德尔

Heine, Heinrich 海因里希·海涅

Heine, Thomas Theodor 托马斯·特奥多尔·海涅

Heiler, Julie, S. Löwy, Julie 尤丽叶·黑勒, 见尤丽叶·勒维

Hellpach, Willy 维利·黑尔帕赫

Helmholtz, Hermann 黑尔曼·赫尔姆霍尔茨

Hermann, Felix 费利克斯·赫尔曼

Hermann, Gerti 格尔蒂·赫尔曼

Hermann, Hanna 汉娜·赫尔曼

Hermann, Karl 卡尔·赫尔曼

Hermann, Otto 奥托·赫尔曼

Hermann, Paul 保尔·赫尔曼

Hermann, Leo 莱奥·赫尔曼

Herzl, Theodor 西奥多·赫茨尔

Hesse, Hermann 赫尔曼·黑塞

Hessel, Franz 弗兰茨·黑塞尔

Heym, Georg 格奥尔格·海姆

Hiller, Kurt 库尔特·希勒

Hitler, Adolf 阿道夫·希特勒

Hoddis, Jakob van 雅各布·范·霍迪斯

Hoeßlin, Julius Konstantin von 尤利乌斯·康斯坦丁·冯·黑斯林

Hofer, Carl 卡尔·霍弗尔

Hofmann, Ludwig von 路德维希·冯·霍夫曼

Hofmannsthal, Hugo von 胡戈·冯·霍夫曼斯塔尔

Hoffmann, Ernst Theodor Amadeus 恩斯特·特奥多尔·阿马多斯·霍夫曼

Hoffmann, Nina 尼娜·霍夫曼

Hölderlin, Friedrich 弗里德里希·赫尔德林

Holitscher, Arthur 阿图尔·霍里切尔

Hohmann, Georg 格奥尔格·霍尔曼

Horkheimer, Max 马克斯·霍克海默

Huch, Friedrich 弗里德里希·胡赫

Huch, Ricarda 里卡尔达·霍赫

Husserl, Edmund 埃德蒙德·胡塞尔

Huysmans, Joris-Karl 约里斯—卡尔·于斯曼斯

Ibsen, Henrik 亨利克·易卜生

Illový, Rudolf 鲁道尔夫·伊洛维

Immermann, Karl Leberecht 卡尔·莱普莱希特·伊默尔曼

Jacobsen, Jens Peter 延斯·彼得·雅各布森

Jacques, Norbert 诺贝特·雅克维斯

Janáček, Leoš 莱奥·约纳赛克

Janouch, Gustav 古斯塔夫·耶诺赫

Janowitz, Franz 弗兰茨·耶诺维茨

Jeiteles, Isidor 伊西多尔·耶泰莱斯

Jesenská, Milena, s. Poliak, Milena 米莱娜·耶森斯卡

Jesenský, Jan 扬·耶森斯卡

Jessner, Leopold 莱奥波尔德·耶斯纳尔

Juncker, Axel 阿克塞尔·容克

Jung, Carl Gustav 卡尔·古斯塔夫·荣格

Jung, Franz 弗兰茨·容格

Jünger, Ernst 恩斯特·荣格尔

Just, Adolf 阿道尔夫·尤斯特

Just, Rudolf 鲁道尔夫·尤斯特

Kafka, Angelus 安格卢斯·卡夫卡

Kafka, Anna 安娜·卡夫卡

Kafka, Bruno 布鲁诺·卡夫卡

Kafka, Emil 埃米尔·卡夫卡

Kafka, Ernst 恩斯特·卡夫卡

Kafka, Filip 菲利普·卡夫卡

Kafka, Franziska 弗兰齐丝卡·卡夫卡

Kafka, Gabriele(Elli)加布里勒（艾丽）·卡夫卡

Kafka, Georg 格奥尔格·卡夫卡

Kafka, Heinrich 海因里希·卡夫卡

Kafka, Heinrich(Bruder des Vaters)海因里希·卡夫卡（父亲的兄弟）

Kafka, Hermann 赫尔曼·卡夫卡

Kafka, Irma 伊尔玛·卡夫卡

Kafka, Jakob 雅各布·卡夫卡

Kafka, Julie 尤丽叶·卡夫卡

Kafka, Julie(Schwester des Vaters)尤丽叶·卡夫卡（父亲的妹妹）l0

Kafka, Karoline 卡洛丽内·卡夫卡

Kafka, Ludwig 路德维希·卡夫卡

Kaika, Moritz 莫里茨·卡夫卡

Kafka, Oskar 奥斯卡·卡夫卡

Kaika, Ottilie(Ottla)奥蒂莉厄（奥特拉）·卡夫卡

Kafka, Otto 奥托·卡夫卡

Kafka, Robert 罗伯特·卡夫卡

Kafka, Samuel 萨穆埃尔·卡夫卡

Kafka, Valerie(Valli)瓦莱丽（瓦丽）·卡夫卡

Kaidanover, Zwi Hirsch 茨维·希尔施·凯达诺弗

Kaiser, Georg 格奥尔格·凯泽

Kaiser, Julie 尤丽叶·凯泽

Kammer, Klaus 克劳斯·卡默尔

Kanitz, Gertrud 格特鲁德·卡尼茨

Kant, Immanuel 伊曼努尔·康德

Kantorowicz, Ernst H. 恩斯特·H·康托罗维茨

Kantorowicz, Gertrud 格特鲁德·康托罗维茨

Kars, Georg 格奥尔格·卡尔斯

Kassner, Rudolf 鲁道夫·卡斯纳尔

Kayser, Rudolf 鲁道夫·卡伊泽

Kaznelson, Siegmund 西格蒙德·卡茨纳尔松

Keller, Gottfried 戈特弗里德·凯勒

Kellermann, Bernhard 伯恩哈德·克勒曼

Kerr, Alfred 阿尔弗雷德·凯尔

Keyserling, Eduard von 爱德华·冯·凯泽林

Khol, František 弗兰蒂塞克·科尔

Kiekegaard, Sören 索伦·克尔凯郭尔

Kießlich, Anton 安东·基斯利希

Kirchner, Margarethe 玛加蕾特·基希讷

Kisch, Egon Erwin 埃贡·埃尔温·基施

Kisch, Paul 保尔·基施

Kittler, Wolf 沃尔夫·基特勒尔

Klabund(d. i. Alfred Henschke)克拉邦德（即阿尔弗雷德·亨施克）

Kleist, Heinrich von 海因里希·冯·克莱斯特

Klopstock, Robert 罗伯特·克洛普施托克

Klug, Flora 弗洛拉·克卢克

Kneipp, Sebastian 塞巴斯蒂安·克奈普

Knöpfelmacher, Salomon 萨洛蒙·克讷普夫马赫

Koch, Ludwig von 路德维希, 冯·科赫

Koch, Hans-Gerd 汉斯—格尔德·科赫

Kodym, Odolen 奥多莱恩·科迪姆

Kohn, Selma 塞尔玛·科恩

Kokoschka, Oskar 奥斯卡尔, 科科施卡

Kolb, Annette 安内特·科尔布

Kölwel, Gottfried 戈特弗里德·柯韦尔

Körner, Josef 约瑟夫·克尔讷

Körner, Theodor 特奥多尔·克尔纳

Kornfeld, Paul 保尔·科恩费尔德

Köster, Albert 阿尔伯特·克斯特尔

Kracauer, Siegfried 西格弗里德·克拉考尔

Krafft-Ebing, Richard von 里夏德·冯·克拉夫特—艾宾

Kramář, Karel 卡雷尔·克拉马尔

Krasnoplski, Horaz 霍拉茨·克拉斯诺波尔斯基

Kraus, Karl 卡尔·克劳斯

Kraus, Oskar 奥斯卡·克劳斯

Kretschmer, Ernst 恩斯特·克雷奇默尔

Kubin, Alfred 阿尔弗雷德·库宾

Kuh, Anton 安东·库

Kuh, Marianne 玛丽安妮·库

Kusmin, Michail 米夏埃尔·库斯明

Lacan, Jacques 雅克·拉康

Laforgue, Jules 朱尔·拉福格

Lagerlöf, Selma 塞尔玛·拉格勒夫

Lammasch, Heinrich 海因里希·拉马施

Landauer, Gustav 古斯塔夫·朗道尔

Lang, Fritz 弗里茨·朗

Langbehn, Julius 尤利乌斯·朗贝恩

Langer, František 弗兰蒂塞克·朗格

Langer, Georg(Jiři)格奥尔格（吉里）·朗格

Lasker-Schüler, Else 埃尔泽·拉斯克尔—许勒

Laßwitz, Kurd 库尔特·拉斯维茨

Lateiner, Josef 约瑟夫·拉泰纳尔

Laube, Heinrich 海因里希·劳贝

Lautensack, Heinrich 海因里希·劳滕萨克

Lehmann, Siegfried 西格弗里德·莱曼

Leitenberger, Friedrich 弗里德里希·莱滕贝格

Leonhard, Rudolf 鲁道尔夫·莱昂哈尔德

Leppin, Paul 保尔·莱平

Lilien, EphraimMoses 埃夫莱姆·莫泽斯·利利恩

Liliencron, Detlevvon 德特勒夫·冯·利利恩克龙

Lindau, Paid 保尔·林道

Lindemann, Gustav 古斯塔夫·林德曼

Lindström, Carl 卡尔·林特施特洛姆

Lippich, Ferdinand 费迪南德·利皮希

Liszt, Franz 弗兰茨·李斯特

Loerke, Oskar 奥斯卡·勒尔克

Loos, Adolf 阿道尔夫·罗斯

Löw, Rabbi(d. i. Juda Loew ben Bezalel)拉比·勒夫

Löwenstein, Thea 特亚·勒文施泰因

Löwenthal, Leo 莱奥·勒文塔尔

Löwy, Alfred 阿尔弗雷德·勒维

Löwy, Bedřich 贝德里希·勒维

Löwy, Esther 埃斯特尔·勒维

Löwy, Jakob 雅各布·勒维

Löwy, Jizchak 吉茨夏克·勒维

Löwy, Josef 约瑟夫·勒维

Löwy, Julie, s. Kafka, Julie 尤丽叶·勒维, 见尤丽叶·卡夫卡

Löwy, Martha 玛尔塔·勒维

Löwy, Richard 里夏德·勒维

Löwy, Richard(Anwalt)里夏德·勒维（律师）

Löwy, Rudolf 鲁道夫·勒维

Löwy, Siegfried 西格弗里德·勒维

Lueger, Kad 卡尔·吕格尔

Luhmann, Niklas 尼克拉斯·卢曼

Lukács, Georg 格奥尔格·卢卡奇

Lumière, Louis Jean 路易·让·吕米埃

Mach, Ernst 恩斯特·马赫

Maeterlinck, Maurice 莫里斯·梅特林克

Mahler, Alma 阿尔玛·马勒尔

Makart, Hans 汉斯·马克尔特

Mallarmé, Stéphane 施特凡·马拉美

Man, Paul de 保尔·德·曼

Mandelstam, Max 马克斯·曼德尔施塔姆

Mandeville, Bernard de 本纳德·德·曼德维勒

Mann, Heinrich 海因里希·曼

Mann, Katia 卡蒂娅·曼

Mann, Thomas 托马斯·曼 69, 75, 80, 87, 136, 140, 141, 148, 149, 162, 165, 197,

Márai, Sándor 桑多尔·马兰

Marbot, Marcellin de 马塞兰·德·马尔博

Mardersteig, Hans 汉斯·马尔德施泰克

Mareš, Michal 米夏尔·马雷

Marinetti, Filippo Tommaso 菲利波·马里内蒂

Marr, Wilhelm 威廉·马尔

Marschner, Robert 罗伯特·马施纳尔

Marty, Anton 安东·马尔蒂

Marx, Karl 卡尔·马克思

Masaryk, Tomáš Garrigue 托马斯·马萨吕克

Mauthner, Fritz 弗兰茨·莫尔纳尔

Meißner, Alfred 阿尔弗雷德·迈斯讷

Mercier, Louis-Sébastian 路易—塞巴斯蒂安·梅尔西尔

Merx Adelbert 阿德尔贝特·梅尔克斯

Meyer, Georg Heinrich 格奥尔格·海因里希·迈尔

Meyrink, Gustav 古斯塔夫·迈林克

Mierendorff, Carlo 卡罗·米伦多尔夫

Mikolaschek, Karl 卡尔·米科拉舍克

Mirbeau, Octave 奥克塔夫·米拉博

Moeller van den Brnck, Arthur 阿图尔·默勒·冯·登·布洛克

Moissi, Alexander 亚历山大·墨西

Mondt, Eugen 欧根·蒙特

Mörike, Eduard269, 288 爱德华·默里克

Morus, Thomas 托马斯·莫洛斯

Mühlstein, Gustav 古斯塔夫·米尔施泰因

Müller, Heiner 海纳尔·米勒

Müller, Jens Peder 延斯·佩德尔·米勒

Mülller, Robert 罗伯特·米勒

Müller-Seidel, Walter 瓦尔特·米勒—赛特尔

Musset, Alfred de 阿尔弗雷德·缪塞

Musil, Robert 罗伯特·穆齐尔

Nadler, Josef 约瑟夫·纳德勒

Nedvědovǎ, Františka 弗兰蒂斯卡·内德韦多瓦

Němcová, Božena 博采娜·内姆科瓦

Nerval, Gěrard de 基拉德·德·奈瓦尔

Nettel, Käthe 克特·内特尔

Netuka, Karel 卡雷尔·内图卡

Neumann, Angelo 安格洛·诺伊曼

Neumann, Gerhard 盖哈尔德·诺伊曼

Neumann, Wilhelm 威廉·诺伊曼

Nielsen, Carl 卡尔·尼尔森

Niethammer, Friedrich Immanuel 弗里德里希·伊马努埃尔·尼特哈默尔

Nietsche, Friedrich 弗里德里希·尼采

Nikolaus vom Kues 尼科劳斯·冯·库艾斯

Novalis(d. i. Friedrich von Hardenberg)（弗里德里希·冯·哈登贝格）诺瓦利斯

Nowack, Wilhelm 威廉·诺瓦克

Nowak, Willy 维利·诺瓦克

Odstrčil, Bedřich 贝德里希·奥德施特里尔

Odys, Gusti 古斯蒂·奥迪斯

Offenbach, Jacques 雅克·奥芬巴赫

Pabst, Georg Wilhelm 格奥尔格·威廉·帕泼斯特

Pachinger, Anton Max 安东·马克斯·帕兴格尔

Palacký, František 弗兰蒂塞克·帕拉屈

Palucca, Gret 格勒特·帕洛卡

Parmenides 帕尔梅尼德斯

Pascal, Blaise 布莱泽·帕斯卡尔

Pascheles, Wolf 沃尔夫·帕谢莱斯

Pasley, Malcolm 马尔科尔姆·帕斯莱伊

Perez, Jizchak Leib 吉茨夏克·莱布·佩莱茨

Perugia, Vicenzo 维森楚·佩鲁吉阿

Perutz, Leo 莱奥·佩鲁茨

Pfaff, Ivo 伊福·普法夫

Pfemfert, Franz 弗兰茨·普费姆费尔特

Pfersche, Emil 埃米尔·普费尔歇

Pfohl, Eugen 欧根·普福尔

Pircasso, Pablo 毕加索

Pick, Gottfried 戈特弗里德·皮克

Pick, Otto 奥托·皮克

Pinès, Meyer Isses 迈耶尔·伊塞斯·平内斯

Pinthus, Kurt 库尔特·平图斯

Piscator, Erwin 埃尔温·皮斯卡托尔

Platon 柏拉图

Poe, Egar Allan 埃加尔·阿兰·波埃

Pollak, Ernst 恩斯特·波拉克

Pollak, Isidor 伊西多尔·波拉克

Pollak, Josef 约瑟夫·波拉克

Pollak, Lotte 洛特·波拉克

Pollak, Marianne 玛丽安妮·波拉克

Pollak, Milena 米莱娜·波拉克

Pollak, Oskar 奥斯卡·波拉克

Pollak, Wilhelm 威廉·波拉克

Popper, Josef 约瑟夫·波珀尔

Popper, Moritz 莫里茨·波珀尔

Porias, Adam 亚当·波里亚斯

Porias, Esther, s. Löwy, Esther 埃斯特尔·波里亚斯, 见埃斯特尔·勒维

Porias, Nathan 纳旦·波里亚斯

Porias, Sarah 萨拉·波里亚斯

Pouzarová, Anna 安娜·波察洛娃

Přibram, Ewald Felix 爱华德·弗利克斯·普里布拉姆

Přibram, Otto 奥托·普里布拉姆

Prochá zková, Staša 施塔萨·普鲁夏茨科娃

Proust, Marcel 马塞尔·普鲁斯特

Pulver, Max 马克斯·普尔韦尔

Raabe, Wilhelm 威廉·拉贝

Radiouet, Raymond 拉伊蒙德·拉迪古艾特

Rath, Moses 莫泽斯·拉特

Rathenau, Walther 瓦尔特·拉特瑙

Rauchberg, Heinrich 海因里希·劳赫贝格

Reiner, Josef 约瑟夫·莱纳尔

Reinerová, Jarmila 娅尔米拉·赖内罗娃

Reinhardt, Max 马克斯·赖因哈德

Reiß, Erich 埃里希·莱斯

Reiß, Esther 艾斯特尔·莱斯

Reiß, Fanny 凡妮·莱斯

Reiß, Tilka 蒂尔卡·莱斯

Richepin, Jean 让·吕歇平

Richter, Moses 莫泽斯·里希特

Ricoeur, Paul 保尔·里克尔

Rieger, František Ladislav 弗兰蒂塞克·拉迪斯拉夫·里格尔

Rilke, Rainer Maria 赖讷·玛丽亚·里尔克

Robitschek, Max 马克斯·鲁比切克

Rosenzweig, Franz 弗兰茨·罗森茨威格

Roskoff, Gustav 古斯塔夫·鲁斯科夫

Rössier, Tile 蒂勒·勒斯莱尔

Roth, Joseph 约瑟夫·罗特

Rowohlt, Ernst 恩斯特·罗沃尔特

Sacher-Masoch, Leopold von 莱奥波德·冯·萨赫尔—马佐赫

Salus, Hugo 胡戈·萨鲁斯

Sallust 萨洛斯特

Salter, Julius Berthold 尤利乌斯·贝特霍尔德·萨尔特

Salveter, Emmy 艾米·萨尔韦特尔

Sanzara, Rahel(d. i. Johanna Bleschke)拉海尔·桑察拉（即约翰娜·布莱施克）

Sauer, August 奥古斯特·绍尔

Scharkansky, Abraham 亚伯拉罕·沙尔康斯基

Scheerbart, Paul 保尔·舍尔巴特

Schickele, Rneé 勒内·席克勒

Schiele, Friedrich Michael 弗里德里希·米夏埃尔·席勒

Schiller, Friedrich 弗里德里希·席勒

Schlaf, Johannes 约翰内斯·施拉夫

Schnabel, Margarete, s. Baum, Margarete 玛加蕾特·施纳贝尔, 即玛加蕾特·鲍姆

Schneider, Lambert 拉姆贝特·施奈特尔

Schnitzer, Moriz 莫里茨·施尼茨勒

Schnitzler, Arthur 阿图尔, 施尼茨勒

Schocken, Wolfgang Alexander 沃尔夫冈·亚历山大·绍肯

Schocken, Salman 萨尔曼·绍肯

Schoeps, Hans-Joachim 汉斯·约阿希姆, 舍普斯

Scholem, Gershom 格斯霍姆·绍莱姆

Schottlaender, Rudolf 鲁道夫·绍特兰德尔

Schreiber, Adolf 阿道夫·施莱贝尔

Schrenck-Notzing, Albert von 阿尔伯特·冯·施伦克—诺青

Schröder, Rudolf Alexander 鲁道夫·亚历山大·施罗德

Schopenhauer, Arthm 阿图尔·叔本华

Schultze-Naumburg, Paul 保尔·舒尔策—瑙姆贝格

Schuster, Heinrich Maria 海因里希·玛丽亚·舒斯特尔

Schwabach, Ernst 恩斯特·施瓦巴赫

Sedlacek, Jaroslav 耶罗斯拉夫·赛德拉采克

Seelig, Carl 卡尔·塞利希

Seume, Johann Gottfried 约翰·戈特弗里德·绍伊默

Seurat, Georges 乔治·修拉

Shaw, George Bernard 格奥尔格·贝尔纳德·肖

Shelley, Mary 玛丽·雪莱

Simmel, Georg 格奥尔格·西默尔

Singer, Heinrich 海因里希·辛格尔

Singer, Irma(Mirjam)伊尔玛（米尔耶姆）·辛格尔

Sklovskij, Viktor 维克托尔·斯克洛夫斯基

Sophokles 索福克勒斯

Sorge, Reinhard Johannes 赖因哈德·约翰内斯·佐尔格

Soukup, František 弗兰蒂塞克·索科泼

Spengler, Oswald 奥斯瓦尔德·施彭勒

Spitteler, Carl 卡尔·施皮特勒

Stadler, Ernst 恩斯特·施塔特勒

Starke, Ottomar 奥托马尔·施泰尔克

Stauffer-Bern, Karl 卡尔·施陶费尔—伯恩

Steiner, Hugo 胡戈·施泰纳

Steiner, Rudolf 鲁道夫·施泰讷

Stekel, Wilhelm 威廉·施泰克尔

Stendhal(d. i. Henri Beyle)司汤达（原名亨利·贝尔）

Sterk, Elvira 埃尔维拉·施泰尔克

Stern, Agathe 阿加特·施特恩

Stemheim, Carl 卡尔·施特恩海姆

Stemheim, Felix 费利克斯·施特恩海姆

Steuer, Otto 奥托·施托伊尔

Stevenson, Robert Louis 罗伯特·路易斯·史蒂文森

Stickney, Alice 艾丽斯·斯蒂克尼

Stifter, Adalbert 阿达尔贝特·施蒂弗特

Stoeßl, Otto 奥托·施特斯尔

Stössinger, Felix 弗利克斯·施脱辛格尔

Strauß, Emil 埃米尔·施特劳斯

Strauß, Johann 约翰·施特劳斯

Strelinger, Leopold 莱奥波德·施特莱林

Strindberg, August 奥古斯特·施特林贝格

Stumpf, Carl 卡尔·施图姆普夫

Swift, Jonathan 乔纳森·斯威夫特

Szafranski, Kurt 库尔特·斯察弗兰斯基

Szokoll, Juliane 尤丽娅妮·斯措科尔

Taaffe, Eduard Graf von 爱德华·格拉夫·冯·泰费

Tagger, Theodor 台奥多尔·塔格尔

Tandler, Julius 尤利乌斯·坦德勒尔

Taussig, Elsa, s. Brod, Elsa 艾尔莎·陶西希（即艾尔莎·布罗德）

Teweles, Heinrich 海因里希·泰韦莱斯

Theilhaber, Felix Aron 费利克斯·阿龙·泰尔哈贝尔

Thieberger, Friedrich 弗里德里希·蒂贝格尔

Thieberger, Gertrude 科特鲁德·蒂贝格尔

Thieberger, Nelly 内丽·蒂贝格尔

Thoma, Hans 汉斯·托马

Tolstoj, Lew Nikolajewitsch 列夫·尼古拉耶维奇·托尔斯泰

Toller, Ernst 恩斯特·托勒

Torge, Paul 保尔·托尔格

Toulouse-Lautrec, Henri de 亨利·德·托罗塞—劳特雷斯

Trakl, Georg 格奥尔格·特拉克尔

Trietsch, Davis 达维斯·特里奇

Tschisik, Amalie 阿玛丽·契西克

Tucholsky, Kurt 库尔特·图霍尔斯基

Uexküll, Jakob von 雅各布·冯·于克斯屈尔

Ulbrich, Josef 约瑟夫·乌尔布利希

Ungar, Hermann 赫尔曼·翁加尔

Urzidil, Johannes 约翰内斯·乌尔齐蒂尔

Utitz, Emil 埃米尔·乌蒂茨

Valenta, Jindřich 金德里希·瓦伦泰

Valéry, Paul 保尔·瓦莱里

Verlaine, Paul 保尔·韦伦

Vesecky, Gustav 古斯塔夫·韦塞屈

Vesper, Will 维利·费斯佩尔

Wagner, Richard 里夏德·瓦格纳

Walden, Herwarth 赫尔瓦斯·瓦尔登

Wallenstein, Albrecht 阿尔布莱希特·瓦伦施泰因

Walser, Robert 罗伯特·瓦尔泽

Walkel, Oskar 奥斯卡尔·瓦尔策

Warburg, Otto 奥托·瓦尔堡

Wassermann, Jakob 雅各布·瓦塞尔曼

Weber, Alfred 阿尔弗雷德·韦伯

Weber, Leopold 莱奥波德·韦伯

Weber, Max 马克斯·韦伯

Wedekind, Frank 弗兰克·韦德金德

Wedekind, Tilly 蒂莉·韦德金德

Wegener, Paul 保尔·韦格讷

Weiler, Hedwig 黑德维希·魏勒

Weinberg, Moritz 莫里茨·魏因贝格

Weiner, Gustav 古斯塔夫·魏纳尔

Weininger, Otto 奥托·魏宁格尔

Weisl, Moritz 莫里茨·魏斯尔

Weiß, Ernst 恩斯特·魏斯

Weißberger, JoséAmaldo 约瑟·阿纳尔多·魏斯贝格

Weiss, Peter 彼得·魏斯

Weltsch, Felix 费利克斯·韦尔奇

Weltsch, Lise 莉泽·韦尔奇

Weltsch, Robert 罗伯特·韦尔奇

Werfel, Franz 弗兰茨·韦弗尔

Werner, Anton von 安东·冯·维尔纳

Werner, Josef 约瑟夫·维尔纳

Werner, Marie 玛丽·维尔纳

Wiegler, Paul 保尔·维格勒尔

Wieser, Friedrich von 弗里德里希·冯·维泽尔

Wiener, Oskar 奥斯卡·维纳尔

Wiesenthal, Grete 格雷特·维森塔尔

Wigmann, Mary 玛丽·维格曼

Wihan, Karl 卡尔·维汉

Wilde, Oscar 奥斯卡·王尔德

Winder, Ludwig 路德维希·温德尔

Wittgenstein, Ludwig 路德维希·维特根斯坦

Wohryzek, Eduard 爱德华, 沃吕察克

Wohryzek, Julie 尤丽叶·沃吕察克

Wolfenstein, Alfred 阿尔弗雷特·沃尔芬施泰因

Wolff, Kurt 库尔特·沃尔夫

Wundt, Wilhelm 威廉·冯特

Wurm, Fritz 弗里茨·武尔姆

Yan-Tsen-Tsai 袁枚

Zanantoni, Eduard 爱德华·察南托尼

Zech, Paul 保尔·蔡希

Zemanová, Marie 玛丽·采马诺瓦

Zenge, Wilhelmine von 威廉明妮·冯·岑格

Zenon von Elea 米农·冯·艾莱阿

Zischler, Hanns 汉斯·齐施勒

Zola, Émile 艾米尔·左拉

Zucker, Alois 阿洛伊斯·楚克尔

Zuckerkandl, Robert 罗伯特·楚克康德尔

Zweig, Arnold 阿诺尔德·茨威格

Zweig, Stefan 斯蒂芬·茨威格

Zycha, Adolf 阿道尔夫·曲夏