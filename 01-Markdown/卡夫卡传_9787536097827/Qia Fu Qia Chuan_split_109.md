   

## 上帝许与亚伯拉罕之地的语言

20世纪20年代初卡夫卡怀着重新燃起的兴趣关注着犹太复国主义的文化政策，这种兴趣无疑包含众所周知的对一个犹太民族国家的怀疑。1917年11月2日的巴尔富尔宣言允诺对建立一个犹太人“固定住处”给予正式支持。这一还没有具体实施的计划宣示在1919年通过巴黎和约以及1920年在圣雷莫会议上得到了有力的确认。然而跟原来规定以法国参与为主的国际行政管理条款相反，由国际联盟给予的行政授权却落入英国手中，而英国则不得不采取一种老练圆滑的方针，它不想危及它与阿拉伯世界的关系。犹太复国主义世界组织在自1912年起影响力日益增长的总书记库尔特·布鲁门费尔德的领导下，竭力支持采取一种积极的移民政策。1918年以后试图完全专注于巴勒斯坦工作的少壮派在协会中占了上风。而在曾被赫茨尔强调为犹太复国主义纲领核心地区的、散居在外犹太教徒的乡镇里，犹太民族意识的促进却明显地退居次要地位。布拉格犹太复国主义者在组织内部扮演一种领导角色，其影响力也在大众传播上得到由他们负责编辑出版的《犹太周报》的支持。西格蒙德·卡茨纳尔松在柏林任出版社社长并担任新任务之后，他们的起主导作用的代表性人物是胡戈·贝格曼、费利克斯·韦尔奇和他的弟弟罗伯特。他们赞同以哈伊姆·阿洛索罗夫为主要人物的犹太复国主义移民运动，这一运动构成巴勒斯坦协会“青年工人”的欧洲分会。在这里，人们谋求在一种实用社会伦理学基础上的社会主义，这种伦理学的纲领，就像古斯塔夫·朗道尔示范性地所拥护的，以乡村生活的集体主义为其生动的榜样。在这方面，人们寄希望于一种进化的前景，人们不谋求彻底推翻被视为堕落的资产阶级世界，而是力图有机地建立一种将在巴勒斯坦接受考验的新秩序。

1920年马克斯·布罗德发表了他的研究报告《犹太复国主义中的社会主义》，报告中他针对约瑟夫·波珀尔的论文《普遍的赡养义务做解决社会问题的途径》提出了一种建立在部分集体的、部分经济自由主义因素基础上的国家观念模式。按照布罗德的以波珀尔为蓝本的、明确针对有塔于洛尔主义特色的、使劳动机械化的想法，男人和女人应该在国家的专门生产机构中分别工作13年和8年，以便团体能够生产出需要供应的物品。然而注意力却远离这种集体的经济形式而集中在个人主动的积极性上，这种积极性要与一种社会主义组织结构的平均主义倾向做斗争。布罗德不无天真的理想主义，在这里追求一种生产过程的社会控制，这种控制使人获得对工作的最大限度的认同。通过技术革新节省下来的时间对私人有好处，将不会被再次投入到工作中去。跟1918年在他的无财产的工人阶级样板中要求彻底没收财产的卡夫卡不一样（“不要钱，不要贵重物品”），布罗德想建立一种增加了自由主义要素的国家社会主义，其同时包含自由的和国家调控的成分。不过他担心在组织模式帮助下可能产生的业余时间过程有导致日常文化活动肤浅化的危险。他的这种疑虑是很有预见性的：“（……）维也纳轻歌剧甜蜜味儿会直上云霄。”

卡夫卡同布罗德的宏伟计划的关系打上了距离的烙印：这种距离，一如他将在1922年1月所表述的，敦促他做禁止他亲自参与政界的“行为观察”。如果说他仍然关注着犹太复国主义的讨论的话，那么吸引他的不是其纲领草案社会方面的，而是文化方面的重要意义。他对人民之家运动怀有强烈的好感，因为这一运动的目标符合他在教育学方面的兴趣。弗兰茨·罗森茨威格，犹太复国主义的极有影响力的哲学著作——《拯救之星》(1921)——的作者，1920年就已经在法兰克福一所犹太成年人教育中央机构担任领导工作。由他创办的《自由犹太人教育之家》不久也为别的大城市教区树立了榜样，其组织方面的基本结构具有样板的性质。由杰出的年轻知识分子如莱奥·贝克、埃里希·弗洛姆、莱奥·勒文塔尔和格斯霍姆·绍莱姆承担的报告和讨论会在《教育之家》传授希伯来语、犹太宗教哲学和犹太教神秘教义的知识。自从1911年秋与演员莱姆贝格会面以来，卡夫卡越来越关注这些为开拓犹太人身份来源而做出努力的机构。1920年11月他虽然对米莱娜·波拉克自称是最具西犹太人特色的西犹太人，但是恰恰是这种自我勾画解放了他对这些可望而不可即的宗教传统的向往。1922年1月底，在一种典型的悖论中，他把自己描述为永远的流亡者，一生下来便离开了圣地并在异乡以工作清偿了同化的“父亲遗产”。说是从迦南迁移出去40年之后，现在他必须作为“最平凡者和最胆怯者”在一个甚至为“最卑贱者”也准备好“闪电般的提升”、但也准备好在“海洋压力般的千年之久的破坏”的世界中忍受他的状况。

1918年2月，卡夫卡把一种肥沃的土壤对信仰的好处称作在他的宏大的人生规划——婚姻、家庭、职业——失败后，他要盯住的“最原始的任务”。一封1913年2月9日至10日的致菲莉丝·鲍尔的信，把对个人和一种“令人放心的、遥远的、也许无穷尽的”欢乐或烦恼之间的“不间断的关系”的感受力描写为确证了的虔敬的指示器。谁不符合这个标准，谁就一定不会感到生活是“寒冷的冬夜”：人们渴望在这样的寒夜“钻进坟墓”。对一种感官和精神上明确的信仰经验的思念自1914年起便把卡夫卡引向希伯来语。这方面的一个重要的动机可以这样猜测：希伯来语，正如绍莱姆所说，保留了符号和事物的原始的统一，因此保持了一种优美的特性。言语使人从感性和肉体上理解宗教经历，如同接近意第绪语所预兆的那样。

![[/卡夫卡传_9787536097827/images/data-url-image43.jpeg]]

普阿·本—托维姆

在倒数第二个战争年，卡夫卡开始自学希伯来语，他以莫泽斯·拉特的以现代词汇为主的教材为依据。1917年9月马克斯·布罗德的记录中说这位朋友事先没有知会他，已经学完了150课中的45课。在他生病之前的几个月里卡夫卡同费利克斯·韦尔奇和年轻的伊尔玛（米尔耶姆）·辛格尔，布罗德的一个一面之识的熟人，一道上了一个吉里·朗格的希伯来语学习班。伊尔玛·辛格尔回忆说，他曾用讽刺的口吻评论学习希伯来语的困难：“布拉格的犹太复国主义者们9月开始学莫泽斯·拉特课程第一课，勤奋地学到6月。在假期里他们把学过的东西又全部忘掉，于是在9月又开始学莫泽斯·拉特课程第一课。”1917年至1918年冬在曲劳以及稍晚些，1918年9月在都瑙，卡夫卡继续自学，没有人指导。1918年秋，他接受30岁的中学教师弗里德里希·蒂贝格尔的私人授课。蒂贝格尔，由奥古斯特·绍尔凭他的一篇日耳曼学专业文学史论文授予过博士学位，他作为一位犹太教经师的儿子拥有圣经希伯来语、但并非现代希伯来语的丰富知识。所以除了拉特的标准教材以外，一本伊西多尔·波拉克和古斯塔夫·魏纳尔编的圣经读本也是基础教材。卡夫卡，他学希伯来语并不是想为移居巴勒斯坦做准备，而是想先揭开这门原汁原味的信仰语言的秘密。据蒂贝格尔的回忆，他是一个热心的学生：“接连许多个星期，我们在严格规定好的钟点在他那儿会面，通常在厨房后面的一个庭院房间里。他都认真记单词和做书面练习并且对教材体系中的不准确性表示很气愤。只有一次他因没完成作业而道歉，他用希伯来语说：‘我病了，我病得很厉害。’”

1920年12月卡夫卡开始在马特利亚里上课，这时蒂贝格尔的课程已经结束。1922年深秋，19岁的巴勒斯坦女人普阿·本—托维姆成为蒂贝格尔的继任人。她出身于一个1888年就已经移居国外的俄罗斯家庭，在耶路撒冷学过德语并于1921年在那里通过高级中学毕业考试。1922年9月她经胡戈·贝格曼的介绍来到布拉格，想进一步了解作为欧洲文化犹太复国主义中心的布拉格。她在贝格曼母亲的寓所住一间转租房间，开始在卡尔斯大学学数学。这符合布拉格犹太教会堂大经师的期望：普阿义务教授希伯来语课。卡夫卡经贝格曼的介绍估计在1922年深秋结识她，并在年底接受她的授课。蒂贝格尔的授课已经向他表明，外语习惯只有通过活生生的交流才能掌握，而这种交流又要求加强学习现代希伯来语。正是口头表达给移民们造成严重问题，如同阿哈龙·大卫·戈尔东1917年在一篇论述迦南地区语言障碍的短文中所说的：“我们在巴勒斯坦的希伯来语是我们在巴勒斯坦的生活的一面明镜。在这面镜子里我们最清楚地看到，我们一举一动中的困难，这些来自我们自身、来自我们内心的精神上的流亡的困难有多么大。”

普阿·本—托维姆精通希伯来语，在1923年春迁居到柏林之前，她一直是卡夫卡的教师。起先重要的不是语言学习，而是巴勒斯坦情况交流。几个星期后才转入正常学习语法、单词和惯用语。这些功课构成卡夫卡在1922年至1923年冬季期间几乎完全在床上度过的每一天都受抑制的生活节奏中仅有的几个固定的活动时刻。在普阿的课程中，除了圣经希伯来语以外，他也加强学习现当代希伯来语，它通过逻各斯向他揭示了一个世界，一个连接传统意识和活动力的世界。这个年轻的巴勒斯坦女人在他眼里代表一种乐观积极的犹太教，这种犹太教不必再把自己的本体藏在适应姿态的后面，因为它已经抛弃“对大屠杀和屈辱的恐惧”。根据朗格的回忆，卡夫卡在20世纪20年代初相当稳当地掌握了希伯来语，他可以就比较艰深的话题进行交谈，并用简单的语句做书面表述。这样一种水准大大超越巴勒斯坦大多数司机的知识。流传下来的他记单词的四本笔记本显示出一种认真、细致的学习态度，说明卡夫卡有着强劲的学习动力。这表明了他自1918年起所处的语言状况：他虽然通过德语学单词，但是使用一本1892年耶罗斯拉夫·赛德拉采克编写的捷克文圣经希伯来语教科书。独特而充满焦急与不安情结的布拉格状况转化为使捷克、德国和犹太身份发生相互交换关系的希伯来语课。

在这段时间里，卡夫卡也可能饶有兴趣地关注过围绕着一种新的希伯来语圣经译文展开的公开讨论。战争结束之后不久，马丁·布贝尔就和政论作者埃富雷姆·弗里施和S·费舍尔出版社首席编辑莫里茨·海曼一起从事这项工程，但由于他的两位合作者负担过重而一度放弃了它。20世纪20年代中在海德堡出版商拉姆贝特·施奈特尔的催促下他才和已然重病在身的弗兰茨·罗森茨威格一起接近这个当时在犹太公众中引起激烈争执的旧日计划。在这些时代的文化犹太复国主义大项目上，卡夫卡看重的是：它们传播对传统活力的信任。在似乎为他勾勒出“相反的荒漠漫游”的人生旅程中令他感到满意的是，他可以观察迈着稳健的步伐走近上帝许以亚伯拉罕之地的朝圣者们。但是在患病最后阶段的阴影里，他也考虑做一次迦南之行的可能性，此行原本可以使他摆脱在外散居的犹太人的孤寂。