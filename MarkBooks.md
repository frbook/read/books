```dataviewjs
let excludeFolderChoicePath = "99-Settings"

dv.table(["封面","书名", "作者", "出版社", "评分", "我的评分"], dv.pages("#reading")
	.filter(b => !b.file.path.includes(excludeFolderChoicePath))
    .map(b => [("![](" + b.cover + ")"), b.file.link, b.author, b.publish, (b.rating + "🌟"), (b.star + "💗")]))
```



- [[故事/故事.md|故事]]
- [[理想国/理想国.md|理想国]]
- [[理想国_9787553809694/理想国_9787553809694.md|理想国_9787553809694]]
- [[鹿川有许多粪_李沧东|鹿川有许多粪_李沧东]]
- [[斯通纳/斯通纳.md|斯通纳]]
- [[南货店_张忌/南货店_张忌.md|南货店_张忌]]
- [[三体全集 - 刘慈欣/三体全集 - 刘慈欣.md|三体全集 - 刘慈欣]]

[[黄金时代]]
宝珀
- [[空响炮/空响炮.md|空响炮]]
- [[八部半/八部半.md|八部半]]
- [[猎人]]
- [[夜晚的潜水艇]]
- [[潮汐图/潮汐图.md|潮汐图]]
- [[一团坚冰/一团坚冰.md|一团坚冰]]